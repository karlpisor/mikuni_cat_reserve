﻿<?php
	// Enforce Tokyo timezone for date/time calculations
	date_default_timezone_set('Asia/Tokyo');
	
	// SITE_ROOT contains the full path to the root folder
	define('SITE_ROOT', dirname(dirname(__FILE__)));
	
	// Application directories
	define('PRESENTATION_DIR', SITE_ROOT .'/presentation/');
	define('BUSINESS_DIR', SITE_ROOT .'/business/');
	define('PANEL_DIR', SITE_ROOT .'/panels/');
	define('LANGUAGE_DIR', SITE_ROOT .'/lang_strings/');
	define('INCLUDE_DIR', SITE_ROOT .'/include/');
	define('MAILCHIMP_DIR', SITE_ROOT .'/lib/Mailchimp/');
	define('MAIL_FACTORY_DIR', SITE_ROOT .'/lib/PHPMailer/');
	
	
	// These should be true while developing the web site
	define('IS_WARNING_FATAL', true);
	define('DEBUGGING', true);
	
	// The error types to be reported
	define('ERROR_TYPES', E_ALL);
	
	// Settings about mailing the error messages to admin
	define('SEND_ERROR_MAIL', false);
	define('ADMIN_ERROR_MAIL', 'karl@missionpeoplesystems.com');
	define('SENDMAIL_FROM', 'system_admin@missionpeoplesystems.com');
	ini_set('sendmail_from', SENDMAIL_FROM);
	
	// By default we don't log errors to a file
	define('LOG_ERRORS', false);
	
	// Generic error message to be displayed instead of debug info (when DEBUGGING is false)
	define('SITE_GENERIC_ERROR_MESSAGE', '<h1>Mikuni Cat Skiing Error!</h1>');
	
	// Set host environment
	$host = 'test';
	
	// Define host environment variables
	if ($host == 'test') {
		// test AWS environment, 
		define('DB_SERVER', '54.248.93.97');
		define('DB_USERNAME', 'mpstestuser');
		define('DB_PASSWORD', 'tumblingdice');
		define('DB_DATABASE', 'testdb');
		
		define('VIRTUAL_LOCATION', '/');
		define('PAGE_TITLE_STEM', 'TEST--Mikuni Cat Skiing');
		define('MAIL_IS_LIVE', true);
		define('USE_SSL', true);
		
		define('STRIPE_STATUS', 'test');
		define('STRIPE_LIVE_SECRET_KEY', 'sk_live_z9NoKjrbZs5H7XHulbWeIheJ');
		define('STRIPE_TEST_SECRET_KEY', 'sk_test_AqKljGxXtvROK9NWjbIWjfSC');
		// public key set and controlled in processCreditCard.js
	
		define('GROUP_LESSON_PRICE_CUTOFF', '15000');
		define('INSTRUCTOR_DESIGNATE_HOURLY_FEE', '1000');
		
		define('JUNIOR_STUDENT_AGE_CUTOFF', '2004-04-02');
		define('PEEWEE_STUDENT_AGE_CUTOFF', '2010-04-02');
		define('PEEWEE_STUDENT_TOO_YOUNG_CUTOFF', '2012-01-01');
		
		
		
		$debugging = false;
		
	} else {
		echo "Error with environment config. Please notify site administrator.";
	}
	
	// Define database connection variables
	//	define('PDO_DSN', 'dblib:host=' .DB_SERVER .':1433;dbname=' .DB_DATABASE . ';charset=sjis');
	define('PDO_DSN', 'dblib:host=' .DB_SERVER .';database=' .DB_DATABASE);
	
	// Server HTTP port (can omit if default 80 is used)
	define('HTTP_SERVER_PORT', '80');
	
	
?>