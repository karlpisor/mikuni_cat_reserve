<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'order.php';
	
	
	
	# return json data, create an array to hold this
	$returnCouponValidation = array();
	
	# look for post from 
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
		
        # get data from ajax call
		$CouponCode = $_POST['CouponCode'];
		
        // process posted values
        if(empty($CouponCode)) {
		
			// user has not entered a coupon code
            $returnCouponValidation['Level'] = 0;
            
        } else {
        	
        	// perform database check on the given coupon code
            $checkCouponCode = Order::OrderCheckCouponCode($CouponCode);
            
            // set return values
            if (isset($checkCouponCode) && !empty($checkCouponCode)) {
            	
            	// valid coupon code
            	$returnCouponValidation['CouponCode'] = $checkCouponCode['t_coupon_code_string'];
            	$returnCouponValidation['Discount'] = $checkCouponCode['n_coupon_code_discount'];
            	$returnCouponValidation['Level'] = '1';
            	
            	
            } else {
            	
            	// invalid code
                $returnCouponValidation['Level'] = -1;
            }
        }
		
	}
    
	

    // return json array
    echo json_encode($returnCouponValidation); 
    
?>