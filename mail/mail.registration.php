<?php
	// $resultSelectOrder resource available
	
	// set up mail message
	// initialize message string to null
	$message = '';
	
	// set mail text strings from customer data
	if ($_SESSION['UserLang'] == 'EN') {
		$buyerFullNameSalutation = $Mail_Salutation .' ' .$mailBuyerFullName_String .',';
	} else {
		// default
		$buyerFullNameSalutation = $mailBuyerFullName_String .' ' .$Mail_Salutation;
	}
	
	// salutation begins the message string
	$message .= "$buyerFullNameSalutation\n\n";
	
	// message salutation, payment advice
	$message .= "$Mail_ThanksForRegistering\n\n";
	
	// branch mail content by status of payment
	// cc payment received, status = paid
	if ($resultSelectOrder['fk_order_status'] == 5){
	
		// customer has paid successfully
			
	
	} else {
	
		// payment not complete, we need customer to send bank transfer
		// how to pay details
		$message .= "$Mail_HowToPayAdvice\n\n";
		$message .= "$Mail_HowToPayDetails\n\n";
		$message .= "$Mail_NoPaymentNoPlaceNotice\n\n";
	}
	
	$message .= "$Mail_PrivacyPolicyAgreementNotice\n\n\n";
	
	
	
	
	// load line items for order, including options
	require_once 'mail/mail.panel.order_line_items.php';
	
	// if discount code is applied, display subtotal (sessions and options), then subtract discount and display total
	$subtotal = 0;
	
	// $orderTotal, $total_discount_amount have been calculated above
	if (isset($total_discount_amount) && $total_discount_amount > 0){
		
		// subtotal (lessons)
		$subtotal = $orderTotal;
		$message .= "-----------------------" ."\r\n";
		$message .= $Mail_OrderSubtotalLabel .' ' .$Mail_YenSign .number_format($subtotal) ."\r\n";
	
		// coupon code discount is applied
		$message .= '"' .$coupon_code .'" '
					 .$Mail_CouponDiscountLabel .' ' .'-' 
					 .$Mail_YenSign .number_format($total_discount_amount) .'' ."\r\n";
		
		// total including all discounts
		$total = $subtotal
				- $total_discount_amount;
		$message .= "-----------------------" ."\r\n";
		$message .= $Mail_OrderTotalLabel .$Mail_YenSign .number_format($total) ."\r\n\n";
		
	} else {
		
		// if no discount code, display total (sessions and options) without subtotal
		$total = $orderTotal;
		$message .= "-----------------------" ."\r\n";
		$message .= $Mail_OrderTotalLabel .$Mail_YenSign .number_format($total) ."\r\n\n";
	}
	
	// if customer has paid by credit card
	if ($resultSelectOrder['fk_order_status'] == 5){
	
		// show payment and balance lines
		$message .= $Mail_PaidOnDateTotalLabel .date("n/j") .' ' .$Mail_YenSign .number_format($total) ."\r\n";
		$message .= "-----------------------" ."\r\n";
		$message .= $Mail_BalanceZeroLabel .$Mail_YenSign .'0' ."\r\n\n";
			
	}
	
	
	
	// append completed pricing information to message string
	$message .= "$Mail_AllPricesIncludeTax\n\n\n";
	
	
	// standard messaging for all orders
	$message .= $Mail_ConfirmationMsg_RequiredChecklistHeader ."\r\n";
	$message .= "-----------------------" ."\r\n";
	$message .= $Mail_ConfirmationMsg_RequiredChecklist ."\r\n\n";
	
	$message .= $Mail_ConfirmationMsg_ContactUs ."\r\n\n";
	
	
	
	
	
	// parent/buyer information
	// set section header
	$message .= $Mail_BuyerInformation ."\r\n";
	$message .= "-----------------------" ."\r\n";
	$message .= "$buyerMailMessage" ."\r\n";
	// spacing at end of section
	
	
	
	// registered skier information
	// set section header
	$message .= $Mail_SkierInformation ."\r\n";
	$message .= "-----------------------" ."\r\n";
	for ($n = 0; $n < count($resultSelectSkiersOfOrder); $n++){
		
		// loop through camper details
		$message .= $skierMailElements[$n];
		$message .= "\r\n";
	}
	
	
	
	
	// cancellations message
	$message .= "$Mail_RegardingCancellationsHeader\r\n";
	$message .= "-----------------------" ."\r\n";
	$message .= "$Mail_RegardingCancellationsText\n\n\n";
	
	
	// message footer
	$message .= "$Mail_ConfirmFooterSalutation\n\n";
	$message .= "$Mail_ConfirmFooter\n";

		
	// strip any <br> from the mail message
	$message = str_replace('<br>', '', $message);
	
	
	
	// set recipient and subject
	$to = $resultSelectBuyer['t_email'];
	$test_email_flag = 'TEST -- ';
	$subject = '';
	if(DB_DATABASE == 'testdb'){ 
		$subject .= $test_email_flag; 
	} 
	$subject .= $Mail_ConfirmationSubject .' #' .$_SESSION['OrderId'];
	
	
	
	
	$filePath = '';
	// set up parent's liability release as attachment
	if(isset($_SESSION['Liability_Release_FilePath'])){
	
		$filePath = $_SESSION['Liability_Release_FilePath'];
		
	}
	
	
	
	// prepare mail and send
	// load mail factory
	require_once MAIL_FACTORY_DIR .'class.phpmailer.php';
	
	// defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
	// invoke new mail object
	$mail = new PHPMailer(true);
	
	try {
		$address = $to;
		$mail->AddAddress($address, $to);
		// sender address must be verified with Amazon SES
		$from = "support@naebasnow.jp";
		$mail->SetFrom($from, 'Mikuni Cat Skiing');
		
		$mail->AddReplyTo('support@naebasnow.jp', 'Mikuni Cat Skiing');
		$mail->AddBCC('karl@missionpeoplesystems.com', 'System Admin');
		$mail->AddBCC('reservations@mikuni-cat.com', 'Mikuni Cat Skiing');
		
		// load SMTP settings
		require_once 'mail/mail.z_smtp_settings_aws.php';
		
		// add the customer's liability release as an attachment
		if (isset($filePath) && !empty($filePath)){
			$mail->AddAttachment($filePath);
		}
		
		
		
		// send it
		$mail->Send();

	} catch (phpmailerException $e) {
		
		echo $e->errorMessage(); //Pretty error messages from PHPMailer
		
	} catch (Exception $e) {
		
		echo $e->getMessage(); //Boring error messages from anything else!
		
	}
		
		
?>		