<?php
	if ($_SESSION['UserLang'] == 'CN'){
		
		$mail->CharSet="UTF-8";
		
	} else if ($_SESSION['UserLang'] == 'JP'){
		
		$mail->CharSet = "iso-2022-jp";
		$mail->Encoding = "7bit";
		
	} else if ($_SESSION['UserLang'] == 'EN'){
		
		$mail->CharSet="UTF-8";
		
	} else {
		// default JP
		$mail->CharSet = "iso-2022-jp";
		$mail->Encoding = "7bit";
	}
	
	
	// telling the class to use SMTP
	$mail->IsSMTP();
	$mail->SMTPDebug  = 0;                     	// enables SMTP debug information (for testing)
	$mail->SMTPAuth   = true;                  	// enable SMTP authentication
	//$mail->SMTPSecure = "ssl";                 	// sets the prefix to the servier
	$mail->Host       = "smtp2.gmoserver.jp";      // sets GMO server as the SMTP server
	$mail->Port       = 587;                   	// set the SMTP port for the GMO email service
	$mail->Username   = "info@naebasnow.jp"; // SMTP account username
	$mail->Password   = "7XL46dGv";           // SMTP account password
	
	// if message is to be Japanese, use special encoding for $subject, $message
	if ($_SESSION['UserLang'] == 'JP'){
		mb_language("japanese");
		mb_internal_encoding("UTF-8");
		$mail->Subject = mb_encode_mimeheader(mb_convert_encoding($subject,"JIS","UTF-8"));
		$mail->Body = mb_convert_encoding($message,"JIS","UTF-8");
	} else {
		$mail->Subject = $subject;
		$mail->Body = mb_convert_encoding($message,"JIS","UTF-8");
	}
?>