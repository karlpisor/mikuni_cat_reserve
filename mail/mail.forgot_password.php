<?php
	// send new password to customer
	// lang strings are available from parent file
	
	
	// set up mail message
	// initialize message string to null
	$message = '';
	
	// we have set a new password, so a NEW select is required
	// $Email string is available from above
	// Contact::ContactCheckEmail has already run above, we know it will produce 1 unique record
	$selectUserResult = Contact::ContactCheckEmail($Email);
	
	// load language strings for this mail
	require_once LANGUAGE_DIR .'lang.mail.php';
	
	// message salutation
	if ($_SESSION['UserLang'] == 'EN'){
		$message .= $Mail_Salutation .' ' 
				   .$selectUserResult[0]['t_firstname_jp'] ." " .$selectUserResult[0]['t_lastname_jp'] 
				   .",\n\n";
	} else {
		// default is JP
		$LastNameJP = mb_convert_encoding($selectUserResult[0]['t_lastname_jp'], "UTF-8", "SJIS");
		$FirstNameJP = mb_convert_encoding($selectUserResult[0]['t_firstname_jp'], "UTF-8", "SJIS");
		$message .= $LastNameJP ." " .$FirstNameJP
					.$Mail_Salutation 
					."\n\n";
	}
	
	// message body
	$message .= $Mail_NewPasswordMessage ."\n\n";
	$message .= $Mail_YourNewPasswordIs ."\n\n";
	$message .= $selectUserResult[0]['t_password'] ."\n\n";
	
	
	// message footer
	$message .= "$Mail_ConfirmFooter\n";
	
	
	
	// set recipient and subject
	$to = $selectUserResult[0]['t_email'];
	$test_email_flag = 'Test Forgot Password - ';
	$subject = '';
	if(DB_DATABASE == 'testdb'){
		$subject .= $test_email_flag;
	}
	
	$subject .= $Mail_NewPasswordSubject;
	
	// prepare mail and send
	// load mail factory
	require_once 'lib/PHPMailer/class.phpmailer.php';
	
	// defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
	// invoke new mail object
	$mail = new PHPMailer(true);
	
	try {
		$mail->AddAddress($to);
		$mail->SetFrom('reservations@mikuni-cat.com', 'Mikuni Cat Skiing');
		$mail->AddReplyTo('reservations@mikuni-cat.com', 'Mikuni Cat Skiing');
		$mail->AddBCC('karl@missionpeoplesystems.com', 'Karl Pisor');
	
		// load SMTP settings
		require_once 'mail/mail.z_smtp_settings_gmo.php';
		
		// send it
		$mail->Send();
	
	} catch (phpmailerException $e) {
		
		echo $e->errorMessage(); //Pretty error messages from PHPMailer
		
	} catch (Exception $e) {
		
		echo $e->getMessage(); //Boring error messages from anything else!
		
	}
?>		