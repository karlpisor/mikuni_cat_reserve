<?php
	// $resultSelectOrderLIs returns only sessions in order, not options
	// $OrderPK is available from above
	// Session UserLang set from above
	

	// pricing display
	// header line
	$message .= $Mail_OrderDetailsLabel ."\r\n";
	$message .= "-----------------------" ."\r\n";
	
	// initialize
	$orderTotal = 0;
	$previous_lesson_id = 0;
	
	
	
	// line items			
	for ($v = 0; $v < count($resultSelectOrderLIs); $v++){
		
		// check for new skier, do not output name if the same
		$this_lesson_id = $resultSelectOrderLIs[$v]['pk_session'];
		
		// print out summary line items
		// first set up strings
		require 'panels/panel.lesson_mail_display.php';
		
		// output
		// output lesson summary
		$message .= $lesson_name_display .' '
			.$summary_date_display .' ';
		
		// show time span for tours only, not rentals	
		if ($raw_price > 10000){
			$message .= '(' .$lessonStartTime .' - ' .$lessonEndTime .') ';
		}
		
		// output skier name
		if ($_SESSION['UserLang'] == 'EN'){
	
			// EN-language
			$message .= $resultSelectOrderLIs[$v]['t_firstname_jp'] .' '
					   .$resultSelectOrderLIs[$v]['t_lastname_jp'];
				
		} else {
				
			// default
			$message .= mb_convert_encoding($resultSelectOrderLIs[$v]['t_lastname_jp'], "UTF-8", "SJIS") .' '
					   .mb_convert_encoding($resultSelectOrderLIs[$v]['t_firstname_jp'], "UTF-8", "SJIS");
		}
		
		// format price
		$display_price = number_format($raw_price);
			
		// output price
		$message .= ' ' .$Mail_YenSign .$display_price;
		
		// line return
		$message .= "\r\n";
		
		
		// increment order total
		$orderTotal = $orderTotal + $raw_price;
		
		// update duplicate check
		$previous_lesson_id = $this_lesson_id;
		
	
		
	} // end for loop cycling through line items	
?>