<?php
	use DrewM\MailChimp\MailChimp;

	// Include utility files
	require_once 'include/config.php';
	
	// Use SSL connection for this page
	require_once 'presentation/link.php';
	Link::EnforceSSL();
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	// Load Mailchimp API wrapper for API v3.0
	require_once 'lib/Mailchimp/MailChimp.php';

	// Select records modified within the last 60 minutes
	// 60 minute interval is set in the MySQL procedure
	// server cron job is set to run every 60 minutes
	$selectContactsResult = Contact::GetMailchimpSyncCurrentRegd();
	
	echo '<pre>';
	print_r($selectContactsResult);
	echo '</pre>'; 
	
	echo 'api key= ' .MAILCHIMP_APIKEY;
	echo '<br>';
	echo 'list key= ' .MAILCHIMP_LIST_ID_CURR_REGD;
	echo '<br>';
	
	// query returns one or more records
	if (count($selectContactsResult)){
	
		echo '<br><br>';
		echo '==================================== Begin updated or new subscriber output =======================';
		echo '<br><br>';
	
		// create new API object
		$api = new MailChimp(MAILCHIMP_APIKEY);
	
		// step through selected records and sync them to Mailchimp list
		for ($i = 0; $i < count($selectContactsResult); $i++){
				
			// format date fields for Mailchimp
			$orderStartDate = date_format(date_create($selectContactsResult[$i]['d_current_order_start_date_mcs']),"m/d/Y");
			$orderEndDate = date_format(date_create($selectContactsResult[$i]['d_current_order_end_date_mcsyn']),"m/d/Y");
			
			// prepare variables to merge
			$merge_vars = array(
					'FIRST_NAME' => mb_convert_encoding($selectContactsResult[$i]['f_nm'], "UTF-8", "SJIS"), 
					'LAST_NAME'=> mb_convert_encoding($selectContactsResult[$i]['l_nm'], "UTF-8", "SJIS"), 
					'USER_ID'=> $selectContactsResult[$i]['academymember'],
					'USERLANG'=> $selectContactsResult[$i]['t_user_language'],
					'CURR_SESS'=> $selectContactsResult[$i]['t_current_ordered_lessons_mcsy'],
					'ORD_START' => $orderStartDate,
					'ORD_END'=> $orderEndDate
						
			);
	
				
			// selected list is Currently Registered Customers
			$list_id = MAILCHIMP_LIST_ID_CURR_REGD;
				
			// subscriber id
			$subscriber_id = md5(strtolower($selectContactsResult[$i]['email']));
				
			// attempt to add the customer to the target list
			$result = $api->post("lists/$list_id/members", [
					'email_address' => $selectContactsResult[$i]['email'],
					'status'        => 'subscribed',
					'merge_fields'	=> $merge_vars
			]);
	
			// successfully added to list
			if ($api->success()) {
					
				echo $i .': ' .$selectContactsResult[$i]['f_nm'] .' '
						.$selectContactsResult[$i]['l_nm'] .' ('
								.$selectContactsResult[$i]['email']
								.'): new user insert complete.';
								echo "<br>";
									
			}
	
			// there was a problem with the initial post() call
			if ($result['status'] == 400){
					
				if ($result['title'] == 'Member Exists'){
	
					// member exists = 400, so perform an update = patch
					$result = $api->patch("lists/$list_id/members/$subscriber_id", [
							'status'        => 'subscribed',
							'merge_fields'	=> $merge_vars
					]);
	
					echo 'Currently subscribed user: ' ;
	
					if ($api->success()) {
							
						echo $i .': ' .$selectContactsResult[$i]['f_nm'] .' '
								.$selectContactsResult[$i]['l_nm'] .' ('
										.$selectContactsResult[$i]['email']
										.'): update complete.';
										echo "<br>";
											
					} else {
							
						echo "<br>";
						echo 'Error: ' .$i .': ' .$selectContactsResult[$i]['f_nm'] .' '
								.$selectContactsResult[$i]['l_nm'] .' ('
										.$selectContactsResult[$i]['email']
										.'): error triggered.';
										echo "  Error Msg = " .$api->getLastError() ."<br>";
										echo "<br>";
											
					}
	
				}
					
			} // end 400 error handling for initial post() call
	
	
			// error from initial post()
			if ($api->getLastError()){
					
				echo $i .': ' .$selectContactsResult[$i]['f_nm'] .' '
						.$selectContactsResult[$i]['l_nm'] .' ('
								.$selectContactsResult[$i]['email'] .'):'
										.' error occurred. Please follow up with admin.';
										echo "<br>";
										echo "  Error Msg = " .$api->getLastError() ."<br>";
										echo "<br>";
											
			}
	
		} // end for loop, going through all selected records
	
	} else {
	
		// query returns 0 results. Throw error.
		echo '<br><br>';
		echo "No contact records to update at this time.";
		echo '<br><br>';
	}
	
	
	
	
	
	// TODO: Script to delete user records if lesson cancelled, etc.
?>