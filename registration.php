<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';

	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	 
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	require_once BUSINESS_DIR .'order.php';
	
	// Delete any $_SESSION vars that show error messages
	$_SESSION['ErrorMsg'] = '';
	$_SESSION['RecordInfo'] = '';
	unset($_SESSION['RecordInfo']);
	
	
	if($debugging){
		/* echo '<pre>';
		echo 'Cart contents' .'<br>';
		print_r($resultSelectOrder);
		echo '</pre>'; 
		
		echo '<pre>';
		echo 'Total presentation' .'<br>';
		print_r($resultSelectOrderLIs);
		echo '</pre>';  */ 
		
	}
	
	
	
	
	
	
	// If the user has chosen to switch language settings, process that change and reload page
	if (isset($_GET['UserLang'])) {
	
	    // get the user-entered data
	    $BuyerPK = $_SESSION['BuyerId'];
		$NewUserLanguage = $_GET['UserLang'];
		
		// execute language change
		$changeLanguageResult = Contact::UpdateUserLanguage($NewUserLanguage, $BuyerPK);
		
		// reset session language
		$_SESSION['UserLang'] = $changeLanguageResult['t_user_language'];
			
	} // end isset() new parent submit
	
	
	
	// database processing for buyer initial update and new skier -> reg_database.php
	
	
	
	
	// Load buyer details--if no buyer available, set session message and redirect to login
	if (isset($_SESSION['BuyerId'])){
		
		// buyer available and logged in
		$BuyerPK = $_SESSION['BuyerId'];
		
	} else {
		
		// no buyer available, must redirect
		$_SESSION['ErrorMsg'] = $ErrorMsg_Login_AuthenticationError;
		
		// send the user back to the login page
		$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'template_login.php';
		$corrected_dir_string = str_replace('//','/',$dir_string);
		$registration_start = 'http://' .$corrected_dir_string;
		header('Location: ' .$registration_start);
		
	}
	
	$BuyerIsOverseas = $_SESSION['Bool_BuyerIsOverseas'];
	if (isset($_SESSION['Bool_BuyerIsNew']) && ($_SESSION['Bool_BuyerIsNew'] == 1)){
		// no country, prefecture data yet for new buyers
		// this is for first time page load
		$updateBuyerResult = Contact::GetNewBuyerDetails($BuyerPK);
	} else {
		// country, prefecture data is available
		$updateBuyerResult = Contact::GetBuyerDetails($BuyerPK, $BuyerIsOverseas);
	}
	$resultSelectSkiersOfBuyer = Contact::GetSkiersOfBuyer($BuyerPK);
	
	if ($debugging){
		
	}
	
	// Load countries and prefectures
	$resultSelectCountries = Contact::GetCountries();
	$PrefectureOrder = $_SESSION['UserLang'];
	$resultSelectPrefectures = Contact::GetPrefectures($PrefectureOrder);
	
	/* echo '<pre>';
	print_r($resultSelectPrefectures);
	echo '</pre>'; */
	
	// Optional load of order details, this loads all line items
	// Nothing loaded at begin of registration, as order has not been made
	if (isset($_SESSION['OrderId']) && !empty($_SESSION['OrderId'])){
		$OrderPK = $_SESSION['OrderId'];
		$resultSelectOrder = Order::GetOrderDetails($OrderPK);
		$resultSelectOrderLIs = Order::GetOrderDetailsPKSort($OrderPK);
		
		// user may be entering page from declined credit card, test for this
		if (isset($_SESSION['RegPageControl']) && $_SESSION['RegPageControl'] == 4){
			
			// no need to reset anything here
			
		} else {
			
			// default
			// the order is made and order line items have been inserted, set the page control to 3
			$_SESSION['RegPageControl'] = 3;
		}
		
		
		// the order may have been paid (page reload case), check for payment status here
		/* if (isset($resultSelectOrder) && !empty($resultSelectOrder)){
			if ($resultSelectOrder['fk_order_status'] == 5){
				$_SESSION['RegPageControl'] = 4;
			}
		} */
	}
	
	
	
	
	
	// Set page presentation variables, local for this page load only
	$showLargeBanner = 0;
	$showSmallBanner = 1;
	$showAccountTools = 0;
	$showNavigation = 0;
	$showSubNavigation = 0;
	$showLanguageToggle = 0;
	$showSocialFlags = 0;
	$pageType = 'Registration';
	
	
	
	/* echo '<pre>';
	echo 'Buyer update' .'<br>';
	print_r($updateBuyerResult);
	echo '</pre>'; */
	
	
	
	// HTML starts here--------------------------------------//
	
	// Load header
	require_once 'header_reg.php';
	
?>

	<!-- Start Main -->
	<div id="main">

	<!-- Hidden text strings -->
	<input type="hidden"
		      id="SuccessMsg_Buyer_Updated"
		      value="<?php echo $SuccessMsg_Buyer_Updated; ?>" />
		      
	<input type="hidden"
		      id="SuccessMsg_Skier_Inserted"
		      value="<?php echo $SuccessMsg_Skier_Inserted; ?>" />
		      	
	<input type="hidden"
		      id="SuccessMsg_Skier_Updated"
		      value="<?php echo $SuccessMsg_Skier_Updated; ?>" />
		      
	<input type="hidden"
		      id="BuyerId"
		      value="<?php echo $_SESSION['BuyerId']; ?>" />
		      
	<input type="hidden"
		      id="SessionLessonStartDate"
		      value="<?php echo $_SESSION['LessonStartDate']; ?>" />	
		      
	<input type="hidden"
		      id="SessionLessonFinishDate"
		      value="<?php echo $_SESSION['LessonFinishDate']; ?>" />	            
		      
	<input type="hidden"
		      id="OrderId"
		      value="<?php if (isset($_SESSION['OrderId'])){ echo $_SESSION['OrderId']; }?>" />		      	 
		      
	<input type="hidden"
		      id="CurrentUserLang"
		      value="<?php echo $_SESSION['UserLang']; ?>" />	      
		      
	<input type="hidden"
		      id="BuyerIsNewSkier"
		      value="<?php if(isset($_SESSION['Bool_BuyerIsNewSkier'])){
		      	echo $_SESSION['Bool_BuyerIsNewSkier'];
		      }?>" />
		      
	<input type="hidden"
		      id="SelectedStartLocation3"
		      value="<?php echo $LessonStartLocation_Bldg3; ?>" />	
		      
	<input type="hidden"
		      id="SelectedStartLocation6"
		      value="<?php echo $LessonStartLocation_Bldg6; ?>" />	 
		       
	<input type="hidden"
		      id="RegPageControl"
		      value="<?php echo $_SESSION['RegPageControl']; ?>" />	      	           	      		      	
	
	<input type="hidden"
		      id="OneDayLesson"
		      value="<?php echo $LessonTimePeriodLabel_OneDay; ?>" />	
	
	<input type="hidden"
		      id="PrefectureAdvice"
		      value="<?php echo $Prefecture_Advice; ?>" />
		      
	<input type="hidden"
		      id="PrefectureErrorAdvice"
		      value="<?php echo $Prefecture_ErrorAdvice; ?>" />	
	      
		 
	<!-- hidden div to hold prefecture select list -->	      
	<div id="PrefectureSelectionList">
		<select name="BuyerPrefectureFK"><?php 
           for ($i = 0; $i < count($resultSelectPrefectures); $i++): 
           ?><option value="<?php echo $resultSelectPrefectures[$i]['pk_prefecture']; ?>"><?php 
           if ($_SESSION['UserLang'] == 'JP'){
           		echo mb_convert_encoding($resultSelectPrefectures[$i]['t_prefecturename_jp'], "UTF-8", "SJIS");
           } else {
           		echo $resultSelectPrefectures[$i]['t_prefecturename_'.strtolower($_SESSION['UserLang'])];
           }
           
           
           ?></option><?php endfor; 
           ?></select>
	</div>	      
		      
		      	        
		                  
	<!-- Level wizard strings -->	      
	<input type="hidden" id="LevelSelectResult_A" 
	           value="<?php echo $LevelBeginnerA; ?>"/>
	            
    <input type="hidden" id="LevelSelectResult_B" 
            value="<?php echo $LevelBeginnerB; ?>"/>
            
    <input type="hidden" id="LevelSelectResult_C" 
            value="<?php echo $LevelIntermediateC; ?>"/>
            
    <input type="hidden" id="LevelSelectResult_D" 
            value="<?php echo $LevelIntermediateD; ?>"/>
            
    <input type="hidden" id="LevelSelectResult_E" 
            value="<?php echo $LevelIntermediateE; ?>"/>
            
    <input type="hidden" id="LevelSelectResult_F" 
            value="<?php echo $LevelExpertF; ?>"/>      
	
	
	
	<!-- Success/error message strings -->
	<input type="hidden"
		      id="ErrorMsg_LessonSelection_NoLessonsSelected"
		      value="<?php echo $ErrorMsg_LessonSelection_NoLessonsSelected; ?>" />	
	
	<input type="hidden"
		      id="ErrorMsg_LessonSelection_LessonIsFull"
		      value="<?php echo $ErrorMsg_LessonSelection_LessonIsFull; ?>" />	
		      	      
	<input type="hidden"
		      id="SuccessMsg_CouponCode_Valid"
		      value="<?php echo $SuccessMsg_CouponCode_Valid; ?>" />
		      
	<input type="hidden"
		      id="ErrorMsg_CouponCode_Invalid"
		      value="<?php echo $ErrorMsg_CouponCode_Invalid; ?>" />
		      
	<input type="hidden"
		      id="ErrorMsg_CouponCode_NoCodeEntered"
		      value="<?php echo $ErrorMsg_CouponCode_NoCodeEntered; ?>" />
		      	      
	<input type="hidden"
		      id="SuccessMsg_Skier_LevelWizardUpdated"
		      value="<?php echo $SuccessMsg_Skier_LevelWizardUpdated; ?>" />	       
	
	<input type="hidden"
		      id="ConfirmMsg_LevelWizard"
		      value="<?php echo $ConfirmWizardLevelSelection; ?>" />	
		      
		      
		      
	<!-- datepicker strings -->	         
	<input type="hidden"
		      id="ErrorMsg_Datepicker_EndDate_BeforeStartDate"
		      value="<?php echo $ErrorMsg_Datepicker_EndDate_BeforeStartDate; ?>" />	
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_10DaysMaxLength"
		      value="<?php echo $ErrorMsg_Datepicker_10DaysMaxLength; ?>" />
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_TooEarlyStart"
		      value="<?php echo $ErrorMsg_Datepicker_TooEarlyStart; ?>" />	
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_TooLateFinish"
		      value="<?php echo $ErrorMsg_Datepicker_TooLateFinish; ?>" />	
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_StartDate"
		      value="<?php echo $ErrorMsg_Datepicker_StartDate; ?>" />  	      
		      
	
	
	<!-- Credit card error message strings -->
	<input type="hidden"
		      id="ErrorMsg_CC_IncorrectNumber"
		      value="<?php echo $ErrorMsg_CC_CardDeclined; ?>" />	
		      
	<input type="hidden"
		      id="ErrorMsg_CC_InvalidNumber"
		      value="<?php echo $ErrorMsg_CC_InvalidNumber; ?>" />
		      
	<input type="hidden"
		      id="ErrorMsg_CC_InvalidCVC"
		      value="<?php echo $ErrorMsg_CC_InvalidCVC; ?>" />	
		      
	<input type="hidden"
		      id="ErrorMsg_CC_ExpiredCard"
		      value="<?php echo $ErrorMsg_CC_ExpiredCard; ?>" />	       
	
	<input type="hidden"
		      id="ErrorMsg_CC_IncorrectCVC"
		      value="<?php echo $ErrorMsg_CC_IncorrectCVC; ?>" />	
		      
	<input type="hidden"
		      id="ErrorMsg_CC_CardDeclined"
		      value="<?php echo $ErrorMsg_CC_CardDeclined; ?>" />	
		      
	<input type="hidden"
		      id="ErrorMsg_CC_ProcessingError"
		      value="<?php echo $ErrorMsg_CC_ProcessingError; ?>" />
		      
	<input type="hidden"
		      id="ErrorMsg_CC_InvalidExpiryMonth"
		      value="<?php echo $ErrorMsg_CC_InvalidExpiryMonth; ?>" />	
		      
	<input type="hidden"
		      id="ErrorMsg_CC_InvalidExpiryYear"
		      value="<?php echo $ErrorMsg_CC_InvalidExpiryYear; ?>" />	
		
		
	<input type="hidden"
		      id="PageTimeoutWarning"
		      value="<?php echo $WarningMsg_PageTimeout; ?>" />      
	
		      
	<?php if(isset($_SESSION['CC_ErrorMsg'])):?>
	<!-- Reset customer's credit card input -->
	<input type="hidden"
		      id="ReturnCC_Name"
		      value="<?php if (isset($_SESSION['ReturnCC_Name'])){ echo $_SESSION['ReturnCC_Name']; }?>" />	
	<input type="hidden"
		      id="ReturnCC_Number"
		      value="<?php if (isset($_SESSION['ReturnCC_Number'])){ echo $_SESSION['ReturnCC_Number']; }?>" />		      	      	      
	<input type="hidden"
		      id="ReturnCC_ExpMonth"
		      value="<?php if (isset($_SESSION['ReturnCC_ExpMonth'])){ echo $_SESSION['ReturnCC_ExpMonth']; }?>" />	
	<input type="hidden"
		      id="ReturnCC_ExpYear"
		      value="<?php if (isset($_SESSION['ReturnCC_ExpYear'])){ echo $_SESSION['ReturnCC_ExpYear']; }?>" />	
	<input type="hidden"
		      id="ReturnCC_CVC"
		      value="<?php if (isset($_SESSION['ReturnCC_CVC'])){ echo $_SESSION['ReturnCC_CVC']; }?>" />	      
		      	      	      
	<!-- Coupon code management on page reload (coupons are not added to the order while on this page) -->
	<input type="hidden"
		      id="ReturnCouponCode"
		      value="<?php if (isset($_SESSION['ReturnCouponCode'])){ echo $_SESSION['ReturnCouponCode']; }?>" />		
		      
	<input type="hidden"
		      id="ReturnCouponAmount"
		      value="<?php if (isset($_SESSION['ReturnCouponAmount'])){ echo $_SESSION['ReturnCouponAmount']; }?>" />	
		      
	<?php endif; ?>      	            
		      
		      	      
					
	<!-- Start Content -->
	<div id="page-content" class="registration">
	<p id="timeout-warning" style="display:none;"></p>
	
	<?php 
	// debug
	/* echo '<pre>';
	echo 'Session vars:' .'<br>';
	print_r($_SESSION);
	echo '</pre>'; */  ?>
	
        <!-- Begin buyer section head --> 
        <p id="page-control-display" style="display:none;"><?php echo $_SESSION['RegPageControl']; ?></p>
        
        <div id="buyer-reg-section-head" class="page-section-head">
	        <span id="BuyerSectionLabel"><?php echo $BreadcrumbBuyerRegLabel; ?></span>
	        
	        <div class="MsgContainer">
		        <span id="BuyerSectionMsg" class="message"></span>
		        <i class="white fa fa-check-circle-o fa-lg"></i>
	        </div>
        </div>
        <!-- End buyer section head --> 
        
        <?php 
        // buyer update form
        require_once 'panels/panel.buyer_update.php'; 
        ?>
        
        
	       
	       
	       
	        
	    <!-- Begin skier section head, showing multiple skiers --> 
        <div id="skier-reg-section-head" class="page-section-head">
        	<span id="SkierSectionLabel"><?php echo $BreadcrumbSkierRegLabel; ?></span>
        	
        	<div class="MsgContainer">
		      	<span id="SkierSectionMsg" class="message"></span>
		        <i class="white fa fa-check-circle-o fa-lg"></i>
	        </div>
        </div>
        <!-- End skier section head --> 
        
        <!-- Load registered skiers --> 
        <?php 
        // cycle through all skiers for this buyer (may include buyer themselves)
	    if (isset($resultSelectSkiersOfBuyer) && !empty($resultSelectSkiersOfBuyer)){
				
			for ($k = 0; $k < count($resultSelectSkiersOfBuyer); $k++){
				require('panels/panel.skier_update.php');
			}
			
		}?>
		<!-- End registered skiers --> 
		
		<!-- Add new skier button --> 
		<div id="add-skier-button-div" class="page-section-head">
			<button name="SkierRegConfirmAdd" id="SkierRegConfirmAdd" class="btn1 add-skier">
			 		<span><?php echo $AddNewSkierLabel; ?></span></a>
			</button>
			<button name="LessonSelectionProceed" id="LessonSelectionProceed" class="btn1 proceed">
			 		<span><?php echo $ProceedToLessonSelectionLabel; ?></span></a>
			</button>
			
			
		</div>
		<!-- End new skier button --> 
		
		<?php 
		// new skier entry form
		require_once 'panels/panel.skier_new_entry.php';
		?>
		
		
		
		
		
	        
	           
		<!-- Begin lesson registration head, showing multiple skiers to register --> 
		<div id="lesson-reg-section-head" class="page-section-head">
	        
	        <span id="LessonSelectionSectionLabel"><?php echo $LessonSelectionSectionLabel; ?></span>
	        
	        <div class="MsgContainer">
		        <span id="LessonSectionMsg" class="message"></span>
		        <i class="white fa fa-check-circle-o fa-lg"></i>
	        </div>
	        
        </div>
        <!-- End lesson registration head --> 
        
        
	    <!-- Start lesson selection parameters -->
	    <?php require_once 'panels/panel.lesson_parameters.php';?>
	        
	         
	        
	        
	        
        
        <!-- Begin lesson registration section -->
        <div id="lesson-summary-container">
        <?php if (isset($resultSelectOrder) && !empty($resultSelectOrder)){
        	
        	// initialize
        	$previous_skier_id = 0;
        	
        	for ($k = 0; $k < count($resultSelectOrder); $k++){
				
				// check for new skier, do not output div if the same
				$this_skier_id = $resultSelectOrder[$k]['pk_contact'];

				if ($this_skier_id != $previous_skier_id){

					if ($previous_skier_id != 0){

						// print the end of the div when the skier id changes
						// we don't print this at the beginning of the sequence
						echo '</div>';
						/* echo '<span><a class="skier-lesson edit-link" href="#">' 
							 .$EditFormContent
							 .'</a></span>'
							 .'</div>'; */
					}

					// create summary div for each skier, output line items
					echo '<div class="page-section-body summary-view lesson-selection">';
					
					
					// skier's name
					if ($_SESSION['UserLang'] == 'EN'){
							
						// EN-language
						echo '<p class="skier-name-display">'
								.$SkierNameLabel
								.$resultSelectOrder[$k]['t_firstname_jp'] .' '
								.$resultSelectOrder[$k]['t_lastname_jp']
								.'</p>';
					} else {
							
						// default
						echo '<p class="skier-name-display">'
								.$SkierNameLabel
								.mb_convert_encoding($resultSelectOrder[$k]['t_lastname_jp'], "UTF-8", "SJIS") .' ' 
	    						.mb_convert_encoding($resultSelectOrder[$k]['t_firstname_jp'], "UTF-8", "SJIS")
								.'</p>';
					}
					
				} // end check for duplicate
				
				
				// print out summary line items
				// first set up strings
				require 'panels/panel.lesson_selection_display.php';
				
				// output lesson line
				if (!empty($instructor_name)){

					// output instructor information
					echo '<p class="summary-content">';
					echo '<span class="summary-instructor-display">'
						 .$instructor_display
						 .'</span>';
					echo '</p>';

				} else {

					// output lesson summary
					echo '<p class="summary-content">';
					echo '<span class="summary-date">' .$summary_date_display .'</span>';
					echo '<span class="summary-lesson-display">'
							 .'<span class="lesson-time">' 
							 .$lesson_time_display
							 .'</span>'	
							 .'<span class="lesson-name">' 
							 .$lesson_name_display
							 .'</span>'
						.'</span>';
					echo '</p>';
				}
			
				// update duplicate check
				$previous_skier_id = $this_skier_id;
				
				
				
			} // end loop $k
			
			
			
			
        }?>
        </div>
				
        <div id="lesson-container">
        </div>
        <div id="lesson-assistance-message" class=""><?php echo $Msg_LessonSelection_AdditionalResources; ?>
        </div>
        <!-- End lesson registration section -->
        
       
       
       
       
       
       
       
        
        
        <!-- Begin liability release head --> 
		<div id="liability-release-section-head" class="page-section-head">
        
	        <span id="LiabilityReleaseLabel"><?php echo $LiabilityReleaseHeader; ?></span>
	        
	        <div class="MsgContainer">
		        <span id="LiabilityReleaseSectionMsg" class="message"></span>
		        <i class="white fa fa-check-circle-o fa-lg"></i>
	        </div>
        
        </div>
        <!-- End liability release head -->
        
        <!-- Begin liability release body --> 
		<div id="liability-release-section-body" class="page-section-body">
		     
			<?php require_once 'panels/panel.liability_release.php'; ?>   
		
	    </div>
	    <!-- End liability release body -->
	    
	    
	    
	    
	    
	    
	    
	    
        
        <!-- Begin confirmation + payment head --> 
		<div id="payment-section-head" class="page-section-head">
	        
	        <span id="PaymentLabel"><?php echo $PaymentSectionHeader; ?></span>
	        
	        <span id="PaymentMsg"></span>
        
        </div>
        <!-- End confirmation + payment head -->
	 
	 
	 
        <!-- Begin confirmation + payment body --> 
		<div id="payment-section-body" class="page-section-body payment">
		
		
		<?php 
		
		// initialize
		$placeholder_order_total_JPY = '';
		
		
		
		
		// wide table output
		require_once 'panels/panel.confirmation_table_wide.php';
		
		// format output
		$placeholder_order_total_JPY = $order_total_JPY;
		$order_total_JPY = number_format($order_total_JPY);
        
        // wide table: coupon code line
        echo '<tr class="confirmation-discount-display">'
        		.'<td class="coupon-code-display" colspan="4">' .$ConfirmationTableCouponCodeLabel 
        		.'<span class="coupon-code"></span>'
        		.'</td><td class="confirmation lesson-price discount">'
        		.'- &yen;'
				.'<span class="coupon-amount"></span>'
				.'</td>'
        	.'</tr>';
	        	
	    // wide table: totals line
	    echo '<tr>'
        		.'<td class="total-label" colspan="4">' .$ConfirmationTableTotalLabel .'</td>'
        		.'<td class="confirmation lesson-price">' .'&yen;'
        		.'<span class="total-display">' .$order_total_JPY .'</span>'
			    .'</td>'
        	.'</tr>';
        
        // wide table: final row with tax advice
        echo '<tr>'
        		.'<td colspan="5" class="tax-advice">' .$AllPricesIncludeTax .'</td>'
        	.'</tr>';
        	
        // wide table: complete
        echo '</table>';
        
        // hidden input to hold current order total
        echo '<input type="hidden"
					 id="OrderTotalJPY"
        			 value="' .$placeholder_order_total_JPY .'" />';
        
        
        
        
        
        
        
        
        // narrow table output, for mobile display
        require_once 'panels/panel.confirmation_table_narrow.php';
        
        // narrow table: coupon code line
        echo '<tr class="confirmation-discount-display">'
		        .'<td class="coupon-code-display" colspan="2">' .$ConfirmationTableCouponCodeLabel
		        .'<span class="coupon-code"></span>'
		        .'</td>'
				.'<td class="confirmation lesson-price discount">'
		        .'- &yen;'
		        .'<span class="coupon-amount"></span>'
		        .'</td>'
	        .'</tr>';
        
        
        
        echo '<tr>'
		        .'<td class="total-label" colspan="2">' .$ConfirmationTableTotalLabel .'</td>'
		        .'<td class="confirmation lesson-price">' .'&yen;'
		        .'<span class="total-display">' .$order_total_JPY .'</span>'
		        .'</td>'
	        .'</tr>';
        
        // narrow table: final row with tax advice
        echo '<tr>'
        		.'<td colspan="3" class="tax-advice">' .$AllPricesIncludeTax .'</td>'
        	.'</tr>';
        
        // narrow table: complete
        echo '</table>';        
        
      
        
        
        
        
        
        
        
        ?>
     
        
        <!-- Coupon code entry -->
	     <div class="row" id="CouponCodeWrapper">
	     <h3 id="CouponCodeHeader"><?php echo $MarketingQuestionCouponCodeLabel; ?></h3>
	     	<input type="text" 
			  		 name="MarketingCouponCode" 
			  		 id="MarketingCouponCode" 
			  		 title="<?php echo $ErrorMsg_CouponCode_Invalid; ?>" />
		      <span id="CheckCouponCodeLink"><?php echo $CheckCouponCodeLinkLabel; ?></span>
		      <p id="CheckCouponCodeReturnMsg"></p>
	     </div> 
    
        <!-- End coupon code entry --> 
        
        
        
        <!-- Payment method selection -->
        <div id="payment-method">
        	
        	<h3 id="PaymentMethodHeader"><?php echo $PaymentMethodSectionHeader; ?></h3>
        
        	<div id="payment-method-selection">
        	<div id="cc-error-display"><?php 
        		if (isset($_SESSION['CC_ErrorMsg']) && !empty($_SESSION['CC_ErrorMsg'])) {
					echo '<span class="fa-stack fa-lg">'
						 .'<i class="fa fa-square fa-stack-2x"></i>'
						 .'<i class="fa fa-exclamation fa-stack-1x"></i>'
						 .'</span>';
					echo '<span class="cc-error-msg">' .$_SESSION['CC_ErrorMsg'] .'</span>';
				} else {
					echo '<span class="fa-stack fa-lg">'
						.'<i class="fa fa-square fa-stack-2x"></i>'
						.'<i class="fa fa-exclamation fa-stack-1x"></i>'
						.'</span>';
					echo '<span class="cc-error-msg"></span>';
				}
        		?></div>
        	<input type="checkbox" class="payment-method-selection" name="payment-method-selection[]" id="PaymentMethodCC" 
	        value=""/>
	        <label class="checkbox" for="PaymentMethodCC"><?php echo $PaymentMethodCreditCard_Title; ?></label>
	        <p class="payment-method-explanation credit-card"><?php echo $PaymentMethodCreditCard_Explanation; ?></p>
	        
	        <!-- Credit card form -->
	        <div class="panel panel-default" id="credit-card-box">
                <div class="panel-heading">
                    <p class="panel-title"><?php echo $PaymentMethodCreditCard_CardsAccepted; ?></p>
                    <img class="accepted-cards" src="images/accepted_cards_vs_mc_disc_ae_uc_jcb.png">
                </div>
                <div class="panel-body">
                    <form role="form" 
                    	  action="template_cc_charge.php"
                    	  method="post"
                    	  name="payment-form"
                    	  id="payment-form">
                    	  
                    	  
                    	  <input type="hidden" name="chargeAmount" id="chargeAmount" value="" />
                    	  <input type="hidden" name="chargeOrderId" id="chargeOrderId" value="" />
                    	  <input type="hidden" name="chargeCouponCode" id="chargeCouponCode" value="" />
                    	  <input type="hidden" name="chargeCouponAmount" id="chargeCouponAmount" value="" />
                    	  <input type="hidden" name="buyerLiabilityText" id="buyerLiabilityText" value="<?php echo $LiabilityReleaseTextFull; ?>" />
             
                    	  
                    	<div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="cardName"><?php echo $PaymentMethodCreditCard_CardName; ?></label>
                                    <div class="input-group">
                                        <input type="text" 
		                                        class="form-control cc-name required" 
		                                        title="<?php echo $CCForm_NoNameEntered_ErrorAdvice; ?>" 
		                                        id="CCcardName" 
		                                        name="cardName" 
		                                        placeholder="<?php echo $PaymentMethodCreditCard_CardNamePlaceholder; ?>" 
		                                        autofocus 
		                                        data-stripe="name" />
                                        
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="cardNumber"><?php echo $PaymentMethodCreditCard_CardNumber; ?></label>
                                    <div class="input-group">
                                        <input type="text" 
		                                        class="form-control cc-number required" 
		                                        title="<?php echo $CCForm_NoNumberEntered_ErrorAdvice; ?>" 
		                                        id="CCcardNumber" 
		                                        name="cardNumber" 
		                                        placeholder="<?php echo $PaymentMethodCreditCard_CardNumberPlaceholder; ?>" 
		                                        autofocus 
		                                        data-stripe="number" />
                                        
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label for="expMonth"><?php echo $PaymentMethodCreditCard_ExpirationDate; ?></label>
                                    <select class="form-control" 
                                    		id="CCexpMonth" 
                                    		name="expMonth" 
                                    		data-stripe="exp_month">
                                    <option value="1">01</option>
                                    		
                                    <?php for ($i = 2; $i <= 12; $i++){
                                    	  if ($i < 10){
											  echo '<option value="' .$i .'">' .'0' .$i .'</option>';
											  
										  } else {
											  echo '<option value="' .$i .'">' .$i .'</option>';
										  }
                                    	 
                                    }?>
                                    </select>
                                    <select class="form-control" 
                                    		id="CCexpYear" 
                                    		name="expYear" 
                                    		data-stripe="exp_year"">
                                    <?php 
                                    $startYear = date("Y");
                                    $endYear = date("Y") + 10;
                                    for ($j = $startYear; $j <= $endYear; $j++){
                                    	  echo '<option value="' .$j .'">' .$j .'</option>';
                                    }?>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5">
                                <div class="form-group">
                                    <label for="cvCode" id="cv-code-label"><?php echo $PaymentMethodCreditCard_SecCode; ?></label>
                                    <input type="password" 
                                    		class="form-control cc-cvc required" 
                                    		title="<?php echo $CCForm_NoCVCEntered_ErrorAdvice; ?>" 
                                    		id="CCcvCode" 
                                    		name="cvCode" 
                                    		placeholder="<?php echo $PaymentMethodCreditCard_SecCodePlaceholder; ?>" 
                                    		data-stripe="cvc" />
                                </div>
                            </div>
                        </div>
                      
                            
                        
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                        
                        <button type="submit" name="CompleteRegistration" id="CompleteRegistrationCC" class="btn2 complete-registration">
                        	<span><?php echo $CompleteRegistration_ButtonLabel; ?></span>
                        </button>
		 	
                    </form>
                </div>
            </div>
                
                
	        <!-- Optional bank transfer selection, not available for Mikuni -->
	        
	        
	        </div>
	        
	        <h3 class="cancellation-advice"><?php echo $RegardingCancellationsHeader; ?></h3>
	        <p class="cancellation-advice"><?php echo $RegardingCancellationsText; ?></p>
        
        
	        
        </div>
        <!-- End payment method selection -->
        
        <!-- Credit card payment -->
        



        
        
        <?php 
        
        
        ?>
        </div>
        <!-- End confirmation + payment body --> 
	 
	 </div>
	<!-- End Content -->
				
	</div>
	<!-- End Main -->        
	
<?php
	// Load footer, Javascript, and page closing code
	require_once 'footer_minimum.php';
	require_once 'js_page_closer.php';
?>