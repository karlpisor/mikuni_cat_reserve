<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'order.php';
	require_once BUSINESS_DIR .'contact.php';
	
	# return json data, create an array to hold this
	$returnCouponInsertResult = array();
	
	# look for post from 
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
		
        // get data from ajax call
		$CouponCode = $_POST['CouponCode'];
		
        // get coupon data 
        $resultGetCouponCode = Order::OrderCheckCouponCode($CouponCode);
            
        // set return values
        if (isset($resultGetCouponCode) && !empty($resultGetCouponCode)) {
            	
	        // valid coupon code
	        $returnCouponInsertResult['Discount'] = $resultGetCouponCode['n_coupon_code_discount'];
	        $returnCouponInsertResult['Level'] = 1;
	        
	        // insert line item into customer order
	        $OrderFK = $_SESSION['OrderId'];
	        $CouponCodeFK = $resultGetCouponCode['pk_coupon_code'];
	        $DiscountJPY = $resultGetCouponCode['n_coupon_code_discount'];
	        
	        $resultInsertCouponCodeAsLI = Order::InsertCouponCodeAsOrderLineItem(
	        		$OrderFK, 
	        		$CouponCodeFK,
	        		$DiscountJPY);
	        
	        if (isset($resultInsertCouponCodeAsLI) && !empty($resultInsertCouponCodeAsLI)) {
	        	$returnCouponInsertResult['Level'] = 2;
	        }
	        	
        } else {
            	
            // invalid code
            $returnCouponInsertResult['Level'] = -1;
        }
     
	}
    
    // return json array
    echo json_encode($returnCouponInsertResult); 
    
?>