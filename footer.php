<!-- Start Footer -->
	<footer>
		<ul>
			<li class="sitemap"><a href="#"><?php echo $NavLink_LessonMenu; ?></a>
			
			</li>
			<li class="sitemap"><a href="#"><?php echo $NavLink_FAQ; ?></a>
				
			</li>
			<li class="sitemap"><a href="#"><?php echo $NavLink_Certification; ?></a>
				
			</li>
			<li class="sitemap"><a href="#"><?php echo $NavLink_AboutUs; ?></a>
				<ul>
					<?php if ($_SESSION['UserLang'] == 'JP'): ?>
					<li><a href="#"><?php echo $NavLink_WorkWithUs; ?></a></li>
					<li><a href="#"><?php echo $NavLink_StaffProfiles; ?></a></li>
					<?php endif; ?>
					
				</ul>
			</li>
			<?php if ($_SESSION['UserLang'] == 'JP'): ?>
			<li class="sitemap"><a href="#"><?php echo $NavLink_Blog; ?></a>
				
			</li>
			<?php endif; ?>
			
			<li class="sitemap"><h3><?php echo $ContactUsHeader; ?></h3>
				<ul>
					<li><?php echo $ContactUsPhoneText; ?></li>
					<li><?php echo $ContactUsMailText; ?></li>
					<li><?php echo $ContactUsOfficeHoursText; ?></li>
					
				</ul>
			</li>
		</ul> 
	<p id="copy">Copyright &copy;<?php echo date("Y"); ?> KK Sherpa. All rights reserved.</p> 	
	</footer>
	    
<!-- End Footer -->
