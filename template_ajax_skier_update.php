<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
    
	function ucname($string){
		$string = ucwords(strtolower($string));
	
		foreach (array('-', '\'') as $delimiter) {
			if (strpos($string, $delimiter)!==false) {
				$string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
			}
		}
		return $string;
	}
	
	
    // update existing skier record
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
		
		$SkierPK = $_POST['SkierPK'];
		
		$SkierLastNameJP = ucname(trim($_POST['SkierLastNameJP']));
		$SkierFirstNameJP = ucname(trim($_POST['SkierFirstNameJP']));
		
		// no kana entry for EN, CN users
		$SkierLastNameJP_Kana = 'Non-JP';
		$SkierFirstNameJP_Kana = 'Non-JP';
		if (isset($_POST['SkierLastNameJP_Kana']) && !empty($_POST['SkierLastNameJP_Kana'])){
			$SkierLastNameJP_Kana = trim($_POST['SkierLastNameJP_Kana']);
		}
		if (isset($_POST['SkierFirstNameJP_Kana']) && !empty($_POST['SkierFirstNameJP_Kana'])){
			$SkierFirstNameJP_Kana = trim($_POST['SkierFirstNameJP_Kana']);
		} 
		
		$SkierGender = $_POST['SkierGender'];
		$SkierBirthMonth = $_POST['SkierBirthMonth'];
		$SkierBirthDay = $_POST['SkierBirthDay'];
		$SkierBirthYear = $_POST['SkierBirthYear'];
		
		$SkierInstructionLang = $_POST['SkierInstructionLang'];
		$SkierLessonType = $_POST['SkierLessonType'];
		$SkierAbilityLevelSnowboard = $_POST['SkierAbilityLevelSnowboard'];
		$SkierAbilityLevelSki = $_POST['SkierAbilityLevelSki'];
		
		$SkierFoodAllergies = '';
		if (isset($_POST['SkierFoodAllergies']) && !empty($_POST['SkierFoodAllergies'])){
			$SkierFoodAllergies = trim($_POST['SkierFoodAllergies']);
		} 
		$InstructorPrefs = '';
		$InstructorPrefs_FreeText = '';
		
		if ($_SESSION['UserLang'] == 'JP'){
			// encode to SJIS
			$SkierLastNameJP = mb_convert_encoding($SkierLastNameJP, "SJIS", "UTF-8");
			$SkierFirstNameJP = mb_convert_encoding($SkierFirstNameJP, "SJIS", "UTF-8");
			$SkierLastNameJP_Kana = mb_convert_encoding($SkierLastNameJP_Kana, "SJIS", "UTF-8");
			$SkierFirstNameJP_Kana = mb_convert_encoding($SkierFirstNameJP_Kana, "SJIS", "UTF-8");
			$SkierFoodAllergies = mb_convert_encoding($SkierFoodAllergies, "SJIS", "UTF-8");
				
			$InstructorPrefs = mb_convert_encoding($InstructorPrefs, "SJIS", "UTF-8");
			$InstructorPrefs_FreeText = mb_convert_encoding($InstructorPrefs_FreeText, "SJIS", "UTF-8");
				
		}
	
		// check for existing skier update
		if (isset($_POST['SkierRegSubmit'])) {
		 	
		 	// update skier record
		 	$updateSkierResult = Contact::UpdateSkier(
		 			
                    $SkierPK,
		 			$SkierLastNameJP,
                    $SkierFirstNameJP,
					$SkierLastNameJP_Kana,
					$SkierFirstNameJP_Kana,
				
                    $SkierGender,
                    $SkierBirthMonth,
                    $SkierBirthDay,
                    $SkierBirthYear,
		 			$SkierInstructionLang,
		 			
		 			$SkierLessonType,
					$SkierAbilityLevelSnowboard,
					$SkierAbilityLevelSki,
					$SkierFoodAllergies,
					$InstructorPrefs,
		 			
					$InstructorPrefs_FreeText);
		 
		 	
		 	 
		 } // end check for existing skier update
	
	} 
		
	// advance registration page control
	// user now has both buyer and skier(s) registered
	$_SESSION['RegPageControl'] = 2;
	
	
    
    
    require 'panels/panel.skier_summary_display.php'; ?>
	
	