<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
    
	
	
	// initialize error message
	$error_msg = '';
	$success_msg = '';
	
	function ucname($string){
		$string = ucwords(strtolower($string));
	
		foreach (array('-', '\'') as $delimiter) {
			if (strpos($string, $delimiter)!==false) {
				$string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
			}
		}
		return $string;
	}
	
    // update buyer record
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
		
		// buyer ID stored in the session
		$BuyerPK = $_SESSION['BuyerId'];
	
		$BuyerLastNameJP = ucname(trim($_POST['BuyerLastNameJP']));
		$BuyerFirstNameJP = ucname(trim($_POST['BuyerFirstNameJP']));
		
		// no kana entry for EN, CN users
		$BuyerLastNameJP_Kana = 'Non-JP';
		$BuyerFirstNameJP_Kana = 'Non-JP';
		if (isset($_POST['BuyerLastNameJP_Kana']) && !empty($_POST['BuyerLastNameJP_Kana'])){
			$BuyerLastNameJP_Kana = trim($_POST['BuyerLastNameJP_Kana']);
		} 
		if (isset($_POST['BuyerFirstNameJP_Kana']) && !empty($_POST['BuyerFirstNameJP_Kana'])){
			$BuyerFirstNameJP_Kana = trim($_POST['BuyerFirstNameJP_Kana']);
		} 
		
		
		
	
		$BuyerEmail = trim($_POST['BuyerEmail']);
	
		// use mb_convert_kana() to hard-convert user-entered full-width numbers
		$phone_str = trim($_POST['BuyerPhone']);
		$BuyerPhone = mb_convert_kana($phone_str,"KVa","UTF-8");
		
		// TODO: Country selection, forces load of country's main prefectures/states in next field
	
	
		// TODO: further processing on postal code input to generate 7-digit JP postalcode
		// will not process foreign addresses (non-JP only?)
		$postcode_str = trim($_POST['BuyerPostalCode']);
		$BuyerPostalCode = mb_convert_kana($postcode_str,"KVa","UTF-8");
	
		$BuyerCountryOfResidenceFK = $_POST['BuyerCountryOfResidenceFK'];
		// this is set by javascript check
		$BuyerIsOverseas = $_POST['BuyerIsOverseas'];
		
		// prefecture is potentially a text string, let's trim it
		$BuyerPrefectureFK = '';
		$BuyerPrefectureFK = trim($_POST['BuyerPrefectureFK']);
		
		$BuyerPrefectureText = '';
		if (isset($_POST['BuyerPrefectureText']) && !empty($_POST['BuyerPrefectureText'])){
			$BuyerPrefectureText = trim($_POST['BuyerPrefectureText']);
		}
		
		$BuyerCityTown = trim($_POST['BuyerCityTown']);
		$BuyerAddress1 = trim($_POST['BuyerAddress1']);
		$BuyerAddress2 = trim($_POST['BuyerAddress2']);
	
		$buyer_emer_phone_str = trim($_POST['BuyerEmergencyPhone']);
		$BuyerEmergencyPhone = mb_convert_kana($buyer_emer_phone_str,"KVa","UTF-8");
		$BuyerEmergencyContact = trim($_POST['BuyerEmergencyContact']);
		$BuyerUserLang = $_POST['BuyerCurrentUserLang'];
		$BuyerReceivesNewsletter = $_POST['BuyerReceivesNewsletter'];
		$BuyerIsSkier = $_POST['BuyerIsSkier'];
		
		
		if ($_SESSION['UserLang'] == 'JP'){
			// encode to SJIS
			$BuyerLastNameJP = mb_convert_encoding($BuyerLastNameJP, "SJIS", "UTF-8");
			$BuyerFirstNameJP = mb_convert_encoding($BuyerFirstNameJP, "SJIS", "UTF-8");
			$BuyerLastNameJP_Kana = mb_convert_encoding($BuyerLastNameJP_Kana, "SJIS", "UTF-8");
			$BuyerFirstNameJP_Kana = mb_convert_encoding($BuyerFirstNameJP_Kana, "SJIS", "UTF-8");
			$BuyerPrefectureText = mb_convert_encoding($BuyerPrefectureText, "SJIS", "UTF-8");
			
			$BuyerCityTown = mb_convert_encoding($BuyerCityTown, "SJIS", "UTF-8");
			$BuyerAddress1 = mb_convert_encoding($BuyerAddress1, "SJIS", "UTF-8");
			$BuyerAddress2 = mb_convert_encoding($BuyerAddress2, "SJIS", "UTF-8");
			$BuyerEmergencyContact = mb_convert_encoding($BuyerEmergencyContact, "SJIS", "UTF-8");
		}
	
		// server-side validation on name entry? How to do this?
		//if (!empty($BuyerFirstNameEN) && !empty($BuyerLastNameEN)) {
	
		// process update
		$updateBuyerResult = Contact::UpdateBuyer(
				$BuyerPK,
				$BuyerLastNameJP,
				$BuyerFirstNameJP,
				$BuyerLastNameJP_Kana,
				$BuyerFirstNameJP_Kana,
		
				$BuyerEmail,
				$BuyerPhone,
				$BuyerPostalCode,
				$BuyerCountryOfResidenceFK,
				$BuyerPrefectureFK,
				$BuyerPrefectureText,
				
				$BuyerCityTown,
				$BuyerAddress1,
				$BuyerAddress2,
				$BuyerEmergencyPhone,
				$BuyerEmergencyContact,
				
				$BuyerUserLang,
				$BuyerReceivesNewsletter,
				$BuyerIsSkier,
				$BuyerIsOverseas);

		if ($updateBuyerResult) {
			// update is okay
			$success_msg = 'Parent record update executed correctly';
			$_SESSION['BuyerFirstNameJP'] = $updateBuyerResult['t_firstname_jp'];
			$_SESSION['BuyerFirstNameJPKana'] = $updateBuyerResult['t_firstname_furigana_jp'];
			$_SESSION['BuyerLastNameJP'] = $updateBuyerResult['t_lastname_jp'];
			$_SESSION['BuyerLastNameJPKana'] = $updateBuyerResult['t_lastname_furigana_jp'];
			
			if ($_SESSION['UserLang'] == 'JP'){
				// decode to UTF8
				$_SESSION['BuyerFirstNameJP'] = mb_convert_encoding($updateBuyerResult['t_firstname_jp'], "UTF-8", "SJIS");
				$_SESSION['BuyerFirstNameJPKana'] = mb_convert_encoding($updateBuyerResult['t_firstname_furigana_jp'], "UTF-8", "SJIS");
				$_SESSION['BuyerLastNameJP'] = mb_convert_encoding($updateBuyerResult['t_lastname_jp'], "UTF-8", "SJIS");
				$_SESSION['BuyerLastNameJPKana'] = mb_convert_encoding($updateBuyerResult['t_lastname_furigana_jp'], "UTF-8", "SJIS");
			}
			
			// reset session variable, buyer no longer first timer
			$_SESSION['Bool_BuyerIsNew'] = 0;
			
			// update session variable for non-Japanese residents
			// this update runs multiple times, so always reset the session variable
			if($updateBuyerResult['fk_contact_country'] != 109){
				$_SESSION['Bool_BuyerIsOverseas'] = 1;
			} else {
				$_SESSION['Bool_BuyerIsOverseas'] = 0;
			}
			
			// return output for buyer display
			require_once 'panels/panel.buyer_summary_display.php';

		} else {
			// query returns 0 results. Throw error.
			$_SESSION['ErrorMsg'] = 'Error with update';
		}	
				
	} // end check for post
	
	
    
	# debug
	if ($debugging){
		# return error message from $SESSION if present
		if (isset($_SESSION['ErrorMsg'])){
			$error_msg .= $_SESSION['ErrorMsg'];
		}
    }

    
	
	
	
	