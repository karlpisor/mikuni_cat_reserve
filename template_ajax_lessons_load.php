<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	require_once BUSINESS_DIR .'order.php';
    
	
	
	// initialize error message
	$error_msg = '';
	$success_msg = '';
	
	// initialize
	
	
	// assemble variables for lesson load
	$StartDate = $_POST['LessonStartDate'];
	$EndDate = $_POST['LessonEndDate'];
	$LessonStartLocationFK = 3;
	

	// update session variables for start and end date
	// these will be access later to insert the order
	$_SESSION['LessonStartDate'] = $_POST['LessonStartDate'];
	$_SESSION['LessonFinishDate'] = $_POST['LessonEndDate'];
	
	// update session variable for lesson start location
	// fixed at 3 for all Mikuni reservations (won't be used in the db call)
	$_SESSION['LessonStartLocation'] = 3;
   
		
	// load skier array, use this to load available lessons for each
	$BuyerPK = $_SESSION['BuyerId'];
	$resultSelectSkiersOfBuyer = Contact::GetSkiersOfBuyer($BuyerPK);	
	
	
	if ((isset($resultSelectSkiersOfBuyer)) && !empty($resultSelectSkiersOfBuyer)){
		
		//echo 'Number of skiers: ' .count($resultSelectSkiersOfBuyer);
		
		// load available lessons
		for ($i = 0; $i < count($resultSelectSkiersOfBuyer); $i++){
			
			// create dynamic variable to hold content for each skier separately
			${'output' .$i} = '';
			
			// create selection div for this skier, used to control view by tabs
			${'output' .$i} .= '<div id="skier-lesson-selection-' .$i .'" class="skier-lesson-selection-body skier-' .$i .'">';
			
			// label with skier's name
			if ($_SESSION['UserLang'] == 'EN'){
					
				// EN-language
				$skierNameString = $resultSelectSkiersOfBuyer[$i]['t_firstname_jp'] .' '
									  .$resultSelectSkiersOfBuyer[$i]['t_lastname_jp'];
				${'output' .$i} .=  '<p class="skier-name-display skier-' .$i .'">'
					.$SkierNameLabel	
					.$skierNameString
					.'</p>';
					
			} else {
					
				// default
				$skierNameString = mb_convert_encoding($resultSelectSkiersOfBuyer[$i]['t_lastname_jp'], "UTF-8", "SJIS") .' '
									 .mb_convert_encoding($resultSelectSkiersOfBuyer[$i]['t_firstname_jp'], "UTF-8", "SJIS");
				${'output' .$i} .=  '<p class="skier-name-display skier-' .$i .'">'
					.$SkierNameLabel	
					.$skierNameString
					.'</p>';
			}
			
			// create set of clickable calendar buttons for each skier
			
			// create array to hold the date range selected
			$date_pointer = $StartDate;
			$date_range = array();
			while (strtotime($date_pointer) <= strtotime($EndDate)) {
				$date_range[] = $date_pointer;
				$date_pointer = date("Y-m-d", strtotime("+1 day", strtotime($date_pointer)));
			}
			
			for ($c = 0; $c < count($date_range); $c++){
						
				// calculate the current day of the week
				$day_of_week = date('w', strtotime($date_range[$c]));
				// day format "d" uses leading zero
				$day_numeral = date('d', strtotime($date_range[$c]));
				// day format "j" does not use leading zero
				$day_display = date('j', strtotime($date_range[$c]));
					
				// create array of days
				switch ($day_of_week){
					case 0:
						$day_of_week = $DayOfWeek_Sun;
						break;
					case 1:
						$day_of_week = $DayOfWeek_Mon;
						break;
					case 2:
						$day_of_week = $DayOfWeek_Tue;
						break;
					case 3:
						$day_of_week = $DayOfWeek_Wed;
						break;
					case 4:
						$day_of_week = $DayOfWeek_Thu;
						break;
					case 5:
						$day_of_week = $DayOfWeek_Fri;
						break;
					case 6:
						$day_of_week = $DayOfWeek_Sat;
						break;
				}
				
				// special formatting for traditional chinese
				// default
				$day_of_week_classes = 'day-of-week';
				if ($_SESSION['UserLang'] == 'CN'){
					$day_of_week_classes = 'day-of-week cn';
				}
					
				${'output' .$i} .= '<div class="calendar ' .'skier-' .$i .' ' .$day_numeral .'">'
						.'<span class="' .$day_of_week_classes .'">' .$day_of_week .'</span>'
						.'<span class="day">' .$day_display .'</span>'
						// hidden input to hold day display with leading zeros		
						.'<input type="hidden" class="day-numeral" value="' .$day_numeral .'"/>'		
						// hidden input to hold the skier index
						.'<input type="hidden" class="day-skier-index" value="' .$i .'"/>'
						// hidden input to hold the skier name
						.'<input type="hidden" class="skier-name" value="' .$skierNameString .'"/>'		
						.'</div>';
				
			} // end output for calendar set, skier-$i
			
			// add a link to reset selections for this day
			${'output' .$i} .= '<div class="selection-reset">'
					.$ResetDaySelectionLink
					.'</div>';
			
			// clear the elements underneath
			${'output' .$i} .= '<br class="Clear">';
			
			
			
			$SkierIsAdult = '';
			$SkierBirthYear = $resultSelectSkiersOfBuyer[$i]['y_birthyear'];
			$SkierBirthMonth = $resultSelectSkiersOfBuyer[$i]['n_birthmonth'];
			$SkierBirthDay = $resultSelectSkiersOfBuyer[$i]['n_birthday'];
			
			$SkierBirthDate = $SkierBirthYear .'-' .$SkierBirthMonth .'-' .$SkierBirthDay;
			
			// calculate junior cutoff date
			// adults must be 12 or older on the cutoff date, or else the skier is a junior
			// cutoff date for juniors set to 4/2/2004, set in config.php
			// cutoff date for peewees set to 4/2/2010, set in config.php
			$JuniorCutoffDate = (int)date("Ymd", strtotime(JUNIOR_STUDENT_AGE_CUTOFF));
			$PeeweeCutoffDate = (int)date("Ymd", strtotime(PEEWEE_STUDENT_AGE_CUTOFF));
			$SkierBirthString = (int)date("Ymd", strtotime($SkierBirthDate));
			
			if ($SkierBirthString > $JuniorCutoffDate){
				
				if ($SkierBirthString > $PeeweeCutoffDate){
					
					// peewee
					$SkierIsAdult = 2;
				} else {
					
					// junior
					$SkierIsAdult = 0;
				}	
				
			} else {
				
				// adult
				$SkierIsAdult = 1;
			}
			
			$SkierPreferredLessonType = $resultSelectSkiersOfBuyer[$i]['n_preferred_lesson_type'];
			$SkierAbilityLevelSki = $resultSelectSkiersOfBuyer[$i]['n_ability_level_ski'];
			$SkierAbilityLevelSnowboard = $resultSelectSkiersOfBuyer[$i]['n_ability_level_snowboard'];
			$SkierInstructionLang = $resultSelectSkiersOfBuyer[$i]['t_instruction_language'];
				
			if ($debugging){

				echo $StartDate .'<br>';
				echo $EndDate .'<br>';
				echo $JuniorCutoffDate .' = Cut off date for 12 year olds <br>';
				echo $SkierBirthString .' = Skier birth date <br>';
				if ($SkierIsAdult == 0){
					echo 'Skier is junior. Call value = ' .$SkierIsAdult .'<br>';
				} else if ($SkierIsAdult == 1){
					echo 'Skier is adult. Call value = ' .$SkierIsAdult .'<br>';
				} else if ($SkierIsAdult == 2){
					echo 'Skier is peewee. Call value = ' .$SkierIsAdult .'<br>';
				}
				echo $SkierPreferredLessonType .'= Lesson Type, 1 = Ski, 2 = Snowboard, 3 = both <br>';
				echo $SkierAbilityLevelSki .'= Ski <br>';
				echo $SkierAbilityLevelSnowboard .'= SB <br>';
				echo $SkierInstructionLang .'= Lang <br>';
				echo $LessonStartLocationFK .'= Start Location <br>';
			}
			
			// set variable to suppress output, if no lessons are found for this skier
			$nBool_suppress_output = 0;
			
			// select this skier's lessons
			$resultSelectAvailableLessonsOfSkier = Order::MikuniGetAvailableLessonsOfSkier(
					$StartDate, 
					$EndDate);
			
			
			
			if ((isset($resultSelectAvailableLessonsOfSkier)) && !empty($resultSelectAvailableLessonsOfSkier)){
				
				
				
				// initialize comparison string to suppress duplicate display
				$last_duplicate_string = '';
				
				// even or odd class control
				$last_lesson_even_odd_control = 'odd';
				
				// skier name string defined above
				
				// echo output
				for ($j = 0; $j < count($resultSelectAvailableLessonsOfSkier); $j++){
					
					require 'panels/panel.lesson_selection_display.php';
					
					//echo 'Length is: ' .$lesson_length_hours_display;
					
					// set duplicate string for this lesson
					// DateMMDD-Bool_Private-Bool_SB-StartTime
					$this_duplicate_string = date_format(date_create($resultSelectAvailableLessonsOfSkier[$j]['d_session_date']), "md")
										.(int)$resultSelectAvailableLessonsOfSkier[$j]['tm_starttime'];
					
					
					// add class tag for odd lesson divs
					if ($last_lesson_even_odd_control == 'odd'){
						$even_odd_display = 'odd';
					} else {
						$even_odd_display = 'even';
					}
					
					// set up day number, format uses leading zero for days before the tenth
					$lesson_day_number = date_format(date_create($resultSelectAvailableLessonsOfSkier[$j]['d_session_date']), "d");
				
					// set up lesson id
					$lesson_id = $resultSelectAvailableLessonsOfSkier[$j]['pk_session'];
					
					
					
					${'output' .$i} .= '<div class="lesson-wrapper ' .$even_odd_display .' skier-' .$i .'">';
				
						
					${'output' .$i} .= '<p class="lesson ' 
							   .$lesson_date_display .' ' 
							   .$i .' ' 
							   .$lesson_id .' ' 	
							   .'skier-' .$i 
							   .'">'
				
							// hidden input to hold the lesson ID
							.'<input type="hidden" class="lesson-id"
							value="' .$lesson_id .'"/>'
				
							// hidden input to hold the day number
							.'<input type="hidden" class="lesson-day-number"
							value="' .$lesson_day_number .'"/>'
				
							// hidden input to hold the skier id
							.'<input type="hidden" class="skier-id"
							value="' .$resultSelectSkiersOfBuyer[$i]['pk_contact'] .'"/>'

							// hidden input to hold the lesson price on that day (two possible prices)
							.'<input type="hidden" class="price"
							value="' .$price_to_apply .'"/>'		
								
							// hidden input to hold the skier index
							.'<input type="hidden" class="lesson-skier-index"
							value="' .$i .'"/>'
									
							// hidden input to hold the skier's name
							// skier name display available from above
							.'<input type="hidden" class="lesson-skier-name"
							value="' .$skierNameString .'"/>'		
									
							// hidden input to hold the length of lesson, in hours
							// private lessons are either 2 or 4 hours 
							.'<input type="hidden" class="lesson-hours"
							value="6"/>'		
				
				
				
							// lesson name, level display
							.'<span class="lesson-name">'
							//.$this_duplicate_string .' '
							//.$lesson_date_display .' '
			// debug
			//.$lesson_id .'-'
			//.'day =' .$lesson_day_number
							.$lesson_name_display
							.'</span>'
				
							// lesson price
							.'<span class="lesson-price">'
							.$lesson_price_display .'</span>'
				
				
							// lesson time
							.'<br />'
							//.'<span class="lesson-short-name">'
				// debug
				//.$lesson_length_display .'&nbsp;'
							//.'</span>'
							.'<span class="lesson-time">'
							.$lesson_time_display
							.'</span>'
				
							// capacity display		
							.'<span class="lesson-regd-skiers">'
							.$resultSelectAvailableLessonsOfSkier[$j]['n_session_regd_skiers']
							.'</span>'
							.'/' 
							.'<span class="lesson-capacity">'
							.$resultSelectAvailableLessonsOfSkier[$j]['n_session_capacity']
							.'</span>'
				
							// tooltip, full explanation
							.'<a class="lesson-details" href="#" title="'
							.$lesson_description
							.'"><i class="fa fa-question-circle fa-lg lesson-desc-popup"></i></a>';
					
							// skier names
							${'output' .$i} .= '<span class="skier-name-holder ' .$lesson_id .'">'	
										.$CurrentSkiersInLesson
											
												
										// finish skier name span		
										.'</span>';		 
							
							// (optional) instructor designation, skier names
							// not used for mikuni lessons
					
					// close lesson p
					${'output' .$i} .= '</p>';
				
					// close lesson wrapper
					${'output' .$i} .= '</div>';
				
				
				
					// update even or odd class control
					if ($even_odd_display == 'odd'){
						$last_lesson_even_odd_control = '';
					} else {
						$last_lesson_even_odd_control = 'odd';
					}
							
					
					
					
					
				} // end found set of lessons for skier-$i
				
				
				
					
			} else {
				
				// error handling
				// no lessons are found for any date for this skier
				echo '<p class="no-lessons-msg">' .sprintf($ErrorMsg_LessonSelection_NoLessonsFound, $skierNameString) .'</p>';
				
				// set output key to blank output for this skier
				$nBool_suppress_output = 1;
				
				//echo 'Suppress: ' .$nBool_suppress_output;
				
			} // end resource check $resultSelectAvailableLessonsOfSkier
			
			
			
		
			
			
			
			
			// clear display at end of first skier-$i's lessons
			${'output' .$i} .= '<br class="Clear">';
			
			// set up pagination display, proceed to previous or next skier button display
			
			// set variables
			$countOfSkiers = count($resultSelectSkiersOfBuyer);
			//echo $countOfSkiers .' skier count. <br>';
			//echo $i .' we are at this count now. <br>';
			
			// load generic error message, toggles to show on dates that do not have lessons available
			${'output' .$i} .= '<p class="no-lessons-of-date-msg ' .$i .'" style="display:none;">'
					.sprintf($ErrorMsg_LessonSelection_NoLessonsOfDateFound, $skierNameString)
					.'</p>';
			
			// start container
			${'output' .$i} .= '<div class="pagination-container">';
			
			if ($i > 0){
				// second or greater skier, print the previous button
				${'output' .$i} .= '<span class="btn3 pagination previous skier-' .$i .'">' 
						   // hidden input to hold the skier index
						  .'<input type="hidden" class="lesson-skier-index" value="' .$i .'"/>'
						  .$PreviousSkierLinkLabel .'</span>';
				
				
			}
			
			// print the next button
			// do not print a next button for the last skier
			// will not print for single skier
			if ($i < ($countOfSkiers - 1)){
				
				${'output' .$i} .=  '<span class="btn3 pagination next skier-' .$i .'">' 
						  // hidden input to hold the skier index
						  .'<input type="hidden" class="lesson-skier-index" value="' .$i .'"/>'
						  .$NextSkierLinkLabel .'</span>';
			} 
		
			
			
			// set up complete lesson registration button, displays on final set of lessons to be assigned
			if ($i == ($countOfSkiers - 1)){
				
				// this is the last skier, print out the complete button
				${'output' .$i} .= '<button name="LessonSelectionComplete" id="LessonSelectionComplete"'
						  .' class="btn3 skier-' .$i .'">'
		 				  .'<span>' .$CompleteLessonRegistration .'</span></a>'
		 				  .'</button>';
			
			}
			
			// end pagination container
			${'output' .$i} .= '</div>';
			
			
		
			// complete the div that holds all calendars, lessons, and pagination for this skier
			${'output' .$i} .= '</div>';
		
			// suppress output for this skier if no lessons are found
			if($nBool_suppress_output == 1){
				${'output' .$i} = '';
			}
			 
			
		} // end for loop through all skiers of buyer
		
		
		
		
	} else {
		// error handling
		
	} // end resource check $resultSelectSkiersOfBuyer
				

	
    
	# debug
	if ($debugging){
		# return error message from $SESSION if present
		if (isset($_SESSION['ErrorMsg'])){
			$error_msg .= $_SESSION['ErrorMsg'];
		}
    }
    
    // assemble together all output strings and print
    for ($i = 0; $i < count($resultSelectSkiersOfBuyer); $i++){
    	echo ${'output' .$i};
    	
    	
    }
    
    	
   
	
	

	
	