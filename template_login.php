<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	// initialize message(s) on this page, not the $_SESSION error holder
	$error_msg = '';
	if (isset($_SESSION['ErrorMsg']) && !empty($_SESSION['ErrorMsg'])){
		$error_msg = $_SESSION['ErrorMsg'];
	}
	$success_msg = '';
	if (isset($_SESSION['SuccessMsg']) && !empty($_SESSION['SuccessMsg'])){
		$success_msg = $_SESSION['SuccessMsg'];
	}
	
	
	
	
	
	
	// Set page presentation variables, local for this page load only
	$showLargeBanner = 0;
	$showSmallBanner = 1;
	$showAccountTools = 0;
	$showNavigation = 0;
	$showSubNavigation = 0;
	$showLanguageToggle = 0;
	$showSocialFlags = 0;
	$pageType = 'Registration';
	
	
	
	
	
	// HTML starts here--------------------------------------//
	
	// load header
	require_once 'header_login.php';	
	
?>

    <!-- Start Main -->
	<div id="main">
					
	<!-- Start Content -->
	<div id="content">
	
		<h1><?php echo $RegistrationPageHeader;?></h1>
       
        <?php
        if (!empty($error_msg)){
	    	echo '<p class="error-msg"><i class="fa fa-times"></i>' 
				.'<span>' .$error_msg .'</span>'
				.'</p>';
	    }
	    if (!empty($success_msg)){
	    	echo '<p class="success-msg"><i class="fa fa-check"></i>'
	    		.'<span class="success-msg">' .$success_msg .'</span>'
	    		.'</p>';
	    }
		
	    // form for buyer login or new registration
		require('panels/panel.buyer_login.php');
	    ?>
	    
		<p id="personal-info-notice"><?php echo $NewRegistrationPersonalInfoNotice; ?></p>
       
        
    </div>
	<!-- End Content -->
				
	</div>
	<!-- End Main -->          
	
	
<?php
	// Load footer, Javascript, and page closing code
	require_once 'footer_login.php';
	require_once 'js_page_closer.php';
?>