<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	require_once BUSINESS_DIR .'order.php';
    
	
	
	// initialize error message
	$error_msg = '';
	$success_msg = '';
	
	// return json data, create an array to hold this
	$returnLiabilityReleaseInsert = array();
	
	// store liability release data
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {	

		// set up variables
		$LiabilityReleaseText = $_POST['LiabilityReleaseText'];
		$BuyerFK = $_SESSION['BuyerId'];
		$OrderFK = $_SESSION['OrderId'];
		
		// insert new record with text, timestamp of liability release
		$resultInsertLiabilityRelease = Order::InsertLiabilityRelease(
				$BuyerFK,
				$OrderFK,
				$LiabilityReleaseText);
			
		// error handling
		if ($resultInsertLiabilityRelease) {
			
			// insert is okay
			$success_msg .= 'Liability release #' 
						 .$resultInsertLiabilityRelease['pk_liability'] 
						 .' inserted. ' ."<br />\n";
			
			$returnLiabilityReleaseInsert['Result'] = 1;
			//echo $success_msg;
			
			// update order status from "temp" to "completed registration"
			$resultUpdateOrderTempFlags = Order::UpdateTempOrderFlags($OrderFK);
			
			// this file insert is not working locally
			if (DB_DATABASE != 'snowschool_local'){
				
				// create PDF of liability release and store
				require_once 'liability/create_liability_release_PDF.php';
				
			}
	
		} else {
			// query returns 0 results. Throw error.
			$error_msg .= 'Error with liability release insert.';
			
			$returnLiabilityReleaseInsert['Result'] = 0;
		}
		
	} else {
		
		// no post available, error handling
	}
				
	# debug
	if ($debugging){
		# return error message from $SESSION if present
		if (isset($_SESSION['ErrorMsg'])){
			$error_msg .= $_SESSION['ErrorMsg'];
		}
    }
    


    // return json array
    echo json_encode($returnLiabilityReleaseInsert);
    

   
	