<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require LANGUAGE_DIR .'setLanguage.php';
	require LANGUAGE_DIR .'lang.registration.php';
	require LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	// initialize error message
	$error_msg = '';
	
	// user requests an email reset
	if (isset($_POST['PasswordResetSubmit'])) {
			
			// get user-entered data
			$Email = trim($_POST['UserEmail']);
			
			// client-side validation for data entry
			if (!empty($Email)) {
				
				// test user credentials
				$validEmailResult = Contact::ContactCheckEmail($Email);
				
				// process output
				if (count($validEmailResult)) {
					
					// test for unique user before proceeding
					if (count($validEmailResult) > 1){
						
						// query returns multiple results. Throw error.
						$error_msg = $ErrorMsg_Login_EmailNotUnique;
						
					} else if(count($validEmailResult) == 1){
						
						// email is unique, only one contact is selected
						$UserId = $validEmailResult[0]['pk_contact'];
						
						// update contact record with random password
						// then send this to user in an email
						
						// set temp password
						$tempPassword = Contact::generateRandstr();	
						
						// update password with temp password
						$updateTempPasswordResult = Contact::SetTempPassword($tempPassword, $UserId);
						
						// mail is not live in local environment
						if (MAIL_IS_LIVE){
							
							// send temp password email to user
							require_once 'mail/mail.forgot_password.php';
							
							// set session success message, telling user to check email
							$_SESSION['SuccessMsg'] = $SuccessMsg_PasswordReset;
						}
						
						// redirect to the standard login page
						header('Location: http://' 
								.str_replace('//', '/', $_SERVER['HTTP_HOST'] 
								.VIRTUAL_LOCATION 
								.'template_login.php'));
							
						
					} // end unique user test
					
				} // end non-null result for count($validEmailResult)
				
				  else {
				  	
					// query returns null results Throw error.
					$error_msg = $ErrorMsg_Login_NoUserFound;
				}
				
				// end check for non-empty user data--client-side validation
		} else {
			
			// this generic error is testing the client-side validation again
			$error_msg = $ErrorMsg_Login_FormError;	
		}
		
	} // end isset() for Submit
				
				
	
	// set page variables
	$page_title_en = "Forgot Password";
	$page_body_id = "ForgotPassword";
	
	// Set page presentation variables, local for this page load only
	$showLargeBanner = 0;
	$showSmallBanner = 1;
	$showAccountTools = 0;
	$showNavigation = 0;
	$showSubNavigation = 0;
	$showLanguageToggle = 0;
	$showSocialFlags = 0;
	$pageType = 'Registration';
	
	
	// HTML starts here--------------------------------------//
	
	
	
	// load header
	require_once 'header_login.php';	
	
	
?>

    <!-- Start Main -->
	<div id="main">
					
	<!-- Start Content -->
	<div id="content">
        
        <h1><?php echo $ForgotPwdNotice; ?></h1>
     
        <?php
		// show any error message
		if (!empty($error_msg)){
	    	echo '<p class="error-msg"><i class="fa fa-times"></i>' 
				.$error_msg .'</p>';
	    }
	    ?>
		
		
		<p class="instructions" ><?php echo $ForgotPwdEnterEmailLabel; ?></p>
		
		<form method="post" 
			  action="<?php echo $_SERVER['PHP_SELF']; ?>" 
			  name="PasswordResetForm" 
			  id="PasswordResetForm"> 
		
		<fieldset>
	      <!-- Email -->  
	      <div class="row">
		      <label for="UserEmail"><?php echo $Email_Label; ?>
			      <span class="StarRequired">*</span></label>
			  <input type="text" 
			      name="UserEmail" 
			      id="UserEmail" 
			      class="required" 
			      title="<?php echo $Email_ErrorAdvice; ?>"
			      value=""/>
		      <span class="example"><?php echo $Email_Advice; ?></span>
	      </div>
	      
	      <div class="actions">
				<button class="btn3 login-button " 
						type="submit"
						value="reset"
						name="PasswordResetSubmit" 
	    		   		id="PasswordResetSubmit" >
	    		<span><?php echo $ResetPassword; ?></span>
	    		</button>
	      </div>
	   </fieldset>    
	   </form>
	
	
	</div>
	<!-- End Content -->
				
	</div>
	<!-- End Main -->          
	
<?php
	// Load footer, Javascript, and page closing code
	require_once 'footer_login.php';
	require_once 'js_page_closer.php';
?>