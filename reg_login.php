<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	
	
	// initialize
	$_SESSION['ErrorMsg'] = '';
	$_SESSION['Bool_BuyerIsNewSkier'] = 0;
	
	if($debugging){
		echo '<pre>';
		echo 'SESSION' .'<br>';
		print_r($_SESSION);
		echo '</pre>';
	
		echo '<pre>';
		echo 'POST' .'<br>';
		print_r($_POST);
		echo '</pre>';
	
	}
	echo '<pre>';
	echo 'SESSION before' .'<br>';
	print_r($_SESSION);
	echo '</pre>';
	
	echo '<pre>';
	echo 'POST' .'<br>';
	print_r($_POST);
	echo '</pre>';
	
	
	
	// attempt access with user's credentials: for registered buyer
	if (isset($_POST['RegisteredBuyerSubmit'])) {
			
		// get the user-entered data
		$Email = trim($_POST['RegisteredBuyerEmail']);
		$Password = trim($_POST['RegisteredBuyerPassword']);
	
		if ((!empty($Email)) && (!empty($Password))) {
				
			// test user credentials
			$accessResult = Contact::ContactTestAccess($Email, $Password);
				
			// access is okay
			if (count($accessResult)) {
	
				// test for unique user before proceeding
				if (count($accessResult) > 1){
						
					// query returns multiple results. Throw error.
					$_SESSION['ErrorMsg'] = $ErrorMsg_Login_EmailNotUnique;
					
					// send the user back to the login page
					$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'template_login.php';
					$corrected_dir_string = str_replace('//','/',$dir_string);
					$registration_start = 'http://' .$corrected_dir_string;
					header('Location: ' .$registration_start);
						
				} else if(count($accessResult) == 1){
						
					// login is unique
					// user is a buyer, with a current account
					
					
					// unset all session variables
					$_SESSION = array();
						
					// set the user session vars
					$_SESSION['BuyerId'] = $accessResult[0]['pk_contact'];
					$_SESSION['BuyerEmail'] = $accessResult[0]['t_email'];
					$_SESSION['BuyerFirstNameJP'] = $accessResult[0]['t_firstname_jp'];
					$_SESSION['BuyerLastNameJP'] = $accessResult[0]['t_lastname_jp'];
					$_SESSION['UserLang'] = $accessResult[0]['t_user_language'];
					$_SESSION['Bool_BuyerIsNew'] = 0;
					$_SESSION['LessonStartDate'] = $_POST['RegisteredBuyerLessonStartDate'];
					$_SESSION['LessonFinishDate'] = $_POST['RegisteredBuyerLessonEndDate'];

					// registration page control
					// user is returning, has complete buyer and skier information
					// TODO: Edge case: incomplete data entry, set $_SESSION['RegPageControl'] = 0 or 1 instead
					$_SESSION['RegPageControl'] = 2;
						
					// set overseas session variable, used to manage prefecture/state entry
					if ($accessResult[0]['fk_contact_country'] != 109){
						$_SESSION['Bool_BuyerIsOverseas'] = 1;
					} else {
						$_SESSION['Bool_BuyerIsOverseas'] = 0;
					}
	
					
	
					// TODO: troubleshoot incomplete initial buyer input
	
					echo '<pre>';
					echo 'SESSION' .'<br>';
					print_r($_SESSION);
					echo '</pre>';
						
					// set cookie to expire in 30 days
					setcookie('ParentEmail', $_SESSION['BuyerEmail'], time() + (60 * 60 * 24 * 30));
						
					// send the user to the registration page
					$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'registration.php';
					$corrected_dir_string = str_replace('//','/',$dir_string);
					$registration_start = 'http://' .$corrected_dir_string;
					header('Location: ' .$registration_start);
	
				} // end unique user test
					
					
			} else {
				// query returns 0 results. Throw error.
				$_SESSION['ErrorMsg'] = $ErrorMsg_Login_NoUserFound;
				
				// send the user back to the login page
				$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'template_login.php';
				$corrected_dir_string = str_replace('//','/',$dir_string);
				$registration_start = 'http://' .$corrected_dir_string;
				header('Location: ' .$registration_start);
			}
		} else {
			// this generic error is testing the client-side validation again
			$_SESSION['ErrorMsg'] = $ErrorMsg_Login_FormError;
			
			// send the user back to the login page
			$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'template_login.php';
			$corrected_dir_string = str_replace('//','/',$dir_string);
			$registration_start = 'http://' .$corrected_dir_string;
			header('Location: ' .$registration_start);
		}
	} // end isset() Submit for registered parent access
	
	
	
	
	// new user
	if (isset($_POST['NewBuyerSubmit'])) {
	
	
		// client-side second verification (after Jquery validate)
		// process new buyer insert
		if (
		(!empty($_POST['NewBuyerEmail'])) && (!empty($_POST['NewBuyerPassword1']))
		&&
		(($_POST['NewBuyerPassword1']) == ($_POST['NewBuyerPassword2']))
		) {
				
			// get the user-entered data
			$Email = trim($_POST['NewBuyerEmail']);
			$Password = trim($_POST['NewBuyerPassword1']);
			$UserLanguage = $_SESSION['UserLang'];
				
			// check for unique email before proceeding
			// TODO: check with AJAX in form (upon email entry) before form is processed
			$checkEmailResult = Contact::ContactCheckEmail($Email);
	
			// result returned (any number of rows returned)
			if ($checkEmailResult) {
	
				// email already exists; update error message
				$_SESSION['ErrorMsg'] = $ErrorMsg_Login_EmailInUse;
				
				// send the user back to the login page
				$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'template_login.php';
				$corrected_dir_string = str_replace('//','/',$dir_string);
				$registration_start = 'http://' .$corrected_dir_string;
				header('Location: ' .$registration_start);

			
					
				
			} else {
				// query returns 0 results. Create the new buyer record
				$insertBuyerResult = Contact::InsertBuyer($Email, $Password, $UserLanguage);
	
				
				echo '<pre>';
				echo 'Insert result:' .'<br>';
				print_r($insertBuyerResult);
				echo '</pre>';
				
				
				
				// unset all session variables
				$_SESSION = array();
				
				// reset session var from UserLanguage
				$_SESSION['UserLang'] = $UserLanguage;
				
				// set session values
				$_SESSION['BuyerId'] = $insertBuyerResult['pk_contact'];
				$_SESSION['BuyerEmail'] = $insertBuyerResult['t_email'];
				$_SESSION['LessonStartDate'] = $_POST['NewBuyerLessonStartDate'];
				$_SESSION['LessonFinishDate'] = $_POST['NewBuyerLessonEndDate'];
				$_SESSION['Bool_BuyerIsNew'] = 1;
	
				// default, can be updated later by user input
				$_SESSION['Bool_BuyerIsOverseas'] = 0;
	
				// initiate registration page control
				$_SESSION['RegPageControl'] = 0;
	
				// set cookie to expire in 30 days
				setcookie('BuyerEmail', $_SESSION['BuyerEmail'], time() + (60 * 60 * 24 * 30));
				
				echo '<pre>';
				echo 'SESSION after new buyer submit' .'<br>';
				print_r($_SESSION);
				echo '</pre>';
	
				// send the user to the registration page
				$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'registration.php';
				$corrected_dir_string = str_replace('//','/',$dir_string);
				$login_url = 'http://' .$corrected_dir_string;
				header('Location: ' .$login_url); 
	
			} // end email check and buyer record insert
				
		} else {
			// this generic error is testing the client-side validation again
			$_SESSION['ErrorMsg'] = $ErrorMsg_Login_FormError;
			
			// send the user back to the login page
			$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'template_login.php';
			$corrected_dir_string = str_replace('//','/',$dir_string);
			$registration_start = 'http://' .$corrected_dir_string;
			header('Location: ' .$registration_start);
	
		} // end validation for inputs
	
	} // end isset() new parent submit
	
	
	
?>