<?php
	// user authentication
	// If the session email isn't set, try to set it with a cookie
	if (!isset($_SESSION['ParentEmail'])) {
		if (isset($_COOKIE['ParentEmail'])) {
		  $_SESSION['ParentEmail'] = $_COOKIE['ParentEmail'];
		}
	}
	
	// Make sure the user is logged in before going any further.
	if (!isset($_SESSION['ParentEmail'])) {
		// show unauthorized message and redirect to admin_login
		echo '<head><meta http-equiv="refresh" content="2;URL='
		.'template_login.php?Action=Login&UserLang='
        .$_SESSION['UserLang']
        .'"></head>';
		echo '<p class="login">User authentication required to access this page.'
		.' You will be redirected to the main login page.</p>';
		exit();
	}
?>