<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	require_once BUSINESS_DIR .'order.php';
	
	// Include Stripe library
	require_once 'lib/stripe-php/init.php';
	
	// initialize
	$success_msg = '';
	$error_msg = '';
	$cc_error_msg = '';
	$_SESSION['CC_ErrorMsg'] = '';
	
	$_SESSION['ReturnCouponCode'] = '';
	$_SESSION['ReturnCouponAmount'] = '';
	
	$_SESSION['ReturnCC_Name'] = '';
	$_SESSION['ReturnCC_Number'] = '';
	$_SESSION['ReturnCC_ExpMonth'] = '';
	$_SESSION['ReturnCC_ExpYear'] = '';
	$_SESSION['ReturnCC_CVC'] = '';
	
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';

	
	
	
	// Process CC charge, after transaction token created by Stripe call
	// form submits because token is valid
	if (isset($_POST['stripeToken'])){
	
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account/apikeys
		if (STRIPE_STATUS == 'live'){
			
			\Stripe\Stripe::setApiKey(STRIPE_LIVE_SECRET_KEY);
			
		} else if (STRIPE_STATUS == 'test'){
			
			// default is test
			\Stripe\Stripe::setApiKey(STRIPE_TEST_SECRET_KEY);
		}
		
	
		// Get the credit card details submitted by the form
		$token = $_POST['stripeToken'];
		
		// set variables
		$amountToCharge = $_POST['chargeAmount'];
		$orderId = $_POST['chargeOrderId'];
	
		// Create the charge on Stripe's servers - this will charge the user's card
		try {
	
			$charge = \Stripe\Charge::create(array(
					"amount" => $amountToCharge, // amount in JPY
					"currency" => "jpy",
					"source" => $token,
					"description" => "",
					"metadata" => array("order_id" => $orderId)
			));
			
			// charge is successful
			
			// update registration page control
			$_SESSION['RegPageControl'] = 4;
			
			// insert coupon code, liability release
			
			// get coupon data, optional
			if ( (isset($_POST['chargeCouponCode'])) && (!empty($_POST['chargeCouponCode']))){
				
				// set variable
				$CouponCode = $_POST['chargeCouponCode'];
				
				$resultGetCouponCode = Order::OrderCheckCouponCode($CouponCode);
				
				// insert coupon line item into customer order
				$CouponCodeFK = $resultGetCouponCode['pk_coupon_code'];
				$DiscountJPY = $resultGetCouponCode['n_coupon_code_discount'];
				
				$resultInsertCouponCodeAsLI = Order::InsertCouponCodeAsOrderLineItem(
						$orderId,
						$CouponCodeFK,
						$DiscountJPY);
			}
			
			
			// insert liability release
			$LiabilityReleaseText = $_POST['buyerLiabilityText'];
			$BuyerFK = $_SESSION['BuyerId'];
			$OrderFK = $_SESSION['OrderId'];
			
			if ($_SESSION['UserLang'] == 'JP'){
				// encode to SJIS
				$LiabilityReleaseText = mb_convert_encoding($LiabilityReleaseText, "SJIS", "UTF-8");
			}
			
			// insert new record with text, timestamp of liability release
			$insertLiabilityRelease = Order::InsertLiabilityRelease(
					$BuyerFK,
					$OrderFK,
					$LiabilityReleaseText);
				
			// error handling
			if ($insertLiabilityRelease) {
					
				// insert is okay
				$success_msg .= 'Liability release #'
						.$insertLiabilityRelease['pk_liability']
						.' inserted. ' ."<br />\n";
				
				// update order status from "temp" to "completed registration"
				$resultUpdateOrderTempFlags = Order::UpdateTempOrderFlags($OrderFK);
					
				// this file insert is not working locally
				if (DB_DATABASE != 'snowschool_local'){
			
					// create PDF of liability release and store
					require_once 'liability/create_liability_release_PDF.php';
			
				}
			
			} else {
				// query returns 0 results. Throw error.
				$error_msg .= 'Error with liability release insert.';
				
			}
			
			
			// mark order as paid
			$resultMarkOrderAsPaid = Order::MarkOrderAsPaid($orderId);	
			
			// send user to confirmation page
			$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'order_confirmation.php';
			$corrected_dir_string = str_replace('//','/',$dir_string);
			$confirmation_url = 'http://' .$corrected_dir_string;
			header('Location: ' .$confirmation_url);
			
			
	
		} catch(\Stripe\Error\Card $e) {
	
			// The card has been declined
			// Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			//print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			
			// Load language string for the error, set to a $_SESSION variable
			
			switch ($err['code']){
				case 'incorrect_number':
					$cc_error_msg = $ErrorMsg_CC_IncorrectNumber;
					break;
				case 'invalid_number':
					$cc_error_msg = $ErrorMsg_CC_InvalidNumber;
					break;
				case 'invalid_cvc':
					$cc_error_msg = $ErrorMsg_CC_InvalidCVC;
					break;
				case 'expired_card':
					$cc_error_msg = $ErrorMsg_CC_ExpiredCard;
					break;
				case 'incorrect_cvc':
					$cc_error_msg = $ErrorMsg_CC_IncorrectCVC;
					break;
				case 'card_declined':
			        $cc_error_msg = $ErrorMsg_CC_CardDeclined;
			        break;
			    case 'processing_error':
			        $cc_error_msg = $ErrorMsg_CC_ProcessingError;
			        break;
		        case 'missing':
		        	$cc_error_msg = '001 No card data available.';
		        	break;
		        case 'rate_limit':
		        	$cc_error_msg = '002 An error occurred due to requests hitting the API too quickly.';
		        	break;
			}
			$_SESSION['CC_ErrorMsg'] = $cc_error_msg;
			echo $_SESSION['CC_ErrorMsg'];
			
			// set PageControl to 4 (liability release is complete)
			$_SESSION['RegPageControl'] = 4;
			
			// use the coupon code in the $_POST array to reset the coupon code line item in the tentative order
			if (isset($_POST['chargeCouponCode']) && !empty($_POST['chargeCouponCode'])){
				
				// set session vars
				$_SESSION['ReturnCouponCode'] = $_POST['chargeCouponCode'];
				$_SESSION['ReturnCouponAmount'] = $_POST['chargeCouponAmount'];
				
			
			}
			
			// reset user credit card values
			$_SESSION['ReturnCC_Name'] = $_POST['cardName'];
			$_SESSION['ReturnCC_Number'] = $_POST['cardNumber'];
			$_SESSION['ReturnCC_ExpMonth'] = $_POST['expMonth'];
			$_SESSION['ReturnCC_ExpYear'] = $_POST['expYear'];
			$_SESSION['ReturnCC_CVC'] = $_POST['cvCode'];
			
			
			echo '<pre>';
			print_r($_SESSION);
			echo '</pre>';
			
			// return to registration page and present error to user
			$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'registration.php';
			$corrected_dir_string = str_replace('//','/',$dir_string);
			$login_url = 'http://' .$corrected_dir_string;
			header('Location: ' .$login_url); 
			
			
			
			
		} catch (\Stripe\Error\RateLimit $e) {
			
			// Too many requests made to the API too quickly
			$_SESSION['CC_ErrorMsg'] = '1401';
			
		} catch (\Stripe\Error\InvalidRequest $e) {
			
			// Invalid parameters were supplied to Stripe's API
			$_SESSION['CC_ErrorMsg'] = '1402';
			
		} catch (\Stripe\Error\Authentication $e) {
			
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$_SESSION['CC_ErrorMsg'] = '1403';
			
		} catch (\Stripe\Error\ApiConnection $e) {
			
			// Network communication with Stripe failed
			$_SESSION['CC_ErrorMsg'] = '1404';
			
		} catch (\Stripe\Error\Base $e) {
			
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$_SESSION['CC_ErrorMsg'] = '1405';
			
		} catch (Exception $e) {
			
			// Something else happened, completely unrelated to Stripe
			$_SESSION['CC_ErrorMsg'] = '1406';
		}
			
		
		
	
	} // end check for Stripe token
	
?>