<?php
	use DrewM\MailChimp\MailChimp;

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	// Load Mailchimp API wrapper for API v3.0
	require_once 'lib/Mailchimp/MailChimp.php';
	
	
	// Select records modified within the last 60 minutes
	// 60 minute interval is set in the MySQL procedure
	// server cron job is set to run every 60 minutes
	$selectContactsResult = Contact::GetMailchimpSyncNewsletter();
	
	 /* echo '<pre>';
	 print_r($selectContactsResult);
	 echo '</pre>'; */
	
	 echo 'api key= ' .MAILCHIMP_APIKEY;
	 echo '<br>';
	 echo 'list key= ' .MAILCHIMP_LIST_ID_NEWSLETTER;
	 echo '<br>'; 
	
	// query returns one or more records
	if (count($selectContactsResult)){
		
		echo '<br><br>';
		echo '==================================== Begin updated or new subscriber output =======================';
		echo '<br><br>';
		
		// create new API object
		$api = new MailChimp(MAILCHIMP_APIKEY);
		
		// step through selected records and sync them to selected Mailchimp list
		for ($i = 0; $i < count($selectContactsResult); $i++){
			
			// prepare variables to merge
			$merge_vars = array(
					'FIRST_NAME' => mb_convert_encoding($selectContactsResult[$i]['f_nm'], "UTF-8", "SJIS"), 
					'LAST_NAME'=> mb_convert_encoding($selectContactsResult[$i]['l_nm'], "UTF-8", "SJIS"), 
					'USER_ID'=> $selectContactsResult[$i]['academymember'],
					'USERLANG'=> $selectContactsResult[$i]['t_user_language']
			);
			
			// selected list is Newsletter
			$list_id = MAILCHIMP_LIST_ID_NEWSLETTER;
				
			// subscriber id
			$subscriber_id = md5(strtolower($selectContactsResult[$i]['email']));
			
			// attempt to add the customer to the target list
			$result = $api->post("lists/$list_id/members", [
					'email_address' => $selectContactsResult[$i]['email'],
					'status'        => 'subscribed',
					'merge_fields'	=> $merge_vars
			]);
				
				
			// TODO: Edge case, user unsubscribes inside of EA database (eg., during registration flow). This should then unsubscribe them from MC.
			// But unlikely. They would just unsubscribe from MC.
				
			// successfully added to list
			if ($api->success()) {
					
				echo $i .': ' .mb_convert_encoding($selectContactsResult[$i]['f_nm'], "UTF-8", "SJIS") .' '
								.mb_convert_encoding($selectContactsResult[$i]['l_nm'], "UTF-8", "SJIS") .' ('
								.$selectContactsResult[$i]['email']
								.'): new user insert complete.';
								echo "<br>";
									
			}
				
			// there was a problem with the initial post() call
			if ($result['status'] == 400){
			
				if ($result['title'] == 'Member Exists'){
						
					// member exists = 400, so perform an update = patch
					$result = $api->patch("lists/$list_id/members/$subscriber_id", [
							'status'        => 'subscribed',
							'merge_fields'	=> $merge_vars
					]);
						
					echo 'Currently subscribed user: ' ;
						
					if ($api->success()) {
							
						echo $i .': ' .mb_convert_encoding($selectContactsResult[$i]['f_nm'], "UTF-8", "SJIS") .' '
										.mb_convert_encoding($selectContactsResult[$i]['l_nm'], "UTF-8", "SJIS") .' ('
										.$selectContactsResult[$i]['email']
										.'): update complete.';
										echo "<br>";
			
					} else {
			
						echo "<br>";
						echo 'Error: ' .$i .': ' .mb_convert_encoding($selectContactsResult[$i]['f_nm'], "UTF-8", "SJIS") .' '
										.mb_convert_encoding($selectContactsResult[$i]['l_nm'], "UTF-8", "SJIS") .' ('
										.$selectContactsResult[$i]['email']
										.'): error triggered.';
										echo "  Error Msg = " .$api->getLastError() ."<br>";
										echo "<br>";
			
					}
						
				}
			
			} // end 400 error handling for initial post() call
				
				
			// error from initial post()
			if ($api->getLastError()){
			
				echo $i .': ' .mb_convert_encoding($selectContactsResult[$i]['f_nm'], "UTF-8", "SJIS") .' '
								.mb_convert_encoding($selectContactsResult[$i]['l_nm'], "UTF-8", "SJIS") .' ('
								.$selectContactsResult[$i]['email'] .'):'
										.' error occurred. Please follow up with admin.';
										echo "<br>";
										echo "  Error Msg = " .$api->getLastError() ."<br>";
										echo "<br>";
											
			}
				
		} // end for loop, going through all selected records
		
	} else {
		
		// query returns 0 results. Throw error.
		echo '<br><br>';
		echo "No contact records to update at this time.";
		echo '<br><br>';
	}
	
?>