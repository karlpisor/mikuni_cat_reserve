<?php
	// default language strings are JP, below in else statement
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
		
		// login
		$ErrorMsg_Login_EmailNotUnique = 'Database error: email not unique. Please contact system administrator.';
		$ErrorMsg_Login_EmailInUse = 'That email address is currently in use. If you\'ve already registered, please log in using the registered user form, or create an account with a new email address.';
		$ErrorMsg_Login_NoUserFound = 'No user found for those credentials. You must enter a valid email and password to log in.';
		$ErrorMsg_Login_FormError = 'Form invalid. Please contact system administrator.';
		$ErrorMsg_Login_AuthenticationError = 'Error with user authentication. Please enter your email and password one more time.';
		
		// datepicker
		$ErrorMsg_Datepicker_EndDate_BeforeStartDate = 'The lesson finish date cannot be before the start date.';
		$ErrorMsg_Datepicker_10DaysMaxLength = 'Up to 10 days may be booked at one time.';
		$ErrorMsg_Datepicker_TooEarlyStart = 'Lesson start date set before the beginning of the season. Please select another start date.';
		$ErrorMsg_Datepicker_TooLateFinish = 'Lesson finish date set after the end of the season. Please select another finish date.';
		$ErrorMsg_Datepicker_StartDate = 'Lesson start date must be today or later.';
		
		// buyer, skiers
		//$SuccessMsg_Skier_Inserted = 'Student %s %s record entered.';
		//$SuccessMsg_Skier_Updated = 'Student %s %s record updated.';
		$SuccessMsg_Buyer_Updated = 'Customer information updated.';
		$SuccessMsg_Skier_Inserted = 'New student record registered.';
		$SuccessMsg_Skier_Updated = 'Student information updated.';
		$SuccessMsg_Skier_LevelWizardUpdated = 'Level updated.';
		
		// lesson selection
		$ErrorMsg_LessonSelection_NoLessonsFound = 'There are no tour dates that match your selection for %s. Please re-confirm your selected tour dates and try again. If you require assistance, please contact Mikuni Cat Skiing at <a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a> or call our phone reservation desk at (+81) (0)70-5550-8807.';
		$ErrorMsg_LessonSelection_NoLessonsOfDateFound = 'There are no tour dates that match your selection for %s. Please re-confirm your selected tour dates and try again. If you require assistance, please contact Mikuni Cat Skiing at <a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a> or call our phone reservation desk at (+81) (0)70-5550-8807.';
		$ErrorMsg_LessonSelection_NoLessonsSelected = 'No tour selected. Please select any tour date to continue.';
		$ErrorMsg_LessonSelection_LessonIsFull = 'Tour capacity is full. Please select another tour date.';
		$ErrorMsg_LessonSelection_SelectedLessonHasBeenBooked = 'A tour date for skier %s on %s has been booked by another guest and is no longer available. Please make another selection.';
		$Msg_LessonSelection_AdditionalResources = 'If you can\'t find a tour date that\'s right for you, let us help! Contact Mikuni Cat Skiing at <a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a> or call our phone reservation desk at (+81) (0)70-5550-8807.';
		
		// coupon codes
		$SuccessMsg_CouponCode_Valid = 'Valid coupon code. Your coupon discount will be applied to your order total.';
		$ErrorMsg_CouponCode_Invalid = 'Error with coupon code. Please check your coupon code and re-enter.';
		$ErrorMsg_CouponCode_NoCodeEntered = 'No coupon code present. Please enter a coupon code.';
		
		// credit cards
		$ErrorMsg_CC_IncorrectNumber = 'The card number is incorrect.';
		$ErrorMsg_CC_InvalidNumber = 'The card number is not a valid credit card number.';
		$ErrorMsg_CC_InvalidCVC = 'The card\'s security code is invalid.';
		$ErrorMsg_CC_ExpiredCard = 'The card has expired.';
		$ErrorMsg_CC_IncorrectCVC = 'The card\'s security code is incorrect.';
		$ErrorMsg_CC_CardDeclined = 'The card was declined.';
		$ErrorMsg_CC_ProcessingError = 'An error occurred while processing the card. Please try again.';
		$ErrorMsg_CC_InvalidExpiryMonth = 'Your card\'s expiration month is invalid. Please confirm and try again.';
		$ErrorMsg_CC_InvalidExpiryYear = 'Your card\'s expiration year is invalid. Please confirm and try again.';
		
		// payment method
		$ErrorMsg_PaymentMethod_BankTransferUnavailable = 'Please note that bank transfers are only accepted up to 7 days before your lesson start date.';

		// password reset
		$SuccessMsg_PasswordReset = 'Your password has been successfully reset. Please check your email for your new password.';
		
		// page timeout warning
		$WarningMsg_PageTimeout = 'Please complete your registration, or your session will be automatically logged out in 5 minutes.';
		
	} else if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'CN')){	
		
		// login
		$ErrorMsg_Login_EmailNotUnique = 'CN_Database error: email not unique. Please contact system administrator.';
		$ErrorMsg_Login_EmailInUse = '此email已被使用，如您已經註冊過，請登入舊帳號，反之，請使用其他email信箱註冊';
		$ErrorMsg_Login_NoUserFound = '您的email或密碼輸入錯誤，請重新輸入';
		$ErrorMsg_Login_FormError = 'CN_Form invalid. Please contact system administrator.';
		$ErrorMsg_Login_AuthenticationError = 'CN_Error with user authentication. Please enter your email and password one more time.';
		
		// datepicker
		$ErrorMsg_Datepicker_EndDate_BeforeStartDate = '您無法設定課程結束日早於課程開始日．';
		$ErrorMsg_Datepicker_10DaysMaxLength = '您最多可預約十天的課程';
		$ErrorMsg_Datepicker_TooEarlyStart = '課程開始日期不能設定早於雪季課程開始前，請重新確認。';
		$ErrorMsg_Datepicker_TooLateFinish = '課程結束日期無法設定晚於雪季課程結束日，請重新確認．';
		$ErrorMsg_Datepicker_StartDate = '課程開始日期請從明天開始選擇。';
		
		// buyer, skiers
		//$SuccessMsg_Skier_Inserted = '學員%s %s 資訊輸入';
		//$SuccessMsg_Skier_Updated = '學員%s %s 資訊更新';
		$SuccessMsg_Buyer_Updated = '預約代表人資訊更新';
		$SuccessMsg_Skier_Inserted = '註冊學員資訊';
		$SuccessMsg_Skier_Updated = '學員資訊更新';
		$SuccessMsg_Skier_LevelWizardUpdated = '等級更新';
		
		// lesson selection
		$ErrorMsg_LessonSelection_NoLessonsFound = '很抱歉對於%s目前沒有合適的課程，請重新選擇日期、時段、Ski/Snowboard或是授課語言。如您需要任何協助，歡迎聯繫我們<a href="mailto:chinese@naebasnow.jp">chinese@naebasnow.jp</a> 或是直接撥打我們的預約專線 +81-70-5550-8807';
		$ErrorMsg_LessonSelection_NoLessonsOfDateFound = '中文_很抱歉對於%s目前沒有合適的課程，請重新選擇日期、時段、Ski/Snowboard或是授課語言。如您需要任何協助，歡迎聯繫我們<a href="mailto:chinese@naebasnow.jp">chinese@naebasnow.jp</a> 或是直接撥打我們的預約專線 +81-70-5550-8807';
		$ErrorMsg_LessonSelection_NoLessonsSelected = '沒有課程選項，請選擇任何課程以繼續下一步';
		$ErrorMsg_LessonSelection_LessonIsFull = '課程名額已滿，請選擇其他課程。';
		$ErrorMsg_LessonSelection_SelectedLessonHasBeenBooked = '很抱歉，您為%s所選擇的%s時段課程已被其他客人預約，因次您的預訂未成功，請重新選擇，謝謝。';
		$Msg_LessonSelection_AdditionalResources = 'CN_ご希望のプログラムやご希望の時間帯が表示されない場合は、メール<a href="mailto:chinese@naebasnow.jp">chinese@naebasnow.jp</a> またはお電話 (+81) (0)25-789-4969 までお問い合わせください。';
		
		// coupon codes
		$SuccessMsg_CouponCode_Valid = '折價卷有效。您的折扣將在總金額中折抵。';
		$ErrorMsg_CouponCode_Invalid = '折價卷代碼錯誤，請確認您的折價卷並重新輸入';
		$ErrorMsg_CouponCode_NoCodeEntered = '尚未輸入代碼，請重新輸入。';
		
		// credit cards
		$ErrorMsg_CC_IncorrectNumber = '信用卡號碼不正確';
		$ErrorMsg_CC_InvalidNumber = '此號碼非有效的信用卡號碼';
		$ErrorMsg_CC_InvalidCVC = '安全碼無效';
		$ErrorMsg_CC_ExpiredCard = '此信用卡已過期';
		$ErrorMsg_CC_IncorrectCVC = '此安全碼不正確';
		$ErrorMsg_CC_CardDeclined = '此信用卡無效';
		$ErrorMsg_CC_ProcessingError = '處理過程發生錯誤，請再試一次';
		$ErrorMsg_CC_InvalidExpiryMonth = '信用卡有效期限錯誤，請重新輸入.';
		$ErrorMsg_CC_InvalidExpiryYear = '信用卡有效期限錯誤，請重新輸入.';
		
		// payment method
		$ErrorMsg_PaymentMethod_BankTransferUnavailable = '課程開始前七日內無法使用銀行匯款，請見諒．';
		
		// password reset
		$SuccessMsg_PasswordReset = '中文_パスワードを更新しました。新しいパスワードをメールで送りましたので、ご確認ください。';
		
		// page timeout warning
		$WarningMsg_PageTimeout = 'CN_Please complete your registration, or your session will be automatically logged out in 5 minutes.';
		
		
	} else {
		
		// JP language strings, default
		// login
		$ErrorMsg_Login_EmailNotUnique = 'JP_Database error: email not unique. Please contact system administrator.';
		$ErrorMsg_Login_EmailInUse = 'このメールアドレスはすでに登録されています。ご登録済みの場合、下のフォームを使ってログインしてください。あるいは、別のメールアドレスを使ってください。';
		$ErrorMsg_Login_NoUserFound = 'メールアドレスまたはパスワードは正しくありません。もう一度入力してください。';
		$ErrorMsg_Login_FormError = 'JP_Form invalid. Please contact system administrator.';
		$ErrorMsg_Login_AuthenticationError = 'ユーザー認識エッラーが発生しました。メールアドレスとパスワードをもう一度入力してください。';
		
		// datepicker
		$ErrorMsg_Datepicker_EndDate_BeforeStartDate = 'レッスン終了日はレッスン開始日より早い日にちに設定できません。';
		$ErrorMsg_Datepicker_10DaysMaxLength = '最大10日間をご予約できます。';
		$ErrorMsg_Datepicker_TooEarlyStart = 'レッスン開始日はシーズン開始日前には設定できません。ご確認ください。';
		$ErrorMsg_Datepicker_TooLateFinish = 'レッスン終了日はシーズン終了日後には設定できません。ご確認ください。';
		$ErrorMsg_Datepicker_StartDate = 'レッスン開始日は本日以降の日にちに設定してください。';
		
		// buyer, skiers
		//$SuccessMsg_Skier_Inserted = '参加者 %s %s を登録しました。';
		//$SuccessMsg_Skier_Updated = '参加者 %s %s の記録をアップデートしました.';
		$SuccessMsg_Buyer_Updated = '申込者の情報を更新しました。';
		$SuccessMsg_Skier_Inserted = '参加者の情報を登録しました。';
		$SuccessMsg_Skier_Updated = '参加者の情報を更新しました。';
		$SuccessMsg_Skier_LevelWizardUpdated = 'レベルを設定しました。';
		
		// lesson selection
		$ErrorMsg_LessonSelection_NoLessonsFound = '参加者 %s 様 にマッチしているレッスンが見つかりません。検索条件を再確認してください。お困りの方は、メール<a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a> またはお電話 (+81) (0)25-789-4969 にてご連絡ください。';
		$ErrorMsg_LessonSelection_NoLessonsOfDateFound = 'この日付の参加者 %s 様 にマッチしているレッスンが見つかりません。別の日付を選択していただくか、もうしくは検索条件を再確認してください。お困りの方は、メール<a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a> またはお電話 (+81) (0)25-789-4969 にてご連絡ください。';
		$ErrorMsg_LessonSelection_NoLessonsSelected = '参加者のレッスンは選択されていません。';
		$ErrorMsg_LessonSelection_LessonIsFull = 'このレッスンは定員になっております。別のレッスンを選択してください。';
		$ErrorMsg_LessonSelection_SelectedLessonHasBeenBooked = '大変申し訳ありませんが、予約がすでに入っているため、参加者 %s 様 の %s のレッスンが登録できません。もう一度選択してください。';
		$Msg_LessonSelection_AdditionalResources = 'ご希望のプログラムやご希望の時間帯が表示されない場合は、メール<a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a> またはお電話 (+81) (0)25-789-4969 までお問い合わせください。';
		
		// coupon codes
		$SuccessMsg_CouponCode_Valid = 'クーポンコードは有効です。ご注文の合計に反映されます。';
		$ErrorMsg_CouponCode_Invalid = 'クーポンコードのエラー。ご確認の上、もう一度コードを入力して下さい。';
		$ErrorMsg_CouponCode_NoCodeEntered = 'コードの入力はありません。コードを入れてください。';
		
		// credit cards
		$ErrorMsg_CC_IncorrectNumber = 'カード番号が正しくありません。ご確認ください。';
		$ErrorMsg_CC_InvalidNumber = 'カード番号は有効ではありません。ご確認ください。';
		$ErrorMsg_CC_InvalidCVC = 'カードのセキュリティコードは有効ではありません。ご確認ください。';
		$ErrorMsg_CC_ExpiredCard = 'このカードの有効期限が過ぎています。別のカードをお使いください。';
		$ErrorMsg_CC_IncorrectCVC = 'カードのセキュリティコードが正しくありません。ご確認ください。';
		$ErrorMsg_CC_CardDeclined = 'カード会社によりこのカードは使えません。';
		$ErrorMsg_CC_ProcessingError = '処理エラーが発生しました。もう一度カードをお試しください。';
		$ErrorMsg_CC_InvalidExpiryMonth = 'カードの有効期限が正しくありません。ご確認ください。';
		$ErrorMsg_CC_InvalidExpiryYear = 'カードの有効期限が正しくありません。ご確認ください。';
		
		// payment method
		$ErrorMsg_PaymentMethod_BankTransferUnavailable = '銀行振込はレッスン開始日の７日間前以降は、ご利用できません。ご了承ください。';
		
		// password reset
		$SuccessMsg_PasswordReset = 'パスワードを更新しました。新しいパスワードをメールで送りましたので、ご確認ください。';
		
		// page timeout warning
		$WarningMsg_PageTimeout = 'あと5分以内に予約を完了してください。完了しない場合、ログアウトしトップ画面に戻ります。';
		
		
	}
?>