<?php
	// default language strings are JP, below in else statement
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){

		// general
		$Mail_Salutation = 'Dear';
		
		// order confirmation
		$Mail_ConfirmationSubject = 'Thank You for Registering';
		$Mail_ThanksForRegistering = 'Thank you for registering for a Naeba Snow School lesson!';
		$Mail_HowToPayAdvice = 'This email is to confirm your registration, and to request payment. You may pay by bank transfer.';
		$Mail_HowToPayDetails = 'Bank: Daishi Bank
Branch: Yuzawa
Account type: Ordinary
Account number: 1282261
For fastest processing, please include your lesson start date and your last name in your transfer information. Ex.: 0125 Smith
Please note: Bank transfer transactions are only accepted from Japanese banks in Japan.';
		
		$Mail_NoPaymentNoPlaceNotice = 'Note: reservations are confirmed in the order we receive and confirm payment in full for registered students. To ensure your place, please make payment as soon as possible. Also, please understand that Japanese banks do not automatically notify Naeba Snow School of your payment. Typically, we will confirm your payment and contact you by email within 3 business days.';
		$Mail_PrivacyPolicyAgreementNotice = 'Your payment of fees indicates your agreement with the terms of KK Sherpa\'s privacy policy. View full policy here (Japanese only): http://www.naebasnow.jp/index.php?UserLang=JP&PageId=67';
				
		$Mail_AllPricesIncludeTax = 'All prices include applicable consumption tax.';
		$Mail_YenSign = 'JPY ';
		$Mail_OrderDetailsLabel = 'Order Details';
		$Mail_OrderSubtotalLabel = 'Subtotal: ';
		$Mail_OrderTotalLabel = 'Order Total: ';
		$Mail_CouponDiscountLabel = 'Coupon code discount: ';
		$Mail_CouponCodeLabel = 'Coupon code: ';
		$Mail_PaidOnDateTotalLabel = 'Paid by credit card ';
		$Mail_BalanceZeroLabel = 'Balance: ';
		
		$Mail_RegardingCancellationsHeader = 'Regarding Cancellations:';
		$Mail_RegardingCancellationsText = 'Cancellation charges are calculated according to number of days prior to your registered lessons:<br>
Cancellation 1 day prior to lesson start date, or on date of lesson: 100% charge.
Cancellation 2 days prior to lesson start date: 50% charge.
Cancellation 3-4 days prior to lesson start date: 30% charge.';
		$Mail_ConfirmFooterSalutation = 'Thank you once again for your registration. All of us at Mikuni Cat Skiing are looking forward to seeing you soon!';
		$Mail_ConfirmFooter = '--------------------------
Mikuni Cat Skiing
KK Sherpa
--------------------------
Tel:  025-789-4969
www.mikuni-cat.com
--------------------------

';
		// buyer information
		$Mail_BuyerInformation = 'Customer Information';
		
		$Mail_FirstNameMei = 'First Name:';
		$Mail_LastNameSei = 'Last Name:';
		$Mail_FullNameBoth = 'Name:';
		
		$Mail_Phone = 'Phone:';
		$Mail_Email = 'Email:';
		$Mail_PostalCode = 'Postal Code:';
		$Mail_PostalCode_SymbolJP = '';
		$Mail_CountryOfResidence_Label = 'Country of Residence:';
		$Mail_Prefecture = 'State/Prefecture:';
		$Mail_CitySuburb = 'City/Suburb:';
		$Mail_Address1 = 'Address 1:';
		$Mail_Address2 = 'Address 2:';
		$Mail_EmergencyPhone = 'Emergency Phone:';
		$Mail_EmergencyContact = 'Emergency Contact Person:';
		
		
		
		
		// skier information
		$Mail_SkierInformation = 'Student Information: ';
		
		$Mail_GenderLabel = 'Gender:';
		$Mail_Male = 'Male';
		$Mail_Female = 'Female';
		$Mail_DateOfBirthLabel = 'Date of Birth:';
		
		$Mail_SkierInstructionLangLabel = 'Language of instruction:';
		$Mail_SkierInstructionLangJP = 'Japanese';
		$Mail_SkierInstructionLangEN = 'English';
		$Mail_SkierInstructionLangCN = 'Chinese';
		
		$Mail_SkierLessonTypeLabel = 'Lesson preference: ';
		$Mail_LessonTypeSki = 'Ski';
		$Mail_LessonTypeSnowboard = 'Snowboard';
		$Mail_LessonTypeBoth = 'Both ski and snowboard';
		
		$Mail_SkiAbilityLevelLabel = 'Ski ability level: ';
		$Mail_SnowboardAbilityLevelLabel = 'Snowboard ability level: ';
		$Mail_LevelBeginnerA = 'First time = A';
		$Mail_LevelBeginnerB = 'Beginner = B';
		$Mail_LevelIntermediateC = 'Advanced Beginner = C';
		$Mail_LevelIntermediateD = 'Intermediate = D';
		$Mail_LevelIntermediateE = 'Advanced Intermediate = E';
		$Mail_LevelExpertF = 'Expert = F';
		
		$Mail_AllergyFoodLabel = 'Food allergies: ';
		$Mail_InstructorPrefsLabel = 'Instructor preferences: ';
		
		$Mail_ConfirmationMsg_ThanksHeader = 'Thank you for your reservation. Please find below a preparation checklist and information to help you get the most out of your day on the slopes. We look forward to seeing you soon!';
		$Mail_ConfirmationMsg_RequiredChecklistHeader = 'Mikuni Cat Tour Checklist';
		$Mail_ConfirmationMsg_RequiredChecklist = '- Snow wear and rental equipment is *not* included in the tour fee. Please take care of your gear requirements before you arrive.<br>
- Print out this confirmation page and present to the instructor (or show your confirmation email on your phone) when you arrive for your lesson.<br>
- Lessons begin on time. Mikuni Cat reserves the right to cancel or re-schedule lessons if participants are not present at the scheduled start time.<br>
- If you think you might be late for your lesson, please call us ahead of time to let us know.';
		$Mail_ConfirmationMsg_PrincePointsHeader = 'Prince Points';
		$Mail_ConfirmationMsg_PrincePoints = 'If you\'d like to accrue Prince Points from your lesson purchase, please present your Prince Card at our Building #3 or Building #6 reservation counters.
Our reservation staff can validate your receipt for you';
		$Mail_ConfirmationMsg_ContactUs = 'Questions? Please mail us at reservations@mikuni-cat.com or call 025-789-4969.';
		
		$Mail_ConfirmationMsg_PrivateHeader = 'Private Lessons';
		$Mail_ConfirmationMsg_Private_MeetingPoint3 = 'Five (5) minutes before your lesson begins, please come to the Snow School reservation desk in Building #3.';
		$Mail_ConfirmationMsg_Private_MeetingPoint6 = 'Five (5) minutes before your lesson begins, please come to the Snow School reservation desk in Building #6.';
		$Mail_ConfirmationMsg_Private_HowToMeet = 'Your instructor will call your name.<br>
Please show your instructor a printout of your confirmation page, or show your confirmation email on your phone.<br>
If unaccompanied minor(s) will be taking a lesson, please confirm with the instructor where your lesson will disperse.';
		$Mail_ConfirmationMsg_Private_LiftTickets = 'Please bring your valid lift ticket(s), one per skier, with you to the lesson. 
Children under age 12 ski for free at Naeba, but every skier must have a lift ticket. Special kids\' passes can be obtained at the lift ticket window.';		
		
		// only private lessons for EN users, these are not used
		$Mail_ConfirmationMsg_AdultGroupHeader = '';
		$Mail_ConfirmationMsg_AdultGroup_Times = '';
		$Mail_ConfirmationMsg_AdultGroup_HowToMeet = '';
		$Mail_ConfirmationMsg_AdultGroup_LiftTickets = '';
		
		$Mail_ConfirmationMsg_JuniorGroupHeader = '';
		$Mail_ConfirmationMsg_JuniorGroup_Times = '';
		$Mail_ConfirmationMsg_JuniorGroup_HowToMeet = '';
		
		$Mail_StartLocationMap_FileName = 'EN_NaebaSnowSchool_集合場所_2016_17.pdf';
		
		// forgot password
		$Mail_NewPasswordSubject = 'Your password has been reset';
		$Mail_NewPasswordMessage = "You recently requested a new password for your Naeba Snow School user account.";
		$Mail_YourNewPasswordIs = "Your new password is: ";
		
		
		
		
	
		
		
		
	} else if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'CN')){
		
		// general
		$Mail_Salutation = '先生／小姐';
		
		// order confirmation
		$Mail_ConfirmationSubject = '感謝您的申請';
		$Mail_ThanksForRegistering = '謝謝您這次預約苗場滑雪學校的課程!';
		$Mail_HowToPayAdvice = '此封郵件是為了確認您的預定，並附上匯款資料。';
		$Mail_HowToPayDetails = '日本境內轉賬：如從日本境外轉帳，請使用下方資訊：[銀行名] 第四銀行
[分行名] 湯沢支店
[帳號類型] 普通戶頭
[帳號] 1282261
[戶頭名義] 株式会社シェルパ
付款時，請備註上課日期及申請者姓名，例：0101王小明．我們只確認一個禮拜內的匯款，請見諒';
		
		$Mail_NoPaymentNoPlaceNotice = '只要我們確定款項匯入指定銀行戶頭，即會發送預約完成信，請在預約完成一個禮拜內匯款；若超過一個禮拜主辦單位無法確認客人匯款有無成功，預約內容將會刪除，請知悉';
		$Mail_PrivacyPolicyAgreementNotice = '主辦單位一旦確認匯款成功後，即代表預訂者同意本課程內容及個人資訊保護政策，內容請參閱: http://www.naebasnow.jp/index.php?UserLang=CN&PageId=67';
		
		$Mail_AllPricesIncludeTax = '所有價格已含稅';
		$Mail_YenSign = 'JPY ';
		$Mail_OrderDetailsLabel = '訂單資訊';
		$Mail_OrderSubtotalLabel = '小計: ';
		$Mail_OrderTotalLabel = '總計: ';
		$Mail_CouponDiscountLabel = '使用優惠券: ';
		$Mail_CouponCodeLabel = '優惠券號碼: ';
		$Mail_PaidOnDateTotalLabel = '通過信用卡支付 ';
		$Mail_BalanceZeroLabel = '餘額: ';
		
		$Mail_RegardingCancellationsHeader = '關於取消政策:';
		$Mail_RegardingCancellationsText = '課程款項一旦付清，預約即成立，取消將會產生手續費，取消手續費將依取消日期而有所不同，取消手續費將從上課日前一日開始起算<br>
課程2天前取消，將收取課程的10%為手續費
課程前日或當日取消，將收取課程的50%為手續費
上課當日未現身或課程開始後才取消，則全額扣款';
		$Mail_ConfirmFooterSalutation = '謝謝您的預定，苗場滑雪學校滿心期待您的光臨!';
		$Mail_ConfirmFooter = '--------------------------
Mikuni Cat Skiing
KK Sherpa
--------------------------
Tel:  025-789-4969
www.mikuni-cat.com
--------------------------
		
';
		// buyer information
		$Mail_BuyerInformation = '會員資訊';
		
		$Mail_FirstNameMei = '名:';
		$Mail_LastNameSei = '姓:';
		$Mail_FullNameBoth = '全名:';
		
		$Mail_Phone = '電話:';
		$Mail_Email = 'Email:';
		$Mail_PostalCode = '郵遞區號:';
		$Mail_PostalCode_SymbolJP = '';
		$Mail_CountryOfResidence_Label = '國籍:';
		$Mail_Prefecture = '州/縣/省份:';
		$Mail_CitySuburb = '城市/區:';
		$Mail_Address1 = '地址一:';
		$Mail_Address2 = '地址二:';
		$Mail_EmergencyPhone = '緊急聯絡電話:';
		$Mail_EmergencyContact = '緊急聯絡人:';
		
		
		
		
		// skier information
		$Mail_SkierInformation = '學員資訊';
		
		$Mail_GenderLabel = '性別:';
		$Mail_Male = '男';
		$Mail_Female = '女';
		$Mail_DateOfBirthLabel = '生日:';
		
		$Mail_SkierInstructionLangLabel = '教學語言選擇:';
		$Mail_SkierInstructionLangJP = '日文';
		$Mail_SkierInstructionLangEN = '英文';
		$Mail_SkierInstructionLangCN = '中文';
		
		$Mail_SkierLessonTypeLabel = '您想上何種類型的課程?';
		$Mail_LessonTypeSki = 'Ski(雙板)';
		$Mail_LessonTypeSnowboard = 'Snowboard(單板)';
		$Mail_LessonTypeBoth = '兩者都想';
		
		$Mail_SkiAbilityLevelLabel = 'Snowboard程度';
		$Mail_SnowboardAbilityLevelLabel = 'Ski程度';
		$Mail_LevelBeginnerA = '第一次 = A';
		$Mail_LevelBeginnerB = '初級 = B';
		$Mail_LevelIntermediateC = '進階初級 = C';
		$Mail_LevelIntermediateD = '中級 = D';
		$Mail_LevelIntermediateE = '進階中級 = E';
		$Mail_LevelExpertF = '高級 = F';
		
		$Mail_AllergyFoodLabel = '食物過敏?';
		$Mail_InstructorPrefsLabel = '對教練的要求: ';
		
		$Mail_ConfirmationMsg_ThanksHeader = '感謝您預約本次苗場滑雪學校的課程，以下將進行上課前的準備事項及當日報到流程，請務必確認';
		$Mail_ConfirmationMsg_RequiredChecklistHeader = '上課前準備';
		$Mail_ConfirmationMsg_RequiredChecklist = '課程不包含裝備、雪具等周邊配備，請自行準備.
<br>
- 請印出此預約確認信（顯示於手機畫面也可）在上課前把內容提交給教練進行確認.
- 集合地點會因課程內容不同而相異，請注意
- 若無法在集合時間前抵達集合點地點的話，將無法參加課程，請知悉。<br>因塞車、天候造成交通阻塞而無法上課，請聯絡主辦單位';
		$Mail_ConfirmationMsg_PrincePointsHeader = '想利用王子飯店卡片之點數集點的客人';
		$Mail_ConfirmationMsg_PrincePoints = '請帶著您的卡片跟課程預約書到三號館／六號館的滑雪學校櫃台';
		$Mail_ConfirmationMsg_ContactUs = '有任何問題請Mail到 chinese@naebasnow.jp 或者撥打我們的專線 +81-70-5550-8807。';
		
		$Mail_ConfirmationMsg_PrivateHeader = '申請私人課程的準備';
		$Mail_ConfirmationMsg_Private_MeetingPoint3 = '網路預約的話請在上課前五分鐘到三號館櫃台集合.';
		$Mail_ConfirmationMsg_Private_MeetingPoint6 = '網路預約的話請在上課前五分鐘到六號館櫃台集合..';
		$Mail_ConfirmationMsg_Private_HowToMeet = '您的教練將會在櫃台前呼喊您的名字.<br>
請在跟教練會合後，提供課程預約書（顯示於手機畫面也可），教練確認完畢後課程即開始.<br>
若參加者為兒童的話，請跟教練確認解散地點.';
		$Mail_ConfirmationMsg_Private_LiftTickets = '纜車票請自行購買，小學生以下不需要購買纜車票，但需到纜車賣票口拿孩童專用券.';
		
		// only private lessons for CN customers, these are not used
		$Mail_ConfirmationMsg_AdultGroupHeader = '一般大人團體課程的申請';
		$Mail_ConfirmationMsg_AdultGroup_Times = '上午：集合時間9:50，上課時間10:00';
		$Mail_ConfirmationMsg_AdultGroup_HowToMeet = '網路預約的話請在上課前十分鐘抵達集合地點，教練會開始點名，與教練會合後請提供課程預約書（顯示於手機畫面也可）給教練確認';
		$Mail_ConfirmationMsg_AdultGroup_LiftTickets = '纜車票請自行購買，但根據參加者的程度也可能不需要坐纜車，因此請遵循教練指示';
		
		$Mail_ConfirmationMsg_AdultGroupHeader = '一般大人團體課程的申請';
		$Mail_ConfirmationMsg_JuniorGroup_Times = '孩童團課程的申請';
		$Mail_ConfirmationMsg_JuniorGroup_HowToMeet = '網路預約的話上課前十五分鐘請到指定地點集合，集合、解散地點為室外滑雪場，請家長也穿著防寒衣物。教練在點名時請提供預約確認信，謝謝。';
		
		$Mail_StartLocationMap_FileName = 'CN_NaebaSnowSchool_集合場所_2016_17.pdf';
		
		
		// forgot password
		$Mail_NewPasswordSubject = '新的密碼是';
		$Mail_NewPasswordMessage = '特此通知您申請一個新的密碼。';
		$Mail_YourNewPasswordIs = '收到您變更新密碼要求: ';
		
		
	} else {
		
		// default JP strings
		
		// general
		$Mail_Salutation = '様';
		
		// order confirmation
		$Mail_ConfirmationSubject = 'お申込みありがとうございます';
		$Mail_ThanksForRegistering = 'この度は、苗場スノースクールのレッスンをご予約いただきありがとうございます。';
		$Mail_HowToPayAdvice = 'レッスン代金のお振込先をお送り致します。お支払いは銀行振込をご利用ください。';
		$Mail_HowToPayDetails =  '[銀行] 第四銀行
[支店] 湯沢支店
[預金種目] 普通口座
[口座番号] 1282261
[口座名義] 株式会社シェルパ
お振込の際、受講開始月日　＋　申込者名義をご記入ください。　例：０１０１ナエバタロウ
恐れ入れますが、1週間以内のご入金をお待ちしております。
[注意：日本国内銀行からのお振込のみとさせていただきます。]';
		
		$Mail_NoPaymentNoPlaceNotice = "指定の銀行口座に入金確認ができ次第、申込み確定のメールをお送りいたします。
お申込みから【一週間以内】にご入金ください。一週間を過ぎてもご入金が
確認できない場合は、一度ご登録を取り消しとさせて頂く場合がございます。
予めご了承ください。";
		$Mail_PrivacyPolicyAgreementNotice = '弊社にて入金確認時点で本サービスの条件又は個人情報保護ポリシーに合意されたこととさせていただきます。';
		
		$Mail_AllPricesIncludeTax = '全ての価格表示は税込みです。';
		$Mail_YenSign = '￥';
		$Mail_OrderDetailsLabel = 'ご注文の詳細';
		$Mail_OrderSubtotalLabel = '小計： ';
		$Mail_OrderTotalLabel = '合計： ';
		$Mail_CouponDiscountLabel = 'クーポンコード使用： ';
		$Mail_CouponCodeLabel = 'クーポンコード： ';
		$Mail_PaidOnDateTotalLabel = 'クレジットカードでのお支払い： ';
		$Mail_BalanceZeroLabel = '残高： ';
		
		$Mail_RegardingCancellationsHeader = 'キャンセルについて';
		$Mail_RegardingCancellationsText = 'キャンセル料は以下の通り頂戴いたします。
レッスン前日～当日：レッスン代金の100%
レッスン２日前：レッスン代金の50%
レッスン４日～３日前：レッスン代金の30%';
		$Mail_ConfirmFooterSalutation =  'スタッフ一同、お会いできることを心待ちにしております。';
		$Mail_ConfirmFooter = '
--------------------------
株式会社シェルパ
三国キャットスキー
--------------------------
Tel:  025-789-4969
www.mikuni-cat.com
--------------------------
';
		
		
		
		
		
		
		
		// buyer information
		$Mail_BuyerInformation = '申込者情報';
		
		$Mail_FirstNameMei = '名:';
		$Mail_LastNameSei = '姓:';
		$Mail_FullNameBoth = 'お名前:';
		
		$Mail_Phone = '電話:';
		$Mail_Email = '電子メール:';
		$Mail_PostalCode = '郵便番号:';
		$Mail_PostalCode_SymbolJP = '〒';
		$Mail_CountryOfResidence_Label = '国';
		$Mail_Prefecture = '都道府県:';
		$Mail_CitySuburb = '市区村町:';
		$Mail_Address1 = '住所１:';
		$Mail_Address2 = '住所２:';
		$Mail_EmergencyPhone = '緊急連絡先:';
		$Mail_EmergencyContact = '緊急連絡先の名前、関係:';
		
		
		
		
		
		// skier information
		$Mail_SkierInformation = '受講生情報: ';
	
		$Mail_GenderLabel = '性別:';
		$Mail_Male = '男';
		$Mail_Female = '女';
		$Mail_DateOfBirthLabel = '生年月日:';
	
		$Mail_SkierInstructionLangLabel = '受講言語:';
		$Mail_SkierInstructionLangJP = '日本語';
		$Mail_SkierInstructionLangEN = '英語';
		$Mail_SkierInstructionLangCN = '中国語';
	
		$Mail_SkierLessonTypeLabel = 'ご受講するレッスンの種類:';
		$Mail_LessonTypeSki = 'スキー';
		$Mail_LessonTypeSnowboard = 'スノーボード';
		$Mail_LessonTypeBoth = '両方';
	
		$Mail_SkiAbilityLevelLabel = 'スキーのレベル: ';
		$Mail_SnowboardAbilityLevelLabel = 'スノーボードのレベル: ';
		$Mail_LevelBeginnerA = '初めてです = A';
		$Mail_LevelBeginnerB = 'まだ初心者です = B';
		$Mail_LevelIntermediateC = '初中級 = C';
		$Mail_LevelIntermediateD = '中級 = D';
		$Mail_LevelIntermediateE = '中上級 = E';
		$Mail_LevelExpertF = '上級 = F';
	
		$Mail_AllergyFoodLabel = '食べ物のアレルギー：';
		$Mail_InstructorPrefsLabel = 'コーチに対してお好み：';
		
		
		$Mail_ConfirmationMsg_ThanksHeader = 'この度は、苗場スノースクールのレッスンをご予約いただきありがとうございます。
レッスン当日までにご準備いただくもの、当日の流れをご説明いたしますので必ずご確認ください。';
		$Mail_ConfirmationMsg_RequiredChecklistHeader = 'レッスンを受けるためには';
		$Mail_ConfirmationMsg_RequiredChecklist = '・ツアー代には、ウェア・道具一式は含まれませんのでご自身で手配お願いいたします。<br>
・この予約確認書をプリントアウトし（携帯電話での表示でも可）レッスン前にインストラクターに提示をお願いいたします。<br>
・集合時間に遅刻した場合、ツアーに参加できなくなる場合もございます。ご了承ください。<br>
・渋滞・悪天候による交通機関遅延等で受講が難しい場合、必ずご連絡ください。';
		$Mail_ConfirmationMsg_PrincePointsHeader = 'プリンスポイント付与希望のお客様';
		$Mail_ConfirmationMsg_PrincePoints = '３号館/６号館のスクール受付にプリンスカードと予約確認書をお持ちください。<br>
受付スタッフが確認し、付与させていただきます。';
		$Mail_ConfirmationMsg_ContactUs = 'お問い合わせはメール yoyaku@mikuni-cat.com 又はお電話 025-789-4969 までご連絡ください。';
		
		$Mail_ConfirmationMsg_PrivateHeader = 'プライベートレッスンお申し込みの方';
		$Mail_ConfirmationMsg_Private_MeetingPoint3 = 'レッスン開始５分前に３号館のスノースクール受付前にお越しください。';
		$Mail_ConfirmationMsg_Private_MeetingPoint6 = 'レッスン開始５分前に６号館のスノースクール受付前にお越しください。';
		$Mail_ConfirmationMsg_Private_HowToMeet = 'インストラクターが、お客様のお名前をお呼びいたします。<br>
合流できましたらインストラクターにこの予約確認書（又は携帯電話での表示）を提示していただきレッスンスタートです。<br>
お子様だけのレッスンの場合、解散場所をインストラクターとご相談ください。';
		$Mail_ConfirmationMsg_Private_LiftTickets = 'リフト券は、各自ご用意ください。小学生以下のお子様はリフト券無料となりますが
リフト券売り場にて専用リフト券をお受け取り下さい。';
		
		$Mail_ConfirmationMsg_AdultGroupHeader = '一般レッスンお申し込みの方';
		$Mail_ConfirmationMsg_AdultGroup_Times = '午前：集合 9:50		レッスン開始 10:00<br>
午後：集合 13:20		レッスン開始 13:30';
		$Mail_ConfirmationMsg_AdultGroup_HowToMeet = 'グループレッスンは開始10分前に、プライベートレッスンは時間に遅れないように、各集合場所にお越しください。受講されるレッスンにより集合場所は異なりますので、必ず地図にてご確認ください。
インストラクターが、お客様のお名前をお呼びいたします。合流できましたらインストラクターにこの予約確認書（又は携帯電話での表示）を提示してください。';
		$Mail_ConfirmationMsg_AdultGroup_LiftTickets = 'リフト券は、各自ご用意ください。ただし、皆様の技術レベルに合わせたレッスンとなるためリフト乗車しない可能性もございます。
その後、インストラクターの指示に従ってください。';
		
		$Mail_ConfirmationMsg_JuniorGroupHeader = 'ジュニアレッスンお申し込みの方';
		$Mail_ConfirmationMsg_JuniorGroup_Times = '午前：集合 9:20		レッスン開始 9:30<br>
午後：集合 13:20		レッスン開始 13:30<br>
ランチ付き：集合 9:20		レッスン開始 9:30<br>
★ 解散は集合の時にご案内いたします。';
		$Mail_ConfirmationMsg_JuniorGroup_HowToMeet = 'レッスン開始１５分前に集合場所へお越しください。<br>
★ 集合の際、保護者の方に説明がございますのでレッスン開始まで必ずお子様と一緒に居てください。<br>
★ 集合・解散の場所はゲレンデになりますので保護者の方も防寒対策お願いいたします。<br>
インストラクターに、予約確認書を提示してください。<br>
その後、インストラクターの指示に従ってください。';
		$Mail_StartLocationMap_FileName = 'NaebaSnowSchool_集合場所_2016_17.pdf';
		
		
		// forgot password
		$Mail_NewPasswordSubject = 'パスワード更新のお知らせ';
		$Mail_NewPasswordMessage = 'パスワード更新のリクエストをいただきました。';
		$Mail_YourNewPasswordIs = '新しいパスワードは:';
		
	}
?>