<?php
	// default language strings are JP, below in else statement
	if ($_SESSION['UserLang'] == 'EN') {
		
		// login
		$PasswordEntryError = 'The current password you entered is incorrect. Please re-enter your current password and try again.';
		$LoginLabel = 'Login';
		$AdminLoginFormSetupNotice = 'Log in with your email address and password below.';
		$AdminLoginFormEmailLabel ='Email:';
		$AdminLoginFormPasswordLabel ='Password:';
		$AdminLoginFormForgotPwdNotice = 'Forgot your password?';
		$AdminLoginError_NotUnique = 'Access credentials are not unique. Please contact site administrator.';
		$AdminLoginError_NoMatchingUser = 'No user found for those credentials. You must enter a valid email and password to log in.';
		$AdminLoginError_FormError = 'Form validation error. Please enter both your email and password to log in.';
		$AdminLoginError_EmailInUse = 'Selected email address is already in use. Please choose a new email address.';
		
		// lang toggle
		$AdminLangToggle_EN = 'English';
		$AdminLangToggle_JP = '日本語';
		$AdminLangToggle_CN = '國語';
		
		// logout
		$AdminLogOutSuccessMsg = 'You have successfully logged out. Would you like to <a href="admin_template_login.php?UserLang=EN" class="ForgotPwd" >log in again</a>?';
		
		
		// password recovery
		$AdminPasswordRecoveryAdvice = 'Please enter your email address. A temporary password will be sent to you.';
		$AdminPasswordRecoverySubmitLabel = 'Reset Password';

		// menu
		$AdminMenuLabel_ContentMgt = 'Content Mgt';
		
		// admin home
		$AdminHomeFunctionLabel_PageMgt = 'Manage Pages/Modules';
		$AdminHomeFunctionLabel_NewPage = 'Create New Page/Module';
		$AdminHomeFunctionLabel_UploadImg = 'Upload Image Files';
		$AdminHomeFunctionLabel_UploadDoc = 'Upload Documents';
		$AdminHome_PageTitle = 'Content Modules and Pages';
		
		// upload images
		$AdminHome_UploadImg_PageTitle = 'Upload Image File';
		$AdminHome_UploadImg_AttachImg = 'Attach image to upload';
		$AdminHome_UploadImg_ChooseFile = 'Choose File';
		$AdminHome_UploadImg_NoFileChosen = 'No file chosen';
		$AdminHome_UploadImg_AcceptedFormats = 'Accepted file formats: JPEG, JPG, PNG, GIF. 1000KB max size.';
		$AdminHome_UploadImg_SubmitLabel = 'Submit';
	
		// edit pages
		$AdminEditPage_PageTitle_Edit = 'Edit Page/Module';
		$AdminEditPage_PageTitle_Create = 'Create New Page/Module';
		$AdminEditPage_Success_PageInsert = 'Page record insert executed correctly.<br />';
		$AdminEditPage_Success_PageUpdate = 'Page record update executed correctly.<br />';
		$AdminEditPage_Success_PageSelect = 'Page record selected.<br />';
		$AdminEditPage_Error_PageInsert = 'Error with page record insert.<br />';
		$AdminEditPage_Error_PageUpdate = 'Error with page record update.<br />';
		$AdminEditPage_Error_PageSelect = 'Error with page selection.<br />';
		
		$AdminEditPage_SubmitLabel_New = 'Save New Page';
		$AdminEditPage_SubmitLabel_Update = 'Update';
		
		$AdminEditPage_EntryLabel_PageNameJP = 'Page Name JP';
		$AdminEditPage_EntryLabel_PageDescJP = 'Page Content JP';
		$AdminEditPage_EntryLabel_PageNameEN = 'Page Name EN';
		$AdminEditPage_EntryLabel_PageDescEN = 'Page Content EN';
		
		$AdminEditPage_ViewPageInBrowser = 'View page in browser';
		
		
		// TODO: edit account
		$CurrentPassword = 'Current Password';
		$CurrentPassword_ErrorAdvice = 'You must enter your current password.';
		$CurrentPassword_Advice = 'Please enter your current password.';
        $NewPassword = 'New Password';
      	$NewPassword_ErrorAdvice = 'Please enter a new password.';
        $NewPassword_Advice = 'New password must be at least 8 characters.';
        $ReenterNewPassword = 'Re-enter New Password';
      	$ReenterNewPassword_ErrorAdvice = 'You must re-enter your new password.';
        $ReenterNewPassword_Advice = 'Re-enter your new password.';
		
		$AdminAccountPanel_UserLabel = 'User: ';
		$AdminAccountPanel_MyAccount = 'My account';
		$AdminAccountPanel_Logout = 'Log out';
		$AdminUpdateSuccessInfo_Pwd = 'Account and/or password information has been successfully updated. A confirmation email has been sent to the mail address you specified.';
		$AdminUpdateSuccessInfo_NoPwd = 'Account information has been successfully updated. A confirmation email has been sent to the mail address you specified.';	
		
		
		
	} else {
		
		// login
		$PasswordEntryError = '入力されたパスワードは違います。もう一度入力してください。';
		$LoginLabel = 'ログイン';
		$AdminLoginFormSetupNotice = 'メールアドレスとパスワードを入力してください。';
		$AdminLoginFormEmailLabel ='メール：';
		$AdminLoginFormPasswordLabel ='パスワード：';
		$AdminLoginFormForgotPwdNotice = 'パスワードを忘れた方';
		$AdminLoginError_NotUnique = 'Access credentials are not unique. Please contact site administrator.';
		$AdminLoginError_NoMatchingUser = 'メールやパスワードは正しくない。もう一度入力してください。';
		$AdminLoginError_FormError = 'Form validation error. Please enter both your email and password to log in.';
		$AdminLoginError_EmailInUse = '入力されたメールアドレスはすでに使われている。新しいメールアドレスを入れてください。';
		
		$AdminLangToggle_EN = 'English';
		$AdminLangToggle_JP = '日本語';
		$AdminLangToggle_CN = '國語';
		
		// logout
		$AdminLogOutSuccessMsg = 'ログアウト完了です。もう一度 <a href="admin_login.php?UserLang=JP" class="ForgotPwd">ログイン</a>されますか？';
		
		
		// password recovery
		$AdminPasswordRecoveryAdvice = '登録済みのメールアドレスを入力してください。一時パスワードをお送りします。';
		$AdminPasswordRecoverySubmitLabel = 'パスワードをリセット';
		
		// menu
		$AdminMenuLabel_ContentMgt = 'コンテンツを管理';
		
		// admin home
		$AdminHomeFunctionLabel_PageMgt = 'コンテンツ管理';
		$AdminHomeFunctionLabel_NewPage = '新規ページ作成';
		$AdminHomeFunctionLabel_UploadImg = 'イメージをアップロード';
		$AdminHomeFunctionLabel_UploadDoc = '書類をアップロード';
		$AdminHome_PageTitle = 'コンテンツ管理：ページとモジュール';
		
		// upload images
		$AdminHome_UploadImg_PageTitle = '画像ファイルをアップロード';
		$AdminHome_UploadImg_AttachImg = '画像ファイルを添付してアップロード';
		$AdminHome_UploadImg_ChooseFile = 'クリックしてお選びください。';
		$AdminHome_UploadImg_NoFileChosen = '画像ファイル未設定';
		$AdminHome_UploadImg_AcceptedFormats = '使用可能のフィアル形式：JPEG, JPG, PNG, GIF. 大きさは1000KB まで。';
		$AdminHome_UploadImg_SubmitLabel = '実行する';
		
		// edit pages
		$AdminEditPage_PageTitle_Edit = 'ページを編集';
		$AdminEditPage_PageTitle_Create = '新規ページ作成';
		$AdminEditPage_Success_PageInsert = 'ページを作成しました。<br />';
		$AdminEditPage_Success_PageUpdate = 'ページをアップデートしました。<br />';
		$AdminEditPage_Success_PageSelect = 'ページをロードしました。<br />';
		$AdminEditPage_Error_PageInsert = 'ページ作成にエラーが発生しました。<br />';
		$AdminEditPage_Error_PageUpdate = 'ページアップデートにエラーが発生しました。<br />';
		$AdminEditPage_Error_PageSelect = 'ページロードにエラーが発生しました。<br />';
		
		$AdminEditPage_SubmitLabel_New = '新規ページを作成';
		$AdminEditPage_SubmitLabel_Update = 'アップデート';
		
		$AdminEditPage_EntryLabel_PageNameJP = 'ページ名';
		$AdminEditPage_EntryLabel_PageDescJP = 'ページ内容';
		$AdminEditPage_EntryLabel_PageNameEN = 'Page Name EN';
		$AdminEditPage_EntryLabel_PageDescEN = 'Page Content EN';
	
		$AdminEditPage_ViewPageInBrowser = 'プレビューをブラウザで見る';
		
		
		// TODO: edit account
		$CurrentPassword = '現在のパスワード';
		$CurrentPassword_ErrorAdvice = '現在のパスワードを入力してください。';
		$CurrentPassword_Advice = 'パスワードを最短８文字にしてください。';
        $NewPassword = '新規パスワード';
      	$NewPassword_ErrorAdvice = '新規パスワードを入力してください。';
        $NewPassword_Advice = 'パスワードを最短８文字にしてください。';
        $ReenterNewPassword = '新規パスワードを再入力';
      	$ReenterNewPassword_ErrorAdvice = '新規パスワードを再入力してください。';
        $ReenterNewPassword_Advice = 'パスワードを最短８文字にしてください。';
		$AdminAccountPanel_UserLabel = 'ユーザー：';
		$AdminAccountPanel_MyAccount = 'マイアカウント';
		$AdminAccountPanel_Logout = 'ログアウト';
		$AdminUpdateSuccessInfo_Pwd = '口座データやパスワードは変更されました。確認メールを指定のメールアドレスまで送信しました。';
		$AdminUpdateSuccessInfo_NoPwd = '口座データは変更されました。確認メールを指定のメールアドレスまで送信しました。';	
		
				
	}
		
?>