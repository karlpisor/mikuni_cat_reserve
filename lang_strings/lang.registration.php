<?php
	// default language strings are JP, below in else statement
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
	
		$BookNowLabel = 'Book Now';
		
		// login
		$AlreadyRegistered = 'Already registered with Mikuni Cat or Naeba Snow School?';
		$NewRegistration = 'New Registration';
		$LoginPageTitle = 'Snow School Login';
		$Login = 'Log in';
		$LoginHereLabel = 'Log in here';
		$Password = 'Password';
		$Email_Label = 'Email';
	
		$NewRegistrationPersonalInfoNotice = '<strong>Notice regarding personal information management</strong><br> Mikuni Cat (Sherpa KK) commits to the responsible handling of your personal information. We collect only information necessary to safely run our instructional programs and to communicate with you regarding your participation (including but not limited to marketing offers, surveys, and product development). We will never sell your information to other parties, and require all subcontractors and vendors to handle personal information as carefully as we would ourselves. Questions about our personal information management practices? Please don\'t hesitate to contact us at <a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a>. Continued use of this website indicates your understanding and acceptance of this policy.';
		
		$RegisterWelcomeMsg = 'To begin your registration, please create a free password-protected user account. Mikuni Cat will send a confirmation email to this email address.';
		$NewUserRegistrationLink = 'New to Mikuni Cat? New users please register here.';
		
	
	
		$Password_Label = 'Password';
		$PasswordNew_Label = 'Enter New Password';
		$Password_ErrorAdvice = 'Please choose a password at least 8 characters in length.';
		$Password_Advice = 'Please choose a password at least 8 characters in length.';
		$ReenterPassword = 'Re-enter Password';
		$ReenterPassword_ErrorAdvice = 'Passwords do not match; please check your entry and try again.';
			
		$LessonStartDate = 'Tour Start Date';
		$LessonStartDate_ErrorAdvice = 'No date entered. Please enter a start date.';
		$LessonEndDate = 'Tour Finish Date';
		$LessonEndDate_ErrorAdvice = 'No date entered. Please enter a finish date.';
	
	
		$UserPasswordMgt_Label = 'User Password Management';
		$ReenterPassword_Advice = '';
		$CreateAccount = 'Create Account';
		$ForgotPwdNotice = 'Forgot your password?';
		$ForgotPwdEnterEmailLabel = 'Please enter your email address. A temporary password will be sent to you.';
		$ResetPassword = 'Reset Password';
		$CreateFreeAccountLabel = 'Create a Free Account Now!';
	
		// header
		$RegistrationPageHeader = 'Registration';
	
		// registration breadcrumbs
		$BreadcrumbBuyerRegLabel = 'Customer Registration';
		$BreadcrumbSkierRegLabel = 'Skier Registration';
		$BreadcrumbSessionSelectLabel = 'Tour Selection';
		$BreadcrumbConfirmationLabel = 'Confirmation';
	
	
	
	
		// buyer registration
		$RegistrationRequiredField = 'Required field';
	
		$BuyerInformation = 'Customer Information';
	
		$FirstNameMei = 'First Name:';
		$FirstNameMei_Advice = 'Ex: George';
		$FirstNameMei_ErrorAdvice = 'Please enter your first name.';
	
		$FirstNameMei_Kana = 'EN_Error_FirstNameMei_Kana';
		$FirstNameMei_Kana_Advice = '例：たろう';
		$FirstNameMei_Kana_ErrorAdvice = '';
	
		$LastNameSei = 'Last Name:';
		$LastNameSei_Advice = 'Ex: Tanaka';
		$LastNameSei_ErrorAdvice = 'Please enter your last name.';
	
		$LastNameSei_Kana = 'EN_Error_LastNameMei_Kana';
		$LastNameSei_Kana_Advice = 'EN_Error_例：たなか';
		$LastNameSei_Kana_ErrorAdvice = 'EN_Error_';
	
		$FullName_en = 'Full Name, English:';
		$FullName_jp = 'EN_Error_FullNameOnamae';
		$FullNameBoth = 'Name:';
	
		$Phone = 'Phone:';
		$Email = 'Email:';
		$PostalCode = 'Postal Code:';
		$PostalCode_SymbolJP = '';
		$CountryOfResidence_Label = 'Country of Residence:';
		$Prefecture = 'State/Prefecture:';
		$PrefectureOptions = '-- Select prefecture --';
		$CountryOptions = '-- Select country --';
		$CitySuburb = 'City/Suburb:';
		$Address1 = 'Address 1:';
		$Address2 = 'Address 2:';
		$EmergencyPhone = 'Emergency Phone:';
		$EmergencyContact = 'Emergency Contact Person:';
	
		$Phone_Advice = 'Ex: (+81) 80-5555-1234';
		$Email_Advice = 'Ex: fresh-powder@mikuni-cat.com';
		$PostalCode_Advice = 'Ex: 105-0014';
		$Prefecture_Advice = 'Ex: Tokyo, California, Ontario, New South Wales, etc.';
		$CitySuburb_Advice = 'Ex: Setagaya-ku, San Francisco, London, etc.';
		$Address1_Advice = 'Ex: Minami Kasahara 5-9-26, 234 Peachtree Lane, etc.';
		$Address2_Advice = 'Ex: Green Palace Apt. 205';
		$EmergencyPhone_Advice = 'Ex: (+1) 234-5555-1234<br>Please enter an emergency contact number for someone not participating in the tour. We would use this number only in an emergency situation.';
		$EmergencyContact_Advice = 'Ex: Jennifer Sanders, aunt';
	
		$Phone_ErrorAdvice = 'Please enter your phone number.';
		$Email_ErrorAdvice = 'Please enter your email address.';
		$PostalCode_ErrorAdvice = ' Please enter your postal code.';
		$Prefecture_ErrorAdvice = 'Please enter your prefecture/state/province.';
		$CitySuburb_ErrorAdvice = 'Please enter your city or ward.';
		$Address1_ErrorAdvice = 'Please enter your address.';
		$EmergencyPhone_ErrorAdvice = 'Please enter an emergency contact number.';
		$EmergencyContact_ErrorAdvice = 'Please enter an emergency contact person, and their relationship.';
	
		$CountryLabel_Japan = 'Japan';
		$CountryLabel_Australia = 'Australia';
		$CountryLabel_Taiwan = 'Taiwan';
		$CountryLabel_HongKong = 'Hong Kong';
		$CountryLabel_Singapore = 'Singapore';
		$CountryLabel_Thailand = 'Thailand';
		$CountryLabel_Indonesia = 'Indonesia';
		$CountryLabel_UK = 'United Kingdom';
		$CountryLabel_USA = 'United States of America';
		$CountryLabel_China = 'China';
	
		$UserLanguage = 'Language preference:';
		$CurrentUserLanguageLabelEN = 'English';
		$CurrentUserLanguageLabelJP = '日本語';
		$CurrentUserLanguageLabelCN = '中文';
		$ChangeUserLanguageToJP = '言語設定を日本語にする';
		$ChangeUserLanguageToEN = 'English';
		$ChangeUserLanguageToCN = '中文';
		$CurrentUserLanguageNotSet = 'Current user language not available. Please contact site administrator.';
	
		$IsSkierLabel = 'Will this person be skiing?';
		$IsSkierYes = 'Yes';
		$IsSkierNo = 'No';
		$IsSkier_Advice = 'We\'ll ask for your personal information later in your registration.';
	
		$ReceivesNewsletterLabel = 'Receive email newsletter?';
		$ReceivesNewsletterYes = 'Yes, please';
		$ReceivesNewsletterNo = 'No, thank you';
		$ReceivesNewsletterExplanation = 'Get the latest news and features from Mikuni Cat Skiing. Special offers, events, gear info, and much more!';
		$ReceivesNewsletter_ErrorAdvice = 'Please make a selection.';
	
		$Submit = 'Submit';
		$Reset = 'Reset Form';
		$Button_AdultRegCancel_Label = 'Cancel';
		$Button_AdultRegConfirmLabel = 'Confirm and Proceed';
		$EditFormContent = 'Edit';
	
	
	
	
	
	
		// skier registration
		$SkierInformation = 'Student Information';
	
		$Gender = 'Gender:';
		$Male = 'Male';
		$Female = 'Female';
		$Gender_ErrorAdvice = 'Please make a selection.';
	
		$DateOfBirth = 'Date of Birth:';
		$MonthJanuary = 'January';
		$MonthFebruary = 'February';
		$MonthMarch = 'March';
		$MonthApril = 'April';
		$MonthMay = 'May';
		$MonthJune = 'June';
		$MonthJuly = 'July';
		$MonthAugust = 'August';
		$MonthSeptember = 'September';
		$MonthOctober = 'October';
		$MonthNovember = 'November';
		$MonthDecember = 'December';
		
		$SkierAgeAdvice = '';
		
		$SkierIsJuniorMessage = 'All guests must be at least 16 years old. Guests under 20 years old require parental consent.';
	
		$SkierInstructionLangLabel = 'Skier language:';
		$SkierInstructionLangJP = 'Japanese';
		$SkierInstructionLangEN = 'English';
		$SkierInstructionLangCN = 'Chinese';
	
		$SkierLessonTypeLabel = 'What will this skier be riding?';
		$PreferredLessonTypeLabel = 'Ski/snowboard selection: ';
		$LessonTypeSki = 'Ski';
		$LessonTypeSnowboard = 'Snowboard';
		$LessonTypeBoth = 'Both';
		$LessonType_ErrorAdvice = 'Please select one.';
	
	
		$AbilityLevelSnowboard = 'Snowboard ability level';
		$AbilityLevelSki = 'Ski ability level';
	
		$AbilityLevelPleaseSelect = '-- Please select --';
		$LevelBeginnerA = 'First time = A';
		$LevelBeginnerB = 'Beginner = B';
		$LevelIntermediateC = 'Advanced Beginner = C';
		$LevelIntermediateD = 'Intermediate = D';
		$LevelIntermediateE = 'Advanced Intermediate = E';
		$LevelExpertF = 'Expert = F';
		$ResetWizard = 'Reset';
		$ConfirmWizardLevelSelection = 'Confirm selection';
		$LevelWizard_Title = '<span class="wizard-title-text">Level Selection Wizard</span>';
	
		$LevelWizard_SnowboardAdult_SkiedBeforeYes = 'I\'ve snowboarded before.';
		$LevelWizard_SnowboardAdult_SkiedBeforeNo = 'This is my first time on a snowboard.';
		$LevelWizard_SnowboardAdult_SideslipHeelYes = 'I can sideslip down a slope on the heel edge of the board.';
		$LevelWizard_SnowboardAdult_SideslipHeelNo = 'Sideslipping? Hmm, I\'ll need to learn that.';
		$LevelWizard_SnowboardAdult_SideslipToeYes = 'I can sideslip down a slope on the toe edge of the board.';
		$LevelWizard_SnowboardAdult_SideslipToeNo = 'On the toe edge? Not sure I can do that.';
		$LevelWizard_SnowboardAdult_TurningYes = 'I can make basic turns okay.';
		$LevelWizard_SnowboardAdult_TurningNo = 'Turning? Well, not really. I\'ll need to learn how.';
		$LevelWizard_SnowboardAdult_ControlledDescentYes = 'I can control my speed and manage turns well on beginner runs.';
		$LevelWizard_SnowboardAdult_ControlledDescentNo = 'Control my speed and turns? I\'d love to learn how.';
		$LevelWizard_SnowboardAdult_AllTerrainYes = 'I can turn accurately at speed on expert and/or bumpy runs.';
		$LevelWizard_SnowboardAdult_AllTerrainNo = 'I\'m still learning how to manage expert and/or bumpy runs.';
		$LevelWizard_SnowboardAdult_HighPerformanceYes = 'I\'m ready to take my snowboard skills to the next level. I\'d like to maybe take a badge test or become a snowboard instructor.';
		$LevelWizard_SnowboardAdult_HighPerformanceNo = 'I don\'t think I\'m ready for a high performance lesson.';
	
		$LevelWizard_SnowboardJunior_SkiedBeforeYes = 'I\'ve snowboarded before, or I\'ve taken snowboard lessons before.';
		$LevelWizard_SnowboardJunior_SkiedBeforeNo = 'This is my first time on a snowboard.';
		$LevelWizard_SnowboardJunior_SideslipHeelYes = 'I can sideslip down a slope on the toe edge of the board. I can also get on and off a lift chair on my own.';
		$LevelWizard_SnowboardJunior_SideslipHeelNo = 'Sideslipping? Hmm, I\'ll need to learn that. I\'m not 100% sure about the lift chair.';
		$LevelWizard_SnowboardJunior_SideslipBothTurningYes = 'I can sideslip down a slope on either the toe or the heel edge of the board. I can also make basic turns okay.';
		$LevelWizard_SnowboardJunior_SideslipBothTurningNo = 'My sideslipping is not so good. Turning? Well, not really. I\'ll need to learn how.';
		$LevelWizard_SnowboardJunior_BeginnerRunYes = 'On beginner runs, I can manage connected turns well and stop cleanly when I want to.';
		$LevelWizard_SnowboardJunior_BeginnerRunNo = 'Control my speed and turns? I\'d love to learn how.';
		$LevelWizard_SnowboardJunior_IntermediateRunYes = 'On intermediate runs, I can control my speed accurately and manage connected turns well.';
		$LevelWizard_SnowboardJunior_IntermediateRunNo = 'I don\'t have much experience with intermediate runs.';
		$LevelWizard_SnowboardJunior_HighPerformanceYes = 'I\'m ready to take my snowboard skills to the next level. I\'d like to maybe take a badge test or become a snowboard instructor.';
		$LevelWizard_SnowboardJunior_HighPerformanceNo = 'I don\'t think I\'m ready for a high performance lesson.';
	
			
		$LevelWizard_Ski_SkiedBeforeYes = 'I\'ve been skiing before.';
		$LevelWizard_Ski_SkiedBeforeNo = 'This is my first time on skis.';
		$LevelWizard_Ski_StoppingYes = 'I can stop when I want to.';
		$LevelWizard_Ski_StoppingNo = 'Stopping? I\'m still working on that.';
		$LevelWizard_Ski_LiftChairYes = 'I can get on and off a lift chair on my own.';
		$LevelWizard_Ski_LiftChairNo = 'I\'m not 100% sure about the lift chair. I might need some practice.';
		$LevelWizard_Ski_BeginnerRunYes = 'On beginner runs, I can manage connected turns well and stop cleanly when I want to.';
		$LevelWizard_Ski_BeginnerRunNo = 'Control my speed and turns? I\'d love to learn how.';
		$LevelWizard_Ski_DifficultRunYes = 'I can ski down more advanced runs without needing to stop.';
		$LevelWizard_Ski_DifficultRunNo = 'I usually take a few breaks when skiing down more advanced runs.';
		$LevelWizard_Ski_AllTerrainYes = 'I can manage expert and/or ungroomed runs.';
		$LevelWizard_Ski_AllTerrainNo = 'I\'m still learning how to manage expert and/or bumpy runs.';
	
		$AbilityLevelWizardAdultSnowboard = 'What level best describes your snowboard ability? <a class="level-wizard-snowboard-adult" href="#">Use our handy wizard to find out.</a>';
		$AbilityLevelWizardJuniorSnowboard = 'What level best describes your minor\'s snowboard ability? <a class="level-wizard-snowboard-junior" href="#">Use our handy wizard to find out.</a>';
		$AbilityLevelWizardAdultSki = 'What level best describes your ski ability? <a class="level-wizard-ski" href="#">Use our handy wizard to find out.</a>';
		$AbilityLevelWizardJuniorSki = 'What level best describes your minor\'s ski ability? <a class="level-wizard-ski" href="#">Use our handy wizard to find out.</a>';
	
		
	
		$SkierAllergyQuestion = 'Food allergies?';
		$SkierAllergyQuestion_Advice = 'If your child will be joining us for a full day group lesson, with lunch, please let us know any food allergies they might have. If anaphlaxis has been diagnosed, please indicate and tell us if the child will have an epipen with them.';
		$SkierAllergyLabel = 'Food allergies: ';
		
		$InstructorPrefsDisclaimer = 'Do you have any preferences for this student\'s instructor? While we will do our best to meet your request, please understand that due to scheduling constraints we cannot guarantee any specific preference but will contact you separately once we receive your booking.';
		$InstructorPrefsFemale = 'Female instructor';
		$InstructorPrefsMale = 'Male instructor';
		$InstructorPrefsGoodWithKids = 'Good with kids';
		$InstructorPrefsSameThroughout = 'Same instructor throughout my stay at Naeba';
		$InstructorPrefsHasCertification = 'Instructor has specific certification. If so, please type in details below:';
		$InstructorPrefsOther = 'Other preferences? Please let us know below:';
		$InstructorPrefsLabel = 'Instructor preferences: ';
	

	
	
		// renting functionality not active, need for future
		$Renting = 'Will this student be renting equipment?';
		$Yes = 'Yes';
		$No = 'No';
		$DominantFoot = 'Dominant foot';
		$Right = 'Right';
		$Left = 'Left';
		$DominantFoot_Advice = 'The dominant foot is behind the body (uphill) when snowboarding, typically the same foot you would kick a soccer ball with.';
	
		$SkierHeight_PleaseSelect = '-- Please select --';
		$SkierHeight = 'Height:';
		$SkierHeight_Advice = 'Please select the next highest 5cm.';
		$SkierHeight_ErrorAdvice = 'Please select this student\'s height.';
	
		$ShoeSizePleaseSelect = '-- Please select --';
		$ShoeSize = 'Shoe size:';
		$ShoeSize_ErrorAdvice = 'Please select this student\'s shoe size.';
	
	
	
	
	
		$AddNewSkierLabel = 'Add New Skier';
	
		$QuitNewSkierLabel = 'Cancel';
		$DataEntryProceedLabel = 'Proceed';
		$DataEntryCancelLabel = 'Do Not Cancel';
		$DataEntryCancelAlertTitle = 'Confirm Cancel';
		$DataEntryCancelAlert = 'Do you want to cancel entry for this student?';
	
		$Button_SkierRegConfirmLabel = 'Confirm and Proceed';
		$Button_SkierRegCancelLabel = 'Cancel';
		$Button_SkierRegConfirmAddLabel = 'Confirm and Add New Skier';
		
	
	
	
	
		// Skier session selection
		$ProceedToLessonSelectionLabel= 'Confirm and Proceed';
		$LessonSelectionSectionLabel = 'Tour Selection';
		$LessonParametersReset = 'Reset';
		$LessonTimePeriodLabel = 'Tour Date: ';
		$LessonTimePeriodLabel_OneDay = ' (One day)';
		$LessonStartLocationLabel = 'Start location: ';
		$StartLocation_ErrorAdvice = 'Please select a start location.';
		$LessonStartLocation_Bldg3 = 'Building 3';
		$LessonStartLocation_Bldg6 = 'Building 6';
		$LessonLocationMapLink = 'Need help? View resort map here.';
		$LessonParametersConfirm = 'Confirm';
		$LessonParametersReset = 'Reset Criteria';
		
	
		// Lesson descriptions
		$LessonDisplay_isPrivate = 'Private';
		$LessonDisplay_isSnowboard = 'Snowboard';
		$LessonDisplay_isSki = 'Ski';
		$LessonDisplay_isAdult = 'Adult';
		$LessonDisplay_isJunior = 'Junior';
		$LessonDisplay_isPeewee = 'Peewee';
		
		$LessonDisplay_isMikuniTour = 'Mikuni Backcountry Tour';
		$LessonDisplay_isMikuniGearRental = 'Avalance Safety Gear Rental';
		
		$LessonLangDisplay_JP = 'Japanese';
		$LessonLangDisplay_EN = 'English';
		$LessonLangDisplay_CN = 'Chinese';
		$LessonLengthDisplay_HalfDay = 'Half day';
		$LessonLengthDisplay_FullDay = 'Full day';
		$LessonLengthDisplay_FullDay_NoLunch = '(Full-day, no lunch)';
		$LessonLengthDisplay_FullDay_Lunch = '(Full-day、with lunch )';
		
		$LessonDescription_AdultGroup = 'Adult (age 13+) group lesson. Instruction matches group ability level. Up to 8 participants per lesson.';
		$LessonDescription_JuniorGroup = 'Junior (up to age 12) group lesson. Instruction matches group ability level, at a slower pace for children. Up to 6 participants per lesson.';
		$LessonDescription_PeeweeGroup = 'EN_Group description_Error';
		$LessonDescription_Private = 'Private lesson, single price for up to 3 participants. Either snowboard or ski instruction offered. Personal attention ensures you get the most from your lesson.';
		$LessonDescription_isMikuniTour = 'Private group tour, up to 8 guests.';
		$LessonDescription_isMikuniGearRental = 'Backpack with a complete set of avalanche safety gear: transceiver beacon, snow probe, and shovel. Guests without their own gear are required to rent.';
		
		
		$DesignateInstructor = 'Designate an instructor?';
		$DesignateInstructorNameLabel = 'Instructor name:';
		$DesignateInstructor_Advice = 'Additional ¥500/hour fee applies. If you\'d like to designate a specific instructor please enter their name and our reservations team will confirm once we have your booking.';
		$CurrentSkiersInLesson = 'Currently in this tour group:';
		$NumSpacesAvailable = '(Available spaces remaining: %s)';
	
		// Date display
		$DayOfWeek_Mon = 'Mon';
		$DayOfWeek_Tue = 'Tue';
		$DayOfWeek_Wed = 'Wed';
		$DayOfWeek_Thu = 'Thu';
		$DayOfWeek_Fri = 'Fri';
		$DayOfWeek_Sat = 'Sat';
		$DayOfWeek_Sun = 'Sun';
		$MonthLabel = '';
		
		$CalendarMonthLabel = 'March 2017';
		$AvailabilityPageTitle = 'Current Tour Availability';
	
		$ResetDaySelectionLink = 'Clear selection';
		$SkierNameLabel = 'Skier: ';
		$PreviousSkierLinkLabel = 'Previous Skier';
		$NextSkierLinkLabel = 'Next Skier';
		$CompleteLessonRegistration = 'Complete tour user registration and proceed';
		
	
	
	
	
		// liability release
		$LiabilityReleaseHeader = 'Liability Release and Terms of Participation';
		$LiabilityReleaseExplanation = 'Thank you for selecting one or more professional tour dates sessions (hereafter, "Tours") offered by Mikuni Cat Skiing, owned and operated by the Japanese corporation KK Sherpa (hereafter, "Sherpa"). Please carefully review the terms of participation below and check all boxes before proceeding. If the registrant for this order will not be participating, we ask that you communicate these terms to all participants, for whom these terms are binding.';
		$LiabilityRelease_Clause1 = 'Participants are in sound health and have no physical limitations or disabilities that would prevent them from fully enjoying Lessons.';
		$LiabilityRelease_Clause2 = 'Sherpa reserves the right to, without notice, modify Lesson content and/or instructor assignment, including assignment to third-party vendors with an appropriate level of instruction expertise.';
		$LiabilityRelease_Clause3 = 'Participants agree to comply with Sherpa staff instructions during Lessons.';
		$LiabilityRelease_Clause4 = 'Sherpa reserves the right to summarily cancel a Lesson if Sherpa judges that a participant is too sick or injured to participate. Lessons may be cancelled in this way before or during a Lesson.';
		$LiabilityRelease_Clause5 = 'Should an ongoing lesson be cancelled for reason of sickness or injury, Sherpa is not liable for any refund of Lesson fees and/or transportation expenses incurred by the participant. Parents/guardians whose children are taking Lessons are expected to meet the Sherpa instructor without delay and to take immediate custody of their child. Sherpa is not liable for any transportation or other expenses incurred by a parent/guardian.';
		$LiabilityRelease_Clause6 = 'In case of a medical emergency during an ongoing Lesson, participants agree to comply with Sherpa staff instructions as well as instructions given by emergency medical personnel.';
		$LiabilityRelease_Clause7 = 'Registrants and participants agree to comply with the letter of these terms of participation. Sherpa takes no responsibility for the actions of participants who violate these terms. ';
		$LiabilityRelease_Clause8 = 'During an ongoing Lesson, if a participant causes damage to themselves, other persons, or other property/personal belongings, Sherpa agrees to be liable to the extent of its current insurance coverage at the time of the Lesson. Damages that exceed this coverage shall be borne by the participant.';
		$LiabilityRelease_Clause9 = 'Terms of cancellation by participant. 1) If Lesson is cancelled 1 day prior to Lesson start date, or on date of Lesson, Sherpa invoices for 100% of Lesson fees. 2) If Lesson is cancelled 2 days prior to Lesson start date, Sherpa invoices for 50% of Lesson fees. 3) If Lesson is cancelled 3-4 days prior to Lesson start date, Sherpa invoices for 30% of Lesson fees. If Lesson fees have already been paid, Sherpa will deduct the above before refunding any outstanding balance.';
		$LiabilityRelease_Clause10 = 'Sherpa reserves the right to use participants\' images in website content, promotional materials (electronic, print, and video), and other media. All media recorded by Sherpa is the exclusive property of Sherpa.';
		$LiabilityRelease_Clause11 = 'Regarding personal information management. Sherpa commits to the responsible handling of participants\' personal information. Our Privacy Policy is displayed on our website at www.naebasnow.jp, and is also available at our reservation desks within the Naeba Prince Hotel. We welcome guests to review our policy at their convenience.';
		$LiabilityRelease_AgreeToAll = 'I have read and agree to all terms of participation.';
		
		$LiabilityReleaseTextParaBreak = '--ParaBreak--';
		$LiabilityReleaseTextFull = $LiabilityRelease_Clause1 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause2 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause3 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause4 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause5 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause6 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause7 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause8 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause9 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause10 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause11 .$LiabilityReleaseTextParaBreak;
		
	
		$LiabilityReleaseConfirmLabel = 'Confirm and Proceed to Payment';
		$LiabilityRelease_ErrorAdvice = 'Please check all boxes to proceed.';
		
		$LiabilityReleasePDF_ParticipantLabel = 'Student(s)';
		$LiabilityReleasePDF_BuyerLabel = 'Customer Information';
		$LiabilityReleasePDF_AddressLabel = 'Address';
		$LiabilityReleasePDF_RegistrationLabel = 'Date';
		$LiabilityReleasePDF_RegistrationMessage = 'Recorded this date by data submission.';
		$LiabilityRelease_PostalCode = '〒';
		
		
	
	
		// Coupon codes
		$MarketingQuestionCouponCodeLabel = 'Have a coupon code? Enter it here:';
		$CheckCouponCodeLinkLabel = 'Check code';
		
	
	
	
		// Payment
		$PaymentSectionHeader = 'Confirmation and Payment';
	
		$ConfirmationTableHeaderNameLabel = 'Skier Name';
		$ConfirmationTableHeaderDateLabel = 'Date';
		$ConfirmationTableHeaderStartTimeLabel = 'Start Time';
		$ConfirmationTableHeaderLessonStartTimeLabel = 'Tour Type, Start Time';
		$ConfirmationTableHeaderLessonNameLabel = 'Tour Type';
		$ConfirmationTableHeaderPriceLabel = 'Price';
		$ConfirmationTableCouponCodeLabel = 'Coupon code: ';
		$ConfirmationTableTotalLabel ='Order Total: ';
		$ConfirmationTablePaidOnDateTotalLabel = 'Paid by credit card';
		$ConfirmationTableBalanceZeroLabel = 'Balance:';
		$AllPricesIncludeTax = 'All prices include applicable consumption tax.';
	
		$PaymentMethodSectionHeader = 'Please select a payment method.';
	
		$PaymentMethodCreditCard_Title = 'Credit card';
		$PaymentMethodCreditCard_CardsAccepted = 'Cards accepted:';
		$PaymentMethodCreditCard_CardName = 'Card name';
		$PaymentMethodCreditCard_CardNamePlaceholder = 'TARO SNOWSCHOOL';
		$PaymentMethodCreditCard_CardNumber = 'Card number';
		$PaymentMethodCreditCard_CardNumberPlaceholder = 'Valid card number';
		$PaymentMethodCreditCard_ExpirationDate = 'Expiration date';
		$PaymentMethodCreditCard_SecCode = 'Security (CV) code';
		$PaymentMethodCreditCard_SecCodePlaceholder = '3-4 digit code on rear of card';
		$PaymentMethodCreditCard_Explanation = 'Payment by credit card immediately confirms your reservation. Upon successful payment, we will send you confirmation by email.';
		
		$CCForm_NoNameEntered_ErrorAdvice = 'Please enter the cardholder\'s name.';
		$CCForm_NoNumberEntered_ErrorAdvice = 'Please enter the card number.';
		$CCForm_NoCVCEntered_ErrorAdvice = 'Please enter the card\'s security code.';
		
		$PaymentMethodBankTransfer_Title = 'Bank transfer';
		$PaymentMethodBankTransfer_Details = 'Bank: Daishi Bank<br />Branch: Yuzawa<br />Account type: Ordinary<br />Account number: 1282261<br />For fastest processing, please include your lesson start date and name in your transfer information. Ex.: 0125 Smith<br />Bank transfer transactions are only accepted from Japanese banks in Japan.';
		$PaymentMethodBankTransfer_Explanation = 'Note: reservations are confirmed in the order we receive and confirm payment in full for registered students. To ensure your place, please make payment as soon as possible. Also, please understand that Japanese banks do not automatically notify Naeba Snow School of your payment. Typically, we will confirm your payment and contact you by email within 3 business days.';
		
		$RegardingCancellationsHeader = 'Regarding Cancellations';
		$RegardingCancellationsText = 'Cancellation charges are calculated according to number of days prior to your registered lessons:<br>
Cancellation 1 day prior to lesson start date, or on date of lesson: 100% charge.<br>
Cancellation 2 days prior to lesson start date: 50% charge.<br>
Cancellation 3-4 days prior to lesson start date: 30% charge.';
	
		$CompleteRegistration_ButtonLabel = 'Place Order';
	
	
	
	
		// process order and thank you
		$AccountToolsWelcome = 'Welcome, ';
		$AccountToolsGuest = 'Guest';
		$AccountToolsJPSan = 'EN_Error';
		$AccountToolsMgtLink = 'My Account';
		$AccountLoginLink = 'Log in';
		$AccountToolsLogOut = 'Log out';
	
		$ThanksForRegisteringPageHeader = 'Thanks for Registering!';
		$ThanksForOrderingPageHeader = 'Thank You for Your Order!';
	
		$OrderDetailsLabel = 'Order Details';
		$SkierNameColumnLabel = 'Skier';
		$SessionNameColumnLabel = 'Session';
		$DatesColumnLabel = 'Date(s)';
		$PriceColumnLabel = 'Price';
		$OrderSubtotalLabel = 'Subtotal:';
		$OrderDiscountLabel = 'Coupon code discount: ';
		$OrderTotalLabel = 'Total:';

		$ConfirmationMsg_ThanksHeader = 'Thank you for your reservation. Please find below a preparation checklist and information to help you get the most out of your day on the slopes. We look forward to seeing you soon!';
		
		$ConfirmationMsg_RequiredChecklistHeader = 'Mikuni Cat Backcountry Tour Checklist';
		$ConfirmationMsg_RequiredChecklist = '- Snow wear and rental equipment is *not* included in the tour fee. Please take care of your gear requirements before you arrive.<br>
- Tours begin on time. Mikuni Cat reserves the right to cancel or re-schedule tours if skiers are not present at the scheduled start time.<br>
- If you think you might be late for your tour, please call us ahead of time to let us know.';
		$ConfirmationMsg_PrincePointsHeader = 'Prince Points';
		$ConfirmationMsg_PrincePoints = 'If you\'d like to accrue Prince Points from your lesson purchase, please present your Prince Card at our Building #3 or Building #6 reservation counters.
Our reservation staff can validate your receipt for you';
		$ConfirmationMsg_ContactUs = 'Questions? Please mail us at <a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a> or call (+81) (0)70-5550-8807.';
		
		$ConfirmationMsg_PrivateHeader = 'Private Lessons';
		$ConfirmationMsg_Private_MeetingPoint3 = 'Five (5) minutes before your lesson begins, please come to the reservation desk in Building #3.';
		$ConfirmationMsg_Private_MeetingPoint6 = 'Five (5) minutes before your lesson begins, please come to the reservation desk in Building #6.';
		$ConfirmationMsg_Private_HowToMeet = 'Your instructor will call your name.<br>
Please show your instructor a printout of your confirmation page, or show your confirmation email on your phone. If unaccompanied minor(s) will be taking a lesson, please confirm with the instructor where your lesson will disperse.';
		$ConfirmationMsg_Private_LiftTickets = 'Please bring your valid lift ticket(s), one per skier, with you to the lesson. Children under age 12 ski for free at Naeba, but every skier must have a lift ticket. Special kids\' passes can be obtained at the lift ticket window.';		
		
		// only private lessons for EN customers, these are not used
		$ConfirmationMsg_AdultGroupHeader = '';
		$ConfirmationMsg_AdultGroup_Times = '';
		$ConfirmationMsg_AdultGroup_HowToMeet = '';
		$ConfirmationMsg_AdultGroup_LiftTickets = '';
		
		$ConfirmationMsg_JuniorGroupHeader = '';
		$ConfirmationMsg_JuniorGroup_Times = '';
		$ConfirmationMsg_JuniorGroup_HowToMeet = '';
		
		$PrintThisPage = '<a href="javascript:window.print()">Print this page for your records.</a>';
		
		
		
		
		
		
	} else if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'CN')){
		
			$BookNowLabel = '立即預訂';
		
			$AlreadyRegistered = '會員登入';
			$NewRegistration = '新會員註冊';
			$LoginPageTitle = '登入';
			$Login = '登入';
			$LoginHereLabel = '請在此登入';
			$Password = '密碼';
			$Email_Label = 'email信箱';
		
			$NewRegistrationPersonalInfoNotice = '<strong>注意事項及會員相關權益與義務</strong><br>
苗場滑雪學校(Naeba Snow School，以下簡稱「我們」致力保護您個人資料的隱私。我們對於顧客資料的使用，僅限於滑雪課程預約及課程記錄建檔，輔助本公司之市場研究、顧客服務、新產品開發等。若無法提供相關個人資訊，將無法順利預訂課程或享受相關服務。我們絕對不會銷售您的資料，並要求相關企業與供應商仔細謹慎地持有您的資料。如您對我們的安全聲明及隱私保護政策有任何意見，歡迎與我們聯絡<a href="mailto:chinese@naebasnow.jp">chinese@naebasnow.jp</a>。繼續操作此網站，表示您已清楚閱讀注意事項及會員相關權益與義務，並同意遵守相關規定。';
			$RegisterWelcomeMsg = '網站會寄送課程預約信到您的信箱，請輸入您的mail';
			$NewUserRegistrationLink = '新使用者請在此註冊！';
		
			$Password_Label = '密碼';
			$PasswordNew_Label = '輸入密碼';
			$Password_ErrorAdvice = '密碼需至少八個字以上(半型英文/數字)';
			$Password_Advice = '密碼需至少八個字以上(半型英文/數字) ';
			$ReenterPassword = '請再次輸入密碼';
			$ReenterPassword_ErrorAdvice = '密碼錯誤，請確認後再次重新輸入。';
				
			$LessonStartDate = '課程開始日';
			$LessonStartDate_ErrorAdvice = '尚未輸入日期';
			$LessonEndDate = '課程結束日';
			$LessonEndDate_ErrorAdvice = '尚未輸入日期';
		
			$UserPasswordMgt_Label = '密碼管理 ';
			$ReenterPassword_Advice = '';
			$CreateAccount = '新增一個會員帳號';
			$ForgotPwdNotice = '忘記您的密碼？';
			$ForgotPwdEnterEmailLabel = '請輸入您的email信箱，網站將寄送一組臨時密碼給您';
			$ResetPassword = '重設密碼';
			$CreateFreeAccountLabel = '現在新增一個免費帳號！';
		
			
			// header
			$RegistrationPageHeader = '課程預約';
		
			// registration breadcrumbs
			$BreadcrumbBuyerRegLabel = '預約代表人註冊（未成年人的預約請由家長註冊）';
			$BreadcrumbSkierRegLabel = '學員註冊';
			$BreadcrumbSessionSelectLabel = '課程選擇';
			$BreadcrumbConfirmationLabel = '確認';
		
		
		
		
			// buyer registration
			$RegistrationRequiredField = '必填資訊';
		
			$BuyerInformation = '會員資訊';
		
			$FirstNameMei = '名:';
			$FirstNameMei_Advice = '半型英文拼音，例：Xiaomei';
			$FirstNameMei_ErrorAdvice = '請輸入您的名字（拼音，半型英文）';
		
			$FirstNameMei_Kana = '';
			$FirstNameMei_Kana_Advice = ' CN_Error_例：たなか ';
			$FirstNameMei_Kana_ErrorAdvice = '';
		
			$LastNameSei = '姓:';
			$LastNameSei_Advice = '半型英文拼音，例：Wang';
			$LastNameSei_ErrorAdvice = '請輸入您的姓氏（拼音，半型英文）';
		
			$LastNameSei_Kana = '';
			$LastNameSei_Kana_Advice = 'CN_Error_例：たなか';
			$LastNameSei_Kana_ErrorAdvice = '';
		
			$FullName_en = '英文全名(護照拼音):';
			$FullName_jp = 'CN_Error_FullNameOnamae';
			$FullNameBoth = '全名:';
		
		
		
			$Phone = '電話:';
			$Email = 'Email:';
			$PostalCode = '郵遞區號:';
			$PostalCode_SymbolJP = '';
			$CountryOfResidence_Label = '國籍:';
			$Prefecture = '州/縣/省份:';
			$PrefectureOptions = '-- 選擇區域 --';
			$CountryOptions = ' -- 選擇國家 --';
			$CitySuburb = '城市/區:';
			$Address1 = '地址一:';
			$Address2 = '地址二:';
			$EmergencyPhone = '緊急聯絡電話:';
			$EmergencyContact = '緊急聯絡人:';
		
		
			$Phone_Advice = '例：(+886) 3-333-1234';
			$Email_Advice = '例：love_snow@naebasnow.jp';
			$PostalCode_Advice = '例：114';
			$Prefecture_Advice = '例：台北市、廣州省、東京都等';
			$CitySuburb_Advice = '例：文山區、廣州市天河區';
			$Address1_Advice = '例：龜山區萬壽路三巷二弄';
			$Address2_Advice = '例：民生社區1-2號3F';
			$EmergencyPhone_Advice = '例：(+886) 937-123456<br>請輸入您的緊急聯絡電話，非同時參與課程的人。兒童課程請提供家長或陪同來滑雪場大人的手機。我們只會在學生受傷或緊急情況時才會使用您提供的號碼。';
			$EmergencyContact_Advice = '半型英文拼音，例：Wang Danny、父<br>請輸入您的緊急聯絡人及其關係。';
		
		
			$Phone_ErrorAdvice = '請輸入您的電話號碼';
			$Email_ErrorAdvice = '請輸入您的email信箱';
			$PostalCode_ErrorAdvice = '請輸入您的郵遞區號';
			$Prefecture_ErrorAdvice = '請輸入您的縣/省份/州';
			$CitySuburb_ErrorAdvice = '請輸入您的城市/行政區';
			$Address1_ErrorAdvice = '請輸入您的地址';
			$EmergencyPhone_ErrorAdvice = '請輸入';
			$EmergencyContact_ErrorAdvice = '請輸入';
		
			/* $CountryLabel_Japan = 'Japan';
				$CountryLabel_Australia = 'Australia';
			$CountryLabel_Taiwan = 'Taiwan';
			$CountryLabel_HongKong = 'Hong Kong';
			$CountryLabel_Singapore = 'Singapore';
			$CountryLabel_Thailand = 'Thailand';
			$CountryLabel_Indonesia = 'Indonesia';
			$CountryLabel_UK = 'United Kingdom';
			$CountryLabel_USA = 'United States of America';
			$CountryLabel_China = 'China'; */
		
			$CountryLabel_Japan = '日本';
			$CountryLabel_Australia = '澳洲';
			$CountryLabel_Taiwan = '臺灣';
			$CountryLabel_HongKong = '香港';
			$CountryLabel_Singapore = '新加坡';
			$CountryLabel_Thailand = '泰國';
			$CountryLabel_Indonesia = '印度';
			$CountryLabel_UK = '英國';
			$CountryLabel_USA = '美國';
			$CountryLabel_China = '中國';
		
			$UserLanguage = '語言偏好:';
			$CurrentUserLanguageLabelEN = 'English';
			$CurrentUserLanguageLabelJP = '日本語';
			$CurrentUserLanguageLabelCN = '中文';
			$ChangeUserLanguageToJP = '言語設定を日本語にする';
			$ChangeUserLanguageToEN = 'Change site language to English';
			$ChangeUserLanguageToCN = '更改語言設定為中文';
			$CurrentUserLanguageNotSet = '您的語言目前未在系統選項，請聯繫網站管理員。';
		
			$IsSkierLabel = '您本人是否會參與課程';
			$IsSkierYes = '是';
			$IsSkierNo = '否';
			$IsSkier_Advice = '我們將在您註冊後，詢問課程預約資訊。';
		
			$ReceivesNewsletterLabel = '接收最新消息電子報？';
			$ReceivesNewsletterYes = '是，請寄給我。';
			$ReceivesNewsletterNo = '不需要，謝謝。';
			$ReceivesNewsletterExplanation = '接收最新消息及苗場滑雪學校資訊，例：特別促銷價、如何選擇裝備、更多滑雪相關資訊等！';
			$ReceivesNewsletter_ErrorAdvice = '請選擇';
		
			$Submit = '確認完畢';
			$Reset = '重新設定';
			$Button_AdultRegCancel_Label = '取消';
			$Button_AdultRegConfirmLabel = '確認並進行下一步';
			$EditFormContent = '編輯';
		
		
		
		
		
		
			// skier registration
			$SkierInformation = '學員資訊';
		
			$Gender = '性別:';
			$Male = '男';
			$Female = '女';
			$Gender_ErrorAdvice = '請選擇';
		
			$DateOfBirth = '生日:';
			$MonthJanuary = '一月';
			$MonthFebruary = '二月';
			$MonthMarch = '三月';
			$MonthApril = '四月';
			$MonthMay = '五月';
			$MonthJune = '六月';
			$MonthJuly = '七月';
			$MonthAugust = '八月';
			$MonthSeptember = '九月';
			$MonthOctober = '十月';
			$MonthNovember = '十一月';
			$MonthDecember = '十二月';
		
			$SkierIsJuniorMessage = '';
		
			$SkierInstructionLangLabel = '教學語言選擇:';
			$SkierInstructionLangJP = '日文';
			$SkierInstructionLangEN = '英文';
			$SkierInstructionLangCN = '中文';
		
			$SkierLessonTypeLabel = '您想上何種類型的課程?';
			$PreferredLessonTypeLabel = '您有興趣的課程類型？: ';
			$LessonTypeSki = 'Ski(雙板)';
			$LessonTypeSnowboard = 'Snowboard(單板)';
			$LessonTypeBoth = '兩者都想';
			$LessonType_ErrorAdvice = '請選擇您想上課的類型';
		
		
			$AbilityLevelSnowboard = 'Snowboard程度';
			$AbilityLevelSki = 'Ski程度';
		
			$AbilityLevelPleaseSelect = '-- 請選擇 --';
			$LevelBeginnerA = '第一次 = A';
			$LevelBeginnerB = '初級 = B';
			$LevelIntermediateC = '進階初級 = C';
			$LevelIntermediateD = '中級 = D';
			$LevelIntermediateE = '進階中級 = E';
			$LevelExpertF = '高級 = F';
			$ResetWizard = '重新設定';
			$ConfirmWizardLevelSelection = '確認程度';
			$LevelWizard_Title = '<span class="title">程度選擇</span>&nbsp;請在下拉選單中選取您的程度。';
		
			$LevelWizard_SnowboardAdult_SkiedBeforeYes = '我曾經有snowboard(單板)的經驗';
			$LevelWizard_SnowboardAdult_SkiedBeforeNo = '這是我第一次玩snowboard';
			$LevelWizard_SnowboardAdult_SideslipHeelYes = '我可以落葉飄，從山坡上用腳跟或腳尖煞車下來';
			$LevelWizard_SnowboardAdult_SideslipHeelNo = '落葉飄？恩…。我需要學這個';
			$LevelWizard_SnowboardAdult_SideslipToeYes = '我可以落葉飄，從山坡上用腳跟或腳尖煞車下來';
			$LevelWizard_SnowboardAdult_SideslipToeNo = '用腳尖/腳跟煞車？我不確定我行不行';
			$LevelWizard_SnowboardAdult_TurningYes = '我會簡單的基本轉彎';
			$LevelWizard_SnowboardAdult_TurningNo = '轉彎？不太會，我想要加強';
			$LevelWizard_SnowboardAdult_ControlledDescentYes = '在初學者滑道上，我能夠控制速度及轉彎方向';
			$LevelWizard_SnowboardAdult_ControlledDescentNo = '控制速度及方向？我想要加強學習這部分';
			$LevelWizard_SnowboardAdult_AllTerrainYes = '我可以在專業滑道或未整地雪道上，快速且準確地轉彎';
			$LevelWizard_SnowboardAdult_AllTerrainNo = '我正在努力學習專業滑道或未整地雪道上控制速度及方向';
			$LevelWizard_SnowboardAdult_HighPerformanceYes = '我已經準備好邁向snowboard的下一個等級。我想要參與技術檢定或是成為滑雪教練';
			$LevelWizard_SnowboardAdult_HighPerformanceNo = '我不認為我已經準備好參與高等級課程';
		
			$LevelWizard_SnowboardJunior_SkiedBeforeYes = '我有snowboard的經驗或者我曾經上過snowboard課程';
			$LevelWizard_SnowboardJunior_SkiedBeforeNo = '這是我第一次玩snowboard';
			$LevelWizard_SnowboardJunior_SideslipHeelYes = '我可以落葉飄，從山坡上用腳尖煞車下來，我也能夠獨自上下纜車';
			$LevelWizard_SnowboardJunior_SideslipHeelNo = '落葉飄？恩…我需要學這個，我不是很清楚怎麼上下纜車';
			$LevelWizard_SnowboardJunior_SideslipBothTurningYes = '我可以落葉飄，我也會簡單的基本轉彎';
			$LevelWizard_SnowboardJunior_SideslipBothTurningNo = '我的落葉飄不太好，轉彎好像也不太好，我想要加強';
			$LevelWizard_SnowboardJunior_BeginnerRunYes = '在綠線(初級滑道)，我可以連續轉彎，並且準確地煞車';
			$LevelWizard_SnowboardJunior_BeginnerRunNo = '控制速度及轉彎？我想要學這個';
			$LevelWizard_SnowboardJunior_IntermediateRunYes = '在中級滑道，我可以漂亮地連續轉彎，並且準確地煞車';
			$LevelWizard_SnowboardJunior_IntermediateRunNo = '我沒有太多中級滑道的經驗';
			$LevelWizard_SnowboardJunior_HighPerformanceYes = '我已經準備好邁向snowboard的下一個等級。我想要參與技術檢定或是成為滑雪教練';
			$LevelWizard_SnowboardJunior_HighPerformanceNo = '我不認為我已經準備好參與高等級課程';
				
		
			$LevelWizard_Ski_SkiedBeforeYes = '我曾經有ski(雙板)的經驗';
			$LevelWizard_Ski_SkiedBeforeNo = '這是我第一次玩ski';
			$LevelWizard_Ski_StoppingYes = '在我想要停下來的時候，我能夠煞車';
			$LevelWizard_Ski_StoppingNo = '煞車？仍在努力學習中';
			$LevelWizard_Ski_LiftChairYes = '我能夠獨自上下纜車';
			$LevelWizard_Ski_LiftChairNo = '對於上下纜車，我不是很有信心，我需要更多練習';
			$LevelWizard_Ski_BeginnerRunYes = '在綠線(初級滑道)，我可以連續轉彎，並且準確地煞車';
			$LevelWizard_Ski_BeginnerRunNo = '控制速度及轉彎？我想要學這個';
			$LevelWizard_Ski_DifficultRunYes = '我可以在高級滑道上快速Ski且不需要暫停';
			$LevelWizard_Ski_DifficultRunNo = '在高級滑道上，我通常需要暫停幾次';
			$LevelWizard_Ski_AllTerrainYes = '我可以全山滑行（含未整雪區域）';
			$LevelWizard_Ski_AllTerrainNo = '我仍在努力學習專業滑道或顛頗不平區域';
		
			$AbilityLevelWizardAdultSnowboard = '適合您的snowboard教練等級為?<br><a class="level-wizard-snowboard-adult" href="#">使用我們的輔助系統判斷</a>';
			$AbilityLevelWizardJuniorSnowboard = '合適您小孩的snowboard教練等級為?<br><a class="level-wizard-snowboard-junior" href="#">使用我們的輔助系統判斷</a>';
			$AbilityLevelWizardAdultSki = '合適您的ski教練等級為?<br><a class="level-wizard-ski" href="#">使用我們的輔助系統判斷</a>';
			$AbilityLevelWizardJuniorSki = '合適您小孩的Snowboard教練等級為?<br><a class="level-wizard-ski" href="#">使用我們的輔助系統判斷</a>';
		
			
		
			$SkierAllergyQuestion = '食物過敏?';
			$SkierAllergyQuestion_Advice = '如果您的小孩參與我們包含中餐的全天課程，請告訴我們任何可能使他們過敏的食物，及醫生診斷過會過敏的情形';
			$SkierAllergyLabel = '食物過敏：';
		
			$InstructorPrefsDisclaimer = '對於教練，您是否有特別要求? 在班表允許情況下，我們會盡全力滿足您的需求，但請諒解因教練人數有限，無法保證100%達到您的要求。在收到您的預約後，會有專員處理您的特別要求。';
			$InstructorPrefsFemale = '女性教練';
			$InstructorPrefsMale = '男性教練';
			$InstructorPrefsGoodWithKids = '善於兒童教學';
			$InstructorPrefsSameThroughout = '希望為同一位教練對我們多日教學';
			$InstructorPrefsHasCertification = '對教練證照有特別的要求，請指出細節:';
			$InstructorPrefsOther = '其他偏好?請在下方告訴我們:';
			$InstructorPrefsLabel = '對教練的要求: ';
		
		
		
		
			// renting functionality not active, need for future
			$Renting = '請問學員是否需要租雪具?';
			$Yes = '需要';
			$No = '不需要';
			$DominantFoot = '慣用腳';
			$Right = '右腳';
			$Left = '左腳';
			$DominantFoot_Advice = '慣用腳是您snowboard時，後面的那隻腳，通常跟您踢足球時是同一隻腳。';
		
			$SkierHeight_PleaseSelect = '-- 請選擇 --';
			$SkierHeight = '身高：';
			$SkierHeight_Advice = '請選擇　板子長度為減掉您身高五公分的總和';
			$SkierHeight_ErrorAdvice = '請選擇學員身高';
		
			$ShoeSizePleaseSelect = '-- 請選擇 --';
			$ShoeSize = '鞋子尺寸:';
			$ShoeSize_ErrorAdvice = '請選擇學員的鞋子尺寸';
		
		
		
		
		
			$AddNewSkierLabel = '新增其他成員';
		
			$QuitNewSkierLabel = '取消';
			$DataEntryProceedLabel = '下一步';
			$DataEntryCancelLabel = '不取消';
			$DataEntryCancelAlertTitle = '確認取消';
			$DataEntryCancelAlert = '請問您是否要取消這位成員記錄？';
		
			$Button_SkierRegConfirmLabel = '確認並進行下一步';
			$Button_SkierRegCancelLabel = '取消';
			$Button_SkierRegConfirmAddLabel = '確認並且新增新成員';
			
		
		
		
			// Skier session selection
			$ProceedToLessonSelectionLabel= '進行課程選擇';
			$LessonSelectionSectionLabel = '課程選擇';
			$LessonParametersReset = '重新設定';
			$LessonTimePeriodLabel = '上課天數:';
			$LessonTimePeriodLabel_OneDay = '(一天)';
			$LessonStartLocationLabel = '集合地點: ';
			$StartLocation_ErrorAdvice = '請選擇集合地點';
			$LessonStartLocation_Bldg3 = '三號館滑雪學校櫃檯';
			$LessonStartLocation_Bldg6 = '六號館滑雪學校櫃檯';
			$LessonLocationMapLink = '點這裡看飯店地圖';
			$LessonParametersConfirm = '確認';
			$LessonParametersReset = '重新設定';
		
			
			// Lesson descriptions
			$LessonDisplay_isPrivate = '私人';
			$LessonDisplay_isSnowboard = 'Snowboard(單板)';
			$LessonDisplay_isSki = 'Ski(雙板)';
			$LessonDisplay_isAdult = '成人';
			$LessonDisplay_isJunior = '兒童';
			$LessonDisplay_isPeewee = 'CN_Error_isPeewee';
			$LessonLangDisplay_JP = '日文';
			$LessonLangDisplay_EN = '英文';
			$LessonLangDisplay_CN = '中文';
			$LessonLengthDisplay_HalfDay = '半天';
			$LessonLengthDisplay_FullDay = '全天';
			$LessonLengthDisplay_FullDay_NoLunch = 'CN_（1日・ランチなし）';
			$LessonLengthDisplay_FullDay_Lunch = 'CN_（1日・ランチ付き）';
		
			$LessonDescription_AdultGroup = '成人課程，最多八位報名，對象為13歲以上，並分級上課。';
			$LessonDescription_JuniorGroup = '兒童課程，最多六位報名，配合兒童程度及進度的課程，為人數少的小班制課程。';
			$LessonDescription_PeeweeGroup = 'CN_Group description_Error';
			$LessonDescription_Private = '私人小班課程，最多三位，請知悉Ski和Snowboard課程必須分開申請；課程內容將依據客人的要求全力配合。';
			$DesignateInstructor = '指定教練?';
			$DesignateInstructorNameLabel = '教練名字:';
			$DesignateInstructor_Advice = 'CN_指定教練將有指名費。如您需要指定教練，請輸入教練名字，我們將有專員為您確認。';
			$CurrentSkiersInLesson = '已經在此課程中:';
			$NumSpacesAvailable = '(此課程仍有名額: %s)';
		
			// Date display
			$DayOfWeek_Mon = '星期一';
			$DayOfWeek_Tue = '星期二';
			$DayOfWeek_Wed = '星期三';
			$DayOfWeek_Thu = '星期四';
			$DayOfWeek_Fri = '星期五';
			$DayOfWeek_Sat = '星期六';
			$DayOfWeek_Sun = '星期日';
			$MonthLabel = '';
		
			$ResetDaySelectionLink = '清除選擇';
			$SkierNameLabel = '學員：';
			$PreviousSkierLinkLabel = '上一位學員';
			$NextSkierLinkLabel = '下一位學員';
			$CompleteLessonRegistration = '完成註冊及課程選擇';
			
		
		
		
		
		
			// liability release
			$LiabilityReleaseHeader = '滑雪課程申請規範';
			$LiabilityReleaseExplanation = '欲參加由Sherpa股份有限公司(以下簡稱主辦單位)所經營的滑雪學校課程(以下簡稱課程)的客人，請詳細閱讀並同意以下11點事項。若報名者與參加課程者為不同人時，報名者應有責任轉達本規範內容，主辦單位已視參加者皆同意以下事項。';
			$LiabilityRelease_Clause1 = '請控管自身健康狀況，對自身體力感到無法負荷者，請勿參加課程；另外，有食物過敏體質的客人，請在網站申請的備註欄裡詳述。';
			$LiabilityRelease_Clause2 = '主辦單位保留變更課程的權力，另外，主辦單位有權將課程委託給有同等舉辦課程能力的第三方。課程申請者及參加者將不得對課程內容變更及課程二次委託有任何異議。。';
			$LiabilityRelease_Clause3 = '課程參加者將聽從主辦單位所制定的規則及指示行動。';
			$LiabilityRelease_Clause4 = '若因人員生病，受傷等理由，由主辦方判斷而無法參加課程者，不得對主辦方的判斷有任何異議。 ';
			$LiabilityRelease_Clause5 = '課程開始之後，在課程結束前，因前述提及原因（受傷、生病)而無法繼續課程者，將不會退還上課費用，返家費用一切由參加人員負責；若參加人員為孩童，家長需自行前往主辦單位指定的時間及地點迎接小孩，所產生的交通費用須自行負擔。';
			$LiabilityRelease_Clause6 = '課程開始之後，若發生任何事故，請聽從主辦單位及醫護人員的指示行動。';
			$LiabilityRelease_Clause7 = '若課程申請者或參加者因違反本規範而發生任何意外，主辦單位將不負任何責任。';
			$LiabilityRelease_Clause8 = '在課程途中，參加者若受到任何損失，在適用主辦單位所投保的保險內容範圍，將會進行補償；若不適用於保險條款之狀況，參加人員須自行負擔損失。';
			$LiabilityRelease_Clause9 = '課程取消之退費政策，（1）課程前一日下午四點之後取消，將必須支付全額課程款項；（2）課程前一日下午四點前取消者，將收取百分之五十的課程款項；課程兩天前下午四點以前取消課程者，將收取百分之十的課程款項。 ';
			$LiabilityRelease_Clause10 = '在課程中拍照、攝影之版權皆為主辦單位所擁有，並且完全同意主辦單位所取得之照片及影片（包含可清楚辨識參加者的臉孔）可使用在簡章、網站、宣傳廣告中。';
			$LiabilityRelease_Clause11 = '關於個人資訊的管理，除了前述事項以外，有另訂個人資訊保護規範。個人資訊保護規範將刊載在本社官方網站上及滑雪學校櫃台，請到上述地點確認。';
			$LiabilityRelease_AgreeToAll = '我已完全閱讀以上內容並且同意一切規定。';
		
			$LiabilityReleaseTextParaBreak = '--ParaBreak--';
			$LiabilityReleaseTextFull = $LiabilityRelease_Clause1 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause2 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause3 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause4 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause5 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause6 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause7 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause8 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause9 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause10 .$LiabilityReleaseTextParaBreak
										.$LiabilityRelease_Clause11 .$LiabilityReleaseTextParaBreak;
		
			$LiabilityReleaseConfirmLabel = '確認並進行付款';
			$LiabilityRelease_ErrorAdvice = '請閱讀並勾選"注意事項及會員相關權益與義務"！';
		
			$LiabilityReleasePDF_ParticipantLabel = '參與者';
			$LiabilityReleasePDF_BuyerLabel = '家長/監護人';
			$LiabilityReleasePDF_AddressLabel = '地址';
			$LiabilityReleasePDF_RegistrationLabel = '日期';
			$LiabilityReleasePDF_RegistrationMessage = '提交日期';
			$LiabilityRelease_PostalCode = '郵遞區號';
		
		
		
		
			// Coupon codes
			$MarketingQuestionCouponCodeLabel = '折價卷代碼:';
			$CheckCouponCodeLinkLabel = '確認有效性';
			
		
		
		
			// Payment
			$PaymentSectionHeader = '確認並付款';
		
			$ConfirmationTableHeaderNameLabel = '學員名稱';
			$ConfirmationTableHeaderDateLabel = '日期';
			$ConfirmationTableHeaderStartTimeLabel = '開始時間';
			$ConfirmationTableHeaderLessonNameLabel = '課程類型';
			$ConfirmationTableHeaderLessonStartTimeLabel = '課程類型、開始時間';
			$ConfirmationTableHeaderPriceLabel = '價錢';
			$ConfirmationTableCouponCodeLabel = '折價卷';
			$ConfirmationTableTotalLabel ='總金額:';
			$ConfirmationTablePaidOnDateTotalLabel = '通過信用卡支付';
			$ConfirmationTableBalanceZeroLabel = '餘額';
			$AllPricesIncludeTax = '所有價格已含稅';
		
			$PaymentMethodSectionHeader = '請選擇付款方式';
		
			$PaymentMethodCreditCard_Title = '信用卡';
			$PaymentMethodCreditCard_CardsAccepted = '有效的信用卡 ';
			$PaymentMethodCreditCard_CardName = '持有人名';
			$PaymentMethodCreditCard_CardNamePlaceholder = 'WANG XIAOMEI';
			$PaymentMethodCreditCard_CardNumber = '信用卡號碼';
			$PaymentMethodCreditCard_CardNumberPlaceholder = '有效的信用卡號碼';
			$PaymentMethodCreditCard_ExpirationDate = '到期日';
			$PaymentMethodCreditCard_SecCode = '安全碼';
			$PaymentMethodCreditCard_SecCodePlaceholder = '卡片背後的3-4代碼';
			$PaymentMethodCreditCard_Explanation = '使用信用卡付款，請立即確認您的預約內容。';
		
			$CCForm_NoNameEntered_ErrorAdvice = '請輸入信用卡持有人姓名。';
			$CCForm_NoNumberEntered_ErrorAdvice = '請輸入信用卡號碼。';
			$CCForm_NoCVCEntered_ErrorAdvice = '請輸入信用卡背面安全碼。';
			
		
			
		
			$PaymentMethodBankTransfer_Title = '銀行轉帳';
			$PaymentMethodBankTransfer_Details = '日本境內轉賬：如從日本境外轉帳，請使用下方資訊：[銀行名] 第四銀行<br />[分行名] 湯沢支店<br />[帳號類型] 普通戶頭<br />[帳號] 1282261<br />[戶頭名義] 株式会社シェルパ<br />';
			$PaymentMethodBankTransfer_Explanation = '請注意：預約順序將採取先付款的註冊者優先。為確認您的預訂，請在一周內盡快付款。此外，請諒解日本銀行不會主動告知我們您的付款記錄。基本上，我們會在三天內確認您的付款並聯繫您。我們無法主動確定您有沒有匯款成功，請見諒';
			
			$RegardingCancellationsHeader = '關於取消政策:';
			$RegardingCancellationsText = '課程款項一旦付清，預約即成立，取消將會產生手續費，取消手續費將依取消日期而有所不同，取消手續費將從上課日前一日開始起算<br>
課程2天前取消，將收取課程的10%為手續費<br>
課程前日或當日取消，將收取課程的50%為手續費<br>
上課當日未現身或課程開始後才取消，則全額扣款';
			
			$CompleteRegistration_ButtonLabel = '訂單確認';
		
		
		
			// process order and thank you
			$AccountToolsWelcome = '歡迎, ';
			$AccountToolsGuest = '訪客';
			$AccountToolsJPSan = '先生/小姐';
			$AccountToolsMgtLink = '我的帳號';
			$AccountLoginLink = '登入';
			$AccountToolsLogOut = '登出';
		
			$ThanksForRegisteringPageHeader = '感謝您的註冊!';
			$ThanksForOrderingPageHeader = '感謝您的預約!';
		
			$OrderDetailsLabel = '訂單資訊';
			$SkierNameColumnLabel = '學員';
			$SessionNameColumnLabel = '時段';
			$DatesColumnLabel = '日期';
			$PriceColumnLabel = '價錢';
			$OrderDiscountLabel = '使用折價卷: ';
			$SiblingDiscountLabel = '親屬優惠: ';
			$OrderTotalLabel = '總金額:';
		
			$ConfirmationMsg_ThanksHeader = '感謝您預約本次苗場滑雪學校的課程，以下將進行上課前的準備事項及當日報到流程，請務必確認';
		
			$ConfirmationMsg_RequiredChecklistHeader = '上課前準備';
			$ConfirmationMsg_RequiredChecklist = '課程不包含裝備、雪具等周邊配備，請自行準備.
<br>
- 請印出此預約確認信（顯示於手機畫面也可）在上課前把內容提交給教練進行確認.
- 集合地點會因課程內容不同而相異，請注意
- 若無法在集合時間前抵達集合點地點的話，將無法參加課程，請知悉。<br>因塞車、天候造成交通阻塞而無法上課，請聯絡主辦單位';
			$ConfirmationMsg_PrincePointsHeader = '想利用王子飯店卡片之點數集點的客人';
			$ConfirmationMsg_PrincePoints = '請帶著您的卡片跟課程預約書到三號館／六號館的滑雪學校櫃台';
			$ConfirmationMsg_ContactUs = '有任何問題請Mail到 <a href="mailto:chinese@naebasnow.jp">chinese@naebasnow.jp</a> 或者撥打我們的專線 +81-70-5550-8807。';
		
			$ConfirmationMsg_PrivateHeader = '申請私人課程的準備';
			$ConfirmationMsg_Private_MeetingPoint3 = '網路預約的話請在上課前五分鐘到三號館櫃台集合.';
			$ConfirmationMsg_Private_MeetingPoint6 = '網路預約的話請在上課前五分鐘到六號館櫃台集合..';
			$ConfirmationMsg_Private_HowToMeet = '您的教練將會在櫃台前呼喊您的名字.<br>
請在跟教練會合後，提供課程預約書（顯示於手機畫面也可），教練確認完畢後課程即開始.<br>
若參加者為兒童的話，請跟教練確認解散地點.';
			$ConfirmationMsg_Private_LiftTickets = '纜車票請自行購買，小學生以下不需要購買纜車票，但需到纜車賣票口拿孩童專用券.';
		
			
			
			// only private lessons for CN customers, these are not used
			$ConfirmationMsg_AdultGroupHeader = '一般大人團體課程的申請';
			$ConfirmationMsg_AdultGroup_Times = '上午：集合時間9:50，上課時間10:00';
			$ConfirmationMsg_AdultGroup_HowToMeet = '網路預約的話請在上課前十分鐘抵達集合地點，教練會開始點名，與教練會合後請提供課程預約書（顯示於手機畫面也可）給教練確認';
			$ConfirmationMsg_AdultGroup_LiftTickets = '纜車票請自行購買，但根據參加者的程度也可能不需要坐纜車，因此請遵循教練指示';
		
			$ConfirmationMsg_AdultGroupHeader = '一般大人團體課程的申請';
			$ConfirmationMsg_JuniorGroup_Times = '孩童團課程的申請';
			$ConfirmationMsg_JuniorGroup_HowToMeet = '網路預約的話上課前十五分鐘請到指定地點集合，集合、解散地點為室外滑雪場，請家長也穿著防寒衣物。教練在點名時請提供預約確認信，謝謝。';
		
		
			$PrintThisPage = '<a href="javascript:window.print()">列印預約記錄</a>';
		
		
	} else {
		
		// JP language strings, default
		// default site language is JP
		$BookNowLabel = '今すぐお申込';
		
		// login
		$AlreadyRegistered = 'ご登録済みの方';
		$NewRegistration = '新規登録の方';
		$LoginPageTitle = 'ログイン';
		$Login = 'ログイン';
		$LoginHereLabel = 'こちらでログイン';
		$Password = 'パスワード';
		$Email_Label = 'メールアドレス';
		
		$NewRegistrationPersonalInfoNotice = '<strong>個人情報の取り扱いについて</strong><br>
ご請求いただいた資料をお送りする目的の他、商品・サービスおよびその決済方法等に関するご案内、調査および、研究・企画開発に利用します。お客様の意思によりご提供いただけない部分がある場合、手続き・サービス等に支障が生じることがあります。また、商品・案内発送等で個人情報の取り扱いを業務委託しますが、厳重に委託先を管理・指導します。個人情報に関するお問い合わせは、<a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a>にて承ります。なお、本サイトwww.mikuni-cat.com をご利用いただく際、本規約に同意していただいたものとみなされます。';
		
		$RegisterWelcomeMsg = 'お申し込み頂いた方には、メールでこのレッスンのお知らせをお送りしますので、メールアドレスを入力してください。';
		$NewUserRegistrationLink = '新規ご登録の方：こちらをクリック！';
		
		
		$Password_Label = 'パスワード';
		$PasswordNew_Label = '新しいパスワードを入力';
		$Password_ErrorAdvice = '８文字以上のパスワード（半角英数字のみ）を選んでください。';
		$Password_Advice = '８文字以上のパスワード（半角英数字のみ）を選んでください。';
		$ReenterPassword = 'パスワード再入力';
		$ReenterPassword_ErrorAdvice = 'パスワードは一致しません。ご確認ください。';
		 
		$LessonStartDate = 'レッスン開始日';
		$LessonStartDate_ErrorAdvice = 'レッスン開始日を入力してください。';
		$LessonEndDate = 'レッスン終了日';
		$LessonEndDate_ErrorAdvice = 'レッスン終了日を入力してください。';
		
		$UserPasswordMgt_Label = 'パスワードを更新';
		$ReenterPassword_Advice = '';
		$CreateAccount = 'アカウントを作成';
		$ForgotPwdNotice = 'パスワードを忘れた方';
		$ForgotPwdEnterEmailLabel = 'メールアドレスを入力してください。このアドレスに仮のパスワードが送られます。';
		$ResetPassword = 'パスワードをリセット';
		$CreateFreeAccountLabel = 'こちらで！';
		
		
		// header
		$RegistrationPageHeader = '受講のお申し込み';
		
		// registration breadcrumbs
		$BreadcrumbBuyerRegLabel = '申込者登録';
		$BreadcrumbSkierRegLabel = '参加者登録';
		$BreadcrumbSessionSelectLabel = 'ツアー選択';
		$BreadcrumbConfirmationLabel = '登録情報確認';
		
		
		
		
		
		// buyer registration
		$RegistrationRequiredField = '入力必須';
		
		$BuyerInformation = '申込者情報';
		
		$FirstNameMei = '名:';
		$FirstNameMei_Advice = '例：太郎';
		$FirstNameMei_ErrorAdvice = '名前を入力して下さい。';
		
		$FirstNameMei_Kana = '名（カタカナ）:';
		$FirstNameMei_Kana_Advice = '例：タロウ';
		$FirstNameMei_Kana_ErrorAdvice = '名前のカタカナを入力して下さい。';
		
		$LastNameSei = '姓:';
		$LastNameSei_Advice = '例：田中';
		$LastNameSei_ErrorAdvice = '名字を入力して下さい。';
		
		$LastNameSei_Kana = '姓（カタカナ）:';
		$LastNameSei_Kana_Advice = '例：タナカ';
		$LastNameSei_Kana_ErrorAdvice = '名字のカタカナを入力して下さい。';
		
		$FullName_en = 'Full Name, English:';
		$FullName_jp = 'お名前:';
		$FullNameBoth = 'お名前:';
		
		$SalutationJP_Sama = '様';
		
		$Phone = '電話:';
		$Email = 'Eメール:';
		$PostalCode = '郵便番号:';
		$PostalCode_SymbolJP = '〒';
		$CountryOfResidence_Label = '国:';
		$Prefecture = '都道府県:';
		$PrefectureOptions = '-- 都道府県をお選び下さい --';
		$CountryOptions = '-- 国をお選び下さい --';
		$CitySuburb = '市区村町:';
		$Address1 = '住所１:';
		$Address2 = '住所２:';
		$EmergencyPhone = '緊急連絡先:';
		$EmergencyContact = '緊急連絡先の<br />名前、関係:';

		$Phone_Advice = '例: 03-5668-1234（半角英数字）'; 
		$Email_Advice = '例: fresh-powder@mikuni-cat.com';
		$PostalCode_Advice = '例: 105-0014（半角英数字）';
		$Prefecture_Advice = '例: New South Wales, Taipei, Guangdong, etc.';
		$CitySuburb_Advice = '例: 世田谷区';
		$Address1_Advice = '例: 南笠原 5-9-26';
		$Address2_Advice = '例: グリーンパレス 205';
		$EmergencyPhone_Advice = '例: 080-5555-1234（半角英数字）<br />参加者本人ではない<br />緊急連絡先を入力して下さい。';
		$EmergencyContact_Advice = '例: 鈴木 太郎、父　<br />緊急連絡先の名前と関係を入力して下さい。';
		
		$Phone_ErrorAdvice = '電話番号を入力して下さい。';
		$Email_ErrorAdvice = 'メールアドレスを入力して下さい。';
		$PostalCode_ErrorAdvice = '郵便番号を入力して下さい。';
		$Prefecture_ErrorAdvice = '都道府県（州、省）を入力して下さい。';
		$CitySuburb_ErrorAdvice = '市区村町入力して下さい。';
		$Address1_ErrorAdvice = '住所を入力して下さい。';
		$EmergencyPhone_ErrorAdvice = '入力して下さい。';
		$EmergencyContact_ErrorAdvice = '入力して下さい。';
		
		$CountryLabel_Japan = '日本';
		$CountryLabel_Australia = 'オーストラリア';
		$CountryLabel_Taiwan = '台湾';
		$CountryLabel_HongKong = '香港';
		$CountryLabel_Singapore = 'シンガポール';
		$CountryLabel_Thailand = 'タイ';
		$CountryLabel_Indonesia = 'インドネシア';
		$CountryLabel_UK = 'イギリス';
		$CountryLabel_USA = 'アメリカ';
		$CountryLabel_China = '中国';
		
		$UserLanguage = '当サイトの言語設定:';
		$CurrentUserLanguageLabelEN = 'English';
		$CurrentUserLanguageLabelJP = '日本語';
		$CurrentUserLanguageLabelCN = '中文';
		$ChangeUserLanguageToJP = '言語設定を日本語にする';
		$ChangeUserLanguageToEN = 'Change language preference to English';
		$ChangeUserLanguageToCN = '中文';
		$CurrentUserLanguageNotSet = 'Current user language not available. Please contact site administrator.';
		
		$IsSkierLabel = 'この申込者は参加者本人ですか？';
		$IsSkierYes = 'はい';
		$IsSkierNo = 'いいえ';
		$IsSkier_Advice = 'レッスンの好みなどをあとでお聞きします。申込者と参加者が異なる場合は、後ほど
参加者登録に進みます。';
		
		$ReceivesNewsletterLabel = 'メールマガジンへ登録しますか？';
		$ReceivesNewsletterYes = 'はい';
		$ReceivesNewsletterNo = 'いいえ';
		$ReceivesNewsletterExplanation = '三国キャットスキーの最新ニュースをメールでお届けします。素敵な特典、おすすめギアの紹介など、楽しいコンテンツが盛りだくさん！';
		$ReceivesNewsletter_ErrorAdvice = '選択して下さい。';
		
		$Submit = '内容を確認する';
		$Reset = '書き直す';
		$Button_AdultRegCancel_Label = '入力をキャンセル';
		$Button_AdultRegConfirmLabel = 'この内容で確定する';
		$EditFormContent = '編集';
		
		
		
		
		
		
		// skier registration
		$SkierInformation = '参加者情報';
		
		$Gender = '性別:';
		$Male = '男';
		$Female = '女';
		$Gender_ErrorAdvice = '選択して下さい。';
		
		$DateOfBirth = '生年月日:';
		$MonthJanuary = '１月';
		$MonthFebruary = '２月';
		$MonthMarch = '３月';
		$MonthApril = '４月';
		$MonthMay = '５月';
		$MonthJune = '６月';
		$MonthJuly = '７月';
		$MonthAugust = '８月';
		$MonthSeptember = '９月';
		$MonthOctober = '１０月';
		$MonthNovember = '１１月';
		$MonthDecember = '１２月';
		
		$SkierAgeAdvice = '参加年齢は１６歳以上、２０歳未満は保護者の同意が必要です。';
		
		$SkierIsJuniorMessage = '誕生日が2004年4月2日以降の参加者は「ジュニア」として登録されます。ジュニア向けのレッスンが受けられます。';
		
		$SkierInstructionLangLabel = '受講言語:';
		$SkierInstructionLangJP = '日本語';
		$SkierInstructionLangEN = '英語';
		$SkierInstructionLangCN = '中国語';
		
		$SkierLessonTypeLabel = '三国キャットでのスキー種類:';
		$PreferredLessonTypeLabel = '三国キャットでのスキー種類:';
		$LessonTypeSki = 'スキー';
		$LessonTypeSnowboard = 'スノーボード';
		$LessonTypeBoth = '両方';
		$LessonType_ErrorAdvice = 'スキー種類を選択して下さい。';
		
		
		$AbilityLevelSnowboard = 'スノーボードのレベル';
		$AbilityLevelSki = 'スキーのレベル';
		
		$AbilityLevelPleaseSelect = '-- レベルをお選び下さい --';
		$LevelBeginnerA = '初めてです = A';
		$LevelBeginnerB = 'まだ初心者です = B';
		$LevelIntermediateC = '初中級 = C';
		$LevelIntermediateD = '中級 = D';
		$LevelIntermediateE = '中上級 = E';
		$LevelExpertF = '上級 = F';
		$ResetWizard = 'リセット';
		$ConfirmWizardLevelSelection = '決定する';
		$LevelWizard_Title = '<span class="wizard-title-text">レベルウィザード</span>';
		
		$LevelWizard_SnowboardAdult_SkiedBeforeYes = 'スノーボードの経験があります。';
		$LevelWizard_SnowboardAdult_SkiedBeforeNo = 'スノーボードは初めてです。';
		$LevelWizard_SnowboardAdult_SideslipHeelYes = 'かかと側の横滑りができます。';
		$LevelWizard_SnowboardAdult_SideslipHeelNo = '横滑りはできません。';
		$LevelWizard_SnowboardAdult_SideslipToeYes = 'つま先側の横滑りができます。';
		$LevelWizard_SnowboardAdult_SideslipToeNo = 'つま先側の横滑りはうまくできません。';
		$LevelWizard_SnowboardAdult_TurningYes = 'なんとなくターンができます。';
		$LevelWizard_SnowboardAdult_TurningNo = 'ターンができません。';
		$LevelWizard_SnowboardAdult_ControlledDescentYes = '緩斜面（初級コース）でスムーズにターンができ、スピードをしっかりとコントロールしながら滑ることができます。';
		$LevelWizard_SnowboardAdult_ControlledDescentNo = 'そこまでターンやスピードをコントロールできません。';
		$LevelWizard_SnowboardAdult_AllTerrainYes = 'カービングができ、急斜面、不整地を滑ることが出来ます。';
		$LevelWizard_SnowboardAdult_AllTerrainNo = '急斜面、不整地はまだちょっと難しいです。';
		$LevelWizard_SnowboardAdult_HighPerformanceYes = 'ハイパフォーマンスレッスン（カービング、バッヂテスト、イントラ資格など）を受けたいです！';
		$LevelWizard_SnowboardAdult_HighPerformanceNo = 'ハイパフォーマンスレッスンを受けたくないです。';
		
		$LevelWizard_SnowboardJunior_SkiedBeforeYes = 'スノーボードの経験があります。あるいは、スクールに入ったことがあります。';
		$LevelWizard_SnowboardJunior_SkiedBeforeNo = 'スノーボードは初めてです。';
		$LevelWizard_SnowboardJunior_SideslipHeelYes = 'かかと側の横滑りができます。リフトに一人で乗れます。';
		$LevelWizard_SnowboardJunior_SideslipHeelNo = '横滑りはできません。リフトにまだ慣れていません。';
		$LevelWizard_SnowboardJunior_SideslipBothTurningYes = 'かかと側、つま先側、両方の横滑りができます。なんとなくターンができます。';
		$LevelWizard_SnowboardJunior_SideslipBothTurningNo = '横滑り、ターンもあまりよくできません。';
		$LevelWizard_SnowboardJunior_BeginnerRunYes = '緩斜面（初級コース）でターンが両方できます。スピードがでても止まれます。連続ターンが安定しています。';
		$LevelWizard_SnowboardJunior_BeginnerRunNo = 'スピードのコントロールや連続ターンはうまくできません。';
		$LevelWizard_SnowboardJunior_IntermediateRunYes = '中斜面（中級コース）でも連続ターンができます。';
		$LevelWizard_SnowboardJunior_IntermediateRunNo = '中斜面（中級コース）の経験はまだ少ないです。';
		$LevelWizard_SnowboardJunior_HighPerformanceYes = 'JP_I\'m ready to take my snowboard skills to the next level. I\'d like to maybe take a badge test or become a snowboard instructor.';
		$LevelWizard_SnowboardJunior_HighPerformanceNo = 'JP_I don\'t think I\'m ready for a high performance lesson.';
		
		$LevelWizard_Ski_SkiedBeforeYes = 'スキーの経験があります。';
		$LevelWizard_Ski_SkiedBeforeNo = 'スキーは初めてです。';
		$LevelWizard_Ski_StoppingYes = '斜面で止まることができます。';
		$LevelWizard_Ski_StoppingNo = 'まず、斜面で止まれるようになりたいです。';
		$LevelWizard_Ski_LiftChairYes = '一人でリフトに乗れます。（検定４級、４級希望者、ジュニアは検定４級、４級希望者）';
		$LevelWizard_Ski_LiftChairNo = 'リフトにまだ慣れていません。';
		$LevelWizard_Ski_BeginnerRunYes = '緩斜面で滑らかに連続ターンができます。（検定３級、３級希望者、ジュニアは検定２級、２級希望者）';
		$LevelWizard_Ski_BeginnerRunNo = '連続ターンはうまくできません。';
		$LevelWizard_Ski_DifficultRunYes = '急斜面をパラレルターンで止まらずに滑れます。（検定２級、２級希望者、ジュニアは検定１級、１級希望者）';
		$LevelWizard_Ski_DifficultRunNo = '急斜面をパラレルターンで滑れません。';
		$LevelWizard_Ski_AllTerrainYes = '中学生以上で、不整地をスムーズに滑れます。（検定１級、１級希望者。）';
		$LevelWizard_Ski_AllTerrainNo = '不整地はまだちょっと難しいです。または、まだ小学生です。';
		
		$AbilityLevelWizardAdultSnowboard = 'あなたにぴったりな受講レベルは？ <br /><a class="level-wizard-snowboard-adult" href="#">レベルウィザードをご利用ください。</a>';
		$AbilityLevelWizardJuniorSnowboard = 'あなたにぴったりな受講レベルは？ <br /><a class="level-wizard-snowboard-junior" href="#">レベルウィザードをご利用ください。</a>';
		$AbilityLevelWizardAdultSki = 'あなたにぴったりな受講レベルは？ <br /><a class="level-wizard-ski" href="#">レベルウィザードをご利用ください。</a>';
		$AbilityLevelWizardJuniorSki = 'あなたにぴったりな受講レベルは？ <br /><a class="level-wizard-ski" href="#">レベルウィザードをご利用ください。</a>';
		
		
		
		$SkierAllergyQuestion = '食べ物のアレルギー';
		$SkierAllergyQuestion_Advice = 'ジュニア１日レッスン（ランチ付き）の方のみ。食べ物のアレルギーを教えて下さい。アナフィラキシーが診断された場合、エピペンの無有も教えて下さい。';
		$SkierAllergyLabel = '食べ物のアレルギー：';
		
		$InstructorPrefsDisclaimer = 'コーチに対して希望はありますか？シフトなどの都合でお約束はできませんが、なるべくお客様の好みに合わせます。（プライベートレッスン受講選択の場合のみ。）';
		$InstructorPrefsFemale = '女性のコーチ';
		$InstructorPrefsMale = '男性のコーチ';
		$InstructorPrefsGoodWithKids = 'こどもが大好きなコーチ';
		$InstructorPrefsSameThroughout = '数日間のレッスンに同じコーチ';
		$InstructorPrefsHasCertification = 'SAJ又はSIAの有資格者';
		$InstructorPrefsOther = 'その他のお好みがあれば、ご記入ください：';
		$InstructorPrefsLabel = 'コーチに対して希望：';
		
		
		
		$AllergyQuestion = 'アレルギー体質ですか？';
		
		
		// renting functionality not active, need for future
		$Renting = 'この方はレンタル';
		$Yes = 'はい';
		$No = 'いいえ';
		$DominantFoot = '利き足';
		$Right = '右';
		$Left = '左';
		$DominantFoot_Advice = 'サッカーボールを蹴る際、この足を使います。';
		
		$SkierHeight = '身長:';
		$SkierHeight_PleaseSelect = '-- お選び下さい --';
		$SkierHeight_Advice = '身長を上の5cm単位にしてください。';
		$SkierHeight_ErrorAdvice = '身長を選択して下さい。';
		
		$ShoeSizePleaseSelect = '-- お選び下さい --';
		$ShoeSize = '靴のサイズ:';
		$ShoeSize_ErrorAdvice = '靴のサイズを選択して下さい。';
		
		
		
		
		
		
		$AddNewSkierLabel = 'もう一人の参加者を追加';
		
		$QuitNewSkierLabel = '新規入力をキャンセル';
		$DataEntryProceedLabel = 'キャンセルを実行';
		$DataEntryCancelLabel = 'キャンセルを中止';
		$DataEntryCancelAlertTitle = 'キャンセルの確認';
		$DataEntryCancelAlert = '新規入力をキャンセルしますか? ';
		
		$Button_SkierRegConfirmLabel = 'この内容で確定する';
		$Button_SkierRegCancelLabel = '入力をキャンセル';
		$Button_SkierRegConfirmAddLabel = 'もう一人を追加';
	
		
		
		
		
		
		
		// skier lesson selection
		$ProceedToLessonSelectionLabel= 'レッスン選択に進む';
		$LessonSelectionSectionLabel = 'レッスン選択';
		$LessonParametersReset = 'リセット';
		$LessonTimePeriodLabel = '受講期間:';
		$LessonTimePeriodLabel_OneDay = '（日帰り）';
		$LessonStartLocationLabel = '集合場所: ';
		$StartLocation_ErrorAdvice = '集合場所をお選び下さい';
		$LessonStartLocation_Bldg3 = '3号館';
		$LessonStartLocation_Bldg6 = '6号館';
		$LessonLocationMapLink = '地図を見る';
		$LessonParametersConfirm = 'この条件で検索する';
		$LessonParametersReset = '条件をリセット';
		
		
		// lesson descriptions
		$LessonDisplay_isPrivate = 'プライベート';
		$LessonDisplay_isSnowboard = 'スノーボード';
		$LessonDisplay_isSki = 'スキー';
		$LessonDisplay_isAdult = '一般';
		$LessonDisplay_isJunior = 'ジュニア';
		$LessonDisplay_isPeewee = 'キッズ';
		
		$LessonDisplay_isMikuniTour = '三国バックカントリーツアー';
		$LessonDisplay_isMikuniGearRental = 'アバランチ安全装備レンタル';
		
		$LessonLangDisplay_JP = '日本語';
		$LessonLangDisplay_EN = '英語';
		$LessonLangDisplay_CN = '中国語';
		$LessonLengthDisplay_HalfDay = '半日';
		$LessonLengthDisplay_FullDay = '１日';
		$LessonLengthDisplay_FullDay_NoLunch = ' (1日・ランチなし)';
		$LessonLengthDisplay_FullDay_Lunch = ' (1日・ランチ付き)';
		$LessonDescription_AdultGroup = '大人のための一般レッスン。対象年齢13歳以上のレベル別グループレッスンです。1グループの参加者は８人程度まで。';
		$LessonDescription_JuniorGroup = '子供のためのジュニアレッスン。お子様のレベル・ペースに合わせたグループレッスンです。参加者６人程度までの少人数制。';
		$LessonDescription_PeeweeGroup = '5歳以上の未就学のお子様向け、５名様程度のグループレッスン。リフトを使わないレッスンになります。';
		$LessonDescription_Private = 'プライベートレッスン。参加者３人まで。スノーボード、スキーのプライベートなレッスンです。ご要望に最大限にお応えします。';
		$LessonDescription_isMikuniTour = 'プライベートグループのツアー。参加者８人まで。';
		$LessonDescription_isMikuniGearRental = 'アバランチ安全装備を備えたリュックサック。ビーコン、スノープローブ、スコープを含む。お持ちでない方はレンタル必須。';
		
		
		$DesignateInstructor = 'インストラクター指名をご希望？';
		$DesignateInstructorNameLabel = 'インストラクター指名：';
		$DesignateInstructor_Advice = '指名料金１時間つき¥500となります。';
		$CurrentSkiersInLesson = '現在の参加者：';
		$NumSpacesAvailable = '(空きあと %s 名様)';
		
		// Date display
		$DayOfWeek_Mon = '月';
		$DayOfWeek_Tue = '火';
		$DayOfWeek_Wed = '水';
		$DayOfWeek_Thu = '木';
		$DayOfWeek_Fri = '金';
		$DayOfWeek_Sat = '土';
		$DayOfWeek_Sun = '日';
		$MonthLabel = '月';
		
		$CalendarMonthLabel = '2017年3月';
		$AvailabilityPageTitle = '現在の空き状況';
		
		$ResetDaySelectionLink = '選択肢をクリア';
		$SkierNameLabel = '参加者：';
		$PreviousSkierLinkLabel = '前の参加者';
		$NextSkierLinkLabel = '次の参加者';
		$CompleteLessonRegistration = 'ツアーの登録を終了して進む';
		
		
		
		
		// liability release
		$LiabilityReleaseHeader = 'スノーレッスン申込規約'; 
		$LiabilityReleaseExplanation ='株式会社シェルパ（以下「主催者」といいます。）が運営する苗場スノースクールのレッスン（以下「本レッスン」といいます。）にご参加いただくにあたり、以下のすべての事項にご同意いただきます。なお、申込者と参加者が異なる場合は、申込者が責任をもって本規約の内容を参加者にお伝えください。当該参加者も本規約に同意したものとみなします。'; 
		$LiabilityRelease_Clause1 = '健康状態には細心の注意を払い、健康に問題があると考えられる場合には本レッスンへの参加を控えて下さい。'; 
		$LiabilityRelease_Clause2 = '主催者の判断により、本レッスンの内容が変更される可能性、又は主催者と同等の技能を有する事業者に本レッスンの実施業務を再委託することがあります。申込者及び参加者は、変更された内容、本レッスンの実施業務の再委託について異議を申し立てることができません。'; 
		$LiabilityRelease_Clause3 = '参加者は主催者が定めた規則等の決まり事、指示等に従い行動していただきます。'; 
		$LiabilityRelease_Clause4 = '参加者の病気、怪我等の理由により、参加者が本レッスンに参加することが困難と主催者が判断した場合、当該参加者は本レッスンに参加できず、主催者の当該判断に異議を申し立てることはできません。'; 
		$LiabilityRelease_Clause5 = '本レッスン開始後、終了前の段階において、前項に基づき参加者が本レッスンに参加できなくなった場合でも、参加費の返却は一切行わず、帰宅にかかる費用等の一切は参加者負担となります。また、申込者が自らの子女等を本レッスンに参加させた場合、申込者は、参加者を主催者の指定する時間・場所に迎えに来ていただきます。なお、その際にかかる交通費等一切の費用は申込者負担となります。'; 
		$LiabilityRelease_Clause6 = '本レッスン開始後、事故が起きた場合、主催者、及び応急医療スタッフの指示に従い行動していただきます。'; 
		$LiabilityRelease_Clause7 = '申込者又は参加者が本規約に違反したことを理由として生じた事故等について、主催者は一切の責任を負いません。'; 
		$LiabilityRelease_Clause8 = '本レッスン中、参加者が何らかの損害を被った場合、主催者が加入している損害保険が適用となる範囲内でのみ補償され、当該保険によって補償されなかった損害については、自ら負担することに同意していただきます。';
		$LiabilityRelease_Clause9 = 'キャンセルの場合の返金条件について（１）レッスン前日～当日：レッスン代金の１００％（２）レッスン２日前：レッスン代金の５０％（３）レッスン４日～３日前：レッスン代金の３０％　をお支払いいただきます。（入金済みの場合は、上記を除いた金額のご返金となります。）';
		$LiabilityRelease_Clause10 = '本レッスン中に撮影された写真・動画等に関する権利の一切は主催者が有すること、及び主催者の判断により当該写真・動画等（参加者の顔が認識できるものを含みます。）についてはパンフレット、ＷＥＢサイトへの掲載等、主催者の広報活動に利用されることに同意していただきます。';
		$LiabilityRelease_Clause11 = '個人情報の取り扱いに関しては、前項の事項以外については、別途定める個人情報保護方針に従い取り扱います。個人情報保護方針については当社ウェブサイト上に掲載すると共に、受付に写しを備え置きしておりますので、ご確認下さい。';
		$LiabilityRelease_AgreeToAll = '私は、上記内容について理解した上、すべて同意します。';
		
		$LiabilityReleaseTextParaBreak = '--ParaBreak--';
		$LiabilityReleaseTextFull = $LiabilityRelease_Clause1 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause2 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause3 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause4 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause5 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause6 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause7 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause8 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause9 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause10 .$LiabilityReleaseTextParaBreak
									.$LiabilityRelease_Clause11 .$LiabilityReleaseTextParaBreak;
		
		
		$LiabilityReleaseConfirmLabel = '確認してお支払いに進む';
		$LiabilityRelease_ErrorAdvice = 'すべてのチェックを入れてください。';
		
		$LiabilityReleasePDF_ParticipantLabel = '参加者';
		$LiabilityReleasePDF_BuyerLabel = '申込者';
		$LiabilityReleasePDF_AddressLabel = '住所';
		$LiabilityReleasePDF_RegistrationLabel = '申込日';
		$LiabilityReleasePDF_RegistrationMessage = 'この日付で同意書を確かに承りました。';
		$LiabilityRelease_PostalCode = '〒';
		
		
		

		// Coupon codes
		$MarketingQuestionCouponCodeLabel = 'クーポンコードをお持ちの方、こちらに入力してください。 ';
		$CheckCouponCodeLinkLabel = 'コードを確認';
		
		
		
		// payment
		$PaymentSectionHeader = 'ご注文内容確認とお支払い';
		
		$ConfirmationTableHeaderNameLabel = '参加者';
		$ConfirmationTableHeaderDateLabel = '日付';
		$ConfirmationTableHeaderStartTimeLabel = '開始時間';
		$ConfirmationTableHeaderLessonNameLabel = 'レッスン名';
		$ConfirmationTableHeaderLessonStartTimeLabel = 'レッスン名、開始時間';
		$ConfirmationTableHeaderPriceLabel = '価格';
		$ConfirmationTableCouponCodeLabel = 'クーポンコード使用： ';
		$ConfirmationTableTotalLabel = '合計';
		$ConfirmationTablePaidOnDateTotalLabel = 'クレジットカードでのお支払い';
		$ConfirmationTableBalanceZeroLabel = '残高';
		$AllPricesIncludeTax = '全ての価格表示は税込みです。';
		
		$PaymentMethodSectionHeader = 'お支払い方法';
		
		$PaymentMethodCreditCard_Title = 'クレジットカード';
		$PaymentMethodCreditCard_CardsAccepted = 'ご使用可能のクレジットカード';
		$PaymentMethodCreditCard_CardName = 'カード名義';
		$PaymentMethodCreditCard_CardNamePlaceholder = 'TARO SNOWSCHOOL';
		$PaymentMethodCreditCard_CardNumber = 'カード番号';
		$PaymentMethodCreditCard_CardNumberPlaceholder = '有効なカード番号';
		$PaymentMethodCreditCard_ExpirationDate = '有効期限';
		$PaymentMethodCreditCard_SecCode = 'セキュリティ番号';
		$PaymentMethodCreditCard_SecCodePlaceholder = 'カードのうらにある、3〜4桁の番号';
		$PaymentMethodCreditCard_Explanation = 'すぐにレッスン予約が確定されますので、クレジットカードのご使用をおすすめします。';

		$CCForm_NoNameEntered_ErrorAdvice = 'カード名義を入力してください。';
		$CCForm_NoNumberEntered_ErrorAdvice = 'カード番号を入力してください。';
		$CCForm_NoCVCEntered_ErrorAdvice = 'カードのセキュリティコードを入力してください。';
		
		
		
		$PaymentMethodBankTransfer_Title = '銀行振込';
		$PaymentMethodBankTransfer_Details = '[銀行] 第四銀行<br />[支店] 湯沢支店<br />[預金種目] 普通口座<br />[口座番号] 1282261<br />[口座名義] 株式会社シェルパ<br />お振込の際、受講開始月日　＋　申込者名義をご記入ください。　例：０１０１ナエバタロウ<br />恐れ入れますが、1週間以内のご入金をお待ちしております。';
		$PaymentMethodBankTransfer_Explanation = "指定の銀行口座に入金確認ができ次第、申込み確定のメールをお送りいたします。
入金確認ができない場合は、確定できませんので、予めご了承ください。";
		
		
		$RegardingCancellationsHeader = 'キャンセルについて';
		$RegardingCancellationsText = 'キャンセル料は以下の通り頂戴いたします。<br />
レッスン前日～当日：レッスン代金の100%<br />
レッスン２日前：レッスン代金の50%<br />
レッスン４日～３日前：レッスン代金の30%';
		
		$CompleteRegistration_ButtonLabel = '注文を確定する';
		
		
		
		
		
		
		// process order and thank you
		$AccountToolsWelcome = 'ようこそ、';
		$AccountToolsGuest = 'ゲスト';
		$AccountToolsJPSan = 'さん';
		$AccountToolsMgtLink = 'アカウント設定';
		$AccountLoginLink = 'ログイン';
		$AccountToolsLogOut = 'ログアウト';
		
		$ThanksForRegisteringPageHeader = 'お申込みありがとうございます';
		$ThanksForOrderingPageHeader = 'ご注文を承りました。';
		
		$OrderDetailsLabel = 'ご注文の詳細';
		$SkierNameColumnLabel = 'キャンパー名';
		$SessionNameColumnLabel = 'セッション';
		$DatesColumnLabel = '日付';
		$PriceColumnLabel = '価格';
		$OrderSubtotalLabel = '小計:';
		$OrderDiscountLabel = 'クーポンコード使用:';
		$OrderTotalLabel = '合計: ';
		
		$ConfirmationMsg_ThanksHeader = 'この度は、苗場スノースクールのレッスンをご予約いただきありがとうございます。
レッスン当日までにご準備いただくもの、当日の流れをご説明いたしますので必ずご確認ください。';
		
		$ConfirmationMsg_RequiredChecklistHeader = 'レッスンを受けるためには';
		$ConfirmationMsg_RequiredChecklist = '・レッスン代には、ウェア・道具一式は含まれませんのでご自身で手配お願いいたします。<br>
・この予約確認書をプリントアウトし（携帯電話での表示でも可）提示をお願いいたします。<br>
・集合場所は、レッスンによって場所が異なりますので必ずご確認ください。<br>
・集合時間に遅刻した場合、レッスンが受講できなくなる場合もございます。ご了承ください。<br>
・渋滞・悪天候による交通機関遅延等で受講が難しい場合、必ずご連絡ください。';
		$ConfirmationMsg_PrincePointsHeader = 'プリンスポイント付与希望のお客様';
		$ConfirmationMsg_PrincePoints = '３号館/６号館のスクール受付にプリンスカードと予約確認書をお持ちください。<br>
受付スタッフが確認し、付与させていただきます。';
		$ConfirmationMsg_ContactUs = 'お問い合わせはメール<a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a>又はお電話 025-789-4969 までご連絡ください。';
		
		$ConfirmationMsg_PrivateHeader = 'プライベートレッスンお申し込みの方';
		$ConfirmationMsg_Private_MeetingPoint3 = 'レッスン開始５分前に３号館のスノースクール受付前にお越しください。';
		$ConfirmationMsg_Private_MeetingPoint6 = 'レッスン開始５分前に６号館のスノースクール受付前にお越しください。';
		$ConfirmationMsg_Private_HowToMeet = 'インストラクターが、お客様のお名前をお呼びいたします。<br>
合流できましたらインストラクターにこの予約確認書（又は携帯電話での表示）を提示していただきレッスンスタートです。<br>
お子様だけのレッスンの場合、解散場所をインストラクターとご相談ください。';
		$ConfirmationMsg_Private_LiftTickets = 'リフト券は、各自ご用意ください。小学生以下のお子様はリフト券無料となりますが リフト券売り場にて専用リフト券をお受け取り下さい。';
		
		$ConfirmationMsg_AdultGroupHeader = '一般レッスンお申し込みの方';
		$ConfirmationMsg_AdultGroup_Times = '午前：集合 9:50		レッスン開始 10:00<br>
午後：集合 13:20		レッスン開始 13:30';
		$ConfirmationMsg_AdultGroup_HowToMeet = 'グループレッスンは開始10分前に、プライベートレッスンは時間に遅れないように、各集合場所にお越しください。受講されるレッスンにより集合場所は異なりますので、必ず地図にてご確認ください。<br>
インストラクターが、お客様のお名前をお呼びいたします。合流できましたらインストラクターにこの予約確認書（又は携帯電話での表示）を提示してください。';
		$ConfirmationMsg_AdultGroup_LiftTickets = 'リフト券は、各自ご用意ください。ただし、皆様の技術レベルに合わせたレッスンとなるためリフト乗車しない可能性もございます。
その後、インストラクターの指示に従ってください。';
		
		$ConfirmationMsg_JuniorGroupHeader = 'ジュニアレッスンお申し込みの方';
		$ConfirmationMsg_JuniorGroup_Times = '午前：集合 9:20		レッスン開始 9:30<br>
午後：集合 13:20		レッスン開始 13:30<br>
ランチ付き：集合 9:20		レッスン開始 9:30<br>
★ 解散は集合の時にご案内いたします。';
		$ConfirmationMsg_JuniorGroup_HowToMeet = 'レッスン開始１５分前に集合場所へお越しください。<br>
★ 集合の際、保護者の方に説明がございますのでレッスン開始まで必ずお子様と一緒に居てください。<br>
★ 集合・解散の場所はゲレンデになりますので保護者の方も防寒対策お願いいたします。<br>
インストラクターに、予約確認書を提示してください。<br>
その後、インストラクターの指示に従ってください。';
		
		
		$PrintThisPage = '<a href="javascript:window.print()">このページを印刷する</a>';

		
	}
?>
