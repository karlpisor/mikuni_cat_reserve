<?php
	// default language strings are JP, below in else statement
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
		
		// front page
		$BookNowLabel = 'Book Now';
		
		// page navigation
		$AccountToolsWelcome = 'Welcome, ';
		$AccountToolsGuest = 'Guest';
		$AccountToolsJPSan = 'EN_Error';
		$AccountToolsMgtLink = 'My Account';
		$AccountLoginLink = 'Log in';
		$AccountToolsLogOut = 'Log out';
		$SuccessfulLogoutMessage = 'You have successfully logged out.';
		$HomeLink = 'Home';
		
		// navigation links
		$NavLink_LessonMenu = '';
		$NavLink_FAQ = 'Q&A';
		$NavLink_Certification = '';
		$NavLink_AboutUs = '';
		$NavLink_WorkWithUs = '';
		$NavLink_StaffProfiles = '';
		$NavLink_Blog = '';
		$NavLink_ContactUs = 'Contact Us';
		
	
		// contact us
		$ContactUsHeader = 'Contact Us';
		$ContactUsPhoneText = '<strong>Phone:</strong><br>025-789-4969';
		$ContactUsMailText = '<strong>Mail:</strong><br>
<a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a>';
		$ContactUsOfficeHoursText = '<strong>Office hours:</strong><br>
Monday - Friday<br>10:00 to 16:00 <br>(Closed on holidays)';
	
		
		
		// social media links
		$SocialFacebookLink = 'Visit Mikuni Cat\'s Facebook page';
		$SocialYouTubeLink = 'Visit Mikuni Catl\'s YouTube channel';
		
	
		
		// page and program content
		$PageTitleStem = 'Mikuni Cat';
		
		
		
		
		
	} else {
		
		$BookNowLabel = '今すぐお申込';
		
		// page navigation
		$AccountToolsWelcome = 'ようこそ、';
		$AccountToolsGuest = 'ゲスト';
		$AccountToolsJPSan = 'さん';
		$AccountToolsMgtLink = 'アカウント設定';
		$AccountLoginLink = 'ログイン';
		$AccountToolsLogOut = 'ログアウト';
		$SuccessfulLogoutMessage = 'ログアウトしました。';
		
		$HomeLink = 'ホーム';
		
		// navigation links
		$NavLink_LessonMenu = 'レッスンメニュー';
		$NavLink_FAQ = 'Q&A';
		$NavLink_Certification = '検定';
		$NavLink_AboutUs = '当社について';
		$NavLink_WorkWithUs = 'スタッフ募集';
		$NavLink_StaffProfiles = 'スタッフプロフィール';
		$NavLink_Blog = 'ブログ';
		$NavLink_ContactUs = 'お問い合わせ';
		
		
		// contact us
		$ContactUsHeader = 'お問い合わせ';
		$ContactUsPhoneText = '<strong>電話：</strong><br>025-789-4969';
		$ContactUsMailText = '<strong>メール：</strong><br>
<a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a>';
		$ContactUsOfficeHoursText = '<strong>営業時間：</strong><br>
月曜日〜金曜日、<br>
10:00〜16:00（祝日を除く）';
		
		
		
		// social media links
		$SocialFacebookLink = '当社のFacebookページへ';
		$SocialYouTubeLink = '当社のYouTubeチャンネルへ';
		
		
		
		
		// page and program content
		$PageTitleStem = '三国キャット';
		
		
		
	}
?>