<?php
	// default language strings are JP, below in else statement
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
		
		// login
		$AdminLoginError_NotUnique = 'Access credentials are not unique. Please contact site administrator.';
		$AdminLoginError_EmailInUse = 'That email address is currently in use. If you\'ve already registered, please log in using the registered user form, or create an account with a new email address.';
		$AdminLoginError_NoMatchingUser = 'No user found for those credentials. You must enter a valid email and password to log in.';
		$AdminLoginError_FormError = 'Form validation error. Please enter both your email and password to log in.';
		$AlreadyRegistered = 'Already registered with English Adventure?';
		$NewToEngAdventure = 'New Registration';
		$NewRegistrationPersonalInfoNotice = '<strong>Notice regarding personal information management</strong><br> Naeba Snow School commits to the responsible handling of your personal information. We collect only information necessary to safely run our instructional programs and to communicate with you regarding your participation (including but not limited to marketing offers, surveys, and product development). We will never sell your information to other parties, and require all subcontractors and vendors to handle personal information as carefully as we would ourselves. Questions about our personal information management practices? Please don\'t hesitate to contact us at <a href="mailto:english@naebasnow.jp">english@naebasnow.jp</a>. Continued use of this website indicates that your understanding and acceptance of this policy.';
		$NewUserRegistrationLink = 'New to Naeba Snow School? New users please register here.';
		$RegisterWelcomeMsg = 'To begin your registration, please create a free password-protected user account. Naeba Snow School will send a confirmation email to this email address.';
		
		$LoginPageTitle = 'Parent Login';
		$Login = 'Log in';
		$LoginHereLabel = 'Log in here';
		$Password = 'Password';
		$Email_Label = 'Email';
		
		$Password_Label = 'Password';
		$PasswordNew_Label = 'Enter New Password';
		$Password_ErrorAdvice = 'Please choose a password at least 8 characters in length.';
		$Password_Advice = 'Please choose a password at least 8 characters in length.';
      	$ReenterPassword = 'Re-enter Password';
      	$ReenterPassword_ErrorAdvice = 'Passwords do not match; please check your entry and try again.';
      	
      	$ArrivalDate = 'Arrival Date';
      	$ArrivalDate_ErrorAdvice = 'No date entered. Please enter an arrival date.';
        $DepartureDate = 'Departure Date';
      	$DepartureDate_ErrorAdvice = 'No date entered. Please enter a departure date.';
      
     	
      	$UserPasswordMgt_Label = 'User Password Management';
      	$ReenterPassword_Advice = '';
		$CreateAccount = 'Create Account';
		$ForgotPwdNotice = 'Forgot your password?';
		$ForgotPwdEnterEmailLabel = 'Please enter your email address. A temporary password will be sent to you.';
		$ResetPassword = 'Reset Password';
		$CreateFreeAccountLabel = 'Create a Free Account Now!';
		
		
		// account management
		$AccountEditPageTitle = 'Manage User Account';
		$AccountEditParentLabel = 'Edit Parent/Guardian Record';
		$AccountEditParentPasswordLabel = 'Edit Parent/Guardian Password';
		$AccountEditCamperLabel = 'Edit Camper Record';	
		$AccountEditLabel = 'Edit Account';
		$AccountEditPasswordLabel = 'Edit Password';
		$AccountMailMessagingLabel = 'Email Messaging';
		$AccountNewRegistrationLabel = 'New Camp Registration';
		$AccountDownloadsLabel = 'Download Documents';
		$AccountDownloadsPageHead = 'Available Downloads';
		$AccountNoAvailableDownloads = 'No downloads are available at this time.';
		$LiabilityReleaseTableHeader = 'Liability Releases on File';
		
		$AccountUpdateToolsLabel = 'Account Update Tools';
		$AccountParentUpdateSubmitLabel = 'Update Contact Details';
		$AccountParentPasswordUpdateSubmitLabel = 'Update Password';
		$AccountCamperUpdateSubmitLabel = 'Update Camper Details';
		$AccountLoginLink = 'Log in';
		$AccountShowOrdersLabel = 'View Orders';
		$AccountSupportMailLabel = 'Customer Service';
		
		
		$AccountMailMessageSubmitSuccess = 'Message successfully submitted.';
		$AccountMailMessageSubmit_ErrorAdvice = 'Error with message submit. Contact Web administrator for assistance.';
		$AccountMailMessageSubject_ErrorAdvice = 'No subject entered. Please enter subject and submit again.';
		$AccountMailMessageBody_ErrorAdvice = 'Error. Message is too long, or no message submitted.';
		
		$AccountMailMessageBody_CharLimit = '1000 character limit. Characters remaining:';
		$AccountMailMessageBody_CharsRemaining = '';
		
		$AccountLanguageUpdateSuccess = 'Account language has been updated.';
		$AccountPasswordUpdateSuccess = 'Account password has been updated.';
		$AccountUpdateSuccessPartialString = 'account information updated.';
		$CamperUpdateSuccessPartialString = 'camper information updated.';
		$AccountErrorWithUpdate = 'Error with record update.';
		
		
		
		
		$SuccessfulLogoutMessage = 'You have successfully logged out.';
		
		$MailSalutation = 'Dear';
		$MailNewPasswordSubject = ': Your password has been reset';
		$MailNewPasswordMessage = 'You recently requested a new password for your account.';
		$MailYourNewPasswordIs = 'Your new password is:';
		$MailToReplaceRandomPassword = 'To replace this random password with one of your choosing, log into your account and click on the "Edit Password" link.';
		
		$RegistrationPageHeader = 'Registration';
		$StaffLoginPageHeader = 'Staff Login';
		$StaffRegLabel = 'Staff Registration';
		$Email_Advice = 'Ex: love_snow@naebasnow.jp';
		$Email_ErrorAdvice = 'Please enter your email address.';
		
		$OrderStatusLabel = 'Order status: ';
		$OrderStatusOrdered = 'Ordered';
		$OrderStatusPaid = 'Paid';
		$OrderStatusCancelled = 'Cancelled';
		$OrderPaidDateLabel = 'Date paid: ';
		$OrderJPYLabel = 'JPY ';
		$OrderPaidDate_Advice = 'Use format yyyy-mm-dd';
		$OrderAmountPaidLabel = 'Amount paid: ';
		$OrderAmountPaid_Advice = 'Enter numbers only, no commas';
		$OrderNumberLabel = 'Order number: ';
		$OrderPlacedDateLabel = 'Order date: ';
		$OrderPlacedDate_Advice = 'Use format yyyy-mm-dd';
		
		$OrderCancelledDateLabel = 'Date cancelled: ';
		$OrderCancelledDate_Advice = 'Use format yyyy-mm-dd';
		$OrderCancelledReasonLabel = 'Reason for cancellation: ';
		$OrderCancelledReason_Advice = 'If refund needed, note here. ';
		$OrderRefundedDateLabel = 'Refunded date: ';
		$OrderRefundedDate_Advice = 'Use format yyyy-mm-dd';
		$OrderAmountRefundedLabel = 'Amount refunded: ';
		$OrderCouponCodeEnteredLabel = 'Coupon code as entered: ';
		$OrderNoCouponCodeEnteredMsg = '(No coupon code entered.)';
		$OrderAmountRefunded_Advice = 'Enter numbers only, no commas';
		$OrderProcessingMemo_Label = 'Order processing memo:';
		
		$CamperSessionPleaseSelect = 'Please select camp session';
		$CampSessionSelection = 'Camp Session:';
		
		$OrderLI_Session_isImmersion = 'Immersion';
		$OrderLI_Session_isChallenge = 'Challenge';
		
	
		
		
		
		
		$AdminMale = 'Male';
		$AdminFemale = 'Female';
		
		
	} else if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'CN')){
		$AdminLoginError_NotUnique = '中文_Access credentials are not unique. Please contact site administrator.';
		$AdminLoginError_NoMatchingUser = '中文_メールアドレスまたはパスワードは正しくありません。もう一度入力してください。';
		$AdminLoginError_EmailInUse = '中文_このメールアドレスはすでに登録されています。ご登録済みの場合、下のフォームを使ってログインしてください。あるいは、別のメールアドレスを使ってください。';
		$AdminLoginError_FormError = '中文_Form validation error. Please enter both your email and password to log in.';
		$AlreadyRegistered = '中文_ご登録済みの方';
		$NewToEngAdventure = '中文_新規登録の方';
		$LoginPageTitle = '中文_ログイン';
		$Login = '中文_ログイン';
		$LoginHereLabel = '中文_こちらでログイン';
		$Password = '中文_パスワード';
		$Email_Label = '中文_メールアドレス';
		$NewRegistrationPersonalInfoNotice = '中文_<strong>個人情報の取り扱いについて</strong><br>
ご請求いただいた資料をお送りする目的の他、キャンプ商品・サービスおよびその決済方法等に関するご案内、調査および、研究・企画開発に利用します。お客様の意思によりご提供いただけない部分がある場合、手続き・サービス等に支障が生じることがあります。また、商品・案内発送等で個人情報の取り扱いを業務委託しますが、厳重に委託先を管理・指導します。個人情報に関するお問い合わせは、<a href="mailto:support@english-adventure">support@english-adventure</a>にて承ります。なお、english-adventure.org をご利用いただいた場合、本規約に同意していただいたものとみなされます。';		
		$RegisterWelcomeMsg = '中文_お申し込み頂いた方には、メールでこのキャンプのお知らせをお送りしますので、メールアドレスを入力してください。';		
		$NewUserRegistrationLink = '中文_新規ご登録の方：こちらをクリック！';
		
		
		$Password_Label = '中文_パスワード';
		$PasswordNew_Label = '中文_新しいパスワードを入力';
		$Password_ErrorAdvice = '中文_８文字以上のパスワード（半角英数字のみ）を選んでください。';
		$Password_Advice = '中文_８文字以上のパスワード（半角英数字のみ）を選んでください。';
      	$ReenterPassword = '中文_パスワード再入力';
      	$ReenterPassword_ErrorAdvice = '中文_パスワードは一致しません。ご確認ください。';
      	
      	$ArrivalDate = '中文_到着予定日';
      	$ArrivalDate_ErrorAdvice = '中文_到着予定日を入力してください。';
      	$DepartureDate = '中文_帰宅予定日';
      	$DepartureDate_ErrorAdvice = '中文_帰宅予定日を入力してください。';
     	
      	$UserPasswordMgt_Label = '中文_パスワードを更新';
      	$ReenterPassword_Advice = '中文_';
		$CreateAccount = '中文_アカウントを作成';
		$ForgotPwdNotice = '中文_パスワードを忘れた方';
		$ForgotPwdEnterEmailLabel = '中文_メールアドレスを入力してください。このアドレスに仮のパスワードが送られます。';
		$ResetPassword = '中文_パスワードをリセット';
		$CreateFreeAccountLabel = '中文_こちらで！';
		
		
		// account management
		$AccountEditPageTitle = '中文_アカウント管理';
		$AccountEditParentLabel = '中文_保護者情報を編集';
		$AccountEditParentPasswordLabel = '中文_保護者のパスワードを編集';
		$AccountEditCamperLabel = '中文_キャンパー情報を編集';	
		$AccountEditLabel = '中文_アカウント情報を編集';
		$AccountEditPasswordLabel = '中文_パスワードを編集';
		$AccountMailMessagingLabel = '中文_キャンパーへのメッセージ';
		
		$AccountUpdateToolsLabel = '中文_アカウント情報の編集機能';
		$AccountParentUpdateSubmitLabel = '中文_連絡先情報を更新する';
		$AccountParentPasswordUpdateSubmitLabel = '中文_パスワードを更新する';
		$AccountCamperUpdateSubmitLabel = '中文_キャンパー情報を更新する';
		$AccountLoginLink = '中文_ログイン';
		$AccountShowOrdersLabel = '中文_注文履歴を表示';
		$AccountSupportMailLabel = '中文_お問い合わせメール作成';
		
		$AccountLanguageUpdateSuccess = '中文_言語設定を更新しました。';
		$AccountPasswordUpdateSuccess = '中文_パスワードを更新しました。';
		$AccountUpdateSuccessPartialString = '中文_のアカウント情報を更新しました。';
		$CamperUpdateSuccessPartialString = '中文_のキャンパー情報を更新しました。';
		$AccountErrorWithUpdate = '中文_アカウント情報の更新にエラーが発生しました。';
		
	
		
		
		
		$SuccessfulLogoutMessage = '中文_ログアウトしました。';
		
		$MailSalutation = '中文_様';
		$MailNewPasswordSubject = '中文_: パスワード更新のお知らせ。';
		$MailNewPasswordMessage = '中文_パスワード更新のリクエストをいただきました。';
		$MailYourNewPasswordIs = '中文_新しいパスワードは:';
		$MailToReplaceRandomPassword = '中文_仮のパスワードを新しくするには、アカウント設定のページで, 「パスワードを編集」のリンクにクリックしてください。';
		
		$RegistrationPageHeader = '中文_キャンプ参加お申し込み';
		$StaffLoginPageHeader = '中文_スタッフのログイン';
		$StaffRegLabel = '中文_スタッフの新規登録';
		$Email_ErrorAdvice = '中文_メールアドレスを入力して下さい。';
		$Email_Advice = '中文_例: love_snow@naebasnow.jp';
		
		$OrderStatusOrdered = '中文_受付中';
		$OrderStatusPaid = '中文_支払い済み';
		$OrderStatusCancelled = '中文_キャンセル済み';
		$OrderStatusLabel = '中文_進行状況';
		$OrderPlacedDateLabel = '中文_注文日：';
		$OrderPlacedDate_Advice = '中文_Use format yyyy-mm-dd';
		$OrderPaidDateLabel = '中文_支払日：';
		$OrderPaidDate_Advice = '中文_';
		$OrderAmountPaidLabel = '中文_支払い金額';
		$OrderAmountPaid_Advice = '中文_';
		$OrderJPYLabel = '中文_JPY ';
		$OrderNumberLabel = '中文_注文番号：';
		
		
		$OrderCancelledDateLabel = '中文_キャンセル日付：';
		$OrderCancelledDate_Advice = '中文_Use format yyyy-mm-dd';
		$OrderCancelledReasonLabel = '中文_キャンセルの理由：';
		$OrderCancelledReason_Advice = '中文_If refund needed, note here. ';
		$OrderRefundedDateLabel = '中文_返金日付：';
		$OrderRefundedDate_Advice = '中文_Use format yyyy-mm-dd';
		$OrderAmountRefundedLabel = '中文_返金金額：';
		$OrderAmountRefunded_Advice = '中文_Enter numbers only, no commas';
		$OrderCouponCodeEnteredLabel = '中文_Coupon code as entered: ';
		$OrderNoCouponCodeEnteredMsg = '中文_No coupon code entered.';
		$OrderProcessingMemo_Label = '中文_注文処理のメモ：';
		
		$CamperSessionPleaseSelect = '中文_-- お選び下さい --';
		$CampSessionSelection = '中文_セッション:';
		
		$OrderLI_Session_isImmersion = '中文_Immersion';
		$OrderLI_Session_isChallenge = '中文_Challenge';
		
		
		
		$AdminMale = '中文_男性';
		$AdminFemale = '中文_女性';
		
	} else {
		
		// default site language is JP
		$AdminLoginError_NotUnique = 'Access credentials are not unique. Please contact site administrator.';
		$AdminLoginError_NoMatchingUser = 'メールアドレスまたはパスワードは正しくありません。もう一度入力してください。';
		$AdminLoginError_EmailInUse = 'このメールアドレスはすでに登録されています。ご登録済みの場合、下のフォームを使ってログインしてください。あるいは、別のメールアドレスを使ってください。';
		$AdminLoginError_FormError = 'Form validation error. Please enter both your email and password to log in.';
		$AlreadyRegistered = 'ご登録済みの方';
		$NewToEngAdventure = '新規登録の方';
		$LoginPageTitle = 'ログイン';
		$Login = 'ログイン';
		$LoginHereLabel = 'こちらでログイン';
		$Password = 'パスワード';
		$Email_Label = 'メールアドレス';
		
		
		$NewRegistrationPersonalInfoNotice = '<strong>個人情報の取り扱いについて</strong><br>
ご請求いただいた資料をお送りする目的の他、キャンプ商品・サービスおよびその決済方法等に関するご案内、調査および、研究・企画開発に利用します。お客様の意思によりご提供いただけない部分がある場合、手続き・サービス等に支障が生じることがあります。また、商品・案内発送等で個人情報の取り扱いを業務委託しますが、厳重に委託先を管理・指導します。個人情報に関するお問い合わせは、<a href="mailto:support@naebasnow.jp">support@naebasnow.jp</a>にて承ります。なお、本サイトwww.naebasnow.jp をご利用いただく際、本規約に同意していただいたものとみなされます。';	
		
		
		
		
		
		
		$RegisterWelcomeMsg = 'お申し込み頂いた方には、メールでこのキャンプのお知らせをお送りしますので、メールアドレスを入力してください。';		
		$NewUserRegistrationLink = '新規ご登録の方：こちらをクリック！';
		
		
		$Password_Label = 'パスワード';
		$PasswordNew_Label = '新しいパスワードを入力';
		$Password_ErrorAdvice = '８文字以上のパスワード（半角英数字のみ）を選んでください。';
		$Password_Advice = '８文字以上のパスワード（半角英数字のみ）を選んでください。';
      	$ReenterPassword = 'パスワード再入力';
      	$ReenterPassword_ErrorAdvice = 'パスワードは一致しません。ご確認ください。';
      	
      	$ArrivalDate = '到着予定日';
      	$ArrivalDate_ErrorAdvice = '到着予定日を入力してください。';
      	$DepartureDate = '帰宅予定日';
      	$DepartureDate_ErrorAdvice = '帰宅予定日を入力してください。';
     	
      	$UserPasswordMgt_Label = 'パスワードを更新';
      	$ReenterPassword_Advice = '';
		$CreateAccount = 'アカウントを作成';
		$ForgotPwdNotice = 'パスワードを忘れた方';
		$ForgotPwdEnterEmailLabel = 'メールアドレスを入力してください。このアドレスに仮のパスワードが送られます。';
		$ResetPassword = 'パスワードをリセット';
		$CreateFreeAccountLabel = 'こちらで！';
		
		
		// account management
		$AccountEditPageTitle = 'アカウント管理';
		$AccountEditParentLabel = '保護者情報を編集';
		$AccountEditParentPasswordLabel = '保護者のパスワードを編集';
		$AccountEditCamperLabel = 'キャンパー情報を編集';	
		$AccountEditLabel = 'アカウント情報を編集';
		$AccountEditPasswordLabel = 'パスワードを編集';
		$AccountMailMessagingLabel = 'キャンパーへのメッセージ';
		
		$AccountUpdateToolsLabel = 'アカウント情報の編集機能';
		$AccountParentUpdateSubmitLabel = '連絡先情報を更新する';
		$AccountParentPasswordUpdateSubmitLabel = 'パスワードを更新する';
		$AccountCamperUpdateSubmitLabel = 'キャンパー情報を更新する';
		$AccountLoginLink = 'ログイン';
		$AccountShowOrdersLabel = '注文履歴を表示';
		$AccountSupportMailLabel = 'お問い合わせメール作成';
		
		$AccountLanguageUpdateSuccess = '言語設定を更新しました。';
		$AccountPasswordUpdateSuccess = 'パスワードを更新しました。';
		$AccountUpdateSuccessPartialString = 'のアカウント情報を更新しました。';
		$CamperUpdateSuccessPartialString = 'のキャンパー情報を更新しました。';
		$AccountErrorWithUpdate = 'アカウント情報の更新にエラーが発生しました。';
		
	
		
		
		
		$SuccessfulLogoutMessage = 'ログアウトしました。';
		
		$MailSalutation = '様';
		$MailNewPasswordSubject = ': パスワード更新のお知らせ。';
		$MailNewPasswordMessage = 'パスワード更新のリクエストをいただきました。';
		$MailYourNewPasswordIs = '新しいパスワードは:';
		$MailToReplaceRandomPassword = '仮のパスワードを新しくするには、アカウント設定のページで, 「パスワードを編集」のリンクにクリックしてください。';
		
		$RegistrationPageHeader = 'お申し込み';
		$StaffLoginPageHeader = 'スタッフのログイン';
		$StaffRegLabel = 'スタッフの新規登録';
		$Email_ErrorAdvice = 'メールアドレスを入力して下さい。';
		$Email_Advice = '例: love_snow@naebasnow.jp';
		
		$OrderStatusOrdered = '受付中';
		$OrderStatusPaid = '支払い済み';
		$OrderStatusCancelled = 'キャンセル済み';
		$OrderStatusLabel = '進行状況';
		$OrderPlacedDateLabel = '注文日：';
		$OrderPlacedDate_Advice = 'Use format yyyy-mm-dd';
		$OrderPaidDateLabel = '支払日：';
		$OrderPaidDate_Advice = '';
		$OrderAmountPaidLabel = '支払い金額';
		$OrderAmountPaid_Advice = '';
		$OrderJPYLabel = 'JPY ';
		$OrderNumberLabel = '注文番号：';
		
		
		$OrderCancelledDateLabel = 'キャンセル日付：';
		$OrderCancelledDate_Advice = 'Use format yyyy-mm-dd';
		$OrderCancelledReasonLabel = 'キャンセルの理由：';
		$OrderCancelledReason_Advice = 'If refund needed, note here. ';
		$OrderRefundedDateLabel = '返金日付：';
		$OrderRefundedDate_Advice = 'Use format yyyy-mm-dd';
		$OrderAmountRefundedLabel = '返金金額：';
		$OrderAmountRefunded_Advice = 'Enter numbers only, no commas';
		$OrderCouponCodeEnteredLabel = 'Coupon code as entered: ';
		$OrderNoCouponCodeEnteredMsg = 'No coupon code entered.';
		$OrderProcessingMemo_Label = '注文処理のメモ：';
		
		$CamperSessionPleaseSelect = '-- お選び下さい --';
		$CampSessionSelection = 'セッション:';
		
		$OrderLI_Session_isImmersion = 'Immersion';
		$OrderLI_Session_isChallenge = 'Challenge';
		
		
		
		$AdminMale = '男性';
		$AdminFemale = '女性';
	}
	
	?>