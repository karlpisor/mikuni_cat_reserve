<?php
	// utility script to set the $_SESSION language, called by all pages
										 
	// see if user has reset session language to EN, if so set session language to EN
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'EN')) {
		$_SESSION['UserLang'] = 'EN';
	} 
	// for JP language reset
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'JP')) {
		$_SESSION['UserLang'] = 'JP';
	}
	// for CN language reset
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'CN')) {
		$_SESSION['UserLang'] = 'CN';
	}
	
	// If the user has chosen to switch language settings, process that change and reload page
	if (isset($_GET['UserLang'])) {
	
		// get the user-entered data
		$AdminContactPK = $_SESSION['AdminUserId'];
		$NewUserLanguage = $_GET['UserLang'];
	
		// execute language change
		$changeLanguageResult = Contact::UpdateUserLanguage($NewUserLanguage, $AdminContactPK);
	
		// reset session language
		if(isset($changeLanguageResult) && !empty($changeLanguageResult)){
				
			$_SESSION['UserLang'] = $changeLanguageResult['t_user_language'];
		}
	
			
	} // end
	
	
?>