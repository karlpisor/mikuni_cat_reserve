<?php
	// utility script to set the $_SESSION language, called by all pages
										 
	// default user session is Japanese
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
		$_SESSION['UserLang'] = 'EN';
	} else if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'CN')){
		$_SESSION['UserLang'] = 'CN';
	} else 	{
		$_SESSION['UserLang'] = 'JP';
	}
	
	// see if user has reset session language to EN, if so set session language to EN
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'EN')) {
		$_SESSION['UserLang'] = 'EN';
	} 
	// for JP language reset
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'JP')) {
		$_SESSION['UserLang'] = 'JP';
	}
	// for CN language reset
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'CN')) {
		$_SESSION['UserLang'] = 'CN';
	}
	
	// set language suffix, used to abbreviate code for database fields (typically "en" or "jp")
	if ($_SESSION['UserLang'] == 'EN'){
		$langSuffix = 'en';
	} else if ($_SESSION['UserLang'] == 'CN'){
		$langSuffix = 'cn';
	} else {
		$langSuffix = 'jp';
	}
	
	
?>