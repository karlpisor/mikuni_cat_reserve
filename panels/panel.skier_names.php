	
	<!-- Skier Last, First Names JP, and hiragana fields -->
    <?php if($_SESSION['UserLang'] == 'JP'):?>
	
    <!-- LastNameJP -->  
	<div class="row">
		<label for="SkierLastNameJP"><?php echo $LastNameSei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text" 
			 name="SkierLastNameJP" 
			 id="SkierLastNameJP_<?php echo $k; ?>" 
			 class="required" 
			 autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
			 title="<?php echo $LastNameSei_ErrorAdvice; ?>"
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	// decode to UTF8
		      	echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_lastname_jp'], "UTF-8", "SJIS"); }?>"/>
		<span class="example"><?php echo $LastNameSei_Advice; ?></span>
	</div>
	
	<!-- LastNameJP, furigana (hiragana) -->  
	<div class="row">
		<label for="SkierLastNameJP_Kana"><?php echo $LastNameSei_Kana; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="SkierLastNameJP_Kana"
			 id="SkierLastNameJP_Kana_<?php echo $k; ?>"
			 class="required"
			 title="<?php echo $LastNameSei_Kana_ErrorAdvice; ?>" 
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
			 	// decode to UTF8
		      	echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_lastname_furigana_jp'], "UTF-8", "SJIS"); }?>"/>
		<span class="example"><?php echo $LastNameSei_Kana_Advice; ?></span>
	</div>
	
		
	<!-- FirstNameJP -->  
	<div class="row">
		<label for="SkierFirstNameJP"><?php echo $FirstNameMei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="SkierFirstNameJP"
			 id="SkierFirstNameJP_<?php echo $k; ?>"
			 class="required"
			 autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
			 title="<?php echo $FirstNameMei_ErrorAdvice; ?>" 
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	// decode to UTF8
		      	echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_firstname_jp'], "UTF-8", "SJIS"); }?>"/>
		<span class="example"><?php echo $FirstNameMei_Advice; ?></span>
	</div>
	
	
	<!-- FirstNameJP, furigana (hiragana) -->  
	<div class="row">
		<label for="SkierFirstNameJP_Kana"><?php echo $FirstNameMei_Kana; ?>
		<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="SkierFirstNameJP_Kana"
			 id="SkierFirstNameJP_Kana_<?php echo $k; ?>"
			 class="required"
			 title="<?php echo $FirstNameMei_Kana_ErrorAdvice; ?>" 
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	// decode to UTF8
		      	echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_firstname_furigana_jp'], "UTF-8", "SJIS"); }?>"/>
		<span class="example"><?php echo $FirstNameMei_Kana_Advice; ?></span>
	</div>
	
	
	
	
	
	
	
	
	
	
	<!-- English -->  
	<?php elseif($_SESSION['UserLang'] == 'EN'):?>
	
	<!-- FirstNameJP, no separate entry for English-language names -->  
	<div class="row">
		<label for="SkierFirstNameJP"><?php echo $FirstNameMei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="SkierFirstNameJP"
			 id="SkierFirstNameJP_<?php echo $k; ?>"
			 class="required"
			 autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
			 title="<?php echo $FirstNameMei_ErrorAdvice; ?>" 
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	echo $resultSelectSkiersOfBuyer[$k]['t_firstname_jp']; }?>"/>
		<span class="example"><?php echo $FirstNameMei_Advice; ?></span>
	</div>
	
	<!-- LastNameJP, no separate entry for English-language names -->  
	<div class="row">
		<label for="SkierLastNameJP"><?php echo $LastNameSei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text" 
			 name="SkierLastNameJP" 
			 id="SkierLastNameJP_<?php echo $k; ?>" 
			 class="required" 
			 autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
			 title="<?php echo $LastNameSei_ErrorAdvice; ?>"
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	echo $resultSelectSkiersOfBuyer[$k]['t_lastname_jp']; }?>"/>
		<span class="example"><?php echo $LastNameSei_Advice; ?></span>
	</div>
	
	
	
	
	
	
	
	
	
	
	
	<!-- Chinese -->  
	<?php elseif($_SESSION['UserLang'] == 'CN'):?>
	
	<!-- Different order for Chinese names in Pinyin --> 
	<!-- LastNameJP, no separate entry for English-language names -->  
	<div class="row">
		<label for="SkierLastNameJP"><?php echo $LastNameSei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text" 
			 name="SkierLastNameJP" 
			 id="SkierLastNameJP_<?php echo $k; ?>" 
			 class="required" 
			 autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
			 title="<?php echo $LastNameSei_ErrorAdvice; ?>"
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	echo $resultSelectSkiersOfBuyer[$k]['t_lastname_jp']; }?>"/>
		<span class="example"><?php echo $LastNameSei_Advice; ?></span>
	</div>
	
	<!-- FirstNameJP, no separate entry for English-language names -->  
	<div class="row">
		<label for="SkierFirstNameJP"><?php echo $FirstNameMei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="SkierFirstNameJP"
			 id="SkierFirstNameJP_<?php echo $k; ?>"
			 class="required"
			 autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
			 title="<?php echo $FirstNameMei_ErrorAdvice; ?>" 
			 value="<?php if (isset($resultSelectSkiersOfBuyer)){
		      	
		      	// must have test for resource
		      	echo $resultSelectSkiersOfBuyer[$k]['t_firstname_jp']; }?>"/>
		<span class="example"><?php echo $FirstNameMei_Advice; ?></span>
	</div>
	
	<?php endif; ?>
	