<?php 
	// return output for skier summary display
    // content changes depending on what UserLang is loaded
    
	$resource = '';
	if (isset($updateSkierResult) && !empty($updateSkierResult)) {
		$resource = $updateSkierResult;
	} 
	if (isset($k)){
	if (isset($resultSelectSkiersOfBuyer[$k]) && !empty($resultSelectSkiersOfBuyer[$k])) {
		$resource = $resultSelectSkiersOfBuyer[$k];
	}
	}
	

    if (isset($resource) && !empty($resource)) {
    	
    	if($_SESSION['UserLang'] == 'JP'){
    		// decode to UTF8
    		echo '<span class="jp-name-display name-display">' 
			    .mb_convert_encoding($resource['t_lastname_jp'], "UTF-8", "SJIS") .' ' 
				.mb_convert_encoding($resource['t_firstname_jp'], "UTF-8", "SJIS") 
				.'</span>';
    	
    	} else if($_SESSION['UserLang'] == 'EN'){
    		echo '<span class="en-name-display name-display">'
    			.$resource['t_firstname_jp'] .' '
    			.$resource['t_lastname_jp']
    			.'</span>';
    	} else if($_SESSION['UserLang'] == 'CN'){
    		echo '<span class="cn-name-display name-display">'
    			.$resource['t_lastname_jp'] .' ' 
				.$resource['t_firstname_jp']
				.'</span>';
    	}
    	
    	// display ski, snowboard ability levels
    	if (($resource['n_preferred_lesson_type'] == 1) || 
    		($resource['n_preferred_lesson_type'] == 3)){
    		
	    	if ($resource['n_ability_level_ski'] > 0){
	    		echo '<span class="ability-level-display">';
	    		echo $LessonTypeSki .' ';
	    		switch ($resource['n_ability_level_ski']){
	    			case 1:
	    				echo 'A';
	    				break;
	    			case 2:
	    				echo 'B';
	    				break;
	    			case 3:
	    				echo 'C';
	    				break;
	    			case 4:
	    				echo 'D';
	    				break;
	    			case 5:
	    				echo 'E';
	    				break;
	    			case 6:
	    				echo 'F';
	    				break;
	    		}
	    		echo '</span>';
	    	}
    	}
	    
    	if (($resource['n_preferred_lesson_type'] == 2) ||
    		($resource['n_preferred_lesson_type'] == 3)){
	    
	    	if ($resource['n_ability_level_snowboard'] > 0){
	    		echo '<span class="ability-level-display">';
	    		echo $LessonTypeSnowboard .' ';
	    		switch ($resource['n_ability_level_snowboard']){
	    			case 1:
	    				echo 'A';
	    				break;
	    			case 2:
	    				echo 'B';
	    				break;
	    			case 3:
	    				echo 'C';
	    				break;
	    			case 4:
	    				echo 'D';
	    				break;
	    			case 5:
	    				echo 'E';
	    				break;
	    			case 6:
	    				echo 'F';
	    				break;
	    		}
	    		echo '</span>';
	    	}
    	}
    	
    	// conditional display of instruction language (not always shown)
    	if ($_SESSION['UserLang'] == 'JP'){
    		switch ($resource['t_instruction_language']){
    			case 'JP':
    				// do not show selection of JP instruction language when buyer lang is JP
    				echo '';
    				break;
    			case 'EN':
    				echo '<span class="instruction-lang-display">'
    					 .$SkierInstructionLangLabel .' '
    					 .$SkierInstructionLangEN .'</span>';
    				break;
    			case 'CN':
    				echo '<span class="instruction-lang-display">'
    					 .$SkierInstructionLangLabel .' '
    					 .$SkierInstructionLangCN .'</span>';
    				break;
    		}
    	} else {
    		// all other languages: always show instruction language
    		switch ($resource['t_instruction_language']){
    			case 'JP':
    				echo '<span class="instruction-lang-display">'
    					 .$SkierInstructionLangLabel .' '
    					 .$SkierInstructionLangJP .'</span>';
    				break;
    			case 'EN':
    				echo '<span class="instruction-lang-display">'
    					 .$SkierInstructionLangLabel .' '
    					 .$SkierInstructionLangEN .'</span>';
    				break;
    			case 'CN':
    				echo '<span class="instruction-lang-display">'
    					 .$SkierInstructionLangLabel .' '
    					 .$SkierInstructionLangCN .'</span>';
    				break;
    		}
    	}

    	
    	
	} // end resource check ?>
	