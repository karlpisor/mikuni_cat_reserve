	
	<input type="hidden" 
		   name="SkierLastNameJP" 
		   id="SkierLastNameJP_<?php echo $k; ?>" 
		   value="<?php 
		    if ($_SESSION['UserLang'] == 'JP'){
		   		// decode to UTF8
				echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_lastname_jp'], "UTF-8", "SJIS"); 
			} else {
				echo $resultSelectSkiersOfBuyer[$k]['t_lastname_jp'];
			}
			?>"/>
			
	<input type="hidden" 
		   name="SkierLastNameJP_Kana" 
		   id="SkierLastNameJP_Kana_<?php echo $k; ?>" 
		   value="<?php 
		    if ($_SESSION['UserLang'] == 'JP'){
		   		// decode to UTF8
				echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_lastname_furigana_jp'], "UTF-8", "SJIS"); 
			} else {
				echo $resultSelectSkiersOfBuyer[$k]['t_lastname_furigana_jp'];
			}
			?>"/>
		   
	<input type="hidden" 
		   name="SkierFirstNameJP" 
		   id="SkierFirstNameJP_<?php echo $k; ?>" 
		   value="<?php 
		    if ($_SESSION['UserLang'] == 'JP'){
		   		// decode to UTF8
				echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_firstname_jp'], "UTF-8", "SJIS"); 
			} else {
				echo $resultSelectSkiersOfBuyer[$k]['t_firstname_jp'];
			}
			?>"/>
	
	<input type="hidden" 
		   name="SkierFirstNameJP_Kana" 
		   id="SkierFirstNameJP_Kana_<?php echo $k; ?>" 
		   value="<?php 
		    if ($_SESSION['UserLang'] == 'JP'){
		   		// decode to UTF8
				echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_firstname_furigana_jp'], "UTF-8", "SJIS"); 
			} else {
				echo $resultSelectSkiersOfBuyer[$k]['t_firstname_furigana_jp'];
			}
			?>"/>
	
	
	
	<!-- Skier Last, First Names JP, and hiragana fields -->
    <?php if($_SESSION['UserLang'] == 'JP'):?>
	
	<!-- Name display -->  
	<div class="row">
		<label for="SkierName"><?php echo $FullNameBoth; ?></label>
		<span class="skier-name-display in-form" id="SkierNameDisplay_<?php echo $k; ?>"><?php 
		// decode to UTF8
		echo mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_lastname_jp'], "UTF-8", "SJIS") 
			 .' ' 
		   	 .mb_convert_encoding($resultSelectSkiersOfBuyer[$k]['t_firstname_jp'], "UTF-8", "SJIS"); ?>
		</span>
	</div>
	
	
	
	
	<!-- English -->  
	<?php elseif($_SESSION['UserLang'] == 'EN'):?>
	
	<!-- LastNameJP, no separate entry for English-language names -->  
	<!-- Name display -->  
	<div class="row">
		<label for="SkierName"><?php echo $FullNameBoth; ?></label>
		<span class="skier-name-display in-form" id="SkierNameDisplay_<?php echo $k; ?>"><?php 
		echo $resultSelectSkiersOfBuyer[$k]['t_firstname_jp'] .' ' .$resultSelectSkiersOfBuyer[$k]['t_lastname_jp']; ?>
		</span>
	</div>
	
	
	
	
	<!-- Chinese -->  
	<?php elseif($_SESSION['UserLang'] == 'CN'):?>
	
	<!-- Different order for Chinese names in Pinyin --> 
	<!-- LastNameJP, no separate entry for English-language names -->  
	<!-- Name display -->  
	<div class="row">
		<label for="SkierName"><?php echo $FullNameBoth; ?></label>
		<span class="skier-name-display in-form" id="SkierNameDisplay_<?php echo $k; ?>"><?php 
		echo $resultSelectSkiersOfBuyer[$k]['t_lastname_jp'] .' ' .$resultSelectSkiersOfBuyer[$k]['t_firstname_jp']; ?>
		</span>
	</div>
	
	<?php endif; ?>
	