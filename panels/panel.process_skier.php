<?php
	// loop through list of registered skiers
	for ($g = 0; $g < count($resultSelectSkiersOfOrder); $g++){
		
		// write camper name EN as information block title on confirmation page
		if ($g > 0){
			echo '<h3 class="student-long-top">';
		} else {
			echo '<h3>';
		}
		
		$FullName_String = '';
		if ($_SESSION['UserLang'] == 'JP'){
			$FullName_String = mb_convert_encoding($resultSelectSkiersOfOrder[$g]['t_lastname_jp'], "UTF-8", "SJIS") .' '
							  .mb_convert_encoding($resultSelectSkiersOfOrder[$g]['t_firstname_jp'], "UTF-8", "SJIS") .' ' .$Mail_Salutation;
		} else if ($_SESSION['UserLang'] == 'EN'){
			$FullName_String = $resultSelectSkiersOfOrder[$g]['t_firstname_jp'] .' '
							  .$resultSelectSkiersOfOrder[$g]['t_lastname_jp'];
		} else if ($_SESSION['UserLang'] == 'CN'){
			$FullName_String = $resultSelectSkiersOfOrder[$g]['t_lastname_jp'] .' '
							  .$resultSelectSkiersOfOrder[$g]['t_firstname_jp'];
		}
		echo $FullName_String;
		// end h3 block
		echo '</h3>';
		
		// start new paragraph
		echo '<p>';
		// gender
		if ($resultSelectSkiersOfOrder[$g]['nBool_gender_isFemale']){
			$GenderString = $Mail_GenderLabel .' ' .$Mail_Female;
		} else {
			$GenderString = $Mail_GenderLabel .' ' .$Mail_Male;
		}
		echo $GenderString;
		
		// date of birth
		$DateOfBirthString = $Mail_DateOfBirthLabel .' '
			.$resultSelectSkiersOfOrder[$g]['n_birthmonth'] .'/' 
			.$resultSelectSkiersOfOrder[$g]['n_birthday'] .'/' 
			.$resultSelectSkiersOfOrder[$g]['y_birthyear'];
		echo '&nbsp;&nbsp;' .$DateOfBirthString;
		echo '<br />';
		
		// language of instruction
		$SkierLangOfInstruction = '';
		$mailSkierLangOfInstruction = '';
		
		if (!empty($SkierLangOfInstruction)){
			echo $SkierLangOfInstruction .'<br />';
			$mailSkierLangOfInstruction = $SkierLangOfInstruction ."\r\n";
		}
		
		
		
		// preferred lesson type
		$SkierPreferredLessonType = '';
		$mailSkierPreferredLessonType = '';
		if ($resultSelectSkiersOfOrder[$g]['n_preferred_lesson_type'] == 1){
		
			// ski
			$SkierPreferredLessonType = $Mail_SkierLessonTypeLabel .' ' .$Mail_LessonTypeSki;
		
		} else if ($resultSelectSkiersOfOrder[$g]['n_preferred_lesson_type'] == 2){
		
			// snowboard
			$SkierPreferredLessonType = $Mail_SkierLessonTypeLabel .' ' .$Mail_LessonTypeSnowboard;
		
		} else if ($resultSelectSkiersOfOrder[$g]['n_preferred_lesson_type'] == 3){
		
			// both
			$SkierPreferredLessonType = $Mail_SkierLessonTypeLabel .' ' .$Mail_LessonTypeBoth;
		}
		//echo $SkierPreferredLessonType .'<br />';
		//$mailSkierPreferredLessonType = $SkierPreferredLessonType ."\r\n";
		
		// ski, snowboard ability level (can display up to two lines here)
		$SkiAbilityLevel = '';
		$mailSkiAbilityLevel = '';
		$SnowboardAbilityLevel = '';
		$mailSnowboardAbilityLevel = '';
		
		if ( (isset($resultSelectSkiersOfOrder[$g]['n_ability_level_ski']))
		&&  ($resultSelectSkiersOfOrder[$g]['n_ability_level_ski'] > 0)){
		
			switch($resultSelectSkiersOfOrder[$g]['n_ability_level_ski']){
				case 1:
					$SkiAbilityLevel = $AbilityLevelSki .': ' .$LevelBeginnerA;
					break;
				case 2:
					$SkiAbilityLevel = $AbilityLevelSki .': ' .$LevelBeginnerB;
					break;
				case 3:
					$SkiAbilityLevel = $AbilityLevelSki .': ' .$LevelIntermediateC;
					break;
				case 4:
					$SkiAbilityLevel = $AbilityLevelSki .': ' .$LevelIntermediateD;
					break;
				case 5:
					$SkiAbilityLevel = $AbilityLevelSki .': ' .$LevelIntermediateE;
					break;
				case 6:
					$SkiAbilityLevel = $AbilityLevelSki .': ' .$LevelExpertF;
					break;
		
			}
			
			// output
			if (!empty($SkiAbilityLevel)){
				echo $SkiAbilityLevel .'<br />';
				$mailSkiAbilityLevel = $SkiAbilityLevel ."\r\n";
			}
		}
		
		
		
		if ( (isset($resultSelectSkiersOfOrder[$g]['n_ability_level_snowboard']))
		&&  ($resultSelectSkiersOfOrder[$g]['n_ability_level_snowboard'] > 0)){
		
			switch($resultSelectSkiersOfOrder[$g]['n_ability_level_snowboard']){
				case 1:
					$SnowboardAbilityLevel = $AbilityLevelSnowboard .': ' .$LevelBeginnerA;
					break;
				case 2:
					$SnowboardAbilityLevel = $AbilityLevelSnowboard .': ' .$LevelBeginnerB;
					break;
				case 3:
					$SnowboardAbilityLevel = $AbilityLevelSnowboard .': ' .$LevelIntermediateC;
					break;
				case 4:
					$SnowboardAbilityLevel = $AbilityLevelSnowboard .': ' .$LevelIntermediateD;
					break;
				case 5:
					$SnowboardAbilityLevel = $AbilityLevelSnowboard .': ' .$LevelIntermediateE;
					break;
				case 6:
					$SnowboardAbilityLevel = $AbilityLevelSnowboard .': ' .$LevelExpertF;
					break;
		
			}
			
			// output
			if (!empty($SnowboardAbilityLevel)){
				echo $SnowboardAbilityLevel .'<br />';
				$mailSnowboardAbilityLevel = $SnowboardAbilityLevel ."\r\n";
			}
		}
		
		
		// allergy label (optional)
		$AllergyString = '';
		$mailAllergyString = '';
		if (!empty($resultSelectSkiersOfOrder[$g]['t_contact_food_allergies'])){
			$AllergyString = $SkierAllergyLabel .$resultSelectSkiersOfOrder[$g]['t_contact_food_allergies'];
			
			if ($_SESSION['UserLang'] == 'JP'){
				$AllergyString = $SkierAllergyLabel .mb_convert_encoding($resultSelectSkiersOfOrder[$g]['t_contact_food_allergies'], "UTF-8", "SJIS");
			}
			
			// output
			//echo $AllergyString .'<br />';
			//$mailAllergyString = $AllergyString ."\r\n";
		}
		
		
		// instructor preferences, free text (optional)
		$InstructorPrefsString = '';
		$mailInstructorPrefsString = '';
		
		$InstructorPrefsFreetext = '';
		if (!empty($resultSelectSkiersOfOrder[$g]['t_instructor_prefs'])){
			// must remove the 01, 02, etc. index strings for display
			$InstructorPrefsString = preg_replace('/[0-9]+/', '', $resultSelectSkiersOfOrder[$g]['t_instructor_prefs']);
			if ($_SESSION['UserLang'] == 'JP'){
				$InstructorPrefsString = preg_replace('/[0-9]+/', '', mb_convert_encoding($resultSelectSkiersOfOrder[$g]['t_instructor_prefs'], "UTF-8", "SJIS"));
			}
		}
		if (!empty($resultSelectSkiersOfOrder[$g]['t_instructor_prefs_freetext'])){
			$InstructorPrefsFreetext = $resultSelectSkiersOfOrder[$g]['t_instructor_prefs_freetext'];
			if ($_SESSION['UserLang'] == 'JP'){
				$InstructorPrefsFreetext = mb_convert_encoding($resultSelectSkiersOfOrder[$g]['t_instructor_prefs_freetext'], "UTF-8", "SJIS");
			}
		}
		$InstructorPrefsString = $InstructorPrefsLabel .$InstructorPrefsString .' ' .$InstructorPrefsFreetext;
		
		
		// output
		if (!empty($InstructorPrefsString)){
			//echo $InstructorPrefsString .'<br />';
			//$mailInstructorPrefsString = $InstructorPrefsString ."\r\n";
		}
		
		
		// end paragraph
		echo '</p>';
		
		
		// build skier email message output, set into index for array prepared above
		$skierMailElements[$g] = $Mail_FullNameBoth .' ' .$FullName_String ."\r\n"
									.$GenderString ."\r\n"
									.$DateOfBirthString ."\r\n"
									//.$mailSkierLangOfInstruction
									//.$mailSkierPreferredLessonType
									.$mailSkiAbilityLevel
									.$mailSnowboardAbilityLevel
									//.$mailAllergyString 
									//.$mailInstructorPrefsString
									;
		
				
	} // end for loop, looping through distinct skiers
?>