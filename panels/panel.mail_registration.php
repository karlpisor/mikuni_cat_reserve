<?php
	// set up mail message
	// initialize message string to null
	$message = '';
	
	// set mail text strings from customer data
	if ($_SESSION['UserLang'] == 'EN') {
		$parentGuardianFullName = $DearSamaSalutation .' ' .$parentGuardianFullName_enString;
	} else {
		$parentGuardianFullName = $parentGuardianFullName_jpString .' ' .$DearSamaSalutation;
	}
	
	// salutation begins the message string
	$message .= "$parentGuardianFullName\n\n";
	
	// message salutation, payment advice
	$message .= "$ThanksForRegistering\n\n";
	$message .= "$HowToPayMailAdvice\n\n";
	$message .= "$Mail_HowToPayDetails\n\n";
	$message .= "$NoPaymentNoPlaceNotice\n\n";
	
	
	// registered camper information
	for ($i = 0; $i < count($selectOrderedCampersResult); $i++){
		
		// camper details
		$message .= $camperMailElements[$i];
		
		// session and pricing details
		// session section header
		$message .= "$Confirmation_RegisteredSessionsHeader\n\n";
		
		// registered sessions for this camper
		for ($k = 0; $k < count($selectOrderDetailsByCamperResult); $k++){
			$message .= $camperMailSessionElements[$k];	
		}
		
		
	}
	
	// append completed pricing information to message string
	$message .= "$Mail_AllPricesIncludeTax\n\n";	
	
	
	// parent/buyer information
	// set section header
	$message .= "$ParentGuardianInformation\n";
	
	// build email message
	$parentGuardianMailMessage =
			$parentGuardianName_jpString .'\r\n'
			.$parentGuardianName_enString .'\r\n'
			.$parentGuardianPhoneString .'\r\n'
			.$parentGuardianEmerPhoneString .'\r\n'
			.$parentGuardianEmailString .'\r\n'
			.$parentGuardianPostalCodeString .'\r\n'
			.$parentGuardianPrefectureString .'\r\n'
			.$parentGuardianCitySuburbString .'\r\n'
			.$parentGuardianAddress1String .'\r\n'
			.$parentGuardianAddress2String .'\r\n'
			.$parentGuardianMarketingString .'\r\n'
			.$parentGuardianMarketingFreeString .'\r\n'
			.$parentGuardianMarketingCodeString .'\r\n'
			;
	
	// spacing at end of section
	$message .= "$parentGuardianMailMessage\n\n";
		

	
	
	
	// set recipient and subject
	$to = $mailParentGuardianEmail;
	$subject = $ConfirmationMailSubject .' #' .$_SESSION['OrderId'];
	
	// build headers
	$headers = '';
	$headers .= 'From: English Adventure <registration@english-adventure.org>' . "\r\n";
	$headers .= 'Reply-To: English Adventure Support <support@english-adventure.org>' . "\r\n";
	//$headers .= 'Cc: Dave Paddock <dave@english-adventure.org>' . "\r\n";
	$headers .= 'Bcc: System Admin <karl@missionpeoplesystems.com>' . "\r\n";
	$headers .= 'X-Mailer: PHP/' . phpversion() ."\r\n";
	$headers .= "MIME-Version: 1.0 \r\n" ;
	
	// message footer
	$message .= "$RegardingCancellationsHeader\r\n";
	$message .= "$RegardingCancellationsText\n\n";
	$message .= "$ConfirmEmailFooterSalutation\n\n";
	$message .= "$ConfirmEmailFooter\n";
	
	// if message is to be Japanese, use special encoding for $headers, $subject, $message
	if ($_SESSION['UserLang'] == 'JP'){
		$headers .= "Content-Type: text/plain; charset=ISO-2022-JP \r\n";
		mb_language("ja");
		$subject = mb_convert_encoding($subject, "ISO-2022-JP","AUTO");
		$subject = mb_encode_mimeheader($subject);
		$message = mb_convert_encoding($message, "ISO-2022-JP","AUTO");
	}
	
	// send it
	$mailSent = mail($to, $subject, $message, $headers);
		
		
?>		