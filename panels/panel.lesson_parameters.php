<!-- Date range is shown here, user can update --> 		
<div id="lesson-parameter-selection" class="page-section-body lesson-parameters">

     <div id="LessonSelectionMsg"><?php echo $LessonTimePeriodLabel .'&nbsp;'; 
	     ?><span id="LessonDateRangeLabel"></span><span id="LessonStartLocationLabel"></span>
	     <span>
	     	<a class="edit-link reset" id="LessonParameterReset" href="#"><?php echo $LessonParametersReset; ?></a>
	     </span>
	 </div>


<div id="LessonParametersFormContainer">
        <form action="" 
    	  class="LessonParametersForm" 
    	  name="LessonParametersForm" 
    	  id="LessonParametersForm" />
    	  
    	  
        <!-- Arrival Date and Departure Date -->
	      <div class="row date-entry">
		      <label for="LessonStartDate"><?php echo $LessonStartDate; ?>
			      <span class="StarRequired">*</span></label>
			  <input type="text" 
			      name="LessonStartDate" 
			      id="LessonStartDate" 
			      class="required date-entry date-start-entry date-entry-arrival" 
			      title="<?php echo $LessonStartDate_ErrorAdvice; ?>"
			      value="<?php echo $_SESSION['LessonStartDate']; ?>"/>
		  </div> 
		  <br class="Clear">
		  
		  <!-- Departure Date -->
		  <div class="row date-entry">
		      <label for="LessonEndDate" class=""><?php echo $LessonEndDate; ?>
			      <span class="StarRequired">*</span></label>
			  <input type="text" 
			      name="LessonEndDate" 
			      id="LessonEndDate" 
			      class="required date-entry date-end-entry date-entry-departure" 
			      title="<?php echo $LessonEndDate_ErrorAdvice; ?>"
			      value="<?php echo $_SESSION['LessonFinishDate'];?>"/>
		  </div>
	  
	  
	  
	  <br class="Clear" />
      <button class="btn1" 
				type="submit"
				name="LessonParametersConfirm" 
				id="LessonParametersConfirm">
		 		<span><?php echo $LessonParametersConfirm; ?></span></a>
		</button>
     
        
        </form>
   </div>
  
   
   
</div> 
	    
<!-- End lesson parameters panel -->    