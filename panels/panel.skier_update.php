<!-- Begin skier section body --> 

		<?php 
		// update session control for new skier entry or skier update
		// bool isUpdate = 1 => update of existing skier record, 0 for new skier
		$_SESSION['Bool_isSkierUpdate'] = 1;?>

        <!-- Begin section summary view --> 
        <div id="skier-reg-summary-<?php echo $k; ?>" class="summary-view skier">
        <span id="skier-success-status-<?php echo $k; ?>" class="skier-success-status"></span>
        <p class="summary-content">
        <?php require 'panels/panel.skier_summary_display.php'; ?>
        </p>
        <span><a class="skier edit-link" href="#"><?php echo $EditFormContent; ?></a></span>
        </div>
        <!-- End section summary view --> 
        
        
        
        <div id="skier-reg-section-body-<?php echo $k; ?>" class="page-section-body skier">
        <span class="input-legend input-legend-star StarRequired">*</span>
        <span class="input-legend input-legend-text"><?php echo $RegistrationRequiredField; ?></span>
        


    <form action="" 
    	  class="SkierRegForm" 
    	  name="SkierRegForm_<?php echo $k; ?>" 
    	  id="SkierRegForm_<?php echo $k; ?>" />
        
    <!-- Start skier entry fieldset -->  
	<fieldset>  
	    
	    
    <!-- Start skier entry -->  
	
    <!-- Skier ID -->  
	<input type="hidden" id="SkierPK_<?php echo $k; ?>"
		   value="<?php echo $resultSelectSkiersOfBuyer[$k]['pk_contact']; ?>" />
	
		   
	    
		
	  <!-- Skier Name -->    
	  <?php 
    
    	// check to see if this skier update form is for a buyer
    	// only load the names, non-editable, in this case
    	if($_SESSION['BuyerId'] == $resultSelectSkiersOfBuyer[$k]['pk_contact']){

			// buyer is skier
			require 'panels/panel.skier_is_buyer_names.php';
			
		} else {

			// default
			require 'panels/panel.skier_names.php'; 
		}?>    
     


	  <!-- Gender -->  
      <div class="row gender">
      	  <label for="SkierGender"><?php echo $Gender; ?><span class="StarRequired">*</span></label>
	      <input type="radio" 
	            name="SkierGender_<?php echo $k; ?>" 
	            class="SkierGender_<?php echo $k; ?> required" 
	            id="SkierGenderMale_<?php echo $k; ?>" 
	            title="<?php echo $Gender_ErrorAdvice; ?>"
	            value="0" 
	            <?php if ($resultSelectSkiersOfBuyer[$k]['nBool_gender_isFemale'] == 0){
	                       echo ' checked = "checked"';
	                  }?>>
          <label class="radio" for="SkierGenderMale"><?php echo $Male; ?></label>  
			
            <input type="radio" 
	            name="SkierGender_<?php echo $k; ?>" 
	            class="SkierGender_<?php echo $k; ?> required" 
	            id="SkierGenderFemale_<?php echo $k; ?>" 
	            value="1"
	            <?php if ($resultSelectSkiersOfBuyer[$k]['nBool_gender_isFemale'] == 1){
	                        echo ' checked = "checked"';
	                  }?>>
            <label class="radio" for="SkierGenderFemale"><?php echo $Female; ?></label>
            
      
      </div> 

	
      
      <!-- Skier Language of Instruction --> 
      <div class="row" style="display:none">
	      <label for="SkierInstructionLang_<?php echo $k; ?>"><?php echo $SkierInstructionLangLabel; ?><span class="StarRequired">*</span></label>
		  
		  <select class="lang-select" name="SkierInstructionLang_<?php echo $k; ?>" id="SkierInstructionLang_<?php echo $k; ?>">
                <option value="JP"<?php 
                	if ( (!empty($resultSelectSkiersOfBuyer[$k]['t_instruction_language'])) && 
						($resultSelectSkiersOfBuyer[$k]['t_instruction_language'] == 'JP') ){
						
						// loading from a saved skier record
						echo 'selected="selected"';
						
					} else if( (empty($resultSelectSkiersOfBuyer[$k]['t_instruction_language'])) &&
						($_SESSION['UserLang'] == 'JP') ){
						
						// default = UserLang on load, no instruction language has been selected
						echo 'selected="selected"';

					}?>><?php echo $SkierInstructionLangJP; ?></option>
                <option value="EN"<?php 
                	if ( (!empty($resultSelectSkiersOfBuyer[$k]['t_instruction_language'])) && 
						($resultSelectSkiersOfBuyer[$k]['t_instruction_language'] == 'EN') ){
						
						// loading from a saved skier record
						echo 'selected="selected"';
						
					} else if( (empty($resultSelectSkiersOfBuyer[$k]['t_instruction_language'])) &&
						($_SESSION['UserLang'] == 'EN') ){
						
						// default = UserLang on load, no instruction language has been selected
						echo 'selected="selected"';

					}?>><?php echo $SkierInstructionLangEN; ?></option>
                
          </select>
            
          <span class="example"></span>
      </div> 


      <!-- DateOfBirth -->  
      <div class="row">
	      <label for="SkierBirthMonth"><?php echo $DateOfBirth; ?><span class="StarRequired">*</span></label>
		  
		  <select class="birth-select required" name="SkierBirthMonth_<?php echo $k; ?>" id="SkierBirthMonth_<?php echo $k; ?>">
                <option value="1"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 1) : ?> selected="selected"<?php endif; ?>><?php echo $MonthJanuary; ?></option>
                <option value="2"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 2) : ?> selected="selected"<?php endif; ?>><?php echo $MonthFebruary; ?></option>
                <option value="3"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 3) : ?> selected="selected"<?php endif; ?>><?php echo $MonthMarch; ?></option>
                <option value="4"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 4) : ?> selected="selected"<?php endif; ?>><?php echo $MonthApril; ?></option>
                <option value="5"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 5) : ?> selected="selected"<?php endif; ?>><?php echo $MonthMay; ?></option>
                <option value="6"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 6) : ?> selected="selected"<?php endif; ?>><?php echo $MonthJune; ?></option>
                <option value="7"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 7) : ?> selected="selected"<?php endif; ?>><?php echo $MonthJuly; ?></option>
                <option value="8"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 8) : ?> selected="selected"<?php endif; ?>><?php echo $MonthAugust; ?></option>
                <option value="9"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 9) : ?> selected="selected"<?php endif; ?>><?php echo $MonthSeptember; ?></option>
                <option value="10"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 10) : ?> selected="selected"<?php endif; ?>><?php echo $MonthOctober; ?></option>
                <option value="11"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 11) : ?> selected="selected"<?php endif; ?>><?php echo $MonthNovember; ?></option>
                <option value="12"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthmonth'] == 12) : ?> selected="selected"<?php endif; ?>><?php echo $MonthDecember; ?></option>
            </select>
            
            <select class="birth-select required" name="SkierBirthDay_<?php echo $k; ?>" id="SkierBirthDay_<?php echo $k; ?>">
                <?php for ($i = 1; $i <= 31; $i++) : ?>
                    <option value="<?php echo $i; ?>"<?php if ($resultSelectSkiersOfBuyer[$k]['n_birthday'] == $i) : ?> selected="selected"<?php endif; ?>><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>

            <select class="birth-select required" name="SkierBirthYear_<?php echo $k; ?>" id="SkierBirthYear_<?php echo $k; ?>">
               <?php if ((isset($resultSelectSkiersOfBuyer[$k]['y_birthyear'])) && ($resultSelectSkiersOfBuyer[$k]['y_birthyear'] > 1000)){
               		// previously entered skier
               		for ($i = 1940; $i <= 2012; $i++){
						echo '<option value="' .$i .'"';
						if ($resultSelectSkiersOfBuyer[$k]['y_birthyear'] == $i){
							echo ' selected="selected"';
						}
						echo '>' .$i .'</option>';
					}
               } else {
					// new buyer as skier entry
					for ($i = 1940; $i <= 1984; $i++){
						echo '<option value="' .$i .'">' .$i .'</option>';
					}
					echo '<option value="1985" selected="selected">1985</option>';
					for ($i = 1986; $i <= 2001; $i++){
						echo '<option value="' .$i .'">' .$i .'</option>';
					}
			   }?>
            </select>
            
	      <span class="example multiline"><?php echo $SkierAgeAdvice; ?></span>
	      
	      <!-- Update this hidden input when the birthdate is complete --> 
	      <input type="hidden" class="Bool_SkierIsAdult" value="1" />
	      
	      
      </div> 
      
      
      
      <!-- Type of Lesson Selection: ski, snowboard, both --> 
      <div class="row gender lesson-type">
      	  <label for="SkierLessonType_<?php echo $k; ?>"><?php echo $SkierLessonTypeLabel; ?><span class="StarRequired">*</span></label>
	      <input type="radio" 
	            name="SkierLessonType_<?php echo $k; ?>" 
	            class="SkierLessonType_<?php echo $k; ?> required" 
	            id="SkierLessonTypeSki_<?php echo $k; ?>" 
	            title="<?php echo $LessonType_ErrorAdvice; ?>"
	            value="1" 
	            <?php if ($resultSelectSkiersOfBuyer[$k]['n_preferred_lesson_type'] == 1){
	                       echo ' checked = "checked"';
	                  }?>>
          <label class="radio" for="SkierLessonTypeSki_<?php echo $k; ?>"><?php echo $LessonTypeSki; ?></label>  
			
          <input type="radio" 
	            name="SkierLessonType_<?php echo $k; ?>" 
	            class="SkierLessonType_<?php echo $k; ?> required" 
	            id="SkierLessonTypeSnowboard_<?php echo $k; ?>" 
	            value="2"
	            <?php if ($resultSelectSkiersOfBuyer[$k]['n_preferred_lesson_type'] == 2){
	                        echo ' checked = "checked"';
	                  }?>>
          <label class="radio" for="SkierLessonTypeSnowboard_<?php echo $k; ?>"><?php echo $LessonTypeSnowboard; ?></label>
          
          
          
      </div> 
      
      
      <!-- Snowboard Ability Level, optional display --> 
      <div class="row snowboard-ability" id="SkierAbilityLevelBlockSnowboard_<?php echo $k; ?>">
	      <label for="SkierAbilityLevelSnowboard_<?php echo $k; ?>"><?php echo $AbilityLevelSnowboard; ?><span class="StarRequired">*</span></label>
		  
		  <select class="level-select required" name="SkierAbilityLevelSnowboard_<?php echo $k; ?>" id="SkierAbilityLevelSnowboard_<?php echo $k; ?>">
                <option value="0"><?php echo $AbilityLevelPleaseSelect; ?></option>
                <option value="1"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_snowboard'] == 1) : ?> selected="selected"<?php endif; ?>><?php echo $LevelBeginnerA; ?></option>
                <option value="2"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_snowboard'] == 2) : ?> selected="selected"<?php endif; ?>><?php echo $LevelBeginnerB; ?></option>
                <option value="3"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_snowboard'] == 3) : ?> selected="selected"<?php endif; ?>><?php echo $LevelIntermediateC; ?></option>
                <option value="4"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_snowboard'] == 4) : ?> selected="selected"<?php endif; ?>><?php echo $LevelIntermediateD; ?></option>
                <option value="5"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_snowboard'] == 5) : ?> selected="selected"<?php endif; ?>><?php echo $LevelIntermediateE; ?></option>
                <option value="6"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_snowboard'] == 6) : ?> selected="selected"<?php endif; ?>><?php echo $LevelExpertF; ?></option>
           </select>
      
          <span class="example snowboard-wizard adult" id="adult-snowboard-level-advice_<?php echo $k; ?>"><?php echo $AbilityLevelWizardAdultSnowboard; ?></span>
         
          <span class="example snowboard-wizard junior" id="junior-snowboard-level-advice_<?php echo $k; ?>"><?php echo $AbilityLevelWizardJuniorSnowboard; ?></span>
      </div> 
      
      <!-- Snowboard Ability Wizard (adult, then junior, display separately) --> 
      <?php require_once 'panels/panel.snowboard_ability_wizard.php'; ?>
     
      
      
      
      
      <!-- Ski Ability Level, optional display --> 
      <div class="row ski-ability" id="SkierAbilityLevelBlockSki_<?php echo $k; ?>">
	      <label for="SkierAbilityLevelSki_<?php echo $k; ?>"><?php echo $AbilityLevelSki; ?><span class="StarRequired">*</span></label>
		  
		  <select class="level-select required" name="SkierAbilityLevelSki_<?php echo $k; ?>" id="SkierAbilityLevelSki_<?php echo $k; ?>">
                <option value="0"><?php echo $AbilityLevelPleaseSelect; ?></option>
                <option value="1"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_ski'] == 1) : ?> selected="selected"<?php endif; ?>><?php echo $LevelBeginnerA; ?></option>
                <option value="2"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_ski'] == 2) : ?> selected="selected"<?php endif; ?>><?php echo $LevelBeginnerB; ?></option>
                <option value="3"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_ski'] == 3) : ?> selected="selected"<?php endif; ?>><?php echo $LevelIntermediateC; ?></option>
                <option value="4"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_ski'] == 4) : ?> selected="selected"<?php endif; ?>><?php echo $LevelIntermediateD; ?></option>
                <option value="5"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_ski'] == 5) : ?> selected="selected"<?php endif; ?>><?php echo $LevelIntermediateE; ?></option>
                <option value="6"<?php if ($resultSelectSkiersOfBuyer[$k]['n_ability_level_ski'] == 6) : ?> selected="selected"<?php endif; ?>><?php echo $LevelExpertF; ?></option>
           </select>
      
          <span class="example ski-wizard adult" id="adult-ski-level-advice_<?php echo $k; ?>"><?php echo $AbilityLevelWizardAdultSki; ?></span>
 
          <span class="example ski-wizard junior" id="junior-ski-level-advice_<?php echo $k; ?>"><?php echo $AbilityLevelWizardJuniorSki; ?></span>
      </div> 
      
      <!-- Ski Ability Wizard (adult + junior, same input) --> 
      <?php require_once 'panels/panel.ski_ability_wizard.php'; ?>
      
      
     
	  
	 
	
	
      
      
      
      
      <div class="actions">
			<button name="SkierRegSubmit" id="SkierRegSubmit_<?php echo $k; ?>" class="btn1" value="Submit" type="submit">
				<span><?php echo $Button_SkierRegConfirmLabel; ?></span>
			</button>
			
	  </div> 
	     
 </fieldset>
 <!-- End Skier fieldset-->
</form>
 
</div>  
<!-- End Skier section body -->  
