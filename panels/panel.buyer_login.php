
	
   <!-- New parent registration form --> 
   <h2><?php echo $NewRegistration; ?>
   </h2>
   <p id="RegisterWelcomeMsg"><?php echo $RegisterWelcomeMsg; ?></p>
   
   <input type="hidden"
		      id="ErrorMsg_Datepicker_EndDate_BeforeStartDate"
		      value="<?php echo $ErrorMsg_Datepicker_EndDate_BeforeStartDate; ?>" />	
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_10DaysMaxLength"
		      value="<?php echo $ErrorMsg_Datepicker_10DaysMaxLength; ?>" />
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_TooEarlyStart"
		      value="<?php echo $ErrorMsg_Datepicker_TooEarlyStart; ?>" />	
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_TooLateFinish"
		      value="<?php echo $ErrorMsg_Datepicker_TooLateFinish; ?>" />	
		      
   <input type="hidden"
		      id="ErrorMsg_Datepicker_StartDate"
		      value="<?php echo $ErrorMsg_Datepicker_StartDate; ?>" />         		      
   
   <form action="reg_login.php"
                  method="post"
                  name="NewBuyerForm"
                  id="NewBuyerForm" >
    
    <fieldset class="registration">
    
   	  <!-- Email -->  
      <div class="row">
	      <label for="NewBuyerEmail"><?php echo $Email_Label; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="NewBuyerEmail" 
		      id="NewBuyerEmail" 
		      class="required" 
		      autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
		      title="<?php echo $Email_ErrorAdvice; ?>"
		      value=""/>
	      <span class="example"><?php echo $Email_Advice; ?></span>
      </div>
      
      
      <!-- Password -->
      <div class="row">
	      <label for="NewBuyerPassword1"><?php echo $Password_Label; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="password" 
		      name="NewBuyerPassword1" 
		      id="NewBuyerPassword1" 
		      class="required" 
		      title="<?php echo $Password_ErrorAdvice; ?>"
		      value=""/>
	      <span class="example"><?php echo $Password_Advice; ?></span>
      </div>
      
      
      <!-- Re-enter Password -->
      <div class="row">
	      <label for="NewBuyerPassword2"><?php echo $ReenterPassword; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="password" 
		      name="NewBuyerPassword2" 
		      id="NewBuyerPassword2" 
		      class="required" 
		      title="<?php echo $ReenterPassword_ErrorAdvice; ?>"
		      value=""/>
	      <span class="example"><?php echo $ReenterPassword_Advice; ?></span>
      </div>
      
      <!-- Lesson Start Date and End Date -->
      <div class="row date-entry">
	      <label for="NewBuyerLessonStartDate"><?php echo $LessonStartDate; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="NewBuyerLessonStartDate" 
		      id="NewBuyerLessonStartDate" 
		      class="required date-entry new date-entry-arrival" 
		      title="<?php echo $LessonStartDate_ErrorAdvice; ?>"
		      value=""/>
	  </div>
	  <br class="Clear">
		     
	  <!-- End Date -->      
	  <div class="row date-entry">
	      <label for="NewBuyerLessonEndDate" class=""><?php echo $LessonEndDate; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="NewBuyerLessonEndDate" 
		      id="NewBuyerLessonEndDate" 
		      class="required date-entry new date-entry-departure" 
		      title="<?php echo $LessonEndDate_ErrorAdvice; ?>"
		      value=""/>
	  </div>
      <br class="Clear">
      
      <div class="actions">
			<button class="btn3 login-button" 
					type="submit"
					name="NewBuyerSubmit" 
    		   		id="NewBuyerSubmit" >
    		<span><?php echo $CreateAccount; ?><i class="icon-caret-right"></i></span>
    		</button>
    		
	  </div>
     
   
   </fieldset>    
   </form>
   

   
   <!-- Previously registered parent login form --> 
   <h2><?php echo $AlreadyRegistered; ?></h2>
    
    
    
    <form action="reg_login.php"
                  method="post"
                  name="RegisteredBuyerLoginForm"
                  id="RegisteredBuyerLoginForm" >
    
    <fieldset class="registration">
    
   	  <!-- Email -->  
      <div class="row">
	      <label for="RegisteredBuyerEmail"><?php echo $Email_Label; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="RegisteredBuyerEmail" 
		      id="RegisteredBuyerEmail" 
		      class="required" 
		      autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
		      title="<?php echo $Email_ErrorAdvice; ?>"
		      value=""/>
	      <span class="example"><?php echo $Email_Advice; ?></span>
      </div>
      
      
      <!-- Password -->
      <div class="row">
	      <label for="RegisteredBuyerPassword"><?php echo $Password_Label; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="password" 
		      name="RegisteredBuyerPassword" 
		      id="RegisteredBuyerPassword" 
		      class="required" 
		      title="<?php echo $Password_ErrorAdvice; ?>"
		      value=""/>
	      <span class="example"><a href='template_forgot_password.php'><?php echo $ForgotPwdNotice; ?></a></span>
      </div>
      
      <!-- Lesson Start and End Date -->
      <div class="row date-entry">
	      <label for="RegisteredBuyerLessonStartDate"><?php echo $LessonStartDate; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="RegisteredBuyerLessonStartDate" 
		      id="RegisteredBuyerLessonStartDate" 
		      class="required date-entry registered date-entry-arrival" 
		      title="<?php echo $LessonStartDate_ErrorAdvice; ?>"
		      value=""/>
	  </div>
	  <br class="Clear">
	 
	  <!-- End Date -->      
	  <div class="row date-entry">
	      <label for="RegisteredBuyerLessonEndDate" class=""><?php echo $LessonEndDate; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="RegisteredBuyerLessonEndDate" 
		      id="RegisteredBuyerLessonEndDate" 
		      class="required date-entry registered date-entry-departure" 
		      title="<?php echo $LessonEndDate_ErrorAdvice; ?>"
		      value=""/>
	  </div>
      
      <br class="Clear">
      <div class="actions">
			<button class="btn3 login-button " 
					type="submit"
					name="RegisteredBuyerSubmit" 
    		   		id="RegisteredBuyerSubmit" >
    		<span><?php echo $Login; ?><i class="icon-caret-right"></i></span>
    		</button>
      </div>
     
   
   </fieldset>    
   </form>
<!-- End buyer login form --> 
