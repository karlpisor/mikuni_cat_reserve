<?php 
	// return output for skier summary display
    // content changes depending on what UserLang is loaded
    
	$resource = '';
	if (isset($h)){
		if (isset($resultSelectOrderLIs) && !empty($resultSelectOrderLIs)) {
			$resource = $resultSelectOrderLIs[$h];
		}
	}
	
	
	
	// initialize debug
	$display_output = '';
	
	
	
    if (isset($resource) && !empty($resource)) {
    	
    	// date display
		$summary_date_display = date_format(date_create($resource['d_session_date']), "n/j");
		$lesson_date_display = date_format(date_create($resource['d_session_date']), "d");
		
		// time display
		$lessonStartTime = date_format(date_create($resource['tm_starttime']), "g:ia");
		$lessonEndTime = date_format(date_create($resource['tm_endtime']), "g:ia");
		$lesson_time_display = $lessonStartTime .' - ' .$lessonEndTime;
		
		
		// price
		$lesson_price_display = '&yen;' .number_format($resource['n_line_item_price']);
		$raw_price = $resource['n_line_item_price'];
		
		// set up lesson name display (= "Ski Private (English)") (= "Snowboard (Japanese)")
		// initialize
		$lesson_name_display = '';
		$lesson_description = '';
		
		// Mikuni tour
		if ($raw_price > 10000){
			$lesson_name_display = $LessonDisplay_isMikuniTour;
			$lesson_description = $LessonDescription_isMikuniTour;
		}
		 
		// avalance safety gear rental
		if ($raw_price < 10000){
			$lesson_name_display = $LessonDisplay_isMikuniGearRental;
			// do not display time interval or start time
			$lesson_time_display = '';
			$lessonStartTime = '';
			$lesson_description = $LessonDescription_isMikuniGearRental;
		}
		 
			
		
			
		// language of instruction
		// initialize
		$lesson_lang_display = '';
		if ($_SESSION['UserLang'] == 'EN'){
			switch ($resource['t_session_instruction_lang']){
				case 'JP':
					$lesson_lang_display = ' (' .$LessonLangDisplay_JP .')';
					break;
				case 'EN':
					$lesson_lang_display = ' (' .$LessonLangDisplay_EN .')';
					break;
				case 'CN':
					$lesson_lang_display = ' (' .$LessonLangDisplay_CN .')';
					break;
			}
		} else if ($_SESSION['UserLang'] == 'CN'){
			switch ($resource['t_session_instruction_lang']){
				case 'JP':
					$lesson_lang_display = ' (' .$LessonLangDisplay_JP .')';
					break;
				case 'EN':
					$lesson_lang_display = ' (' .$LessonLangDisplay_EN .')';
					break;
				case 'CN':
					$lesson_lang_display = ' (' .$LessonLangDisplay_CN .')';
					break;
			}
		} else {
			// default is JP
			switch ($resource['t_session_instruction_lang']){
				case 'JP':
					// most common case, no display for JP skiers
					$lesson_lang_display = '';
					break;
				case 'EN':
					$lesson_lang_display = ' (' .$LessonLangDisplay_EN .')';
					break;
				case 'CN':
					$lesson_lang_display = ' (' .$LessonLangDisplay_CN .')';
					break;
			}
		}
		
		// set up short name (half-day, etc.)
		$start_time_val = date_format(date_create($resource['tm_starttime']), "G");
    	$end_time_val = date_format(date_create($resource['tm_endtime']), "G");
			//echo 'Start is: ' .$start_time_val .'<br>';
			//echo 'End is: ' .$end_time_val .'<br>';
		$lesson_length_hours = ($end_time_val - $start_time_val);
			//echo 'Duration is: ' .$duration .'<br>';
			
		$lesson_length_hours_display = '';
		
		if ($lesson_length_hours > 7){
    		
    		$lesson_length_hours_display = 8;
    	} else if ($lesson_length_hours < 7 && $lesson_length_hours > 5){
    		
	    	$lesson_length_hours_display = 6;
    	} else if ($lesson_length_hours < 5 && $lesson_length_hours > 3){
    		
	    	$lesson_length_hours_display = 4;
    	} else {
    		
    		$lesson_length_hours_display = 2;
    	}
    	
		// UNUSED--do not show lesson length for private lessons
    	if ($resource['nBool_session_isPrivate'] == 1){
			$lesson_length_display = '';
		}
		
		
		// set up ability level display
		$lesson_level_display = '';
		if ($resource['inAbilityLevel'] > 0){
			switch ($resource['inAbilityLevel']){
				case 1:
					$lesson_level_display = ' A';
					break;
				case 2:
					$lesson_level_display = ' B';
					break;
				case 3:
					$lesson_level_display = ' C';
					break;
				case 4:
					$lesson_level_display = ' D';
					break;
				case 5:
					$lesson_level_display = ' E';
					break;
				case 6:
					$lesson_level_display = ' F';
					break;
			}
		}
		
		
		
		
		
		// add final description for full day, with/without lunch
		// any lesson longer than 5 hours
		$lesson_full_day_display = '';
		 
		
		
		
		// debug
		//echo 'Session UserLang: ' .$_SESSION['UserLang'] .'<br>';
		//echo 'This selected lesson\'s instruction language (EN, CN, or JP): ' .$resource['t_session_instruction_lang'] .'<br>';
		
		$display_output = 'Lesson date: ' .$summary_date_display .'<br>'
				.'Lesson time: ' .$lesson_time_display .'<br>'
				.'Lesson name: '.$lesson_name_display .'<br>'
				.'Lesson price: '.$lesson_price_display .'<br>'
				.'  -- end lesson output--' .'<br>';
		
		//echo $display_output;		
				
    	
    	
	} // end resource check ?>
	