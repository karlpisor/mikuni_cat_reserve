<?php
	// prefecture table not referenced for overseas residents
	// initialize
	$PrefectureNameJP = '';
	$PrefectureNameEN = '';
	if (isset($updateBuyerResult['fk_contact_country']) && $updateBuyerResult['fk_contact_country'] == 109){
		// Japanese domestic address
		$Bool_addressIsOverseas = 0;
		$PrefectureNameJP = mb_convert_encoding($updateBuyerResult['t_prefecturename_jp'], "UTF-8", "SJIS");
		$PrefectureNameEN = $updateBuyerResult['t_prefecturename_en'];
	} else {
		// overseas address
		$Bool_addressIsOverseas = 1;
		$PrefectureNameJP = mb_convert_encoding($updateBuyerResult['t_prefecture_text'], "UTF-8", "SJIS");
		$PrefectureNameEN = $updateBuyerResult['t_prefecture_text'];
	}
	
	// language-dependent display of name, address
	if($_SESSION['UserLang'] == 'JP'){
		echo '<span class="jp-name-display">' 
		.mb_convert_encoding($updateBuyerResult['t_lastname_jp'], "UTF-8", "SJIS") .' ' 
		.mb_convert_encoding($updateBuyerResult['t_firstname_jp'], "UTF-8", "SJIS")
		.'</span>'
		.'<span class="address-display">'
		.$updateBuyerResult['t_postalcode'] .' ' 
		.$PrefectureNameJP
		.mb_convert_encoding($updateBuyerResult['t_city'], "UTF-8", "SJIS")
		.mb_convert_encoding($updateBuyerResult['t_address1'], "UTF-8", "SJIS")
		.mb_convert_encoding($updateBuyerResult['t_address2'], "UTF-8", "SJIS")
		.'</span>';
	} else if($_SESSION['UserLang'] == 'EN'){
		if ($Bool_addressIsOverseas == 1){
			echo '<span class="en-name-display">'
				.$updateBuyerResult['t_firstname_jp'] .' '
				.$updateBuyerResult['t_lastname_jp']
				.'</span><br />'
				.'<span class="address-display">'
				.$updateBuyerResult['t_address1'] .' '
				.$updateBuyerResult['t_address2'] .'<br />'
				.$updateBuyerResult['t_city'] .', '
				.$PrefectureNameEN .'<br>'
				.$updateBuyerResult['t_country_name_en'] .' '
				.$updateBuyerResult['t_postalcode']
				.'</span>';
		} else {
			echo '<span class="en-name-display">'
				.$updateBuyerResult['t_firstname_jp'] .' '
				.$updateBuyerResult['t_lastname_jp']
				.'</span><br />'
				.'<span class="address-display">'
				.$updateBuyerResult['t_address1'] .' '
				.$updateBuyerResult['t_address2'] .'<br />'
				.$updateBuyerResult['t_city'] .', '
				.$PrefectureNameEN .'&nbsp;'
				.$updateBuyerResult['t_postalcode']
				.'</span>';
		}
		
	} else if($_SESSION['UserLang'] == 'CN'){
		echo '<span class="cn-name-display">'
		.$updateBuyerResult['t_lastname_jp'] .' ' 
		.$updateBuyerResult['t_firstname_jp']
		.'</span><br />'
		.'<span class="address-display">'
		.$updateBuyerResult['t_address1'] .' '
		.$updateBuyerResult['t_address2'] .'<br />'
		.$updateBuyerResult['t_city'] .', '
		.$PrefectureNameEN .'&nbsp;'
		.$updateBuyerResult['t_postalcode']
		.'</span>';
	}
?>