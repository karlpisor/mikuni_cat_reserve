
<?php 
$getSchedule = Order::MikuniGetSeasonLessonStatus();

/* $capacity_values = array(0,0,0,2,4,6,8);

for ($month = 3; $month < 4; $month++){
	
	
	
	for ($day = 1; $day < 31; $day++){
		
		
		$cap_value_key = array_rand($capacity_values);
		$capacity_value[$day] = $capacity_values[$cap_value_key];
		
		
		$getSchedule[$month][$day]['date'] = '2017' .$month .$day; 
		$getSchedule[$month][$day]['curr_skiers'] = $capacity_value[$day];
		
	}
} */

/* echo '<pre>';
print_r($getSchedule);
echo '</pre>'; */

/* echo '<pre>';
for ($k = 0; $k < count($getSchedule); $k++){
	echo $k .'<br>';
	echo $getSchedule[$k]['program_code'] .'<br>';
	echo $getSchedule[$k]['d_session_date'] .'<br>';
	echo $getSchedule[$k]['n_session_capacityremain'] .'<br><br>';
}
echo '</pre>'; */

?>

<!-- Start Calendar -->


<div id="calendar-container">

<!-- <div class="row thumbnails">
	<div class="calendar-thumbnail">
		<div class="thumbnail-head">0</div>
		<div class="thumbnail-body">Dec</div>
	</div>
	<div class="calendar-thumbnail">
		<div class="thumbnail-head">0</div>
		<div class="thumbnail-body">Jan</div>
	</div>
	<div class="calendar-thumbnail">
		<div class="thumbnail-head">0</div>
		<div class="thumbnail-body">Feb</div>
	</div>
	<div class="calendar-thumbnail">
		<div class="thumbnail-head">0</div>
		<div class="thumbnail-body">Mar</div>
	</div>
	<div class="calendar-thumbnail">
		<div class="thumbnail-head">0</div>
		<div class="thumbnail-body">Apr</div>
	</div>
</div> -->

<br class="Clear">

<div class="calendar-block">
	<div class="calendar-month-head"><?php echo $CalendarMonthLabel; ?></div>
	
	<table class="calendar-body">
		<tr class="calendar-day-header">
			<th><?php echo $DayOfWeek_Mon; ?></th>
	    	<th><?php echo $DayOfWeek_Tue; ?></th>
	    	<th><?php echo $DayOfWeek_Wed; ?></th>
	    	<th><?php echo $DayOfWeek_Thu; ?></th>
	    	<th><?php echo $DayOfWeek_Fri; ?></th>
	    	<th><?php echo $DayOfWeek_Sat; ?></th>
	    	<th><?php echo $DayOfWeek_Sun; ?></th>
  		</tr>
  		<tr class="calendar-day">
  			<td class="prev-month">
  				<span class="day-number">27</span>
  				<span class="capacity-indicator"><i class="fa fa-times"></i></span>
  				<span class="capacity-status"></span>
  			</td>
  			<td class="prev-month">
  				<span class="day-number">28</span>
  				<span class="capacity-indicator"><i class="fa fa-times"></i></span>
  				<span class="capacity-status"></span>
  			</td>
  			<td class="inactive">
  				<span class="day-number">1</span>
  				<span class="capacity-indicator"><i class="fa fa-times"></i></span>
  				<span class="capacity-status"></span>
  			</td>
  			<?php for ($i = 19; $i < 78; $i+=2):?>
  			<?php $dayOfWeek = date_format(date_create($getSchedule[$i]['d_session_date']),'D'); 
  				if ($dayOfWeek == 'Mon'){
  					echo '<tr class="calendar-day">';
  				}
  			?>
  			
  			<td<?php if ($dayOfWeek == 'Sun'){ echo ' class="last"';}?>>
  				<span class="day-number"><?php echo date_format(date_create($getSchedule[$i]['d_session_date']),'j'); ?></span>
  				<span class="capacity-indicator"><i class="fa<?php 
  				if ($getSchedule[$i]['n_session_capacityremain'] < 4 && $getSchedule[$i]['n_session_capacityremain'] > 0){
  					echo ' fa-exclamation-triangle';
  				} else if ($getSchedule[$i]['n_session_capacityremain'] == 0){
  					echo ' fa-times';
  				} else {
  					echo ' fa-circle-o';
  				}?>"></i></span>
  				<?php if ($getSchedule[$i]['n_session_capacityremain'] > 0):?>
  				<span class="capacity-status"><?php echo $getSchedule[$i]['n_session_capacityremain']; ?>/8</span>
  				<?php endif; ?>
  			</td>
  			<?php if ($dayOfWeek == 'Sun'){
  					echo '</tr>';
  				}
  			?>
  			<?php endfor; ?>
  		</tr>
  		
	
	</table>
</div>

<p class="message"><?php echo $Msg_LessonSelection_AdditionalResources; ?></p>


</div>	
<!-- End Calendar -->
