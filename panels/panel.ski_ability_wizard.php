<!-- Begin Skier Ability Selection Wizard -->
    
    <br class="Clear">
        
        <!-- Begin Adult or Junior Ski Level Select -->
        <div class="LevelSelectionWizard SkiLevelSelect">
        
        	<p class="wizard-title-wrapper"><?php echo $LevelWizard_Title; ?></p>
        
        	<p class="SkiSelect choice step-1 SkiLevelSelect_Step1_Yes">
        	<span><?php echo $LevelWizard_Ski_SkiedBeforeYes; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-1 SkiLevelSelect_Step1_No">
        	<span><?php echo $LevelWizard_Ski_SkiedBeforeNo; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-2 SkiLevelSelect_Step2_Yes">
        	<span><?php echo $LevelWizard_Ski_StoppingYes; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-2 SkiLevelSelect_Step2_No">
        	<span><?php echo $LevelWizard_Ski_StoppingNo; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-3 SkiLevelSelect_Step3_Yes">
        	<span><?php echo $LevelWizard_Ski_LiftChairYes; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-3 SkiLevelSelect_Step3_No">
        	<span><?php echo $LevelWizard_Ski_LiftChairNo; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-4 SkiLevelSelect_Step4_Yes">
        	<span><?php echo $LevelWizard_Ski_BeginnerRunYes; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-4 SkiLevelSelect_Step4_No">
        	<span><?php echo $LevelWizard_Ski_BeginnerRunNo; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-5 SkiLevelSelect_Step5_Yes">
        	<span><?php echo $LevelWizard_Ski_DifficultRunYes; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-5 SkiLevelSelect_Step5_No">
        	<span><?php echo $LevelWizard_Ski_DifficultRunNo; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-6 SkiLevelSelect_Step6_Yes">
        	<span><?php echo $LevelWizard_Ski_AllTerrainYes; ?></span>
        	</p>
        	
        	<p class="SkiSelect choice step-6 SkiLevelSelect_Step6_No">
        	<span><?php echo $LevelWizard_Ski_AllTerrainNo; ?></span>
        	</p>
        	
        </div>
        <!-- End Adult or Junior Ski Level Select -->
        
        
        
        <div class="ControlButtons ski">
        <p class="ResultBtn choice display SkiResultDisplay"></p>
        <p class="ResultBtn choice confirm SkiConfirmButton" <?php if ($_SESSION['Bool_isSkierUpdate']): ?>
        id="UpdateSkierIndex<?php echo '_' .$k; ?>"
        <?php else: ?>
        id="NewSkierIndex"
        <?php endif; ?>><?php echo $ConfirmWizardLevelSelection; ?></p> 
        
        
        <p class="ResultBtn choice reset SkiResetButton" <?php if ($_SESSION['Bool_isSkierUpdate']): ?>
        id="UpdateSkierIndex<?php echo '_' .$k; ?>"
        <?php else: ?>
        id="NewSkierIndex"
        <?php endif; ?>><?php echo $ResetWizard; ?></p>
        
        <input type="hidden"
		      class="SkiLevel"
		      value="" />	
        </div>
        <!-- End Result display -->
        
  <!-- End Skier Ability Selection Wizard div --> 