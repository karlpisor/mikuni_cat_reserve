<!-- Begin new skier section body --> 

		<?php 
		// update session control for new skier entry or skier update
		// bool isUpdate = 1 => update of existing skier record, 0 for new skier
		$_SESSION['Bool_isSkierUpdate'] = 0;?>
		
		<!-- No summary view for new skier --> 
        <div id="new-skier-reg-section-body" class="page-section-body skier">
        <span class="input-legend input-legend-star StarRequired">*</span>
        <span class="input-legend input-legend-text"><?php echo $RegistrationRequiredField; ?></span>



    <form action="reg_database.php"
                  method="post"
                  name="NewSkierRegForm"
                  id="NewSkierRegForm" />
        
    <!-- Start skier entry fieldset -->  
	<fieldset>  
	    
	    
    <!-- Start skier entry -->        
    <?php require 'panels/panel.skier_new_names.php'; ?>
    
    
    


	  <!-- Gender -->  
      <div class="row gender">
      	  <label for="SkierGender"><?php echo $Gender; ?><span class="StarRequired">*</span></label>
	      <input type="radio" 
	            name="SkierGender" 
	            id="SkierGenderMale" 
	            required="required"
	            title="<?php echo $Gender_ErrorAdvice; ?>"
	            value="0">
          <label class="radio" for="SkierGenderMale"><?php echo $Male; ?></label>  
			
            <input type="radio" 
	            name="SkierGender" 
	            id="SkierGenderFemale" 
	            value="1">
            <label class="radio" for="SkierGenderFemale"><?php echo $Female; ?></label>
      </div> 


	  <!-- Skier Language of Instruction --> 
      <div class="row" style="display:none">
	      <label for="SkierInstructionLang"><?php echo $SkierInstructionLangLabel; ?><span class="StarRequired">*</span></label>
		  
		  <?php if ($_SESSION['UserLang'] == 'JP') : ?>
		  <select class="lang-select" name="SkierInstructionLang" id="SkierInstructionLang">
                <option value="JP" selected="selected"><?php echo $SkierInstructionLangJP; ?></option>
                </select>
          <?php endif; ?>
          
          <?php if ($_SESSION['UserLang'] == 'EN') : ?>
		  <select class="lang-select" name="SkierInstructionLang" id="SkierInstructionLang">
                <option value="EN" selected="selected"><?php echo $SkierInstructionLangEN; ?></option>
          </select>
          <?php endif; ?>
            
          <span class="example"></span>
      </div> 
      
      

      <!-- DateOfBirth -->  
      <div class="row">
	      <label for="SkierBirthMonth"><?php echo $DateOfBirth; ?><span class="StarRequired">*</span></label>
		  
		  <select class="birth-select required" name="SkierBirthMonth" id="SkierBirthMonth">
                <option value="1"><?php echo $MonthJanuary; ?></option>
                <option value="2"><?php echo $MonthFebruary; ?></option>
                <option value="3"><?php echo $MonthMarch; ?></option>
                <option value="4"><?php echo $MonthApril; ?></option>
                <option value="5"><?php echo $MonthMay; ?></option>
                <option value="6"><?php echo $MonthJune; ?></option>
                <option value="7"><?php echo $MonthJuly; ?></option>
                <option value="8"><?php echo $MonthAugust; ?></option>
                <option value="9"><?php echo $MonthSeptember; ?></option>
                <option value="10"><?php echo $MonthOctober; ?></option>
                <option value="11"><?php echo $MonthNovember; ?></option>
                <option value="12"><?php echo $MonthDecember; ?></option>
            </select>
            
            <select class="birth-select required" name="SkierBirthDay" id="SkierBirthDay">
                <?php for ($i = 1; $i <= 31; $i++) : ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>

            <select class="birth-select required" name="SkierBirthYear" id="SkierBirthYear">
               <?php for ($i = 1940; $i <= 1984; $i++) : ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
               <?php endfor; ?>
               <option value="1985" selected="selected">1985</option>
               <?php for ($i = 1986; $i <= 2001; $i++) : ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
               <?php endfor; ?>
            </select>
            
	      <span class="example multiline"><?php echo $SkierAgeAdvice; ?></span>
      </div> 
      <!-- hide/show message to user about this skier being junior or adult -->
      <div class="row" id="junior-age-advice"><span><?php echo $SkierIsJuniorMessage; ?></span></div>
      
      
      
      
      
      
      <!-- Type of Lesson Selection: ski, snowboard, both --> 
      <div class="row gender lesson-type">
      	  <label for="SkierLessonType"><?php echo $SkierLessonTypeLabel; ?><span class="StarRequired">*</span></label>
	      
	      <input type="radio" 
	            name="SkierLessonType" 
	            id="SkierLessonTypeSki" 
	            value="1"
	            title="<?php echo $LessonType_ErrorAdvice; ?>"
	            required="required">
          <label class="radio" for="SkierLessonTypeSki"><?php echo $LessonTypeSki; ?></label>  
			
          <input type="radio" 
	            name="SkierLessonType" 
	            id="SkierLessonTypeSnowboard" 
	            value="2">
          <label class="radio" for="SkierLessonTypeSnowboard"><?php echo $LessonTypeSnowboard; ?></label>
                   			
          
      </div> 
      
      
      <!-- Snowboard Ability Level, optional display --> 
      <div class="row snowboard-ability" id="SkierAbilityLevelBlockSnowboard">
	      <label for="SkierAbilityLevelSnowboard"><?php echo $AbilityLevelSnowboard; ?><span class="StarRequired">*</span></label>
		  
		  <select class="level-select" name="SkierAbilityLevelSnowboard" id="SkierAbilityLevelSnowboard">
                <option value=""><?php echo $AbilityLevelPleaseSelect; ?></option>
                <option value="1"><?php echo $LevelBeginnerA; ?></option>
                <option value="2"><?php echo $LevelBeginnerB; ?></option>
                <option value="3"><?php echo $LevelIntermediateC; ?></option>
                <option value="4"><?php echo $LevelIntermediateD; ?></option>
                <option value="5"><?php echo $LevelIntermediateE; ?></option>
                <option value="6"><?php echo $LevelExpertF; ?></option>
           </select>
            
          <span class="example snowboard-wizard adult" id="adult-snowboard-level-advice"><?php echo $AbilityLevelWizardAdultSnowboard; ?></span>
          	
          <span class="example snowboard-wizard junior" id="junior-snowboard-level-advice"><?php echo $AbilityLevelWizardJuniorSnowboard; ?></span>
      </div> 
      
      <!-- Snowboard Ability Wizard (adult + junior) --> 
      <?php require 'panels/panel.snowboard_ability_wizard.php'; ?>
      
      
      
      
      <!-- Ski Ability Level, optional display --> 
      <div class="row ski-ability" id="SkierAbilityLevelBlockSki">
	      <label for="SkierAbilityLevelSki"><?php echo $AbilityLevelSki; ?><span class="StarRequired">*</span></label>
		  
		  <select class="level-select" name="SkierAbilityLevelSki" id="SkierAbilityLevelSki">
                <option value=""><?php echo $AbilityLevelPleaseSelect; ?></option>
                <option value="1"><?php echo $LevelBeginnerA; ?></option>
                <option value="2"><?php echo $LevelBeginnerB; ?></option>
                <option value="3"><?php echo $LevelIntermediateC; ?></option>
                <option value="4"><?php echo $LevelIntermediateD; ?></option>
                <option value="5"><?php echo $LevelIntermediateE; ?></option>
                <option value="6"><?php echo $LevelExpertF; ?></option>
           </select>
            
          <span class="example ski-wizard adult" id="adult-ski-level-advice"><?php echo $AbilityLevelWizardAdultSki; ?></span>
          	
          <span class="example ski-wizard junior" id="junior-ski-level-advice"><?php echo $AbilityLevelWizardJuniorSki; ?></span>
      </div> 
      
      <!-- Ski Ability Wizard (adult + junior) --> 
      <?php require 'panels/panel.ski_ability_wizard.php'; ?>
      
      
    

     
     
	
	
      
      
      
      
      <div class="actions">
      	 	<button name="NewSkierRegCancel" id="NewSkierRegCancel" class="btn1 cancel" type="button">
				<span><?php echo $Button_SkierRegCancelLabel; ?><i class="icon-caret-right"></i></span>
			</button>
			<button name="NewSkierRegSubmit" id="NewSkierRegSubmit" class="btn1" type="submit">
				<span><?php echo $Button_SkierRegConfirmLabel; ?><i class="icon-caret-right"></i></span>
			</button>
	  </div> 
	     
 </fieldset>
 <!-- End Skier fieldset-->
 </form>
 
</div>  
<!-- End Skier section body -->  
