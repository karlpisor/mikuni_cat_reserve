<?php
            // initialize language toggle link
            $ActiveSessionToggleLink = '';
            
            // in this case, we are viewing a page, so we pass the pk_page
            if (isset($_GET['PageId'])) {
                $ActiveSessionToggleLink = '&PageId=' .$_GET['PageId'];
            } 
            
            
            // set links
            $JPToggleLink = '<a href="' .$_SERVER['PHP_SELF'] .'?UserLang=JP' 
				.$ActiveSessionToggleLink .'">日本語</a>';	
            $ENToggleLink = '<a id="EnglishToggleLink" href="' .$_SERVER['PHP_SELF'] .'?UserLang=EN' 
				.$ActiveSessionToggleLink .'">english</a>';
            $CNToggleLink = '<a id="ChineseToggleLink" href="' .$_SERVER['PHP_SELF'] .'?UserLang=CN'
            		.$ActiveSessionToggleLink .'">國語</a>';
            
?>
<!-- Language toggle -->

<ul id="lang">
	<?php if ($_SESSION['UserLang'] == 'EN'){
		echo '<li>' .$JPToggleLink .'</li>';
		echo '<li>' .$CNToggleLink .'</li>';
	} else if ($_SESSION['UserLang'] == 'CN'){
		echo '<li>' .$JPToggleLink .'</li>';
		echo '<li>' .$ENToggleLink .'</li>';
	} else {
		echo '<li>' .$CNToggleLink .'</li>';
		echo '<li>' .$ENToggleLink .'</li>';
	}
	?>
	
</ul>

<!-- End language toggle -->