
<!-- Start Subnav -->

	<?php if ($pageType == 'About'): ?>
	<nav id="subnav">
		<i class="icon-caret-up"></i>
		<ul>                                
			<li><a href="index.php?PageId=5"><?php echo $EA_Mission; ?></a></li>
			<li><a href="index.php?PageId=4"><?php echo $EA_Management; ?></a></li>
			<li><a href="index.php?PageId=4"><?php echo $EA_Staff; ?></a></li>
			<li><a href="index.php?PageId=6"><?php echo $EA_Clients; ?></a></li>
			<li><a href="index.php?PageId=24"><?php echo $EA_Partners; ?></a></li>
		</ul>
	</nav>
	<?php endif; ?>
	
	<?php if ($pageType == 'Program'): ?>
	<nav id="subnav">
		<i class="icon-caret-up"></i>
		<ul> 
			<!-- <li><a href="template_program.php?ProgramId=49">//$PreCampEnglishCourseShort;</a></li> -->
			<li><a href="template_program.php?ProgramId=48"><?php echo $CampSummerChallenge; ?></a></li>
			<li><a href="template_program.php?ProgramId=42"><?php echo $CampSummerImmersion; ?></a></li>
			<li><a href="template_program.php?ProgramId=41"><?php echo $CampSummerUltimate; ?></a></li> 
			<li><a href="template_program.php?ProgramId=45"><?php echo $CampSnowChallenge; ?></a></li>
			<li><a href="template_program.php?ProgramId=39"><?php echo $CampSnowImmersion; ?></a></li>
			<li><a href="index.php?PageId=7"><?php echo $CampFAQ; ?></a></li>
		</ul>
	</nav>
	<?php endif; ?>
	
	
	
<!-- End Subnav -->
