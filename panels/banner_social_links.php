
<!-- Social media links -->

<ul id="social">
	
	<?php if($_SESSION['UserLang'] == 'EN'): ?>
	<li id="facebook"><a href="http://www.facebook.com/NaebaEnglishSnowSchool" title="<?php echo $SocialFacebookLink; ?>"></a></li>
	
	<?php elseif($_SESSION['UserLang'] == 'JP'): ?>
	<li id="facebook"><a href="http://www.facebook.com/naebasnow" title="<?php echo $SocialFacebookLink; ?>"></a></li>
	<li id="yt"><a href="http://youtu.be/R8ryqhp5U3M" title="<?php echo $SocialYouTubeLink; ?>"></a></li>
	
	<?php endif; ?>
	
</ul>

<!-- End social media links -->