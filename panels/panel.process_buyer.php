<?php

	// set up lang strings
	
	// lang suffix
	$userLangSuffix = strtolower($_SESSION['UserLang']);
	
	// buyer name
	$buyerFullName_String = '';
	$mailBuyerFullName_String = '';
	if ($_SESSION['UserLang'] == 'EN'){
		
		$buyerFullName_String = $resultSelectBuyer['t_firstname_jp'] .' ' .$resultSelectBuyer['t_lastname_jp'];
		$mailBuyerFullName_String = $resultSelectBuyer['t_firstname_jp'] .' ' .$resultSelectBuyer['t_lastname_jp'];
		
	} else if ($_SESSION['UserLang'] == 'CN'){
		
		$buyerFullName_String = $resultSelectBuyer['t_lastname_jp'] .' ' .$resultSelectBuyer['t_firstname_jp'];
		$mailBuyerFullName_String = $resultSelectBuyer['t_lastname_jp'] .' ' .$resultSelectBuyer['t_firstname_jp'];
		
	} else {
		
		// default JP
		$buyerFullName_String = mb_convert_encoding($resultSelectBuyer['t_lastname_jp'], "UTF-8", "SJIS") 
				.' ' .mb_convert_encoding($resultSelectBuyer['t_firstname_jp'], "UTF-8", "SJIS") .' ' .$SalutationJP_Sama;
		$mailBuyerFullName_String = mb_convert_encoding($resultSelectBuyer['t_lastname_jp'], "UTF-8", "SJIS") 
				.' ' .mb_convert_encoding($resultSelectBuyer['t_firstname_jp'], "UTF-8", "SJIS");
	}
	
	// phone
	$buyerPhoneString = $Phone .' ' .$resultSelectBuyer['t_phone'];
	
	// email
	$buyerEmailString = $Email .' ' .$resultSelectBuyer['t_email'];
	
	// postal code
	$buyerPostalCodeString = $PostalCode .' ' .$resultSelectBuyer['t_postalcode'];
	
	// country
	$buyerCountryString = '';
	// do not display country string if buyer resides in Japan
	if($resultSelectBuyer['fk_contact_country'] != 109){
		$buyerCountryString = $CountryOfResidence_Label .' ' .$resultSelectBuyer['t_country_name_' .$userLangSuffix];
	}
	
	// prefecture/state
	// overseas customers will display name directly in fk_prefecture, not using prefecture table
	if($resultSelectBuyer['fk_contact_country'] != 109){
		
		$buyerPrefectureString = $Prefecture .' ' .$resultSelectBuyer['t_prefecture_text'];
		
	} else {
		
		// default, resident of Japan
		$buyerPrefectureString = $Prefecture .' ' .$resultSelectBuyer['t_prefecturename_' .$userLangSuffix];
		if ($_SESSION['UserLang'] == 'JP'){
			$buyerPrefectureString = $Prefecture .' ' .mb_convert_encoding($resultSelectBuyer['t_prefecturename_jp'], "UTF-8", "SJIS");
		}
			
	}
	
	// city/suburb
	$buyerCitySuburbString = $CitySuburb .' ' .$resultSelectBuyer['t_city'];
	
	// address 1
	$buyerAddress1String = $Address1 .' ' .$resultSelectBuyer['t_address1'];
	
	// address 2 (optional)
	$buyerAddress2String = '';
	if (!empty($resultSelectBuyer['t_address2'])){
		$buyerAddress2String = $Address2 .' ' .$resultSelectBuyer['t_address2'];
		
	}
	if ($_SESSION['UserLang'] == 'JP'){
		$buyerCitySuburbString = $CitySuburb .' ' .mb_convert_encoding($resultSelectBuyer['t_city'], "UTF-8", "SJIS");
		$buyerAddress1String = $Address1 .' ' .mb_convert_encoding($resultSelectBuyer['t_address1'], "UTF-8", "SJIS");
		$buyerAddress2String = $Address2 .' ' .mb_convert_encoding($resultSelectBuyer['t_address2'], "UTF-8", "SJIS");
	}
	
	// emergency phone
	$buyerEmergencyPhoneString = $EmergencyPhone .' ' .$resultSelectBuyer['t_phone_emergency'];
	
	// emergency contact
	$phoneEmergencyMemoString = $resultSelectBuyer['t_phone_emergency_memo'];
	if ($_SESSION['UserLang'] == 'JP'){
		$phoneEmergencyMemoString = mb_convert_encoding($resultSelectBuyer['t_phone_emergency_memo'], "UTF-8", "SJIS");
	}
	$buyerEmergencyContactString = $EmergencyContact .' ' .$phoneEmergencyMemoString;
	$mailBuyerEmergencyContactString = $Mail_EmergencyContact .' ' .$phoneEmergencyMemoString;
	
	// receives newsletter?
	if ($resultSelectBuyer['nBool_receivesNewsletter'] == 1){
		$receivesNewsletterString = $ReceivesNewsletterYes;
	} else {
		// default
		$receivesNewsletterString = $ReceivesNewsletterNo;
	}
	$buyerReceiveNewsletterString = $ReceivesNewsletterLabel .' ' .$receivesNewsletterString;
	
	
	
	
	
	
	
	// output to confirmation page
	echo '<h3>';
	echo $buyerFullName_String;
	echo '</h3>';
	
	echo '<p>';
	// output strings in different combinations depending on user language
	if ($_SESSION['UserLang'] == 'EN'){
		
		if (!empty($buyerCountryString)){
			echo $buyerCountryString .'<br />';
		}
		echo $buyerPostalCodeString .'<br />';
		echo $buyerPrefectureString .'<br />';
		echo $buyerCitySuburbString  .'<br />';
		echo $buyerAddress1String .'<br />';
		if (!empty($buyerAddress2String)){
			echo $buyerAddress2String .'<br />';
		
		}
		
		echo $buyerPhoneString .'<br />';
		echo $buyerEmailString .'<br />';
		echo $buyerEmergencyPhoneString .'<br />';
		echo $buyerEmergencyContactString .'<br />';
		echo $buyerReceiveNewsletterString .'<br />';	
		
	} else if ($_SESSION['UserLang'] == 'CN'){
		
		// Chinese
		if (!empty($buyerCountryString)){
			echo $buyerCountryString .'<br />';
		}
		echo $buyerPostalCodeString .'<br />';
		echo $buyerPrefectureString .'<br />';
		echo $buyerCitySuburbString  .'<br />';
		echo $buyerAddress1String .'<br />';
		if (!empty($buyerAddress2String)){
			echo $buyerAddress2String .'<br />';
		
		}
		
		echo $buyerPhoneString .'<br />';
		echo $buyerEmailString .'<br />';
		echo $buyerEmergencyPhoneString .'<br />';
		echo $buyerEmergencyContactString .'<br />';
		echo $buyerReceiveNewsletterString .'<br />';		
		
	} else {
		
		// default display = JP
		if (!empty($buyerCountryString)){
			echo $buyerCountryString .'<br />';
		}
		echo $buyerPostalCodeString .'<br />';
		echo $buyerPrefectureString .'<br />';
		echo $buyerCitySuburbString  .'<br />';
		echo $buyerAddress1String .'<br />';
		if (!empty($buyerAddress2String)){
			echo $buyerAddress2String .'<br />';
		}
		
		echo $buyerPhoneString .'<br />';
		echo $buyerEmailString .'<br />';
		echo $buyerEmergencyPhoneString .'<br />';
		echo $buyerEmergencyContactString .'<br />';
		echo $buyerReceiveNewsletterString .'<br />';			
		
	}
	
	// end buyer information page output
	echo '</p>';	
	
	
	
	
	
	
	
	
	
	// output to email
	$buyerCountryStringEmailDisplay = '';
	$buyerAddress2StringEmailDisplay = '';
	if (!empty($buyerCountryString)){
		$buyerCountryStringEmailDisplay = $buyerCountryString ."\r\n";
	}
	if (!empty($buyerAddress2String)){
		$buyerAddress2StringEmailDisplay = $buyerAddress2String ."\r\n";
	}
	// output strings in different combinations depending on user language
	if ($_SESSION['UserLang'] == 'EN'){
		
		$buyerMailMessage =	$mailBuyerFullName_String ."\r\n"
				.$buyerCountryStringEmailDisplay
				.$buyerPostalCodeString ."\r\n"
				.$buyerPrefectureString ."\r\n"
				.$buyerCitySuburbString ."\r\n"
				.$buyerAddress1String ."\r\n"
				.$buyerAddress2StringEmailDisplay
				.$buyerPhoneString ."\r\n"
				.$buyerEmailString ."\r\n"
				.$buyerEmergencyPhoneString ."\r\n"
				.$mailBuyerEmergencyContactString ."\r\n"
				.$buyerReceiveNewsletterString ."\r\n"
				;
	
	} else if ($_SESSION['UserLang'] == 'CN'){
	
		// Chinese
		$buyerMailMessage =	$mailBuyerFullName_String ."\r\n"
				.$buyerCountryStringEmailDisplay
				.$buyerPostalCodeString ."\r\n"
				.$buyerPrefectureString ."\r\n"
				.$buyerCitySuburbString ."\r\n"
				.$buyerAddress1String ."\r\n"
				.$buyerAddress2StringEmailDisplay
				.$buyerPhoneString ."\r\n"
				.$buyerEmailString ."\r\n"
				.$buyerEmergencyPhoneString ."\r\n"
				.$mailBuyerEmergencyContactString ."\r\n"
				.$buyerReceiveNewsletterString ."\r\n"
				;

	} else {
	
		// default display = JP
		$buyerMailMessage =	$Mail_FullNameBoth .$mailBuyerFullName_String .' ' .$Mail_Salutation."\r\n"
				.$buyerCountryStringEmailDisplay
				.$buyerPostalCodeString ."\r\n"
				.$buyerPrefectureString ."\r\n"
				.$buyerCitySuburbString ."\r\n"
				.$buyerAddress1String ."\r\n"
				.$buyerAddress2StringEmailDisplay
				.$buyerPhoneString ."\r\n"
				.$buyerEmailString ."\r\n"
				.$buyerEmergencyPhoneString ."\r\n"
				.$mailBuyerEmergencyContactString ."\r\n"
				.$buyerReceiveNewsletterString ."\r\n"
				;

	}				
?>