	<!-- Buyer Last, First Names JP, and hiragana fields -->
    
    <?php if($_SESSION['UserLang'] == 'JP'):?>
    
    <!-- LastNameJP -->  
	<div class="row">
		<label for="BuyerLastNameJP"><?php echo $LastNameSei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text" 
			 name="BuyerLastNameJP" 
			 id="BuyerLastNameJP" 
			 class="required" 
			 title="<?php echo $LastNameSei_ErrorAdvice; ?>"
			 value="<?php echo mb_convert_encoding($updateBuyerResult['t_lastname_jp'], "UTF-8", "SJIS"); ?>"/>
		<span class="example"><?php echo $LastNameSei_Advice; ?></span>
	</div>
	
    <!-- LastNameJP, furigana (hiragana) -->  
	<div class="row">
		<label for="BuyerLastNameJP_Kana"><?php echo $LastNameSei_Kana; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="BuyerLastNameJP_Kana"
			 id="BuyerLastNameJP_Kana"
			 class="required"
			 title="<?php echo $LastNameSei_Kana_ErrorAdvice; ?>" 
			 value="<?php echo mb_convert_encoding($updateBuyerResult['t_lastname_furigana_jp'], "UTF-8", "SJIS"); ?>"/>
		<span class="example"><?php echo $LastNameSei_Kana_Advice; ?></span>
	</div>

    <!-- FirstNameJP -->  
	<div class="row">
		<label for="BuyerFirstNameJP"><?php echo $FirstNameMei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="BuyerFirstNameJP"
			 id="BuyerFirstNameJP"
			 class="required"
			 title="<?php echo $FirstNameMei_ErrorAdvice; ?>" 
			 value="<?php echo mb_convert_encoding($updateBuyerResult['t_firstname_jp'], "UTF-8", "SJIS"); ?>"/>
		<span class="example"><?php echo $FirstNameMei_Advice; ?></span>
	</div>
	
    <!-- FirstNameJP, furigana (hiragana) -->  
	<div class="row">
		<label for="BuyerFirstNameJP_Kana"><?php echo $FirstNameMei_Kana; ?>
		<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="BuyerFirstNameJP_Kana"
			 id="BuyerFirstNameJP_Kana"
			 class="required"
			 title="<?php echo $FirstNameMei_Kana_ErrorAdvice; ?>" 
			 value="<?php echo mb_convert_encoding($updateBuyerResult['t_firstname_furigana_jp'], "UTF-8", "SJIS"); ?>"/>
		<span class="example"><?php echo $FirstNameMei_Kana_Advice; ?></span>
	</div>
	
	
	
	
	
	
	
	<!-- English -->  
	<?php elseif($_SESSION['UserLang'] == 'EN'):?>
	
	<!-- FirstNameJP -->  
	<div class="row">
		<label for="BuyerFirstNameJP"><?php echo $FirstNameMei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="BuyerFirstNameJP"
			 id="BuyerFirstNameJP"
			 class="required"
			 title="<?php echo $FirstNameMei_ErrorAdvice; ?>" 
			 value="<?php echo $updateBuyerResult['t_firstname_jp']; ?>"/>
		<span class="example"><?php echo $FirstNameMei_Advice; ?></span>
	</div>
	
	<!-- LastNameJP -->  
	<div class="row">
		<label for="BuyerLastNameJP"><?php echo $LastNameSei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text" 
			 name="BuyerLastNameJP" 
			 id="BuyerLastNameJP" 
			 class="required" 
			 title="<?php echo $LastNameSei_ErrorAdvice; ?>"
			 value="<?php echo $updateBuyerResult['t_lastname_jp']; ?>"/>
		<span class="example"><?php echo $LastNameSei_Advice; ?></span>
	</div>
	
	
	
	
	
	
	<!-- Chinese -->  
	<?php elseif($_SESSION['UserLang'] == 'CN'):?>
	
	<!-- LastNameJP -->  
	<div class="row">
		<label for="BuyerLastNameJP"><?php echo $LastNameSei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text" 
			 name="BuyerLastNameJP" 
			 id="BuyerLastNameJP" 
			 class="required" 
			 title="<?php echo $LastNameSei_ErrorAdvice; ?>"
			 value="<?php echo $updateBuyerResult['t_lastname_jp']; ?>"/>
		<span class="example"><?php echo $LastNameSei_Advice; ?></span>
	</div>
	
	<!-- FirstNameJP -->  
	<div class="row">
		<label for="BuyerFirstNameJP"><?php echo $FirstNameMei; ?>
			<span class="StarRequired">*</span>
		</label>
		<input type="text"
			 name="BuyerFirstNameJP"
			 id="BuyerFirstNameJP"
			 class="required"
			 title="<?php echo $FirstNameMei_ErrorAdvice; ?>" 
			 value="<?php echo $updateBuyerResult['t_firstname_jp']; ?>"/>
		<span class="example"><?php echo $FirstNameMei_Advice; ?></span>
	</div>
	
	
	
	
	<?php endif; ?>
	