<!-- Account tools -->

<div id="AccountTools">
		<?php 
		// display logout message if user has logged out
	    if ((isset($_GET['Action'])) && ($_GET['Action'] == 'Logout')){
	        echo '<span id="LogoutMessage">';
	        echo $SuccessfulLogoutMessage;
	        echo '</span>';
	    }
	    	// look for the $_SESSION or $_COOKIE email variable
        	// this shows that the user is or has logged in
        	if (isset($_SESSION['ParentEmail'])){
        			
        		echo $AccountToolsWelcome;
        			
        			if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
		        		if (isset($_SESSION['ParentFirstNameEN'])){
		        			echo $_SESSION['ParentFirstNameEN'];
		        		} else {
		        			echo 'Error_EN';
		        		}
        			} else {
        				// default
        				if (isset($_SESSION['ParentLastNameJP'])){
		        			echo $_SESSION['ParentLastNameJP'];
		        		} else {
		        			echo 'Error_JP';
		        		}
        				echo $AccountToolsJPSan;
        			}
        	} else {
        		// default
        		// new user, or user not logged in, or no cookie available
        		echo $AccountToolsWelcome;
        		echo $AccountToolsGuest;
        		
        		
        	}
        
       		// account tools, logout link only available for logged-in users
            if (isset($_SESSION['ParentEmail'])){
        		
	        	// link to user account
        		echo '<a href="template_edit_account.php?Edit=ParentDetails&ParentId='
        		.$_SESSION['ParentId']
        		.'">'
            	.$AccountToolsMgtLink
            	.'</a>';
            	
            	// logout link
            	echo '<a href="index.php?Action=Logout">'
            	.$AccountToolsLogOut
            	.'</a>';
            	
        	} else {
        		// default
        		// new user, or user not logged in, or no cookie available
        		// log in link
        		echo '<a href="template_login.php?Action=Login&UserLang='
        		.$_SESSION['UserLang']
        		.'">'
            	.$AccountLoginLink
            	.'</a>';
        	}
         ?>              
</div> 
<!-- End account tools -->