<?php 

	if (isset($resultSelectOrderLIs) && !empty($resultSelectOrderLIs)){
	 
	// initialize
	$previous_lesson_id = '';
	$order_total_JPY = 0;
	
	// initialize product flags, used to hide or show messages on the confirmation page/email
	$bool_Order_hasPrivateLesson = 0;
	$bool_Order_hasAdultGroupLesson = 0;
	$bool_Order_hasJuniorGroupLesson = 0;
	 
	// start wide table output
	echo '<table class="confirmation wide-table">';
	echo '<tr>'
			.'<th>' .$ConfirmationTableHeaderLessonNameLabel .'</th>'
			.'<th>' .$ConfirmationTableHeaderDateLabel .'</th>'
			.'<th>' .$ConfirmationTableHeaderStartTimeLabel .'</th>'
			.'<th>' .$ConfirmationTableHeaderNameLabel .'</th>'
			.'<th class="lesson-price">' .$ConfirmationTableHeaderPriceLabel .'</th>'
		.'</tr>';
	 
	for ($h = 0; $h < count($resultSelectOrderLIs); $h++){

		// start new row
		echo '<tr>';

		// check for new lesson, do not output lesson name if the same
		$this_lesson_id = $resultSelectOrderLIs[$h]['pk_session'];

		// print out summary line items
		// first set up strings
		require 'panels/panel.lesson_confirmation_display.php';

		// output
		
		
		// lesson output
		if ($this_lesson_id != $previous_lesson_id){

			// output lesson summary
			echo '<td class="confirmation lesson-name">'
					//.$lesson_length_display
					.$lesson_name_display;
			echo '</td>';
			echo '<td class="confirmation date">' .$summary_date_display .'</td>';
			echo '<td class="confirmation date">' .$lessonStartTime .'</td>';
				

		} else {
				
			echo '<td></td><td></td><td></td>';
			

		} // end check for duplicate id
		
		
		// output skier name
		if ($_SESSION['UserLang'] == 'EN'){

			// EN-language
			echo '<td class="confirmation name-display">'
			//.$SkierNameLabel
			.$resultSelectOrderLIs[$h]['t_firstname_jp'] .' '
			.$resultSelectOrderLIs[$h]['t_lastname_jp']
			.'</td>';
				
		} else {
				
			// default
			echo '<td class="confirmation name-display">'
			//.$SkierNameLabel
			.mb_convert_encoding($resultSelectOrderLIs[$h]['t_lastname_jp'], "UTF-8", "SJIS") .' '
			.mb_convert_encoding($resultSelectOrderLIs[$h]['t_firstname_jp'], "UTF-8", "SJIS")
			.'</td>';
		}
		
		// output lesson price
		echo '<td class="confirmation lesson-price">'
				.$lesson_price_display
			.'</td>';



		// increment total JPY
		$order_total_JPY = $order_total_JPY + $raw_price;
			
		// update duplicate check
		$previous_lesson_id = $this_lesson_id;

		// end row output
		echo '</tr>';

	} // end loop $h

} // end isset check $resultSelectOrderLIs
?>