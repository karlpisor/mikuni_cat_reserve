
<!-- Begin section body --> 
        <!-- Begin section summary view --> 
        <div id="buyer-reg-summary" class="summary-view buyer">
        <span id="buyer-success-status">
        </span>
        <p class="summary-content">
        	<?php require_once 'panels/panel.buyer_summary_display.php';?>
        </p>
        <span>
        	<a class="buyer edit-link" href="#"><?php echo $EditFormContent; ?></a>
        </span>
        </div>
        <!-- End section summary view --> 



<div id="buyer-reg-section-body" class="page-section-body buyer">
        <span class="input-legend input-legend-star StarRequired">*</span>
        <span class="input-legend input-legend-text"><?php echo $RegistrationRequiredField; ?></span>
        
         
       

<form action="reg_database.php"
                  method="post"
                  name="BuyerRegForm"
                  id="BuyerRegForm" />
                  
     <input type="hidden" 
            name="BuyerFirstTimeEntry" 
            id="BuyerFirstTimeEntry" 
            value="<?php echo $_SESSION['Bool_BuyerIsNew']; ?>"
        />               
        
     <!-- Start buyer entry fieldset -->  
	 <fieldset>  
	
	
	
	<?php require_once 'panels/panel.buyer_names.php'; ?>
	 
      
      
      
      
      <!-- Email -->  
      <div class="row">
	      <label for="BuyerEmail"><?php echo $Email; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerEmail" 
		      id="BuyerEmail" 
		      class="required" 
		      title="<?php echo $Email_ErrorAdvice; ?>"
		      value="<?php echo $updateBuyerResult['t_email']; ?>"/>
	      <span class="example"><?php echo $Email_Advice; ?></span>
      </div>
      
      
      <!-- Phone --> 
      <div class="row">
	      <label for="BuyerPhone"><?php echo $Phone; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerPhone" 
		      id="BuyerPhone" 
		      class="required" 
		      title="<?php echo $Phone_ErrorAdvice; ?>"
		      value="<?php echo $updateBuyerResult['t_phone']; ?>"/>
	      <span class="example"><?php echo $Phone_Advice; ?></span>
      </div>
      
  
      <!-- Postal Code --> 
      <div class="row">
	      <label for="BuyerPostalCode"><?php echo $PostalCode; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerPostalCode" 
		      id="BuyerPostalCode" 
		      class="required" 
		      title="<?php echo $PostalCode_ErrorAdvice; ?>"
		      value="<?php echo $updateBuyerResult['t_postalcode']; ?>"/>
	      <span class="example"><?php echo $PostalCode_Advice; ?></span>
      </div>
      
      
      <!-- Country of Residence --> 
      <div class="row">
	      <label for="BuyerCountryOfResidenceFK"><?php echo $CountryOfResidence_Label; ?><span class="StarRequired">*</span></label>
		  <select class="required" name="BuyerCountryOfResidenceFK" id="BuyerCountryOfResidenceFK" >
		  		
		  		<?php if($updateBuyerResult['fk_contact_country'] > 0){
		  			
		  			// load buyer's stored value
		  			echo '<option selected="selected" value="' .$updateBuyerResult['fk_contact_country'] .'">';
		  			
		  			// load lang string from database
		  			$CountryPK = $updateBuyerResult['fk_contact_country'];
		  			$resultSelectCountry = Contact::GetCountryDetails($CountryPK);
		  			
		  			if ($_SESSION['UserLang'] == 'JP'){
		  				echo mb_convert_encoding($resultSelectCountry['t_country_name_jp'], "UTF-8", "SJIS");
		  			} else {
		  				echo $resultSelectCountry['t_country_name_'.strtolower($_SESSION['UserLang'])];
		  			}
		  			echo '</option>';
		  		}?>
		  		
		  		<!-- default is Japan --> 
		  		<option value="109"><?php echo $CountryLabel_Japan; ?></option>
	            	
	            <!-- display popular country list -->
				<option value="11"><?php echo $CountryLabel_Australia; ?></option>
				<option value="44"><?php echo $CountryLabel_Taiwan; ?></option>
				<option value="97"><?php echo $CountryLabel_HongKong; ?></option>
				<option value="197"><?php echo $CountryLabel_Singapore; ?></option>
				<option value="214"><?php echo $CountryLabel_Thailand; ?></option>
				<option value="101"><?php echo $CountryLabel_Indonesia; ?></option>
				<option value="229"><?php echo $CountryLabel_UK; ?></option>
				<option value="234"><?php echo $CountryLabel_USA; ?></option>
				<option value="43"><?php echo $CountryLabel_China; ?></option>
		
					
				<?php 
				// display all country list
				for ($i = 0; $i < count($resultSelectCountries); $i++) {
				  	echo '<option value="' .$resultSelectCountries[$i]['pk_country'] .'">';
				  	if ($_SESSION['UserLang'] == 'JP'){
				  		echo mb_convert_encoding($resultSelectCountries[$i]['t_country_name_jp'], "UTF-8", "SJIS");
				  	} else {
				  		echo $resultSelectCountries[$i]['t_country_name_'.strtolower($_SESSION['UserLang'])];
				  	}
				  	echo '</option>';
				} // end for
				?>
		  </select>
	  </div>
      
       
      <!-- Prefecture -->   
      <!-- This is a plain text box entry for non-Japan residents -->  
      <div class="prefecture row">
	      <label for="BuyerPrefectureFK"><?php echo $Prefecture; ?>
		      <span class="StarRequired">*</span></label>
		  <?php 
		  
		  
		  // 109 is Japan country code, most common case
		  // fk_country is loaded through prefecture link, not through the buyer's contact record
		  if(isset($updateBuyerResult['fk_contact_country']) && ($updateBuyerResult['fk_contact_country'] == 109)):?>    
		  <select name="BuyerPrefectureFK" id="BuyerPrefectureFK">
	            <option value="<?php echo $updateBuyerResult['pk_prefecture']; ?>"
	                	selected="selected">
	                	<?php 
	                	// get prefecture for this buyer
	                	$PrefecturePK = $updateBuyerResult['pk_prefecture'];
	                	$resultSelectPrefecture = Contact::GetPrefectureDetails($PrefecturePK);
	                	if ($_SESSION['UserLang'] == 'JP'){
	                		echo mb_convert_encoding($resultSelectPrefecture['t_prefecturename_jp'], "UTF-8", "SJIS");
	                	} else {
	                		echo $resultSelectPrefecture['t_prefecturename_'.strtolower($_SESSION['UserLang'])];
	                	}?>
	            </option>
	       
		        <?php 
		        // write the rest of the prefecture values
		        for ($i = 0; $i < count($resultSelectPrefectures); $i++): ?>
		        <option value="<?php echo $resultSelectPrefectures[$i]['pk_prefecture']; ?>"><?php 
		        if ($_SESSION['UserLang'] == 'JP'){
	                echo mb_convert_encoding($resultSelectPrefectures[$i]['t_prefecturename_jp'], "UTF-8", "SJIS");
	            } else {
	                echo $resultSelectPrefectures[$i]['t_prefecturename_'.strtolower($_SESSION['UserLang'])]; 
	            }?></option><?php endfor; 
	       ?></select>
           <?php 
           
           
           // country is not Japan, do not load select element rather input element
           elseif(isset($updateBuyerResult['fk_contact_country']) && ($updateBuyerResult['fk_contact_country'] != 109)):?>    
           		<input type="text" 
           			   name="BuyerPrefectureText" 
           			   id="BuyerPrefectureText" 
           			   class="required" 
           			   value="<?php if (isset($updateBuyerResult['t_prefecture_text'])){
						        if ($_SESSION['UserLang'] == 'JP'){
					                echo mb_convert_encoding($updateBuyerResult['t_prefecture_text'], "UTF-8", "SJIS");
					            } else {
					                echo $updateBuyerResult['t_prefecture_text'];; 
					           	}
							}?>"
           			   title=""/>
           <?php 
           
           
           // initial load for new buyer, no prefecture has been recorded
           else: ?>
           <select name="BuyerPrefectureFK" id="BuyerPrefectureFK"><?php 
           for ($i = 0; $i < count($resultSelectPrefectures); $i++): 
           ?><option value="<?php echo $resultSelectPrefectures[$i]['pk_prefecture']; ?>"<?php 
           	
           		// default value of 1 = Tokyo for first time buyers
           		if(($_SESSION['Bool_BuyerIsNew'] == 1) && ($resultSelectPrefectures[$i]['pk_prefecture'] == 1)){
           			echo ' selected="selected"';
           		}
           	
           		?>><?php 
           if ($_SESSION['UserLang'] == 'JP'){
	                echo mb_convert_encoding($resultSelectPrefectures[$i]['t_prefecturename_jp'], "UTF-8", "SJIS");
	            } else {
	                echo $resultSelectPrefectures[$i]['t_prefecturename_'.strtolower($_SESSION['UserLang'])]; 
	            }?></option><?php endfor; 
           ?></select><?php endif;
           ?>
          
           <span class="example prefecture"><?php echo $Prefecture_Advice; ?></span>
        
      </div>
      
      
      
      
      <!-- CitySuburb --> 
      <div class="row">
	      <label for="BuyerCityTown"><?php echo $CitySuburb; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerCityTown" 
		      id="BuyerCityTown" 
		      class="required" 
		      title="<?php echo $CitySuburb_ErrorAdvice; ?>"
		      value="<?php echo mb_convert_encoding($updateBuyerResult['t_city'], "UTF-8", "SJIS"); ?>"/>
	      <span class="example multiline"><?php echo $CitySuburb_Advice; ?></span>
      </div>
      
      
      <!-- Address1 --> 
      <div class="row">
	      <label for="BuyerAddress1"><?php echo $Address1; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerAddress1" 
		      id="BuyerAddress1" 
		      class="required" 
		      title="<?php echo $Address1_ErrorAdvice; ?>"
		      value="<?php echo mb_convert_encoding($updateBuyerResult['t_address1'], "UTF-8", "SJIS"); ?>"/>
	      <span class="example multiline"><?php echo $Address1_Advice; ?></span>
      </div>
      
      
      <!-- Address2 -->   
      <div class="row">
	      <label for="BuyerAddress2"><?php echo $Address2; ?></label>
		  <input type="text" 
		      name="BuyerAddress2" 
		      id="BuyerAddress2" 
		      value="<?php echo mb_convert_encoding($updateBuyerResult['t_address2'], "UTF-8", "SJIS"); ?>"/>
	      <span class="example"><?php echo $Address2_Advice; ?></span>
      </div>
      
    <!-- Emergency Phone, Contact Person --> 
	<div class="row">
	      <label for="BuyerEmergencyPhone"><?php echo $EmergencyPhone; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerEmergencyPhone" 
		      id="BuyerEmergencyPhone" 
		      class="required" 
		      title="<?php echo $EmergencyPhone_ErrorAdvice; ?>"
		      value="<?php echo $updateBuyerResult['t_phone_emergency']; ?>"/>
	      <span class="example multiline"><?php echo $EmergencyPhone_Advice; ?></span>
      </div>
      
      <div class="row">
	      <label for="BuyerEmergencyContact"><?php echo $EmergencyContact; ?>
		      <span class="StarRequired">*</span></label>
		  <input type="text" 
		      name="BuyerEmergencyContact" 
		      id="BuyerEmergencyContact" 
		      class="required" 
		      title="<?php echo $EmergencyContact_ErrorAdvice; ?>"
		      value="<?php echo mb_convert_encoding($updateBuyerResult['t_phone_emergency_memo'], "UTF-8", "SJIS"); ?>"/>
	      <span class="example multiline"><?php echo $EmergencyContact_Advice; ?></span>
      </div>
      
      
      <!-- Buyer is skier -->   
      <div class="row gender">
      	  <label for="BuyerIsSkier"><?php echo $IsSkierLabel; ?></label>
	      <input type="radio" class="printed-mats"
	            name="BuyerIsSkier" 
	            id="BuyerIsSkierYes" 
	            value="1"
	            <?php if ((isset($updateBuyerResult['nBool_isSkier'])) && 
	            		  ($updateBuyerResult['nBool_isSkier'] == 1)){
	                       echo ' checked = "checked"';
	                  }?>>
          <label class="radio printed-mats" for="AccountReceivesNewsletterYes"><?php echo $IsSkierYes; ?></label>
          
	     
	      <input type="radio" class="printed-mats"
	            name="BuyerIsSkier" 
	            id="BuyerIsSkierNo" 
	            value="0"
	            <?php if ((isset($updateBuyerResult['nBool_isSkier'])) && 
	            		  ($updateBuyerResult['nBool_isSkier'] == 0)){
	                       echo ' checked = "checked"';
	                  }?>>
          <label class="radio printed-mats" for="BuyerIsSkierNo"><?php echo $IsSkierNo; ?></label>  
			
          <span class="example printed-mats" id="BuyerIsSkier_Advice"><?php echo $IsSkier_Advice; ?></span>
      </div> 
      
      
      <!-- User Language -->
      <div class="row" id="BuyerUserLanguage">
	      <label for="BuyerUserLanguage"><?php echo $UserLanguage; ?></label>
		  <span class="BuyerUserLanguageText"><?php 
		  // write user's current language
	      if ($updateBuyerResult['t_user_language'] == 'EN'){
	          echo $CurrentUserLanguageLabelEN; 
	      } else if ($updateBuyerResult['t_user_language'] == 'JP'){
			  echo $CurrentUserLanguageLabelJP;
		  } else if ($updateBuyerResult['t_user_language'] == 'CN'){
		      echo $CurrentUserLanguageLabelCN;
	      }
	      ?></span>
		  <span class="example"><?php 
			  if ($_SESSION['UserLang'] == 'EN'){
		      		$TargetUserLanguage1 = 'JP';
		      		//$TargetUserLanguage2 = 'CN';
		      		$ChangeUserLanguageLink1 = $ChangeUserLanguageToJP;
		      		//$ChangeUserLanguageLink2 = $ChangeUserLanguageToCN;
		      } else if ($_SESSION['UserLang'] == 'JP'){
		      		$TargetUserLanguage1 = 'EN';
		      		//$TargetUserLanguage2 = 'CN';
		      		$ChangeUserLanguageLink1 = $ChangeUserLanguageToEN;
		      		//$ChangeUserLanguageLink2 = $ChangeUserLanguageToCN;
		      } else if ($_SESSION['UserLang'] == 'CN'){
		      		$TargetUserLanguage1 = 'EN';
		      		$TargetUserLanguage2 = 'JP';
		      		$ChangeUserLanguageLink1 = $ChangeUserLanguageToEN;
		      		$ChangeUserLanguageLink2 = $ChangeUserLanguageToJP;
		      } else {
		      		echo $CurrentUserLanguageNotSet;
		      }
		      // set up language toggle link
		      echo '<a href="'
			       .$_SERVER['PHP_SELF']
			       // note that this resets the $_SESSION UserLang here as well
			       .'?UserLang='
			       .$TargetUserLanguage1 .'">' 
			       .$ChangeUserLanguageLink1 .'</a>';
		      /* echo '&nbsp;&nbsp;<a href="'
		      		.$_SERVER['PHP_SELF']
		      		// note that this resets the $_SESSION UserLang here as well
		      		.'?UserLang='
		      		.$TargetUserLanguage2 .'">'
		      		.$ChangeUserLanguageLink2 .'</a>'; */
		      ?>
		  </span>
      </div>
      
      <input type="hidden"
		      name="BuyerCurrentUserLang"
		      id="BuyerCurrentUserLang"
		      value="<?php echo $_SESSION['UserLang']; ?>"/>
      
      
      
      
      <!-- Receive Newsletter Preference -->   
      <div class="row gender">
      	  <label for="BuyerReceivesNewsletter"><?php echo $ReceivesNewsletterLabel; ?>
      	  	<span class="StarRequired">*</span></label>
	      <input type="radio" class="printed-mats required"
	            name="BuyerReceivesNewsletter" 
	            id="BuyerReceivesNewsletterYes" 
	            value="1"
	            title="<?php echo $ReceivesNewsletter_ErrorAdvice; ?>"
	            <?php if ((isset($updateBuyerResult['nBool_receivesNewsletter'])) && 
	            		  ($updateBuyerResult['nBool_receivesNewsletter'] == 1)){
	                       echo ' checked = "checked"';
	                  }?>>
          <label class="radio printed-mats" for="AccountReceivesNewsletterYes"><?php echo $ReceivesNewsletterYes; ?></label>
          
	     
	      <input type="radio" class="printed-mats required"
	            name="BuyerReceivesNewsletter" 
	            id="BuyerReceivesNewsletterNo" 
	            value="0"
	            
	            <?php if ((isset($updateBuyerResult['nBool_receivesNewsletter'])) && 
	            		  ($updateBuyerResult['nBool_receivesNewsletter'] == 0)){
	                       echo ' checked = "checked"';
	                  }?>>
          <label class="radio printed-mats" for="BuyerReceivesNewsletterNo"><?php echo $ReceivesNewsletterNo; ?></label>  
			
          <span class="example printed-mats" id="BuyerReceivesNewsletterAdvice"><?php echo $ReceivesNewsletterExplanation; ?></span>
      </div> 
     
     
      </form>
     <div class="actions">
				<button name="BuyerRegCancel" id="BuyerRegCancel" class="btn1 cancel">
			 		<span><?php echo $Button_AdultRegCancel_Label; ?></span>
				</button>
				<button name="<?php if ((isset($_SESSION['Bool_BuyerIsNew'])) && 
										($_SESSION['Bool_BuyerIsNew'] == 1)){
										echo 'FirstTimeBuyerRegSubmit';
									} else {
										echo 'BuyerRegSubmit';
									}?>" 
						id="<?php if ((isset($_SESSION['Bool_BuyerIsNew'])) && 
										($_SESSION['Bool_BuyerIsNew'] == 1)){
										echo 'FirstTimeBuyerRegSubmit';
									} else {
										echo 'BuyerRegSubmit';
									}?>" 
						class="btn1" type="submit" value="submit">
					<span><?php echo $Button_AdultRegConfirmLabel; ?></span>
				</button>
				
			</div> 
			
			    
		</fieldset>
		<!-- End buyer fieldset-->   
      
     
      
      
</div>  
<!-- End buyer section body -->  
      
      