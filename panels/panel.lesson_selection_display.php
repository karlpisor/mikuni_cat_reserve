<?php 
	// return output for skier summary display
    // content changes depending on what UserLang is loaded
    
	$resource = '';
	if (isset($j)){
	if (isset($resultSelectAvailableLessonsOfSkier) && !empty($resultSelectAvailableLessonsOfSkier)) {
		$resource = $resultSelectAvailableLessonsOfSkier[$j];
	} 
	}
	
	if (isset($k)){
		if (isset($resultSelectOrder) && !empty($resultSelectOrder)) {
			$resource = $resultSelectOrder[$k];
		}
	}
	
	// initialize debug
	$display_output = '';
	
    if (isset($resource) && !empty($resource)) {
    	
    	// date display
    	$summary_date_display = date_format(date_create($resource['d_session_date']), "n/j");
    	$lesson_date_display = date_format(date_create($resource['d_session_date']), "d");
    	
    	// time display
    	$lessonStartTime = date_format(date_create($resource['tm_starttime']), "g:ia");
    	$lessonEndTime = date_format(date_create($resource['tm_endtime']), "g:ia");
    	$lesson_time_display = $lessonStartTime .' - ' .$lessonEndTime;
    	
    	
    	
    	// set up short name (half-day, etc.)
    	$start_time_val = date_format(date_create($resource['tm_starttime']), "G");
    	$end_time_val = date_format(date_create($resource['tm_endtime']), "G");
    	//echo 'Start is: ' .$start_time_val .'<br>';
    	//echo 'End is: ' .$end_time_val .'<br>';
    	$lesson_length_hours = ($end_time_val - $start_time_val);
    	//echo 'Duration is: ' .$duration .'<br>';
    		
    	$lesson_length_display = '';
    	$lesson_length_hours_display = '';
    	
    	if ($lesson_length_hours > 7){
    		
    		$lesson_length_hours_display = 8;
    	} else if ($lesson_length_hours < 7 && $lesson_length_hours > 5){
    		
	    	$lesson_length_hours_display = 6;
    	} else if ($lesson_length_hours < 5 && $lesson_length_hours > 3){
    		
	    	$lesson_length_hours_display = 4;
    	} else if ($lesson_length_hours < 3 && $lesson_length_hours > 1){
    		
    		$lesson_length_hours_display = 2;
    	} else {
    		
    		$lesson_length_hours_display = 1;
    	}
    	
    	
    	// price
    	$lesson_price_display = '&yen;' .number_format($resource['n_price_1']);
    	$price_to_apply = $resource['n_price_1'];
    	
    	
    	
    	// set up lesson name display (= "Ski Private (English)") (= "Snowboard (Japanese)")
    	// initialize
    	$lesson_name_display = '';
    	$lesson_description = '';
    	
    	// Mikuni tour
    	if ($price_to_apply > 10000){
    		$lesson_name_display = $LessonDisplay_isMikuniTour;
    		$lesson_description = $LessonDescription_isMikuniTour;
    	}
    	
    	// avalance safety gear rental
    	if ($price_to_apply < 10000){
    		$lesson_name_display = $LessonDisplay_isMikuniGearRental;
    		// do not display time interval
    		$lesson_time_display = '';
    		$lesson_description = $LessonDescription_isMikuniGearRental;
    	}
    	
    	// language of instruction
    	// initialize
    	$lesson_lang_display = '';
    	if ($_SESSION['UserLang'] == 'EN'){
    		switch ($resource['t_session_instruction_lang']){
    			case 'JP':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_JP .')';
    				break;
    			case 'EN':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_EN .')';
    				break;
    			case 'CN':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_CN .')';
    				break;
    		}
    	} else if ($_SESSION['UserLang'] == 'CN'){
    		switch ($resource['t_session_instruction_lang']){
    			case 'JP':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_JP .')';
    				break;
    			case 'EN':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_EN .')';
    				break;
    			case 'CN':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_CN .')';
    				break;
    		}
    	} else {
    		// default is JP
    		switch ($resource['t_session_instruction_lang']){
    			case 'JP':
    				// most common case, no display for JP skiers
    				$lesson_lang_display = '';
    				break;
    			case 'EN':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_EN .')';
    				break;
    			case 'CN':
    				$lesson_lang_display = '&nbsp;(' .$LessonLangDisplay_CN .')';
    				break;
    		}
    	}
    	
    	
    	
    	
    	// set up ability level display
    	// only display for group (adult or junior) lessons
    	$lesson_level_display = '';
    	
    	if ($resource['nBool_session_isPrivate'] == 0){
    		
    		switch ($resource['inAbilityLevel']){
    			case 1:
    				$lesson_level_display = ' A';
    				break;
    			case 2:
    				$lesson_level_display = ' B';
    				break;
    			case 3:
    				$lesson_level_display = ' C';
    				break;
    			case 4:
    				$lesson_level_display = ' D';
    				break;
    			case 5:
    				$lesson_level_display = ' E';
    				break;
    			case 6:
    				$lesson_level_display = ' F';
    				break;
    		} 
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	// debug
    	$display_output = 'Lesson date: ' .$lesson_date_display .'<br>'
    					.'Lesson time: ' .$lesson_time_display .'<br>'
    					.'Lesson name: '.$lesson_name_display .'<br>'
    					.'Lesson price: '.$lesson_price_display .'<br>';
    	
    	//echo $display_output;
    	
	} // end resource check 
?>
	