

<!-- Start Nav -->

<nav>				
	<ul>
		<li class="item1"><a href="#"><?php echo $NavLink_LessonMenu; ?></a>
			
		</li>
		<li class="item2"><a href="#"><?php echo $NavLink_FAQ; ?></a>
			
		</li>
		<li class="item2"><a href="#"><?php echo $NavLink_Certification; ?></a>
			
		</li>
		<li class="item3"><a href="#"><?php echo $NavLink_AboutUs; ?></a>
			<ul>
				<?php if ($_SESSION['UserLang'] == 'JP'): ?>
				<li><a href="#"><?php echo $NavLink_WorkWithUs; ?></a></li>
				<li><a href="#"><?php echo $NavLink_StaffProfiles; ?></a></li>
				<?php endif; ?>
				
			</ul>
		</li>
		<?php if ($_SESSION['UserLang'] == 'JP'): ?>
		<li class="item2"><a href="#"><?php echo $NavLink_Blog; ?></a>
			
		</li>
		<?php endif; ?>
	</ul>				
</nav>
	
<!-- End Nav -->
