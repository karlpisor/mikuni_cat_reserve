<!-- Begin Skier Ability Selection Wizard -->
    
    <br class="Clear">
        
        <!-- Begin Adult Snowboard Level Select -->
        <div class="LevelSelectionWizard AdultSnowboardLevelSelectDiv">
        
        	<p class="wizard-title-wrapper"><?php echo $LevelWizard_Title; ?></p>
        
        	<p class="AdultSnowboardSelect choice step-1 AdultSnowboardLevelSelect_Step1_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_SkiedBeforeYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-1 AdultSnowboardLevelSelect_Step1_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_SkiedBeforeNo; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-2 AdultSnowboardLevelSelect_Step2_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_SideslipHeelYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-2 AdultSnowboardLevelSelect_Step2_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_SideslipHeelNo; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-3 AdultSnowboardLevelSelect_Step3_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_SideslipToeYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-3 AdultSnowboardLevelSelect_Step3_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_SideslipToeNo; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-4 AdultSnowboardLevelSelect_Step4_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_TurningYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-4 AdultSnowboardLevelSelect_Step4_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_TurningNo; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-5 AdultSnowboardLevelSelect_Step5_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_ControlledDescentYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-5 AdultSnowboardLevelSelect_Step5_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_ControlledDescentNo; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-6 AdultSnowboardLevelSelect_Step6_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_AllTerrainYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-6 AdultSnowboardLevelSelect_Step6_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_AllTerrainNo; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-7 AdultSnowboardLevelSelect_Step7_Yes">
        	<span><?php echo $LevelWizard_SnowboardAdult_HighPerformanceYes; ?></span>
        	</p>
        	
        	<p class="AdultSnowboardSelect choice step-7 AdultSnowboardLevelSelect_Step7_No">
        	<span><?php echo $LevelWizard_SnowboardAdult_HighPerformanceNo; ?></span>
        	</p>
        	
        </div>
        <!-- End Adult Snowboard Level Select -->
        
        
        
        <!-- Begin Junior Snowboard Level Select -->
        <div class="LevelSelectionWizard JuniorSnowboardLevelSelectDiv">
        
        	<p class="wizard-title-wrapper"><?php echo $LevelWizard_Title; ?></p>
        
        	<p class="JuniorSnowboardSelect choice step-1 JuniorSnowboardLevelSelect_Step1_Yes">
        	<span><?php echo $LevelWizard_SnowboardJunior_SkiedBeforeYes; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-1 JuniorSnowboardLevelSelect_Step1_No">
        	<span><?php echo $LevelWizard_SnowboardJunior_SkiedBeforeNo; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-2 JuniorSnowboardLevelSelect_Step2_Yes">
        	<span><?php echo $LevelWizard_SnowboardJunior_SideslipHeelYes; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-2 JuniorSnowboardLevelSelect_Step2_No">
        	<span><?php echo $LevelWizard_SnowboardJunior_SideslipHeelNo; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-3 JuniorSnowboardLevelSelect_Step3_Yes">
        	<span><?php echo $LevelWizard_SnowboardJunior_SideslipBothTurningYes; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-3 JuniorSnowboardLevelSelect_Step3_No">
        	<span><?php echo $LevelWizard_SnowboardJunior_SideslipBothTurningNo; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-4 JuniorSnowboardLevelSelect_Step4_Yes">
        	<span><?php echo $LevelWizard_SnowboardJunior_BeginnerRunYes; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-4 JuniorSnowboardLevelSelect_Step4_No">
        	<span><?php echo $LevelWizard_SnowboardJunior_BeginnerRunNo; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-5 JuniorSnowboardLevelSelect_Step5_Yes">
        	<span><?php echo $LevelWizard_SnowboardJunior_IntermediateRunYes; ?></span>
        	</p>
        	
        	<p class="JuniorSnowboardSelect choice step-5 JuniorSnowboardLevelSelect_Step5_No">
        	<span><?php echo $LevelWizard_SnowboardJunior_IntermediateRunNo; ?></span>
        	</p>
        	
        	
        	
        	
        	
        </div>
        <!-- End Junior Snowboard Level Select -->
        
        
        <div class="ControlButtons snowboard">
        <p class="ResultBtn choice display SnowboardResultDisplay"></p>
        
        <p class="ResultBtn choice confirm SnowboardConfirmButton" <?php if ($_SESSION['Bool_isSkierUpdate']): ?>
        id="UpdateSnowboarderIndex<?php echo '_' .$k; ?>"
        <?php else: ?>
        id="NewSnowboarderIndex"
        <?php endif; ?>><?php echo $ConfirmWizardLevelSelection; ?></p> 
        
        
        <p class="ResultBtn choice reset SnowboardResetButton" <?php if ($_SESSION['Bool_isSkierUpdate']): ?>
        id="UpdateSnowboarderIndex<?php echo '_' .$k; ?>"
        <?php else: ?>
        id="NewSnowboarderIndex"
        <?php endif; ?>><?php echo $ResetWizard; ?></p>
        
        
        
        <input type="hidden"
		      class="SnowboardLevel"
		      value="" />	

        </div>
        <!-- End Result display -->
        
   <!-- End Skier Ability Selection Wizard div --> 