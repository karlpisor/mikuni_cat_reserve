<?php
	// $selectLiabilityReleaseDetails is declared and available
	
	// load PDF creation library
	require_once 'lib/tcpdf/tcpdf.php';
	
	// invoke new PDF object
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
	
	// set up A4 page
	$pdf->AddPage('P', 'A4');
	$pdf->SetMargins(5, 0, 10, true);
	
	
	
	
	
	// set JPEG quality
	$pdf->setJPEGQuality(100);
	
	// output liability release JPEG from file
	$pdf->SetY($pdf->GetY()+3);
	
	// Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()) {
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'EN'){
		
		// insert image
		$pdf->Image('images/liability-release-EN-0314.jpg', 10, 10, '', '', 'JPEG', false, 'T', false, 300, 'C', false, false, 0, false, false, false);
		
	} else if ($selectLiabilityReleaseDetails['t_user_language'] == 'CN'){
		
		// insert image
		$pdf->Image('images/liability-release-JP-0314.jpg', 10, 10, '', '', 'JPEG', false, 'T', false, 300, 'C', false, false, 0, false, false, false);
		
	} else {
		
		// default JP
		$pdf->Image('images/liability-release-JP-0314.jpg', 10, 10, '', '', 'JPEG', false, 'T', false, 300, 'C', false, false, 0, false, false, false);
	}
	$pdf->Ln();
	
	
	
	
	// write names of skiers in JP
	$pdf->SetY($pdf->GetY()+215);
	
	// write label
	$pdf->SetFont('', 'B');
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'EN'){
		$pdf->SetFont('Helvetica', '', 10);
	} else {
		$pdf->SetFont('cid0jp', '', 10);
	}
	$pdf->SetX(45);
	$pdf->Cell(30, 6, $LiabilityReleasePDF_ParticipantLabel, 0, 0, 'L', 0, '', 0);
	$pdf->SetFont('', '');
	$pdf->Ln();
	$pdf->Line(45, $pdf->GetY()+1, 180, $pdf->GetY()+1, array('width' => 0.1));
	$pdf->SetY($pdf->GetY()+1);
	
	// skier output, one line each
	for ($i = 0; $i < count($resultSelectSkierDetails); $i++){
	
		// set start of line
		$pdf->SetX(60);
	
		// JP name output
		if ($selectLiabilityReleaseDetails['t_user_language'] == 'JP'){
			$pdf->SetFont('cid0jp', '', 10);
			$fullname_jp = mb_convert_encoding($resultSelectSkierDetails[$i]['t_lastname_jp'], "UTF-8", "SJIS") .' ' 
						  .mb_convert_encoding($resultSelectSkierDetails[$i]['t_firstname_jp'], "UTF-8", "SJIS");
			$pdf->Cell(30, 7, $fullname_jp, 0, 0, 'L', 0, '', 0);
				
				
		} else {
			// EN name output
			$pdf->SetFont('Helvetica', '', 10);
			$fullname_en = $resultSelectSkierDetails[$i]['t_firstname_jp'] .' ' .$resultSelectSkierDetails[$i]['t_lastname_jp'];
			$pdf->Cell(50, 7, $fullname_en, 0, 0, 'L', 0, '', 0);
		}
	
		// line return at end of each line item output
		$pdf->Ln();
	}
	
	
	
	
	
	
	
	
	// write name and address of buyer
	$pdf->SetY($pdf->GetY());
	
	// write buyer label
	$pdf->SetFont('', 'B');
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'EN'){
		$pdf->SetFont('Helvetica', '', 10);
	} else {
		$pdf->SetFont('cid0jp', '', 10);
	}
	$pdf->SetX(45);
	$pdf->Cell(15, 6, $LiabilityReleasePDF_BuyerLabel, 0, 0, 'L', 0, '', 0);
	$pdf->SetFont('', '');
	$pdf->Ln();
	$pdf->Line(45, $pdf->GetY()+1, 180, $pdf->GetY()+1, array('width' => 0.1));
	$pdf->SetY($pdf->GetY()+1);
	
	// JP name output
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'JP'){
		$pdf->SetX(60);
		$pdf->SetFont('cid0jp', '', 10);
		$fullname_jp = mb_convert_encoding($selectLiabilityReleaseDetails['t_lastname_jp'], "UTF-8", "SJIS") .' '
					  .mb_convert_encoding($selectLiabilityReleaseDetails['t_firstname_jp'], "UTF-8", "SJIS");
		$pdf->Cell(30, 7, $fullname_jp, 0, 0, 'L', 0, '', 0);
	
	} else {
		$pdf->SetX(60);
		// EN name output
		$pdf->SetFont('Helvetica', '', 10);
		$fullname_en = $selectLiabilityReleaseDetails['t_firstname_jp'] .' ' .$selectLiabilityReleaseDetails['t_lastname_jp'];
		$pdf->Cell(80, 7, $fullname_en, 0, 0, 'L', 0, '', 0);
	}
	
	
	
	
	
	
	
	// write address label
	$pdf->SetY($pdf->GetY()+7);
	$pdf->SetFont('', 'B');
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'EN'){
		$pdf->SetFont('Helvetica', '', 10);
	} else {
		$pdf->SetFont('cid0jp', '', 10);
	}
	$pdf->SetFont('', '');
	
	// prepare strings for display
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'JP'){
		
		$PrefectureJP = mb_convert_encoding($selectLiabilityReleaseDetails['t_prefecturename_jp'], "UTF-8", "SJIS");
		$CityTownString = mb_convert_encoding($selectLiabilityReleaseDetails['t_city'], "UTF-8", "SJIS");
		$Address1String = mb_convert_encoding($selectLiabilityReleaseDetails['t_address1'], "UTF-8", "SJIS");
		$Address2String = mb_convert_encoding($selectLiabilityReleaseDetails['t_address2'], "UTF-8", "SJIS");
		$PrefectureText = mb_convert_encoding($selectLiabilityReleaseDetails['t_prefecture_text'], "UTF-8", "SJIS");
		
	} else {
		
		$PrefectureEN = $selectLiabilityReleaseDetails['t_prefecturename_en'];
		$CityTownString = $selectLiabilityReleaseDetails['t_city'];
		$Address1String = $selectLiabilityReleaseDetails['t_address1'];
		$Address2String = $selectLiabilityReleaseDetails['t_address2'];
		$PrefectureText = $selectLiabilityReleaseDetails['t_prefecture_text'];
	}
	$PostalCodeString = $selectLiabilityReleaseDetails['t_postalcode'];
	
	
	// test for prefecture, overseas users do not use the integer prefecture key
	if (isset($selectLiabilityReleaseDetails['fk_prefecture']) 
		   && $selectLiabilityReleaseDetails['fk_prefecture'] > 0){
		
		// fk_prefecture is present = JP domestic address
		// test for CJK, if present, reset font
		$test_for_CJK_string = $CityTownString
							  .$Address1String
							  .$Address2String;
		require 'lib/detect_CJK.php';
		
		if (is_CJK($test_for_CJK_string)) {
			// CJK present
			$pdf->SetFont('Helvetica', '', 10);
			$pdf->Text(60, $pdf->GetY(), $PostalCodeString, false, false, true, 0, 0, $align='');
			$pdf->SetFont('cid0jp', '', 10);
			$city_prefecture = $PrefectureJP .$CityTownString;
			$pdf->Text(80, $pdf->GetY(), $city_prefecture, false, false, true, 0, 1, $align='');
			$address1_and_2 = $Address1String .' ' .$Address2String;
			$pdf->Text(60, $pdf->GetY(), $address1_and_2, false, false, true, 0, 1, $align='');
			$pdf->SetFont('Helvetica', '', 10);
		} else {
			// no CJK present
			$pdf->SetFont('Helvetica', '', 10);
		
			if (isset($Address2String) && !empty($Address2String)){
				$pdf->Text(60, $pdf->GetY(), $Address2String, false, false, true, 0, 1, $align='');
			}
			$pdf->Text(60, $pdf->GetY(), $Address1String, false, false, true, 0, 1, $align='');
			$city_prefecture_and_postal_code = $CityTownString .', ' .$PrefectureEN .' ' .$PostalCodeString;
			$pdf->Text(60, $pdf->GetY(), $city_prefecture_and_postal_code, false, false, true, 0, 1, $align='');
		}
		
	} else {
		
		// t_prefecture_text is a string = Ohio, Guangdong, etc.
		// test for CJK, if present, reset font
		$test_for_CJK_string = $CityTownString
								.$Address1String
								.$Address2String
								.$PrefectureText;
		require 'lib/detect_CJK.php';
		
		if (is_CJK($test_for_CJK_string)) {
			// CJK present
			$pdf->SetFont('Helvetica', '', 10);
			$pdf->Text(60, $pdf->GetY(), $PostalCodeString, false, false, true, 0, 0, $align='');
			$pdf->SetFont('cid0jp', '', 10);
			$city_prefecture = $PrefectureText .$CityTownString;
			$pdf->Text(80, $pdf->GetY(), $city_prefecture, false, false, true, 0, 1, $align='');
			$address1_and_2 = $Address1String .' ' .$Address2String;
			$pdf->Text(60, $pdf->GetY(), $address1_and_2, false, false, true, 0, 1, $align='');
			$pdf->SetFont('Helvetica', '', 10);
			
		} else {
			// no CJK present
			$pdf->SetFont('Helvetica', '', 10);
		
			if (isset($Address2String) && !empty($Address2String)){
				$pdf->Text(60, $pdf->GetY(), $Address2String, false, false, true, 0, 1, $align='');
			}
			$pdf->Text(60, $pdf->GetY(), $Address1String, false, false, true, 0, 1, $align='');
			$city_prefecture_and_postal_code = $CityTownString .', ' .$PrefectureText .' ' .$PostalCodeString;
			$pdf->Text(60, $pdf->GetY(), $city_prefecture_and_postal_code, false, false, true, 0, 1, $align='');
		}
	}
	
	
	
	
	
	
	
	
	
	// write timestamp and message
	$pdf->SetY($pdf->GetY()+1);
	
	// set up timestamp display
	$date = strtotime($selectLiabilityReleaseDetails['tm_liability_creation']);
	$timestamp_display = date('j-M-Y H:ia', $date);
	
	// write timestamp label
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'EN'){
		$pdf->SetFont('Helvetica', '', 10);
	} else {
		$pdf->SetFont('cid0jp', '', 10);
	}
	$pdf->SetX(45);
	$pdf->Cell(20, 6, $LiabilityReleasePDF_RegistrationLabel, 0, 0, 'L', 0, '', 0);
	$pdf->SetX(60);
	$pdf->SetFont('Helvetica', '', 10);
	$pdf->Cell(40, 6, $timestamp_display, 0, 0, 'L', 0, '', 0);
	//$pdf->Ln();
	
	// received message
	$pdf->SetX(100);
	if ($selectLiabilityReleaseDetails['t_user_language'] == 'EN'){
		$pdf->SetFont('Helvetica', '', 10);
	} else {
		$pdf->SetFont('cid0jp', '', 10);
	}
	$pdf->Cell(66, 5, $LiabilityReleasePDF_RegistrationMessage, 0, 0, 'L', 0, '', 0);
	
	?>
	
	
	
	
	
	