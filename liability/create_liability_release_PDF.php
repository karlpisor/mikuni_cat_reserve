<?php
	// creates and stores PDF for later printing

	// select liability release
	if (isset($_SESSION['OrderId'])){
		
		$orderId = $_SESSION['OrderId'];
		$selectLiabilityReleaseDetails = Order::GetLiabilityReleaseDetails($orderId);
		// load skier details
		$resultSelectSkierDetails = Order::GetSkierDetailsByOrder($orderId);
		
		// display in UTF-8
		echo 'Full text before update: ' .$selectLiabilityReleaseDetails['t_liability_text'] .'<br>';
	} 
	
	// layout PDF document
	require_once 'liability/panel.liability_PDF_layout.php';
	
	// set save folder location
	$folder_location = 'liability/liability_docs/';

	// set time display
	$time_display = date('d_M_Hi', time());

	// create filename: 12854_Liability_Release_11_Jan_1205.pdf
	// don't use JP double-byte name as this may cause error on file mail attachment
	$filename = $orderId
			.'_' .'Liability_Release'
			.'_' .$time_display .'.pdf';
	
	echo 'Filename = ' .$filename .'<br>';
	// encode to SJIS for insert
	$filename_to_insert = mb_convert_encoding($filename, "SJIS", "UTF-8");
	echo 'Filename to insert = ' .$filename_to_insert .'<br>';
	
	// udpate filename in the liability record
	$LiabilityId = $selectLiabilityReleaseDetails['pk_liability'];
	$updateLiabilityReleaseFilename = Order::UpdateLiabilityReleaseFilename(
			$filename_to_insert,
			$LiabilityId);
	
		// error handling
		if ($updateLiabilityReleaseFilename) {
			
			// update is okay
			$success_msg .= 'Liability release #' 
						 .$updateLiabilityReleaseFilename['pk_liability'] 
						 .' updated with filename "'
						 // display in UTF-8
						 .mb_convert_encoding($updateLiabilityReleaseFilename['t_liability_filename'], "UTF-8", "SJIS")
						 .' with content: "'
						 // display in UTF-8
						 .mb_convert_encoding($updateLiabilityReleaseFilename['t_liability_text'], "UTF-8", "SJIS")
						 .'" <br />';
				
		} else {
			// query returns 0 results. Throw error.
			$error_msg .= 'Error with liability filename update.';
		}
	
	// set full filename string including location, store for use in mail scripts
	$full_directory_and_filename = $folder_location .$filename;
	$_SESSION['Liability_Release_FilePath'] = $full_directory_and_filename;
	
	//output PDF file on server
	$pdf -> Output($full_directory_and_filename, "F");
	
	// record update is okay
	$success_msg .= 'The liability release record "' 
					 .$filename 
					 .'"<br>has been saved to the "' 
					 .$folder_location 
					 .'" directory.<br />';
	
	echo $success_msg;
	
?>