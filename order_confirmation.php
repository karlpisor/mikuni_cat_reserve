<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	require_once LANGUAGE_DIR .'lang.mail.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	require_once BUSINESS_DIR .'order.php';
	
	
	
	// initialize error message
	$error_msg = '';
	
	// debug
	if ($debugging){
		echo '<pre>';
		echo 'GET ';
		print_r($_GET);
		echo '</pre>';
	
	
		echo '<pre>';
		echo 'POST ';
		print_r($_POST);
		echo '</pre>';
	
		if (isset($resultSelectCouponsOfOrder) && !empty($resultSelectCouponsOfOrder)){
			echo '<pre>';
			echo 'Resource <br />';
			print_r($resultSelectCouponsOfOrder);
			echo '</pre>';
		}
		
	}
	
	// Load buyer, order details
	$BuyerPK = $_SESSION['BuyerId'];
	$BuyerIsOverseas = $_SESSION['Bool_BuyerIsOverseas'];
	$resultSelectBuyer = Contact::GetBuyerDetails($BuyerPK, $BuyerIsOverseas);
	
	$OrderPK = $_SESSION['OrderId'];
	$resultSelectOrder = Order::GetOrder($OrderPK);
	$resultSelectSkiersOfOrder = Order::GetSkierDetailsByOrder($OrderPK);
	$resultSelectOrderLIs = Order::GetOrderDetailsPKSort($OrderPK);
	$resultSelectCouponsOfOrder = Order::GetCouponsByOrder($OrderPK);
	
	
	
	// order is complete, totals updated, customer may or may not have paid
	$_SESSION['RegPageControl'] = 4;
	
	
	
	
	// Set page presentation variables, local for this page load only
	$showLargeBanner = 0;
	$showSmallBanner = 1;
	$showAccountTools = 0;
	$showNavigation = 0;
	$showSubNavigation = 0;
	$showLanguageToggle = 0;
	$showSocialFlags = 0;
	$pageType = 'Registration';
	
	
	
	
	
	// HTML starts here--------------------------------------//
	
	
	
	// load header
	require_once 'header_confirmation.php';
	
	?>
	
	    <!-- Start Main -->
		<div id="main">
						
		<!-- Start Content -->
		<div id="content" class="confirmation">
		
		<?php 
		// branch output based on order status
		if ($resultSelectOrder['fk_order_status'] == 5){
	
			// customer has paid successfully
	    	echo '<h1>' .$ThanksForOrderingPageHeader .'</h1>';
						
		} else {
			
			// customer has reserved and will pay by bank transfer
			echo '<h1>' .$ThanksForRegisteringPageHeader .'</h1>';
			
		}?>
	    
	    <p class="confirmation-header"><?php echo $ConfirmationMsg_ThanksHeader; ?></p>
	    
	    
	    
	    
	    <!---------------- order line item confirmation table --------------------------->
	    
	    <h2><?php echo $OrderDetailsLabel  .' ' .'#' .$_SESSION['OrderId'];?> </h2>
	  
	  		<?php
	  		// wide table output
	    	require_once 'panels/panel.confirmation_table_wide.php';
	    	
	    	// wide table: add optional coupon discount line(s)
	        if ((isset($resultSelectCouponsOfOrder)) && !empty($resultSelectCouponsOfOrder)){
	        	
	        	// initialize
	        	$coupon_code = '';
	        	$discount_amount = 0;
	        	$total_discount_amount = 0;
	        	
	        	for($j = 0; $j < count($resultSelectCouponsOfOrder); $j++){
	        		
	        		$coupon_code = $resultSelectCouponsOfOrder[$j]['t_coupon_code_string'];
	        		$discount_amount = $resultSelectCouponsOfOrder[$j]['n_line_item_price'];
	        		$discount_display = number_format($discount_amount);
	        	
		        	// coupon code line
			        echo '<tr class="discount-display">'
			        		.'<td class="coupon-code-display" colspan="4">' .$ConfirmationTableCouponCodeLabel 
			        		.'<span class="coupon-code">'
							.$coupon_code
							.'</span>'
			        		.'</td><td class="confirmation lesson-price discount">'
			        		.'- &yen;'
							.'<span class="coupon-amount">'
							.$discount_display
							.'</span>'
							.'</td>'
			        		.'</tr>';
			        
			        // increment total JPY
			        $order_total_JPY = $order_total_JPY - $discount_amount;
			        
			        // increment total discount for more than 1 coupon
			        $total_discount_amount = $total_discount_amount + $discount_amount;
				
	        	} // end for loop
		        
		    } // end check for coupon line item
		    
		    
		    
		    // output totals 
		    // must format output
		    $order_total_JPY = number_format($order_total_JPY);
		    
		    echo '<tr>'
		    	.'<td class="total-label" colspan="4">' .$ConfirmationTableTotalLabel .'</td>'
		    	.'<td class="confirmation lesson-price">' .'&yen;'
		    	.'<span class="total-display">' .$order_total_JPY .'</span>'
		    	.'</td>'
		    	.'</tr>';
		    
		    if ($resultSelectOrder['fk_order_status'] == 5){	
		    	echo '<tr>'
		    		.'<td class="total-label" colspan="4">' .$ConfirmationTablePaidOnDateTotalLabel .'&nbsp;' .date("n/j") .'</td>'
		    		.'<td class="confirmation lesson-price">' .'&yen;'
		    		.'<span class="total-display">' .$order_total_JPY .'</span>'
		    		.'</td>'
		    		.'</tr>';
		    	echo '<tr>'
		    		 .'<td class="total-label" colspan="4">' .$ConfirmationTableBalanceZeroLabel .'</td>'
		    		 .'<td class="confirmation lesson-price">' .'&yen;'
		    		 .'<span class="total-display">' .'0' .'</span>'
		    		 .'</td>'
		    		 .'</tr>';
		    }
		    	
		    // wide table: final row with tax advice
		    echo '<tr>'
		    	.'<td colspan="5" class="tax-advice">' .$AllPricesIncludeTax .'</td>'
		    	.'</tr>';
		     
		    // wide table: complete
		    echo '</table>';
		    
		    
		    
		    
		    
		    
		    
		    // narrow table output, for mobile display
		    require_once 'panels/panel.confirmation_table_narrow.php';
		     
		    // narrow table: add optional coupon discount line(s)
		    if ((isset($resultSelectCouponsOfOrder)) && !empty($resultSelectCouponsOfOrder)){
		    	 
		    	// initialize
		    	$coupon_code = '';
		    	$discount_amount = '';
		    	 
		    	for($j = 0; $j < count($resultSelectCouponsOfOrder); $j++){
		    
		    		$coupon_code = $resultSelectCouponsOfOrder[$j]['t_coupon_code_string'];
		    		$discount_amount = $resultSelectCouponsOfOrder[$j]['n_line_item_price'];
		    		$discount_display = number_format($discount_amount);
		    		 
		    		// coupon code line
		    		echo '<tr class="discount-display">'
		    				.'<td class="coupon-code-display" colspan="2">' .$ConfirmationTableCouponCodeLabel
		    				.'<span class="coupon-code">'
		    				.$coupon_code
		    				.'</span>'
		    				.'</td><td class="confirmation lesson-price discount">'
		    				.'- &yen;'
		    				.'<span class="coupon-amount">'
		    				.$discount_display
		    				.'</span>'
		    				.'</td>'
		    			.'</tr>';
		    
		    		// DO NOT increment total JPY
		    		
		    		// DO NOT increment discount total JPY
		    	
		    																			
		    	} // end for loop
		    	 
		    } // end check for coupon line item
		     
		     
		     
		    // narrow table: output totals
		    echo '<tr>'
		    		.'<td class="total-label" colspan="2">' .$ConfirmationTableTotalLabel .'</td>'
		    		.'<td class="confirmation lesson-price">' .'&yen;'
		    	    .'<span class="total-display">' .$order_total_JPY .'</span>'
		    	    .'</td>'
		    	 .'</tr>';
		    
		    if ($resultSelectOrder['fk_order_status'] == 5){	
		    	echo '<tr>'
		    		.'<td class="total-label" colspan="2">' .$ConfirmationTablePaidOnDateTotalLabel .'&nbsp;' .date("n/j") .'</td>'
		    		.'<td class="confirmation lesson-price">' .'&yen;'
		    		.'<span class="total-display">' .$order_total_JPY .'</span>'
		    		.'</td>'
		    		.'</tr>';
		    	echo '<tr>'
		    		 .'<td class="total-label" colspan="2">' .$ConfirmationTableBalanceZeroLabel .'</td>'
		    		 .'<td class="confirmation lesson-price">' .'&yen;'
		    		 .'<span class="total-display">' .'0' .'</span>'
		    		 .'</td>'
		    		 .'</tr>';
		    }
		    	 
		    // narrow table: final row with tax advice
		    echo '<tr>'
		    	    .'<td colspan="3" class="tax-advice">' .$AllPricesIncludeTax .'</td>'
		    	  .'</tr>';
		    
		    // narrow table: complete
		    echo '</table>';
		    
		    
		    
		    
		    
		    
		    
		    
		    // ------------------ confirmation messaging --------------------------------
		    
		    // branch output based on order status
		    if ($resultSelectOrder['fk_order_status'] == 5){
	
				// customer has paid successfully, show payment evidence, last4, etc.
		    	//echo '<h2>' .$PaymentMethodCC_Title .'</h2>';
		    	//echo '<p class="account">' .$PaymentMethodCC_Details .'</p><br />';
							
			} else {
	
				// payment not complete, we need customer to send bank transfer
				// how to pay details
				echo '<h2>' .$PaymentMethodBankTransfer_Title .'</h2>';
				echo '<p class="account">' .$PaymentMethodBankTransfer_Details .'</p><br />';
				echo '<p>' .$PaymentMethodBankTransfer_Explanation .'</p>';
			}
			
			// standard messaging for all orders
			echo '<h2>' .$ConfirmationMsg_RequiredChecklistHeader .'</h2>';
			echo '<p>' .$ConfirmationMsg_RequiredChecklist .'</p>';
			
			
			echo '<p class"contact-us">' .$ConfirmationMsg_ContactUs .'</p>';
			
			
			// messaging for ordered content
			if ($bool_Order_hasPrivateLesson == 1){
	
				// set meeting location from session value
				if (isset($_SESSION['LessonStartLocation']) && !empty($_SESSION['LessonStartLocation'])){
					
					// TODO: troubleshoot where this variable loads
					// Cannot do at present as we have no location = 6 lessons to choose from
					$meetingPoint = $_SESSION['LessonStartLocation'];
	
				} else {
	
					// default
					$meetingPoint = 3;
				}
				
				echo '<h2>' .$ConfirmationMsg_PrivateHeader .'</h2>';
				if ($meetingPoint == 3){
					echo '<p>' .$ConfirmationMsg_Private_MeetingPoint3 .'</p>'; 
				} else if ($meetingPoint == 6){
					echo '<p>' .$ConfirmationMsg_Private_MeetingPoint6 .'</p>'; 	
				}
				echo '<p>' .$ConfirmationMsg_Private_HowToMeet .'</p>'; 
				echo '<p>' .$ConfirmationMsg_Private_LiftTickets .'</p>';
				
			}
			if ($bool_Order_hasAdultGroupLesson == 1){
				echo '<h2>' .$ConfirmationMsg_AdultGroupHeader .'</h2>';
				echo '<p>' .$ConfirmationMsg_AdultGroup_HowToMeet .'</p>';
				echo '<p>' .$ConfirmationMsg_AdultGroup_Times .'</p>';
				echo '<p>' .$ConfirmationMsg_AdultGroup_LiftTickets .'</p>';
			}
			if ($bool_Order_hasJuniorGroupLesson == 1){
				echo '<h2>' .$ConfirmationMsg_JuniorGroupHeader .'</h2>';
				echo '<p>' .$ConfirmationMsg_AdultGroup_HowToMeet .'</p>';
				echo '<p>' .$ConfirmationMsg_JuniorGroup_Times .'</p>';
			}
			
				
			// buyer details
	        echo '<h2>' .$BuyerInformation .'</h2>';
	        require_once 'panels/panel.process_buyer.php';
	        
	        
	        // skier details
	        echo '<h2>' .$SkierInformation .'</h2>';
	        // loop through the skier information sets, create mail strings
	        require_once 'panels/panel.process_skier.php';
	        
	        
	        
	        // cancellation advice
	        echo '<h2>' .$RegardingCancellationsHeader .'</h2>';
	        echo '<p>' .$RegardingCancellationsText .'</p>';
	        
	        echo '<br />';
	        echo '<p>' .$PrintThisPage .'</p>';
	        
	        
	        //debug
	        //echo '<br>' .$_SESSION['Liability_Release_FilePath'];
	         
	         
	         
	        // ------------------ email --------------------------------
	        // mail is not live in local environment
	        if (MAIL_IS_LIVE){
	
	        	// send confirmation mail to customer
	        	include_once 'mail/mail.registration.php';
	        
	        }
	    
	    
	    ?>
	    </div>
		<!-- End Content -->
					
		</div>
		<!-- End Main -->  
	
	
	
	        
<?php 
	// Load footer, Javascript, and page closing code
	require_once 'footer_minimum.php';
	require_once 'js_page_closer.php';
?>