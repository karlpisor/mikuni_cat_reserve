<?php

	// Include utility files
	require_once 'include/config.php';
	
	// Use SSL connection for this page
	require_once 'presentation/link.php';
	Link::EnforceSSL();
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	
	// prep of sync fields for mailchimp
	// will only be managing contacts coming in from Web reservations, as we are polling the order_item table
	// phone/in-person reservations do not write to the order_item table
	
	
	// clear all sync fields for contact buyer, entire database
	Contact::ClearBuyerMailchimpSyncFields();
	
	
	
	
	// select all order line items with start dates out two weeks
	$selectUpcomingOrders = Contact::GetUpcomingRegisteredOrders();
	
	/* echo '<pre>';
	print_r($selectUpcomingOrders);
	echo '</pre>'; */
	
	// initialize vars
	$previous_order_pk = 0;
	$previous_buyer_fk = 0;
	$buyer_ordered_lessons = '';
	$_SESSION['BuyerOrderedLessons'] = '';
	
	// register function
	function updateBuyer(
			$previous_buyer_fk,
			$buyer_order_start_date,
			$buyer_order_end_date,
			$buyer_ordered_lessons){
	
				// update buyer mailchimp sync fields
				$resultUpdateSyncFields = Contact::UpdateBuyerMailchimpSyncFields(
						$previous_buyer_fk,
						$buyer_order_start_date,
						$buyer_order_end_date,
						$buyer_ordered_lessons);
					
				if ($resultUpdateSyncFields){
	
					echo '-------- SUCCESS ----------- Updated mailchimp sync fields for buyer record: "' .$resultUpdateSyncFields['email'] .'"<br />';
	
				} else {
	
					echo '-------- ERROR ----------- Error with sync field update.<br />';
				}
	
	} // end udpateBuyer()
	
	
	function processLineItem(
			$new_order_pk,
			$previous_order_pk,
			$line_item_start_time, 
			$line_item_program_code,
			$buyer_order_start_date,
			$buyer_order_end_date,
			$buyer_ordered_lessons){
		
		// set up new lesson date from lesson data
		$compare_lesson_date = date_create($line_item_start_time);
		$compare_lesson_date_string = date_format($compare_lesson_date,'Ymd');
		$new_lesson_date = date_format($compare_lesson_date, 'Y-m-d');
		
		echo 'This line item\'s lesson start date: ' .$new_lesson_date .'<br />';
		
		
		// compare with start date
		echo 'Comparing with order start date: ' .$buyer_order_start_date .'<br />';
		$compare_order_start_date_string = date_format(date_create($buyer_order_start_date),'Ymd');
		if ($compare_lesson_date_string < $compare_order_start_date_string){
		
			// new lesson date is EARLIER than the current start date
			echo $compare_lesson_date_string .' is less than ' .$compare_order_start_date_string .'.<br>';
			
			// update session var start date
			$_SESSION['BuyerOrderStartDate'] = $new_lesson_date;
				
			echo 'Mailchimp order start date updated to: ' .$_SESSION['BuyerOrderStartDate'] .'<br /><br />';
		} else {
			echo 'No update. Mailchimp order start date still: ' .$_SESSION['BuyerOrderStartDate'] .'<br /><br />';
		}
		
		// compare with end date
		echo 'Comparing with order end date: ' .$buyer_order_end_date .'<br />';
		$compare_order_end_date_string = date_format(date_create($buyer_order_end_date),'Ymd');
		if ($compare_lesson_date_string > $compare_order_end_date_string){
		
			// new lesson date is LATER than the current end date
			echo $compare_lesson_date_string .' is greater than ' .$compare_order_end_date_string .'.<br>';
			
			// update local var end date
			$_SESSION['BuyerOrderEndDate'] = $new_lesson_date;
				
			echo 'Mailchimp order end date updated to: ' .$_SESSION['BuyerOrderEndDate'] .'<br /><br />';
		} else {
			echo 'No update. Mailchimp order end date still: ' .$_SESSION['BuyerOrderEndDate'] .'<br /><br />';
		}
			
		// store program code
		// typical order will have multiple program codes
		if ($new_order_pk == $previous_order_pk){
			
			// same order, concatenate ordered lesson names
			$_SESSION['BuyerOrderedLessons'] = $_SESSION['BuyerOrderedLessons'] .', ' .$line_item_program_code;
			
		} else {
			
			// new order, clear lesson names
			$_SESSION['BuyerOrderedLessons'] = $line_item_program_code;
		}
		
		// update variable to use in user record update
		$buyer_order_start_date = $_SESSION['BuyerOrderStartDate'];
		$buyer_order_end_date = $_SESSION['BuyerOrderEndDate'];
		echo 'Buyer ordered lessons updated to: ' .$_SESSION['BuyerOrderedLessons'] .'<br>';
	}
	
	
	
	
	
	// loop through selected set to get data for sync field update, update buyer records
	if (count($selectUpcomingOrders)){
		
		for ($i = 0; $i < count($selectUpcomingOrders); $i++){
			
			// only one order per buyer processed, the first order for a buyer
			// select returns line items in order_item.fk_contact_buyer ASC, in order_item.pk_order ASC
			
			// set this line item's variables
			$line_item_start_time = $selectUpcomingOrders[$i]['start_time'];
			$line_item_program_code = $selectUpcomingOrders[$i]['program_code'];
			$new_order_pk = $selectUpcomingOrders[$i]['pk_order'];
			$new_buyer_fk = $selectUpcomingOrders[$i]['fk_contact_buyer'];
			
			
			
			echo '<br><br>---------------- Begin new order line item -----------------<br>';
			echo 'Previous order id = ' .$previous_order_pk .'<br>';
			echo 'This order id = ' .$new_order_pk .'<br>';
			echo 'Previous buyer id = ' .$previous_buyer_fk .'<br>';
			echo 'This buyer id = ' .$new_buyer_fk .'<br><br>';
			
			// SAME buyer fk
			if ($new_buyer_fk == $previous_buyer_fk){
				
				echo '------------- Processing order line item number ' .$i .' -----------------<br>';
				echo 'Buyer ids match. This is the same buyer as the previous line item.<br>';
				
				// SAME buyer fk, and SAME order, so process line item
				if ($new_order_pk == $previous_order_pk){
					
					echo 'Order ids match, proceed to process line item.<br><br>';
					
					// process the line item
					processLineItem(
						$new_order_pk,
						$previous_order_pk,
						$line_item_start_time, 
						$line_item_program_code,
						$_SESSION['BuyerOrderStartDate'],
						$_SESSION['BuyerOrderEndDate'],
						$_SESSION['BuyerOrderedLessons']);
					
					// is this the last line item of the found set? then we will update the current buyer
					if (($i + 1) == count($selectUpcomingOrders)){
						
						// recast the previous variable
						$previous_buyer_fk = $new_buyer_fk;
						
						// update buyer record
						updateBuyer(
							$previous_buyer_fk,
							$_SESSION['BuyerOrderStartDate'],
							$_SESSION['BuyerOrderEndDate'],
							$_SESSION['BuyerOrderedLessons']);
					}
					
				} 
				// SAME buyer fk, but different order = do not process line item
				else if ($new_order_pk != $previous_order_pk) {
					
					echo 'New order for same buyer. Do not process.<br />';
					
				}
				
				
				
			} 
			// NEW buyer fk, so process line item
			else if ($new_buyer_fk != $previous_buyer_fk){
				
				echo '------------- Processing order number ' .$i .' -----------------<br>';
				echo 'Buyer ids do not match. This is a new buyer.<br><br>';
				
				if ($previous_buyer_fk > 0){
					// we are now processing a line item for a new buyer, so update the LAST buyer's data
					// this happens before the local variables have been updated
					
					
					echo 'Updating previous buyer\'s contact record.<br>';
					// update buyer record
					echo 'Previous buyer id = ' .$previous_buyer_fk .'<br>';
					echo 'This order start date = ' .$_SESSION['BuyerOrderStartDate'] .'<br>';
					echo 'This order end date = ' .$_SESSION['BuyerOrderEndDate'] .'<br>';
					echo 'Ordered lessons = ' .$_SESSION['BuyerOrderedLessons'] .'<br>';
					
					updateBuyer(
						$previous_buyer_fk,
						$_SESSION['BuyerOrderStartDate'],
						$_SESSION['BuyerOrderEndDate'],
						$_SESSION['BuyerOrderedLessons']);
				}
				
				// new order, so initialize local order vars
				// will NOT run for the second and later line items in an order
				if ($new_order_pk != $previous_order_pk){
				
					echo '<br>Order ids do not match. This is a new order, initializing local variables.<br>';
					
					$_SESSION['BuyerOrderStartDate'] = '2050-01-01'; // far in the future
					$_SESSION['BuyerOrderEndDate'] = '1970-01-01'; // far in the past
					$_SESSION['BuyerOrderedLessons'] = ''; 
				}
				
				echo 'Order start date for comparison: ' .$_SESSION['BuyerOrderStartDate'] .'<br />';
				echo 'Order end date for comparison: ' .$_SESSION['BuyerOrderEndDate'] .'<br /><br>';
				
				// process the line item
				processLineItem(
					$new_order_pk,
					$previous_order_pk,
					$line_item_start_time, 
					$line_item_program_code,
					$_SESSION['BuyerOrderStartDate'],
					$_SESSION['BuyerOrderEndDate'],
					$_SESSION['BuyerOrderedLessons']);
				
			}
					
					
			
			
			
			
				
			
			
			// update duplicate check vars at end of line item
			$previous_order_pk = $selectUpcomingOrders[$i]['pk_order'];
			$previous_buyer_fk = $selectUpcomingOrders[$i]['fk_contact_buyer'];
				
		}
	
	} else {
	
		// query returns 0 results. Throw error.
		echo '<br><br>';
		echo 'No registered order line items available to process.';
		echo '<br><br>';
		
	}
	
	
	
	
	
	
	
	
	
?>