<?php
  // Set the 500 status code
  header('HTTP/1.0 500 Internal Server Error');

  require_once 'include/config.php';
  require_once PRESENTATION_DIR . 'link.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>
      TShirtShop Application Error (500): Demo Product Catalog from
      Beginning PHP and MySQL E-Commerce
    </title>
  <link type="text/css" rel="stylesheet" href="<?php echo Link::Build('css/reset.css'); ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo Link::Build('css/fonts_jp.css'); ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo Link::Build('css/programmer.css'); ?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo Link::Build('css/layout.css'); ?>" />
  
  
  
  
  </head>
  
  <body>
<div id="Outer">
	
  <div id="Home_Header">
    	<div id="LogoContainer">
    	<a href="<?php echo Link::Build(''); ?>">
        	<img src="<?php echo Link::Build('images/gomu_logo_380w.gif'); ?>" alt="ゴム報知新聞" />
        </a>
        </div>
        <div id="Home_Header_Ad">
        <a href="<?php echo Link::Build(''); ?>">
        	<img src="<?php echo Link::Build('images/ad_90x300.gif'); ?>" alt="ad 90x300" />
        </a>
        </div>
    </div>
    <!-- End Home_Header div -->
    
    <div id="Column1_Wide">
      <h1>
            TShirtShop is experiencing technical difficulties.
          </h1>
          <p>
            Please
            <a href="<?php echo Link::Build(''); ?>">visit us</a> soon,
            or <a href="mailto:<?php echo ADMIN_ERROR_MAIL; ?>">contact us</a>.
          </p>
          <p>Thank you!</p>
          <p>The TShirtShop team.</p>
          <div id="DeptList_Col1">
          </div>
          <div id="DeptList_Col2_Col3_Wrapper">
              <div id="DeptList_Col2">
              </div>
              <div id="DeptList_Col3">
              </div>
          </div>
      
      </div>
      <!-- End Column1_Wide div --> 
      <div id="Column3_Bottom" class="Column3">
      
      </div>
      <!-- End Column3_Bottom div -->
        
        
    
          
          
    <div id="Footer">
    
    </div> 
    <!-- End Footer div --> 
    
    </div>
    <!-- End Outer div -->  
</body>
</html>