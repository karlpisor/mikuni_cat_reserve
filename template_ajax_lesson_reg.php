<?php
	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	require_once LANGUAGE_DIR .'lang.messages.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'order.php';
	require_once BUSINESS_DIR .'contact.php';
    
	
	
	// initialize error message
	$error_msg = '';
	$success_msg = '';
	
	
	
	// initialize
	$JSONinput = '';
	$output = '';
		
	// get the raw POST data
    $rawData = file_get_contents("php://input");

    // this returns null if not valid json
    $JSONinput = json_decode($rawData);
    
    // return json data, create an array to hold this
    $resultInsertLessons = array();
  
    if (isset($JSONinput) && !empty($JSONinput)){
    	
    	// go through JSON array and check if any selected private lessons have been already ordered
    	// if so, stop processing and return error to user
    	// this loop must capture all errors
    	$checkResult = 0;
    	
    	// set up error array to hold multiple lines of errors
    	$SessionAvailabilityErrorArray = array();
    	$errorIndex = 0;
    	
    	// set up success array to hold multiple lines of success messages (at least 2)
    	$SessionReserveSuccessArray = array();
    	
    	
    	foreach ($JSONinput as $index => $reservation_check) {
    	
    		// set up variables
    		$LessonFK = $reservation_check->LessonId;
    		$SkierPK = $reservation_check->SkierId;
    		
    		// check each lesson for current availability
    		$resultCheckLessonAvailability = Order::CheckSessionAvailability($LessonFK);
    			 
    		if (isset($resultCheckLessonAvailability) && !empty($resultCheckLessonAvailability)){
    				 
    			// found a lesson in the JSON array that is already booked, update flag
    			$checkResult = 1;
    			
    			// skier name
	    		$resultSelectSkier = Contact::GetSkierDetails($SkierPK);
	    		if ($_SESSION['UserLang'] == 'EN'){
	    			$skierFullName = $resultSelectSkier['t_firstname_jp'] .' ' .$resultSelectSkier['t_lastname_jp'];
	    		} else if ($_SESSION['UserLang'] == 'CN'){
	    			// for CN
	    			$skierFullName = $resultSelectSkier['t_lastname_jp'] .' ' .$resultSelectSkier['t_firstname_jp'];
	    		} else {
	    			// default, for JP
	    			$skierFullName = mb_convert_encoding($resultSelectSkier['t_lastname_jp'], "UTF-8", "SJIS") .' ' 
	    							.mb_convert_encoding($resultSelectSkier['t_firstname_jp'], "UTF-8", "SJIS");
	    		}
	    			
	    		// date display
	    		$lesson_date = date_create($resultCheckLessonAvailability['d_session_date']);
	    		$formatted_lesson_date = date_format($lesson_date, 'n/j/Y');
	    		$lesson_day_number = date_format($lesson_date, 'j');
	    			
	    		// error string display
	    		$skier_error_message = sprintf($ErrorMsg_LessonSelection_SelectedLessonHasBeenBooked, $skierFullName, $formatted_lesson_date);
	    			
	    		// add these keys and values to the error array
	    		$SessionAvailabilityErrorArray[$errorIndex]['lesson_id']  = $LessonFK;
	    		$SessionAvailabilityErrorArray[$errorIndex]['formatted_date']  = $formatted_lesson_date;
	    		$SessionAvailabilityErrorArray[$errorIndex]['day_number']  = $lesson_day_number;
	    		$SessionAvailabilityErrorArray[$errorIndex]['skier_error_message'] = $skier_error_message;
    			
	    		// increment error index
	    		$errorIndex++;
    				 
    		} // end if isset
    		
    	} // end session availability check, looping foreach through all selected sessions
    	
    	
    	
    	if ($checkResult == 1){
    		 
    		echo json_encode($SessionAvailabilityErrorArray);
    		 
    		// break out of all loops and return to ajax script
    		//break;
    		//exit();
    	}
    	
    	
    	
    	
    	if ($checkResult == 0){
    		
    		// there are no conflicts to the creation of a new order with the selected lessons
    		// insert new order for this buyer
    		
    		// set up variables
    		$OrderBuyerFK = $_SESSION['BuyerId'];
    		$OrderStartDate = $_SESSION['LessonStartDate'];
    		$OrderEndDate = $_SESSION['LessonFinishDate'];
    		
    		$resultInsertOrder = Order::InsertOrder($OrderBuyerFK, $OrderStartDate, $OrderEndDate);
    		
    		if (isset($resultInsertOrder) && !empty($resultInsertOrder)){
    			 
    			// success
    			// add keys and values to the success array
    			$SessionReserveSuccessArray['success_message'] = 'Success! Your new order PK is: ' 
    					.$resultInsertOrder['pk_order'] .'.<br>';
    			 
    			// update session variable with order Id
    			$_SESSION['OrderId'] = $resultInsertOrder['pk_order'];
    			 
    		} else {
    			// error handling
    		}
    		 
    		// initialize
    		$sameLessonId = '';
    		$lessonPriceCheckLimit = GROUP_LESSON_PRICE_CUTOFF;
    		
	    	
	    	
	    	// insert lesson line items
	    	foreach ($JSONinput as $index => $insert_array) {
	    		//echo "Outside key: $outside_index" ."<br />\n";
	    		//print_r($insert_array)  ."<br />\n";
	    		//echo 'Skier ID is: ' .$insert_array->SkierId ."<br />\n";
	    		//echo 'Lesson ID is: ' .$insert_array->LessonId ."<br />\n";
	    		//echo 'Name is: ' .$insert_array->InstructorName ."<br />\n";
	    		
	    		// set up variables
	    		$OrderFK = $_SESSION['OrderId'];
	    		$LessonFK = $insert_array->LessonId;
	    		$SkierFK = $insert_array->SkierId;
	    		$LessonPrice = $insert_array->Price;
	    		// optional: instructor name
	    		//$InstructorName = $insert_array->InstructorName;
	    		$LessonHours = $insert_array->Hours;
	    		
	    		
	    		
	    		
	    		// insert skier lesson line item
	    		
	    		
	    		// insert order line item
	    		$resultInsertOrderLI = Order::InsertOrderLineItem(
	    				$OrderFK,
	    				$LessonFK,
	    				$SkierFK,
	    				$LessonPrice,
	    				$LessonHours);
	    		 
	    		if (isset($resultInsertOrderLI) && !empty($resultInsertOrderLI)){
	    		
	    			// success
	    			// add keys and values to the success array
	    			$SessionReserveSuccessArray['success_message'] .= ' Success! Your new order line item PK is: ' 
	    					.$resultInsertOrderLI['pk_order_line_item'] .'.<br>';
	    			 
	    			
	    			
	    		} else {
	    			// error handling
	    		}
	    		
	    		
	    		// set this row's id in a variable to check
	    		$sameLessonId = $LessonFK;
	    		
	    		
	    			
	    	
	    		
	    		
	    	} // all lesson line items inserted
	    	
	    	
	    	// insert instructor line items, iterating again over same array
	    	foreach ($JSONinput as $index => $instructor_insert_array) {
	    		
	    		// set up variables
	    		$OrderFK = $_SESSION['OrderId'];
	    		$LessonFK = $instructor_insert_array->LessonId;
	    		$SkierFK = $instructor_insert_array->SkierId;
	    		// encode to SJIS
	    		$InstructorName = mb_convert_encoding($instructor_insert_array->InstructorName, "SJIS", "UTF-8");
	    		$LessonHours = $instructor_insert_array->Hours;
	    		
	    		// insert another line item with the instructor designation
	    		// calculate price for option by number of hours
	    		$designationPrice = $LessonHours * INSTRUCTOR_DESIGNATE_HOURLY_FEE;
	    		
	    		// insert line item if instructor present
	    		if (!empty($InstructorName)){
		    		$resultInsertDesignationAsOrderLI = Order::InsertDesignationAsOrderLI(
		    				$OrderFK,
		    				$LessonFK,
		    				$SkierFK,
		    				$InstructorName,
		    				$LessonHours,
		    				$designationPrice);
		    		
		    		if (isset($resultInsertDesignationAsOrderLI) && !empty($resultInsertDesignationAsOrderLI)){
		    			 
		    			// success
		    			// add keys and values to the success array
		    			$SessionReserveSuccessArray['success_message'] .= ' Success! Instructor designate line item PK is: ' 
		    					   .$resultInsertDesignationAsOrderLI['pk_order_line_item'] .'.<br>';
		    			
		    		
		    			 
		    		} else {
		    			// error handling
		    		}
	    		}
	    	}
	    	
	    	
	    	// advance registration page control
	    	// user now has buyer and skier(s), and lesson(s) registered
	    	$_SESSION['RegPageControl'] = 3;
	    	
	    	//echo $output;
	    	echo json_encode($SessionReserveSuccessArray);

    	} // end check for previously registered lesson, to avoid double booking
    	
    } // end check for resource
    

    ?>