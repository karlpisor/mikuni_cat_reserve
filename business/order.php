<?php
	// Business tier class for reading order information
	class Order
	{
		
		// Retrieves available lessons for one skier
		public static function GetAvailableLessonsOfSkier(
				$LessonStartDate,
				$LessonEndDate,
				$BoolSkierIsAdult,
				$SkierPreferredLessonType,
				$SkierAbilityLevelSki,
				
				$SkierAbilityLevelSnowboard,
				$SkierInstructionLang,
				$LessonStartLocationFK)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_available_lessons_of_skier(
				:d_lesson_startdate,
				:d_lesson_enddate,
				:nBool_skierIsAdult,
				:n_preferred_lesson_type,
				:n_ability_level_ski,
					
				:n_ability_level_snowboard,
				:t_instruction_language,
				:fk_start_location)'; */
				
				
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inStartDate date
			2. @inEndDate date
			3. @inSkierIsAdult tinyint
			4. @inPreferredLessonType int
			5. @inSkiAbilityLevel int
			
			6. @inSnowboardAbilityLevel int
			7. @inSkierInstructionLang varchar(4)
			8. @inStartLocation int
			*/
			
			// Write SQL statement
			$sql = "exec order_get_available_lessons_of_skier ?,?,?,?,?,?,?,? ";
		
			// Build the parameters array
			$params = array(
				 $LessonStartDate
				,$LessonEndDate
				,$BoolSkierIsAdult
				,$SkierPreferredLessonType
				,$SkierAbilityLevelSki
					
				,$SkierAbilityLevelSnowboard
				,$SkierInstructionLang
				,$LessonStartLocationFK);
				
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Retrieves Mikuni guide lesson status for entire season
		public static function MikuniGetSeasonLessonStatus()
		{
			// Write SQL statement
			/* $sql = 'CALL mikuni_get_season_lesson_status()'; */
		
			// Write SQL statement
			$sql = "exec mikuni_get_season_lesson_status";
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}
		
		// Retrieves Mikuni guide lessons for one skier
		public static function MikuniGetAvailableLessonsOfSkier(
				$LessonStartDate,
				$LessonEndDate)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_available_lessons_of_skier(
			:d_lesson_startdate,
			:d_lesson_enddate)'; */
		
		
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			 1. @inStartDate date
			 2. @inEndDate date
			 */
				
			// Write SQL statement
			$sql = "exec mikuni_order_get_available_lessons_of_skier ?,? ";
		
			// Build the parameters array
			$params = array(
					$LessonStartDate
					,$LessonEndDate);
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Inserts new order record
		public static function InsertOrder(
				$OrderBuyerFK,
				$OrderStartDate,
				$OrderEndDate)
		{
			// Write SQL statement
			/* $sql = 'CALL order_insert_order(
						  :fk_parent,
						  :d_order_startdate,
						  :d_order_enddate)'; */
			
			
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inBuyerFK int
			2. @inOrderStartDate date
			3. @inOrderEndDate date
			*/
			
			// Write SQL statement
			$sql = "exec order_insert_order ?,?,? ";
		
			// Build the parameters array
			$params = array(
					 $OrderBuyerFK
					,$OrderStartDate
					,$OrderEndDate);
				
			// Execute the query and return the results
			// Must use GetRow() here because query returns single contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves single order record
		public static function GetOrder($orderId)
		{
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			//$sql = 'CALL order_get_order(:pk_order)';
			$sql = "exec order_get_order ? ";
				
			// Build the parameters array
			$params = array($orderId);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves detailed line items from an order, including skier and lesson data
		public static function GetOrderDetails($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_order_details(:fk_order)'; */
			
			
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_get_order_details ? ";	
			
			// Build the parameters array
			$params = array($orderId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		
		// Retrieves detailed line items from an order, including skier and lesson data, sorted by lesson PK
		public static function GetOrderDetailsPKSort($orderId)
		{
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			/* $sql = 'CALL order_get_order_details(:fk_order)'; */
			$sql = "exec order_get_order_details_pk_sort ? ";
		
			// Build the parameters array
			$params = array($orderId);
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Retrieves data for distinct skiers in a single order
		public static function GetSkierDetailsByOrder($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_skier_details_by_order(:pk_order)'; */
			 	
			 	 	
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_get_skier_details_by_order ? ";	
			
			// Build the parameters array
			$params = array($orderId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Retrieves buyer details for an order
		public static function GetOrderBuyerDetails($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_buyer_details(:pk_order)'; */
			 	
			 	  
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_get_buyer_details ? ";	
			
			// Build the parameters array
			$params = array($orderId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Inserts an instructor designation as a new line item for given order
		public static function InsertDesignationAsOrderLI(
				$OrderFK,
				$OrderLILessonFK,
				$OrderLISkierFK,
				$OrderLIInstructorName,
				$OrderLessonHours,
				$OrderLIPrice)
		{
			// Write SQL statement
			/* $sql = 'CALL order_insert_designation_as_order_line_item(
			:fk_order,
			:fk_lesson,
			:fk_contact_skier,
			:t_instructor_request,
			:n_line_item_price)'; */
				
				
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inOrderFK int
			2. @inLessonFK int
			3. @inSkierFK int
			4. @inInstructorName nvarchar(255)
			5. @inPrice int
			*/
			
			// Write SQL statement
			$sql = "exec order_insert_designation_as_order_line_item ?,?,?,?,?,? ";
		
			// Build the parameters array
			$params = array(
					 $OrderFK
					,$OrderLILessonFK
					,$OrderLISkierFK
					,$OrderLIInstructorName
					,$OrderLessonHours
					,$OrderLIPrice);
		
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
			
		// Marks an order as paid, updates order status, writes date and amount paid
		public static function MarkOrderAsPaid($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_mark_order_as_paid(:pk_order)'; */
			 	
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_mark_order_as_paid ? ";
				
			// Build the parameters array
			$params = array($orderId);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Checks the lesson FK of an order line item before insert, to prevent double bookings
		public static function CheckSessionAvailability($OrderLILessonFK)
		{
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inLessonFK int
			*/
				
			// Write SQL statement
			$sql = "exec order_check_session_availability ? ";
		
			// Build the parameters array
			$params = array($OrderLILessonFK);
		
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Inserts new line item for given order
		public static function InsertOrderLineItem(
									$OrderFK,
									$OrderLILessonFK,
									$OrderLISkierFK,
									$OrderLILessonPrice,
									$OrderLessonHours)
		{
			// Write SQL statement
			/* $sql = 'CALL order_insert_order_line_item(
						  :fk_order,
						  :fk_lesson,
						  :fk_contact_skier,
						  :n_line_item_price)'; */
			
			
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inOrderFK int
			2. @inLessonFK int
			3. @inSkierFK int
			4. @inPrice int
			*/
			
			// Write SQL statement
			$sql = "exec order_insert_order_line_item ?,?,?,?,? ";	
						  
			// Build the parameters array
			$params = array(
						   $OrderFK
						  ,$OrderLILessonFK
						  ,$OrderLISkierFK
						  ,$OrderLILessonPrice
						  ,$OrderLessonHours);		  
			
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Inserts new skier lesson line item for given order
		public static function InsertSkierLessonLineItem(
				$OrderFK,
				$OrderLILessonFK,
				$OrderLISkierFK)
		{
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inOrderFK int
			2. @inLessonFK int
			3. @inSkierFK int
			*/
			
			// Write SQL statement
			$sql = "exec order_insert_skier_lesson_line_item ?,?,? ";
		
			// Build the parameters array
			$params = array(
					 $OrderFK
					,$OrderLILessonFK
					,$OrderLISkierFK);
		
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Permanently deletes an order!
		// Any line items attached to this order will be deleted!
		public static function DeleteOrder($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_delete_order_and_line_items(:pk_order)'; */
			 	
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_delete_order_and_line_items ? ";
		
			// Build the parameters array
			$params = array($orderId);
		
			// Execute the query and return the results
			return DatabaseHandler::Execute($sql, $params);
		}
		
		// Updates an order from "temp" status to "completed reservation" status
		public static function UpdateTempOrderFlags($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_coupons_by_order(:fk_order)'; */
				
					 
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			 1. @inOrderId int
			 */
				
			// Write SQL statement
			$sql = "exec order_update_temp_flags ? ";
		
			// Build the parameters array
			$params = array($orderId);
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		
		
		
		
		
		
		
		
		
		//------------------Liability Releases----------------------------------
		
		// Inserts new liability release data
		public static function InsertLiabilityRelease(
				$ContactParentFK,
				$OrderFK,
				$LiabilityReleaseText)
		{
			// Write SQL statement
			/* $sql = 'CALL order_insert_liability_release(
						  :fk_contact_parent,
						  :fk_order,
						  :t_liability_text)'; */
				
				
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inContactBuyerFK int
			2. @inOrderFK int
			3. @inLiabilityReleaseText nchar
			*/
			
			// Write SQL statement
			$sql = "exec order_insert_liability_release ?,?,? ";
		
			// Build the parameters array
			$params = array(
					 $ContactParentFK
					,$OrderFK
					,$LiabilityReleaseText);
				
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves data for liability release, used to create PDF
		public static function GetLiabilityReleaseDetails($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_liability_release_details(:fk_order)'; */
			 	
			 	  
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_get_liability_release_details ? ";
				
			// Build the parameters array
			$params = array($orderId);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves data for liability release, used to create PDF
		public static function UpdateLiabilityReleaseFilename(
				$filename, 
				$liabilityId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_update_liability_release_filename(
					:t_liability_filename, 
					:pk_liability)'; */
			
			
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inLiabilityReleaseFilename varchar(200)
			2. @inLiabilityPK int
			*/
			
			// Write SQL statement
			$sql = "exec order_update_liability_release_filename ?,? ";
		
			// Build the parameters array
			$params = array(
					 $filename
					,$liabilityId);
		
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		
		
		
		
		
		
		//------------------Coupon Codes----------------------------------
		
		// Checks for valid coupon code
		public static function OrderCheckCouponCode($OrderCouponCode)
		{
			// Write SQL statement
			/* $sql = 'CALL order_check_coupon_code(:t_coupon_code_string)'; */
			 	
			 	  
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inCouponCodeString varchar(100)
			*/
			
			// Write SQL statement
			$sql = "exec order_check_coupon_code ? ";
				
			// Build the parameters array
			$params = array($OrderCouponCode);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Inserts a coupon code as a new line item for given order
		public static function InsertCouponCodeAsOrderLineItem(
				$OrderFK,
				$CouponCodeFK,
				$DiscountJPY)
		{
			// Write SQL statement
			/* $sql = 'CALL order_insert_coupon_code_as_order_line_item(
				:fk_order,
				:fk_coupon_code,
				:n_line_item_price)'; */
			
			
			/* Write SQL statement Call SP for MSSQL Ver.
			INParam
			1. @inOrderFK int
			2. @inCouponCodeFK int
			3. @inDiscountJPY int
			*/
			
			// Write SQL statement
			$sql = "exec order_insert_coupon_code_as_order_line_item ?,?,? ";
		
			// Build the parameters array
			$params = array(
					 $OrderFK
					,$CouponCodeFK
					,$DiscountJPY);
				
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves all coupons for single order, including donations
		public static function GetCouponsByOrder($orderId)
		{
			// Write SQL statement
			/* $sql = 'CALL order_get_coupons_by_order(:fk_order)'; */
			 	
			 	  
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inOrderId int
			*/
			
			// Write SQL statement
			$sql = "exec order_get_coupons_by_order ? ";
		
			// Build the parameters array
			$params = array($orderId);
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Deactivates a used coupon code
		public static function DeactivateCouponCode($couponId)
		{
			// Write SQL statement
			/* $sql = ' order_deactivate_coupon_code(:pk_coupon_code)'; */
				
					 
			/* Write SQL statement Call SP for MSSQL Ver.
			 INParam
			1. @inCouponCodePK int
			*/
				
			// Write SQL statement
			$sql = "exec order_deactivate_coupon_code ? ";
		
			// Build the parameters array
			$params = array($couponId);
		
			// Execute the query and return the results
			return DatabaseHandler::GetOne($sql, $params);
		}
		
		
		
		
		
		
		
		
		
		
		//------------------**optional**  Coupon Codes  **optional**----------------------------------
		
		// Updates order with coupon code string (used to enter Campfriend after the fact)
		public static function UpdateOrderCouponCode(
				$OrderPK,
				$OrderCouponCodeText)
		{
			// Write SQL statement
			$sql = 'CALL order_update_order_coupon_code(
				:pk_order,
				:t_order_marketing_coupon_code)';
		
			// Build the parameters array
			$params = array(
					':pk_order' => $OrderPK,
					':t_order_marketing_coupon_code' => $OrderCouponCodeText);
		
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves maximum pk_option value (used for duplication)
		public static function GetMaxCouponCodePK()
		{
			// Write SQL statement
			$sql = 'CALL order_get_max_coupon_code_pk';

			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql);
		}

		// Retrieves all coupon codes
		public static function GetAllCouponCodes()
		{
			// Write SQL statement
			$sql = 'CALL content_admin_get_all_coupon_codes';

			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}

		// Retrieves complete details for a single coupon code
		public static function GetCouponCodeDetails($couponId)
		{
			// Write SQL statement
			$sql = 'CALL content_admin_get_coupon_code(:pk_coupon_code)';

			// Build the parameters array
			$params = array(':pk_coupon_code' => $couponId);

			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Updates existing coupon code record
		public static function UpdateCouponCode(
				$CouponCodePK,
				$CouponCodeDesc_en,
				$CouponCodeDesc_jp,
				$CouponCodeString,
				$CouponCodeDiscount,
				$CouponCode_isActive,
				$CouponCode_isOneTime)
		{
			// Write SQL statement
			$sql = 'CALL content_admin_update_coupon_code(
				:pk_coupon_code,
				:t_coupon_desc_en,
				:t_coupon_desc_jp,
				:t_coupon_code_string,
				:n_coupon_code_discount,
				:nBool_coupon_code_isActive,
				:nBool_coupon_code_isOneTime)';
			
			// Build the parameters array
			$params = array(
				':pk_coupon_code' => $CouponCodePK,
				':t_coupon_desc_en' => $CouponCodeDesc_en,
				':t_coupon_desc_jp' => $CouponCodeDesc_jp,
				':t_coupon_code_string' => $CouponCodeString,
				':n_coupon_code_discount' => $CouponCodeDiscount,
				':nBool_coupon_code_isActive' => $CouponCode_isActive,
				':nBool_coupon_code_isOneTime' => $CouponCode_isOneTime);
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}

		// Inserts new coupon code record
		public static function InsertCouponCode(
				$CouponCodeDesc_en,
				$CouponCodeDesc_jp,
				$CouponCodeString,
				$CouponCodeDiscount,
				$CouponCode_isActive,
				$CouponCode_isOneTime)
		{
			// Write SQL statement
			$sql = 'CALL content_admin_insert_coupon_code(
				:t_coupon_desc_en,
				:t_coupon_desc_jp,
				:t_coupon_code_string,
				:n_coupon_code_discount,
				:nBool_coupon_code_isActive,
				:nBool_coupon_code_isOneTime)';
			
			// Build the parameters array
			$params = array(
				':t_coupon_desc_en' => $CouponCodeDesc_en,
				':t_coupon_desc_jp' => $CouponCodeDesc_jp,
				':t_coupon_code_string' => $CouponCodeString,
				':n_coupon_code_discount' => $CouponCodeDiscount,
				':nBool_coupon_code_isActive' => $CouponCode_isActive,
				':nBool_coupon_code_isOneTime' => $CouponCode_isOneTime);

			// Execute the query and return the results
			return DatabaseHandler::GetOne($sql, $params);
		}

		// Duplicates currently selected coupon code
		public static function DuplicateCouponCode($couponId, $targetCouponId)
		{
			// Write SQL statement
			$sql = 'CALL content_duplicate_coupon_code(
			:pk_coupon_code,
			:target_pk_coupon_code)';

			// Build the parameters array
			$params = array(
					':pk_coupon_code' => $couponId,
					':target_pk_coupon_code' => $targetCouponId);

			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);

		}

		
		

		
	
		
		
		
		
	}
?>