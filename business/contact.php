<?php
	// Business tier class for reading contact information
	class Contact
	{
		//------------------ Buyers and Skiers ----------------------------------
		
		// Retrieves complete details for a buyer
		public static function GetBuyerDetails(
					$buyerId, 
					$BuyerIsOverseas)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_get_buyer(
					:pk_contact, 
					:nBool_isOverseas)';
			*/		
			
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inBuyerId varchar(24)
			//	 2. @inBool_isOverseas int
			
			$sql = "exec contact_get_buyer ?,?";
			
			// Build the parameters array
			$params = array($buyerId,$BuyerIsOverseas);
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves complete details for a buyer
		public static function GetNewBuyerDetails($buyerId)
		{
			// Write SQL statement
			// $sql = 'CALL contact_get_new_buyer(:pk_contact)';
			
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inBuyerId varchar(24)
			
			$sql = "exec contact_get_new_buyer ?";
				
			// Build the parameters array
			$params = array($buyerId);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Inserts new buyer record
		public static function InsertBuyer(
				$Email,
				$Password,
				$UserLanguage)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_insert_buyer(
						  :t_email,
						  :t_password,
						  :t_user_language)';
			*/
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inEmail varchar(100)
			//	 2. @inPassword varchar(50)
			//	 3. @inUserLanguage varchar(10)
			
			$sql = "exec contact_insert_buyer ?,?,? ";

			// Build the parameters array
			$params = array($Email,$Password,$UserLanguage);
				
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
			
		// Updates existing buyer record
		public static function UpdateBuyer(
				$BuyerPK,
				$BuyerLastNameJP,
				$BuyerFirstNameJP,
				$BuyerLastNameJP_Kana,
				$BuyerFirstNameJP_Kana,
		
				$BuyerEmail,
				$BuyerPhone,
				$BuyerPostalCode,
				$BuyerCountryFK,
				$BuyerPrefectureFK,
				$BuyerPrefectureText,
				
				$BuyerCityTown,
				$BuyerAddress1,
				$BuyerAddress2,
				$BuyerEmergencyPhone,
				$BuyerEmergencyContact,
				
				$BuyerUserLang,
				$BuyerReceivesNewsletter,
				$BuyerIsSkier,
				$BuyerIsOverseas)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_update_buyer(
						  :pk_contact,
						  :t_lastname_jp,
						  :t_firstname_jp,
						  :t_lastname_furigana_jp,
						  :t_firstname_furigana_jp,
			
						  :t_email,
						  :t_phone,
						  :t_postalcode,
						  :fk_country,
						  :fk_prefecture,
					
						  :t_city_town,
						  :t_address1,
						  :t_address2,
						  :t_phone_emergency,
						  :t_contact_emergency,
					
						  :t_user_language,
						  :nBool_receivesNewsletter,
						  :nBool_isSkier,
						  :nBool_isOverseas)';
			*/
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inBuyerId varchar(24)
			//	2. @inLastNameJP varchar(255)
			//	3. @inFirstNameJP varchar(255)
			//	4. @inLastNameJP_Kana varchar(255)
			//	5. @inFirstNameJP_Kana varchar(255)
			//
			//	6. @inEmail varchar(100)
			//	7. @inPhone varchar(50)
			//	8. @inPostalCode varchar(30)
			//	9. @inCountryFK int
			//	10.@inPrefectureFK int
			//
			//	11.@inCity varchar(100)
			//	12.@inAddress1 varchar(100)
			//	13.@inAddress2 varchar(100)
			//	14.@inEmergencyPhone varchar(100)
			//	15.@inEmergencyContact varchar(100)
			//
			//	16.@inUserLang varchar(10)
			//	17.@inBoolReceivesNewsletter tinyint
			//	18.@inBool_isSkier tinyint
			//
			//	19.@inBool_isOverseas tinyint
			
			$sql = "exec contact_update_buyer ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";



			// Build the parameters array
			$params = array( $BuyerPK
							,$BuyerLastNameJP
							,$BuyerFirstNameJP
							,$BuyerLastNameJP_Kana
							,$BuyerFirstNameJP_Kana
					
							,$BuyerEmail
							,$BuyerPhone
							,$BuyerPostalCode
							,$BuyerCountryFK
							,$BuyerPrefectureFK
							,$BuyerPrefectureText

							,$BuyerCityTown
							,$BuyerAddress1
							,$BuyerAddress2
							,$BuyerEmergencyPhone
							,$BuyerEmergencyContact

							,$BuyerUserLang
							,$BuyerReceivesNewsletter
							,$BuyerIsSkier
							,$BuyerIsOverseas);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Updates existing user language setting
		public static function UpdateUserLanguage($NewUserLanguage, $BuyerPK)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_update_user_language(
						  :t_user_language,
						  :pk_contact)';
			*/
	
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inNewUserLang varchar(255)
			//	 2. @inBuyerPK int			

			$sql = "exec contact_update_user_language ?,?";

			// Build the parameters array
			$params = array($NewUserLanguage,$BuyerPK);
				
			// Execute the query; this does return a select statement
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves complete details for a skier
		public static function GetSkierDetails($skierId)
		{
			// Write SQL statement
			$sql = "exec contact_get_skier ?";
			
			// Build the parameters array
			$params = array($skierId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves list of skiers that belong to a buyer
		public static function GetSkiersOfBuyer($buyerId)
		{
			// Write SQL statement
			$sql = 'CALL contact_get_skiers_of_buyer(:fk_contact_buyer)';	
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inBuyerId varchar(24)
			$sql = "exec contact_get_skiers_of_buyer ?";
			
			// Build the parameters array
			$params = array($buyerId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}	
		
		// Inserts new skier record
		public static function InsertSkier(
				$BuyerFK,
				$SkierLastNameJP,
				$SkierFirstNameJP,
				$SkierLastNameJP_Kana,
				$SkierFirstNameJP_Kana,
		
				$SkierGender,
				$SkierBirthMonth,
				$SkierBirthDay,
				$SkierBirthYear,
				$SkierInstructionLang,
				
				$SkierLessonType,
				$SkierAbilityLevelSnowboard,
				$SkierAbilityLevelSki,
				$SkierFoodAllergies,
				$InstructorPrefs,
				
				$InstructorPrefs_FreeText)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_insert_skier(
						  :fk_contact_buyer,
						  :t_lastname_jp,
						  :t_firstname_jp,
						  :t_lastname_furigana_jp,
						  :t_firstname_furigana_jp,
		
						  :nBool_gender_isFemale,
						  :n_birthmonth,
						  :n_birthday,
						  :y_birthyear,
						  :t_instruction_language,
					
						  :n_preferred_lesson_type,
						  :n_ability_level_snowboard,
						  :n_ability_level_ski,
						  :t_contact_food_allergies,
						  :t_instructor_prefs,
					
						  :t_instructor_prefs_freetext)';
			*/
			
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inBuyerFK int
			//	2. @inLastNameJP varchar(255)
			//	3. @inFirstNameJP varchar(255)
			//	4. @inLastNameJPKana varchar(255)
			//	5. @inFirstNameJPKana varchar(255)
			//	6. @inGenderIsFemale tinyint
			//
			//	7. @inBirthMonth int
			//	7. @inBirthDay int
			//	7. @inBirthYear int
			//	8. @inInstructionLang varchar(10)
			//
			//	9. @inPreferredLessonType int
			//	10.@inAbilityLevelSnowboard int
			//	11.@inAbilityLevelSki int
			//	12.@inFoodAllergies varchar(255)
			//	13.@inInstructorPrefs varchar(500)
			//
			//	14.@inInstructorPrefsFreetext varchar(255)			
			
			$sql = "exec contact_insert_skier ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ";
			
			// Build the parameters array
			$params = array(
					 $BuyerFK
					,$SkierLastNameJP
					,$SkierFirstNameJP
					,$SkierLastNameJP_Kana
					,$SkierFirstNameJP_Kana

					,$SkierGender
					,$SkierBirthMonth
					,$SkierBirthDay
					,$SkierBirthYear
					,$SkierInstructionLang

					,$SkierLessonType
					,$SkierAbilityLevelSnowboard
					,$SkierAbilityLevelSki
					,$SkierFoodAllergies
					,$InstructorPrefs

					,$InstructorPrefs_FreeText);
				
			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
			
		// Updates existing skier record
		public static function UpdateSkier(
				
				$SkierPK,
				$SkierLastNameJP,
				$SkierFirstNameJP,
				$SkierLastNameJP_Kana,
				$SkierFirstNameJP_Kana,
		
				$SkierGender,
				$SkierBirthMonth,
				$SkierBirthDay,
				$SkierBirthYear,
				$SkierInstructionLang,
				
				$SkierLessonType,
				$SkierAbilityLevelSnowboard,
				$SkierAbilityLevelSki,
				$SkierFoodAllergies,
				$InstructorPrefs,
				
				$InstructorPrefs_FreeText)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_update_skier(
						  :pk_contact,
						  :t_lastname_jp,
						  :t_firstname_jp,
						  :t_lastname_furigana_jp,
						  :t_firstname_furigana_jp,
		
						  :nBool_gender_isFemale,
						  :n_birthmonth,
						  :n_birthday,
						  :y_birthyear,
						  :t_instruction_language,
					
						  :n_preferred_lesson_type,
						  :n_ability_level_snowboard,
						  :n_ability_level_ski,
						  :t_contact_food_allergies,
						  :t_instructor_prefs,
					
						  :t_instructor_prefs_freetext)';
			*/
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inSkierPK varchar(24)
			//	2. @inLastNameJP varchar(255)
			//	3. @inFirstNameJP varchar(255)
			//	4. @inLastNameJPKana varchar(255)
			//	5. @inFirstNameJPKana varchar(255)
			//
			//	6. @inGenderIsFemale tinyint
			//  7. @inBirthMonth int
			//  7. @inBirthDay int
			//	7. @inBirthYear int
			//	8. @inInstructionLang varchar(10)
			//	9. @inPreferredLessonType int
			//	10.@inAbilityLevelSnowboard int
			//	11.@inAbilityLevelSki int
			//	12.@inFoodAllergies varchar(255)
			//	13.@inInstructorPrefs varchar(255)
			//	14.@inInstructorPrefsFreetext varchar(255)
			$sql = "exec contact_update_skier ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ";


		
			// Build the parameters array
			$params = array(
							 $SkierPK
							,$SkierLastNameJP
							,$SkierFirstNameJP
							,$SkierLastNameJP_Kana
							,$SkierFirstNameJP_Kana
					
							,$SkierGender
							,$SkierBirthMonth
							,$SkierBirthDay
							,$SkierBirthYear
							,$SkierInstructionLang
					
							,$SkierLessonType
							,$SkierAbilityLevelSnowboard
							,$SkierAbilityLevelSki
							,$SkierFoodAllergies
							,$InstructorPrefs
					
							,$InstructorPrefs_FreeText);

			// Execute the query and return the results
			// Must use GetRow() here because query returns contact row
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Updates buyer record status, used to track buyer progress through registration
		public static function UpdateBuyerProgressStatus($PageControlStatus, $BuyerPK)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_update_buyer_progress_status(
					:fk_progress_status,
					:pk_contact)';
			*/
		
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inBuyerProgressStatusFK int
			//	 2. @inBuyerPK int
		
			$sql = "exec contact_update_buyer_progress_status ?,?";
		
			// Build the parameters array
			$params = array($PageControlStatus,$BuyerPK);
		
			// Execute the query; this does return a select statement
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		
		
		
		
		
		
		
		
		
		//------------------ Mailchimp ----------------------------------
		
		// Clears buyer sync fields
		public static function ClearBuyerMailchimpSyncFields()
		{
			// Write SQL statement Call SP for MSSQL
		
			$sql = "exec contact_clear_buyer_mailchimp_sync_fields";
		
			// Execute the query; this does return a select statement
			return DatabaseHandler::Execute($sql);
		}
		
		
		// Selects upcoming customer visits (orders)
		public static function GetUpcomingRegisteredOrders()
		{
			// Write SQL statement Call SP for MSSQL
			$sql = "exec contact_get_upcoming_regd_orders";
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}
		
		// Updates buyer sync fields
		public static function UpdateBuyerMailchimpSyncFields(
					$buyerId
					,$orderStartDate
					,$orderEndDate
					,$orderedLessons)
		{
			// Write SQL statement
			$sql = "exec contact_update_buyer_mailchimp_sync_fields ?,?,?,?";
				
			// Build the parameters array
			$params = array(
					$buyerId
					,$orderStartDate
					,$orderEndDate
					,$orderedLessons);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		
		// Selects records for Mailchimp newsletter list sync
		public static function GetMailchimpSyncNewsletter()
		{
			// Write SQL statement Call SP for MSSQL
		
			$sql = "exec contact_get_mailchimp_sync_newsletter";
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}
		
		
		// Selects records for Mailchimp currently registered list sync
		public static function GetMailchimpSyncCurrentRegd()
		{
			// Write SQL statement Call SP for MSSQL
		
			$sql = "exec contact_get_mailchimp_sync_current_regd";
		
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}
		
		
		
		
		
		
		
		//------------------ Country, Prefecture ----------------------------------
		
		// Retrieves all prefectures details
		public static function GetPrefectures($SortOrderLang)
		{
			// Write SQL statement
			// $sql = 'CALL contact_get_prefectures(:sort_order_lang)';
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @sortOrder varchar(10)
			
			$sql = 'exec contact_get_prefectures ? ';
				
			// Build the parameters array
			$params = array($SortOrderLang);

			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql, $params);
		}	
		
		// Retrieves details of a specific prefecture
		public static function GetPrefectureDetails($prefectureId)
		{
			// Write SQL statement
			// $sql = 'CALL contact_get_prefecture(:pk_prefecture)';

			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inPrefectureId int
			$sql = "exec contact_get_prefecture ? ";

				
			// Build the parameters array
			$params = array($prefectureId);
				
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves all country details
		public static function GetCountries()
		{
			// Write SQL statement
			// $sql = 'CALL contact_get_countries';

			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	None
			$sql = "exec contact_get_countries ";
			
			
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}
		
		// Retrieves details of a specific country
		public static function GetCountryDetails($countryId)
		{
			// Write SQL statement
			// $sql = 'CALL contact_get_country(:pk_country)';

			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inCountryId int
			$sql = "exec contact_get_country ? ";


			
			// Build the parameters array
			$params = array($countryId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
			
		
		
		
		
		//------------------ Security/Access ----------------------------------
		
		// Executes lookup on a user's contact record by matching access credentials
		public static function ContactTestAccess($Email, $Password)
		{
			// Write SQL statement
			/*
			$sql = 'CALL contact_test_access(
						  :email,
						  :password)';	
			*/
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inEmail varchar(100)
			//	2. @inPassword varchar(50)
			$sql = "exec contact_test_access ?,? ";

			// Build the parameters array
			$params = array($Email,$Password);
			
			// Execute the query and return the results
			// Use GetAll here to flush out potential doubles
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Checks database for previously registered email
		public static function ContactCheckEmail($Email)
		{
			// Write SQL statement
		//	$sql = 'CALL contact_check_email(

			// Write SQL statement Call SP for MSSQL Ver.
		//	$sql = "exec contact_check_email '" .$Email ."'";
			$sql = "exec contact_check_email ? ";

			// Build the parameters array >>> Case MSSQL > 0 strings
			$params = array($Email);

/*
require_once 'extentions/dBug.php';
new dBug($sql);
new dBug($params);
exit;
*/

			// Execute the query and return the results
			// Use GetAll here to flush out potential doubles
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		// Updates existing password with temp password
		public static function SetTempPassword($tempPassword, $UserId)
		{
			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	 1. @inUserPK varchar(24)
			//	 2. @inTempPassword varchar(20)
			
			$sql = "exec contact_set_temp_password ?,? ";


			// Build the parameters array
			$params = array($UserId,$tempPassword);

			// Execute the query
			return DatabaseHandler::Execute($sql, $params);
		}
		
		// Generate a random character string of 8 characters length
		public static function generateRandStr($length = 8, $chars = 'ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstvwxyz23456789')
		{
			// Length of character list
			$chars_length = (strlen($chars) - 1);
			// Start our string
			$string = $chars{rand(0, $chars_length)};
		    
			// Generate random string
			for ($i = 1; $i < $length; $i = strlen($string))
			{
				// Grab a random character from our list
				$r = $chars{rand(0, $chars_length)};
			    // Make sure the same two characters don't appear next to each other
				if ($r != $string{$i - 1}) $string .=  $r;
			}
		   
			// Execute and return string
			return $string;
		}
		
		// Executes lookup on an admin user's contact record by matching access credentials
		public static function AdminContactTestAccess($Email, $Password)
		{
			// Write SQL statement
			$sql = 'CALL admin_contact_test_access(
						  :email,
						  :password)';

			// Write SQL statement Call SP for MSSQL Ver.
			//	 INParam
			//	1. @inBuyerId varchar(24)
			//	2. @inLastNameJP varchar(255)

			$sql = "exec contact_update_buyer ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";

				
			// Build the parameters array
			$params = array(
					':email' => $Email,
					':password' => $Password);
				
			// Execute the query and return the results
			// Use GetAll here to flush out potential doubles
			return DatabaseHandler::GetAll($sql, $params);
		}
		
		
		
		
		
		
	}
?>