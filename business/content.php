<?php
	// Business tier class for page/module data
	
	class Content
	{
		//------------------Index and Static Pages---------------------------
		
		// Retrieves complete details for a page
		public static function GetPage($pageId)
		{
			// Write SQL statement
			$sql = 'CALL content_get_page(:pk_page)';	
			
			// Build the parameters array
			$params = array(':pk_page' => $pageId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Retrieves page name
		public static function GetPageTitle($pageId)
		{
			// Write SQL statement
			if ($_SESSION['UserLang'] == 'EN'){
				$sql = 'CALL content_get_page_title_en(:pk_page)';
			} else {
				$sql = 'CALL content_get_page_title_jp(:pk_page)';
			}
				
			// Build the parameters array
			$params = array(':pk_page' => $pageId);
			
			// Execute the query and return the results
			return DatabaseHandler::GetOne($sql, $params);
		}
		
		// Retrieves array of all pages
		public static function GetAllPages()
		{
			// Write SQL statement
			$sql = 'CALL content_get_all_pages';	
			
			// Execute the query and return the results
			return DatabaseHandler::GetAll($sql);
		}	
		
		// Updates existing page record
		public static function UpdatePage(
						$PageNameEN,
						$PageNameJP,
						$PageDescEN,
						$PageDescJP,
						$PageSortOrder,
						$PageBodyId,
						$PagePK)
		{
			// Write SQL statement
			$sql = 'CALL admin_content_update_page(
						  :t_page_name_en,
						  :t_page_name_jp,
						  :t_page_description_en,
  						  :t_page_description_jp,
  						  :n_page_sort_order,
  						  :t_page_body_id,
  						  :pk_page)';	
						  
			// Build the parameters array
			$params = array(
						  ':t_page_name_en' => $PageNameEN,
						  ':t_page_name_jp' => $PageNameJP,
						  ':t_page_description_en' => $PageDescEN,
						  ':t_page_description_jp' => $PageDescJP,
						  ':n_page_sort_order' => $PageSortOrder,
						  ':t_page_body_id' => $PageBodyId,
						  ':pk_page' => $PagePK);		  
			
			// Execute the query and return the results
			return DatabaseHandler::GetRow($sql, $params);
		}
		
		// Inserts new page record
		public static function InsertPage(
						$PageNameEN,
						$PageNameJP,
						$PageDescEN,
						$PageDescJP,
						$PageSortOrder,
						$PageBodyId)
		{
			// Write SQL statement
			$sql = 'CALL admin_content_insert_page(
						  :t_page_name_en,
						  :t_page_name_jp,
						  :t_page_description_en,
  						  :t_page_description_jp,
  						  :n_page_sort_order,
  						  :t_page_body_id)';	
						  
			// Build the parameters array
			$params = array(
						  ':t_page_name_en' => $PageNameEN,
						  ':t_page_name_jp' => $PageNameJP,
						  ':t_page_description_en' => $PageDescEN,
						  ':t_page_description_jp' => $PageDescJP,
						  ':n_page_sort_order' => $PageSortOrder,
						  ':t_page_body_id' => $PageBodyId);		  
			
			// Execute the query and return the results
			return DatabaseHandler::GetOne($sql, $params);
		}
		
		
		
		
		
		
		
		
		
		
		
		
	} // end Class Content
?>
