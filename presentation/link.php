<?php
	class Link
	{
		public static function Build($link, $type = 'http')
		{
			// The Build method strips down the URL and rebuilds it
			// The first parameter sets the URL string to be processed
			// The second parameter assigns http or https as the type of URL string to be processed
			// $base assigns http or https, then adds the base server name
			$base = (($type == 'http' || USE_SSL == 'false') ? 'http://' : 'https://') .
					getenv('SERVER_NAME');
		
			// VIRTUAL_LOCATION is the folder in which the site resides
			// this is /eng_adv_v2/ for the local dev site
			$link = $base .VIRTUAL_LOCATION .$link;
		
			return $link;
		}
		
		// Enforce page to be accessed through HTTPS
		public static function EnforceSSL()
		{
			// Enforce page to be accessed through HTTPS if USE_SSL is on
			if (USE_SSL == 'true' && getenv('HTTPS') != 'on'){
				
				// SSL is live, and the server is NOT in HTTPS mode
				// This switches the browser mode to secure
				header('Location: https://'
					.getenv('SERVER_NAME')
					.getenv('REQUEST_URI'));
				exit();	
			}
		}
		
		// Enforce page to be accessed through HTTP
		public static function RelaxSSL()
		{
			// Enforce page to be accessed through HTTP if USE_SSL is on
			if (USE_SSL == 'true' && getenv('HTTPS') == 'on'){
				
				// SSL is live, and the server is in HTTPS mode
				// This forces the page to come back to normal mode
				header('Location: http://'
					.getenv('SERVER_NAME')
					.getenv('REQUEST_URI'));
				exit();	
			}
		}
		
		
		
	}
?>