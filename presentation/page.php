<?php
class Page
{
	//public $mSiteUrl;
	
	// Define the default template file for the page contents
	// Not using Smarty right now
	public $mPageContents;
	
	// Page title
	// TODO: This is returning the word "Array" right now...must fix
	public $mPageTitle;
	
	// Private members
	private $_mPageId;
	
	
	// Class constructor
	public function __construct()
	{
		//$this->mSiteUrl = Link::Build('');
		
		// Check that PageId is set in the query string
		if (isset($_GET['PageId'])) {
			$this->_mPageId = (int)$_GET['PageId'];
		} else {
			trigger_error('PageId not set.');
		}
	}
	
	// Initialize presentation object
	public function init()
	{
		// Load page details
		if (isset($_GET['PageId'])){
			$this->mPageContents = Content::GetPage($this->_mPageId);
		}
		
		// TODO: Load the page title
		$this->mPageTitle = $this->_GetPageTitle();
			
			
	} // end init()
	
	
	// Returns the page title
	private function _GetPageTitle()
	{
		// default value
		$page_title = PAGE_TITLE_STEM;
		
		if (isset($_GET['PageId']))
		{
			$page_title = PAGE_TITLE_STEM .Content::GetPageTitle($this->_mPageId);
		}
		
		return $page_title;
	} // end function _GetPageTitle()
			
	
	
} // end class Page
?>