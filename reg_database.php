<?php
	// Activate session
	session_start();
	
	// Start output buffer; this allows us to manage links without
	// directly affecting the header output to the client
	// This is required for 301 redirects
	ob_start();

	// Include utility files
	require_once 'include/config.php';
	
	// Enforce SSL connection for this page
	require_once PRESENTATION_DIR .'link.php';
	Link::EnforceSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.registration.php';
	
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'contact.php';
	
	
	// Delete any $_SESSION vars that show error messages
	$_SESSION['ErrorMsg'] = '';
	$_SESSION['RecordInfo'] = '';
	unset($_SESSION['RecordInfo']);
	
	// initialize
	$_SESSION['Bool_BuyerIsNewSkier'] = 0;
	
	echo 'Output: ------------------------------ Session, post vars on page call <br>';
	
	echo '<pre>';
	echo 'SESSION' .'<br>';
	print_r($_SESSION);
	echo '</pre>';
	
	echo '<pre>';
	echo 'POST' .'<br>';
	print_r($_POST);
	echo '</pre>';
	
	function ucname($string){
		$string = ucwords(strtolower($string));
	
		foreach (array('-', '\'') as $delimiter) {
			if (strpos($string, $delimiter)!==false) {
				$string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
			}
		}
		return $string;
	}
	
	// update buyer record
	// this update only runs the first time a record is submitted for update
	if (isset($_POST['FirstTimeBuyerRegSubmit'])) {
	
		
		echo 'Start: ------------------------------ Processing first-time buyer <br>';
		
		
		// buyer ID stored in the session
		$BuyerPK = $_SESSION['BuyerId'];
	
		$BuyerLastNameJP = ucname(trim($_POST['BuyerLastNameJP']));
		$BuyerFirstNameJP = ucname(trim($_POST['BuyerFirstNameJP']));
		
		
		
		
		// no kana entry for EN, CN users
		$BuyerLastNameJP_Kana = 'Non-JP';
		$BuyerFirstNameJP_Kana = 'Non-JP';
		if (isset($_POST['BuyerLastNameJP_Kana']) && !empty($_POST['BuyerLastNameJP_Kana'])){
			$BuyerLastNameJP_Kana = trim($_POST['BuyerLastNameJP_Kana']);
		} 
		if (isset($_POST['BuyerFirstNameJP_Kana']) && !empty($_POST['BuyerFirstNameJP_Kana'])){
			$BuyerFirstNameJP_Kana = trim($_POST['BuyerFirstNameJP_Kana']);
		} 
		
		
	
		$BuyerEmail = trim($_POST['BuyerEmail']);
		
	
		// use mb_convert_kana() to hard-convert user-entered full-width numbers
		$phone_str = trim($_POST['BuyerPhone']);
		$BuyerPhone = mb_convert_kana($phone_str,"KVa","UTF-8");
		
		// TODO: Country selection, forces load of country's main prefectures/states in next field
	
	
		// TODO: further processing on postal code input to generate 7-digit JP postalcode
		// will not process foreign addresses (non-JP only?)
		$postcode_str = trim($_POST['BuyerPostalCode']);
		$BuyerPostalCode = mb_convert_kana($postcode_str,"KVa","UTF-8");
	
		$BuyerCountryOfResidenceFK = $_POST['BuyerCountryOfResidenceFK'];
		// check entry for non-Japanese country selection
		if ($_POST['BuyerCountryOfResidenceFK'] == 109){
			
			$BuyerIsOverseas = 0;
			
		} else {
			
			$BuyerIsOverseas = 1;
			
			// update session value
			$_SESSION['Bool_BuyerIsOverseas'] = 1;
		}
		  
		
		// prefecture is potentially a text string, let's trim it
		$BuyerPrefectureFK = '';
		$BuyerPrefectureFK = trim($_POST['BuyerPrefectureFK']);
		
		
		$BuyerPrefectureText = '';
		if (isset($_POST['BuyerPrefectureText']) && !empty($_POST['BuyerPrefectureText'])){
			$BuyerPrefectureText = trim($_POST['BuyerPrefectureText']);
		}
		$BuyerCityTown = trim($_POST['BuyerCityTown']);
		$BuyerAddress1 = trim($_POST['BuyerAddress1']);
		$BuyerAddress2 = trim($_POST['BuyerAddress2']);
	
		$buyer_emer_phone_str = trim($_POST['BuyerEmergencyPhone']);
		$BuyerEmergencyPhone = mb_convert_kana($buyer_emer_phone_str,"KVa","UTF-8");
		$BuyerEmergencyContact = trim($_POST['BuyerEmergencyContact']);
		$BuyerUserLang = $_POST['BuyerCurrentUserLang'];
		$BuyerReceivesNewsletter = $_POST['BuyerReceivesNewsletter'];
		$BuyerIsSkier = $_POST['BuyerIsSkier'];
		
		if ($_SESSION['UserLang'] == 'JP'){
			// encode to SJIS
			$BuyerLastNameJP = mb_convert_encoding($BuyerLastNameJP, "SJIS", "UTF-8");
			$BuyerFirstNameJP = mb_convert_encoding($BuyerFirstNameJP, "SJIS", "UTF-8");
			$BuyerLastNameJP_Kana = mb_convert_encoding($BuyerLastNameJP_Kana, "SJIS", "UTF-8");
			$BuyerFirstNameJP_Kana = mb_convert_encoding($BuyerFirstNameJP_Kana, "SJIS", "UTF-8");
			$BuyerPrefectureText = mb_convert_encoding($BuyerPrefectureText, "SJIS", "UTF-8");
			
			$BuyerCityTown = mb_convert_encoding($BuyerCityTown, "SJIS", "UTF-8");
			$BuyerAddress1 = mb_convert_encoding($BuyerAddress1, "SJIS", "UTF-8");
			$BuyerAddress2 = mb_convert_encoding($BuyerAddress2, "SJIS", "UTF-8");
			$BuyerEmergencyContact = mb_convert_encoding($BuyerEmergencyContact, "SJIS", "UTF-8");
		}
		
		
		echo 'Start: ------------------------------ Buyer update <br>';
	
		// process update
		$updateBuyerResult = Contact::UpdateBuyer(
				$BuyerPK,
				$BuyerLastNameJP,
				$BuyerFirstNameJP,
				$BuyerLastNameJP_Kana,
				$BuyerFirstNameJP_Kana,
		
				$BuyerEmail,
				$BuyerPhone,
				$BuyerPostalCode,
				$BuyerCountryOfResidenceFK,
				$BuyerPrefectureFK,
				$BuyerPrefectureText,
				
				$BuyerCityTown,
				$BuyerAddress1,
				$BuyerAddress2,
				$BuyerEmergencyPhone,
				$BuyerEmergencyContact,
				
				$BuyerUserLang,
				$BuyerReceivesNewsletter,
				$BuyerIsSkier,
				$BuyerIsOverseas);
		
		

		if ($updateBuyerResult) {
			// update is okay
			echo 'Completed: ------------------------------ Buyer record update executed correctly. <br>';
			
			// load name strings into session, will be used for display
			$_SESSION['BuyerFirstNameJP'] = $updateBuyerResult['t_firstname_jp'];
			$_SESSION['BuyerFirstNameJPKana'] = $updateBuyerResult['t_firstname_furigana_jp'];
			$_SESSION['BuyerLastNameJP'] = $updateBuyerResult['t_lastname_jp'];
			$_SESSION['BuyerLastNameJPKana'] = $updateBuyerResult['t_lastname_furigana_jp'];
			
			if ($_SESSION['UserLang'] == 'JP'){
				// decode to UTF8
				$_SESSION['BuyerFirstNameJPKana'] = mb_convert_encoding($updateBuyerResult['t_firstname_furigana_jp'], "UTF-8", "SJIS");
				$_SESSION['BuyerFirstNameJP'] = mb_convert_encoding($updateBuyerResult['t_firstname_jp'], "UTF-8", "SJIS");
				$_SESSION['BuyerLastNameJPKana'] = mb_convert_encoding($updateBuyerResult['t_lastname_furigana_jp'], "UTF-8", "SJIS");
				$_SESSION['BuyerLastNameJP'] = mb_convert_encoding($updateBuyerResult['t_lastname_jp'], "UTF-8", "SJIS");
			}
			
			
			
			
			echo '<pre>';
			echo 'Output: ------------------------------ Buyer update result: <br>';
			print_r($updateBuyerResult);
			echo '</pre>';
			
			
			
			// reset session variable, buyer no longer first timer
			if (isset($_SESSION['Bool_BuyerIsNew']) && ($_SESSION['Bool_BuyerIsNew'] == 1)){
				
				// reset session variable
				$_SESSION['Bool_BuyerIsNew'] = 0;
				
				// advance registration page control
				$_SESSION['RegPageControl'] = 1;
				
				// set session variable to mark this as a special page reload (only happens once)
				// this will reset with the next skier registration and page reload
				if ($BuyerIsSkier == 1){
					$_SESSION['Bool_BuyerIsNewSkier'] = 1;
				} 
				
			} 
			
			echo '<pre>';
			echo 'Output: ------------------------------ Session vars after buyer update: <br>';
			echo 'SESSION' .'<br>';
			print_r($_SESSION);
			echo '</pre>';
			
			// send the user to the registration page
		    $dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'registration.php';
			$corrected_dir_string = str_replace('//','/',$dir_string);
			$login_url = 'http://' .$corrected_dir_string;
			header('Location: ' .$login_url);
			exit(); 
			
		} else {
			// query returns 0 results. Throw error.
			echo 'Error: ------------------------------ Error with buyer record update. <br>';
		}
		
	} // end check for first-time buyer submit
	
	
	
	
	// check for new skier submit
	if (isset($_POST['NewSkierRegSubmit'])) {
		
		echo 'Start: ------------------------------ Processing new skier insert <br>';
		
		// set up variables for new skier submit
		// buyer ID stored in the session
		$BuyerFK = $_SESSION['BuyerId'];
		
		$SkierLastNameJP = ucname(trim($_POST['SkierLastNameJP']));
		$SkierFirstNameJP = ucname(trim($_POST['SkierFirstNameJP']));
		// no kana entry for EN, CN users
		$SkierLastNameJP_Kana = 'Non-JP';
		$SkierFirstNameJP_Kana = 'Non-JP';
		if (isset($_POST['SkierLastNameJP_Kana']) && !empty($_POST['SkierLastNameJP_Kana'])){
			$SkierLastNameJP_Kana = trim($_POST['SkierLastNameJP_Kana']);
		}
		if (isset($_POST['SkierFirstNameJP_Kana']) && !empty($_POST['SkierFirstNameJP_Kana'])){
			$SkierFirstNameJP_Kana = trim($_POST['SkierFirstNameJP_Kana']);
		}
		
		$SkierGender = $_POST['SkierGender'];
		$SkierBirthMonth = $_POST['SkierBirthMonth'];
		$SkierBirthDay = $_POST['SkierBirthDay'];
		$SkierBirthYear = $_POST['SkierBirthYear'];
		
		$SkierInstructionLang = $_POST['SkierInstructionLang'];
		$SkierLessonType = $_POST['SkierLessonType'];
		if (isset($_POST['SkierAbilityLevelSnowboard'])){
			$SkierAbilityLevelSnowboard = $_POST['SkierAbilityLevelSnowboard'];
		} else {
			$SkierAbilityLevelSnowboard = 0;
		}
		if (isset($_POST['SkierAbilityLevelSki'])){
			$SkierAbilityLevelSki = $_POST['SkierAbilityLevelSki'];
		} else {
			$SkierAbilityLevelSki = 0;
		}
		
		if (isset($_POST['SkierFoodAllergies'])){
			$SkierFoodAllergies = trim($_POST['SkierFoodAllergies']);
		} else {
			$SkierFoodAllergies = '';
		}
		$InstructorPrefs = '';
		$InstructorPrefs_FreeText = '';
		
		if ($_SESSION['UserLang'] == 'JP'){
			// encode to SJIS
			$SkierLastNameJP = mb_convert_encoding($SkierLastNameJP, "SJIS", "UTF-8");
			$SkierFirstNameJP = mb_convert_encoding($SkierFirstNameJP, "SJIS", "UTF-8");
			$SkierLastNameJP_Kana = mb_convert_encoding($SkierLastNameJP_Kana, "SJIS", "UTF-8");
			$SkierFirstNameJP_Kana = mb_convert_encoding($SkierFirstNameJP_Kana, "SJIS", "UTF-8");
			$SkierFoodAllergies = mb_convert_encoding($SkierFoodAllergies, "SJIS", "UTF-8");
			
			$InstructorPrefs = mb_convert_encoding($InstructorPrefs, "SJIS", "UTF-8");
			$InstructorPrefs_FreeText = mb_convert_encoding($InstructorPrefs_FreeText, "SJIS", "UTF-8");
			
		}
		
			// insert new skier record
			$insertSkierResult = Contact::InsertSkier(
					$BuyerFK,
					$SkierLastNameJP,
					$SkierFirstNameJP,
					$SkierLastNameJP_Kana,
					$SkierFirstNameJP_Kana,
						
					$SkierGender,
					$SkierBirthMonth,
					$SkierBirthDay,
					$SkierBirthYear,
					$SkierInstructionLang,
					
					$SkierLessonType,
					$SkierAbilityLevelSnowboard,
					$SkierAbilityLevelSki,
					$SkierFoodAllergies,
					$InstructorPrefs,
					
					$InstructorPrefs_FreeText);
		
			if ($insertSkierResult) {
				
				echo 'Completed: ------------------------------ Skier record insert executed correctly. <br>';
				
				echo '<pre>';
				echo 'Output: ------------------------------ Skier insert result: <br>';
				print_r($insertSkierResult);
				echo '</pre>';
				
				// advance registration page control
				// user now has both buyer and skier(s) registered
				$_SESSION['RegPageControl'] = 2;
			
				// send the user to the registration page
 				$dir_string = $_SERVER['HTTP_HOST'] .VIRTUAL_LOCATION .'registration.php';
				$corrected_dir_string = str_replace('//','/',$dir_string);
				$registration_url = 'http://' .$corrected_dir_string;
				header('Location: ' .$registration_url);
				exit();   
				 
			} else {
				 
				// error handling
				$_SESSION['RecordInfo'] = '<p class="ErrorMsg">Error with skier insert.</p>';
				echo 'Error: ------------------------------ Error with skier record insert. <br>';
			}
		
	
		 
	} // end check for new skier submit
	
	
	
?>