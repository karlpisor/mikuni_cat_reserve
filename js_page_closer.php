</div>
<!-- End Container -->
			
</div>
<!-- End Main Container -->



<!-- Start tracking code for Google Analytics, only active on live server -->
<?php 
	if (DB_DATABASE == 'ea_live_tokyo'){
		require_once 'js/google_analytics.php';
	}
?>
<!-- End tracking code for Google Analytics -->


<a href="#" title="Back to Top" class="scroll-top" style="display: none;">
    <img src="images/arrow-top.png" alt="Back to Top" />
</a>


</body>
</html>

<?php
	// Close database connection
	DatabaseHandler::Close();
	
	// Output and erase content from the buffer; required for 301 redirects
	flush();
	ob_flush();
	ob_end_clean();
?>