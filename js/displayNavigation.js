jQuery.noConflict();

/* load after all images */
jQuery(window).load(function(){
	/**
	 * Snippet to set equal column height
	 * Check elements with class 'equal_column'
	 * Get tallest and apply height to all containers
	 */
	function equal(className) {
				
		//set a default class
		var className = (!className)? '.equal_column':className;
	
		if(jQuery(className).length > 0) {
						
			var $tallest = 0;
			//each item that has the above class
			jQuery.each(jQuery(className), function(){
				var $height = jQuery(this).height();	
				
				
				if($height > $tallest) {
					$tallest = $height;//set tallest
				}
			});	
			//add class for styling only if js enabled
			jQuery(className).addClass('js_col').height($tallest);
		}
	}
	
	equal(); //calls class = .equal_height					 
});

jQuery(document).ready(function($){ 

	


	
	jQuery('a[rel=external]').click(function(){window.open(this.href,'_blank'); return false;});
	
	/**
	 * Code to generate main menu
	 * Splits generated list into 3 columns
	 */
	(function( $ ){
		$.each($('#nav > ul > li > ul'), function(index, value) {
			
			//get number of child elements and half (so we can split into two)
			var $ceil = Math.ceil(($(this).children('li').length/2));
			
			var $menu = $(value);
			var $menu1 = $(value).clone();
			var $parent = $(value).parent();
			
			//hacky way to get db menu id from menu item. Return example 'parent item3' so we just need the last number
			var $parentid = ($(value).parent().attr("class").match(/\d+/))?$(value).parent().attr("class").match(/\d+/):0;
					
					
			//add 3 columns
			$parent.append('<ul><li></li><li></li><li class="module" id="mid_' + $parentid + '"></li></ul>');		
			
			//removes the original menu
			$(value).remove();		
			
			//creates new menus in colums with classes left and right
			$menu.appendTo($parent.find('li').eq(1)).addClass('right');
			$menu1.appendTo($parent.find('li').eq(0)).addClass('left');
			
			//the chapters columns were askew so we need to hack it
			/*if(index==1){ //about
				$parent.find('ul.right > li:lt('+($ceil+1)+')').remove();
				$parent.find('ul.left > li:gt('+($ceil)+')').remove();
			} else*/ if(index==2){ //news		
				//remove right column because theres only 2 items
				$parent.find('ul.right').remove(); 
			} else {
				$parent.find('ul.right > li:lt('+($ceil)+')').remove();
				$parent.find('ul.left > li:gt('+($ceil-1)+')').remove();	
			}			
		});
		
		/**
		 * Append content from one container to another
		 */
		function appendItem($from, $to) {		
			//check that the element exists
			if($($from).length == 0) return false;
			$($to).append($($from));				
		}
		
		//move modules from index into menu
		appendItem("#nav_about", "#mid_3");
		appendItem("#nav_chapters", "#mid_4");
		appendItem("#nav_news", "#mid_5");
		appendItem("#nav_membership", "#mid_6");
		appendItem("#nav_events", "#mid_7");
		appendItem("#nav_advocacy", "#mid_8");
		
	})( jQuery );
			
	// accordion, subnav
	jQuery("#left_col .menu > li.parent > a").click(function(evt){		
		//stop the normal action when you click a link
		evt.preventDefault(); 			
		jQuery("#left_col .menu > li").removeClass('focus');		
		if(jQuery(this).next().is(':visible') == false) {
			jQuery('#left_col .menu ul').slideUp(300);
			jQuery(this).parent().addClass('focus');
		}			
		jQuery(this).next().slideToggle(300);
		
	});
	
									
});