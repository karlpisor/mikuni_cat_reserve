/* Utilities for responsiveness
 * @author: Paul Romelot - https://jp.linkedin.com/pub/paul-romelot/4b/1aa/5a7
 * April 2015
 *   */

var nMobileBreak = 768;
var nTabletBreak = 1010;

var isTablet = false;
var isMobile = false;
var isOthers = false;
var scrollTopOn = false;

function getScreenWidth() {
    return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}

function getScreenHeight() {
    return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
}

function setIsMobile() {

    var screenWidth = getScreenWidth();
    var wasMobile = isMobile;

    isMobile = (!isNaN(screenWidth) && (screenWidth < nMobileBreak));

    return (wasMobile !== isMobile);
}

function setIsTablet() {

    var screenWidth = getScreenWidth();
    var wasTablet = isTablet;

    isTablet = (!isMobile && (!isNaN(screenWidth) && (screenWidth < nTabletBreak)));

    return (wasTablet !== isTablet);
}

//ready function, runs on page load
jQuery(document).ready(function() {

    function resizeYoutube(pbIsMobile) {
        if (pbIsMobile) {
            jQuery("iframe.player").each(function () {
                var nWidth = parseInt(jQuery(this).attr("width"));
                var nHeight = parseInt(jQuery(this).attr("height"));
                var ratio = nWidth / nHeight;

                var nNewWidth = jQuery(this).parent().width();
                var nNewHeight = nNewWidth / ratio;

                jQuery(this).attr("width", nNewWidth).attr("height", nNewHeight);
            });
        }
        else
        {
            jQuery("iframe.player").attr("width", "560").attr("height", "315");
        }
    }

    function moveSidebar(pbIsMobile) {
        if (pbIsMobile)
        {
            jQuery("#sidebar").insertAfter(".page-content");
            jQuery("#sidebar-in-page").appendTo("#main");
        }
        else
        {
            jQuery("#sidebar").insertAfter("#main > h1");
            jQuery("#sidebar-in-page").prependTo(".page-content");
        }
    }

    jQuery(window).bind("resize", function () {

        var wasMobile = setIsMobile();
        var wasTablet = setIsTablet();

        if (wasMobile) {
            jQuery("header nav > ul > li > ul").removeAttr("style");
            jQuery("header .navigation").removeAttr("style");
            moveSidebar(false);
            resizeYoutube(false);
        }

        if (wasTablet && isMobile) {
            moveSidebar(true);
        }
        if (isMobile) {
            setTimeout(function () {
                resizeYoutube(true);
            }, 1000);
        }
    });

    setIsMobile();
    setIsTablet();

    // HTML output has desktop/tablets settings
    if (isMobile)
    {
        moveSidebar(true);
        resizeYoutube(true);
    }

    /*
     * Top navigation menu for mobiles
     *   */

    // Toggle submenus on click
    jQuery("header nav > ul > li > a").click(function () {
        if (isMobile) {
            jQuery(this).siblings("ul").slideToggle();
            return false;
        }
    });

    // Toggle navigation 
    jQuery(".menu-mobile").click(function () {
        if (isMobile) {
            jQuery(".navigation").slideToggle(function () {
                jQuery("header nav > ul > li > ul").removeAttr("style");
            });
            return false;
        }
    });

    jQuery(window).bind("scroll", function () {
        toggleScrollToTop();
    });

    function toggleScrollToTop() {
        var sScreenHeight = getScreenHeight();
        var sOffsetTop = jQuery(window).scrollTop();
        var oScrollTopLink = jQuery(".scroll-top");

        if ((!scrollTopOn) && (sOffsetTop > sScreenHeight)) {
            oScrollTopLink.fadeIn();
            scrollTopOn = true;
        }
        else
        {
            if ((scrollTopOn) && (sOffsetTop <= sScreenHeight)) {
                oScrollTopLink.fadeOut();
                scrollTopOn = false;
            }
        }
    }

}); // end ready()