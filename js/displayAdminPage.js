// JavaScript Document
jQuery(document).ready(function() {
		
	// Admin pages
	
	// Admin login box styling
	 jQuery('#frmLogin').submit(function() {
		var f = document.frmLogin;

		if(f.usermail.value == '')
		{
			alert('Please enter your e-mail address.');
			f.usermail.focus();
			f.usermail.select();
			return false;
		}

		// Everything is OK
		return true;
	});

	function sizeBox() {
		var w = jQuery(window).width();
		var h = document.getElementsByTagName('html')[0].clientHeight;
		jQuery('#box').css('position', 'absolute');
		jQuery('#box').css('top', h/2-(jQuery('#box').height()/2)-50);
		jQuery('#box').css('left', w/2-(jQuery('#box').width()/2));
	}

	// prep entry boxes on admin login
	jQuery('#AdminUserEmail').val('');
	jQuery('#AdminPassword').val('');
	jQuery('#AdminUserEmail').focus();
	

	jQuery(window).resize(function() {
		sizeBox();
	});
	
	
	
	
	
	// show each language input as tab is clicked
	jQuery('#english').click(function(){
		jQuery('#page-en-entry').show();
		jQuery('#page-jp-entry, #page-cn-entry, #page-admin').hide();
		jQuery('#japanese, #chinese, #admin').addClass('deselected').removeClass('selected');
		jQuery('#english').removeClass('deselected').addClass('selected');
	});
	jQuery('#japanese').click(function(){
		jQuery('#page-jp-entry').show();
		jQuery('#page-en-entry, #page-cn-entry, #page-admin').hide();
		jQuery('#english, #chinese, #admin').addClass('deselected').removeClass('selected');
		jQuery('#japanese').removeClass('deselected').addClass('selected');
	});
	jQuery('#chinese').click(function(){
		jQuery('#page-cn-entry').show();
		jQuery('#page-jp-entry, #page-en-entry, #page-admin').hide();
		jQuery('#japanese, #english, #admin').addClass('deselected').removeClass('selected');
		jQuery('#chinese').removeClass('deselected').addClass('selected');
	});
	jQuery('#admin').click(function(){
		jQuery('#page-admin').show();
		jQuery('#page-jp-entry, #page-cn-entry, #page-en-entry').hide();
		jQuery('#japanese, #chinese, #english').addClass('deselected').removeClass('selected');
		jQuery('#admin').removeClass('deselected').addClass('selected');
	});
		
		
				
		// fancy tooltip
		jQuery('.ContentPanel a[href][title]').qtip({
		      content: {
		          text: false // Use the element's title attribute
		       },
		       style: {
		    	   border: {
		    	         width: 4,
		    	         radius: 8
		    	   },
		    	   width: 250,
		    	   name: 'blue',
		    	   background: 'white'
		       }
		    });
		
		// delete order dialog
		jQuery(function () {
			$("#dialog-confirm").dialog({

	            autoOpen: false,
	            modal: true,
	            buttons: {
	                "Delete order": function() {
	                  var deleteLocation = jQuery('#DeleteOrderLocation').val();	
	                  window.location.replace(deleteLocation);
	                },
	                Cancel: function() {
	                  $(this).dialog('close');
	                }
	              }
	        });
	    });
		
		
	
		jQuery("#DeleteOrderLink").click(function () {
		    $("#dialog-confirm").dialog("open");
		});
		
	
		 
		 
		 
		 
		
		
			
			
		 
}); //end ready function