// JavaScript Document
jQuery(document).ready(function() {
		
		// ski or snowboard level selection wizard
	
		// default page load
		jQuery('.LevelSelectionWizard').hide();
		jQuery('.SnowboardResultDisplay').hide();
		jQuery('.SnowboardResetButton').hide();
		jQuery('.SkiResultDisplay').hide();
		jQuery('.SkiResetButton').hide();
		
		
		// master label strings
		/*var levelResult = jQuery('#LevelSelectResult_Prefix').val();
		var labelConfirmSelection = jQuery('#LevelSelectResult_Chosen').val() + ' ';
		var tempLevelChosen = '';*/
		
		
		
		
		
		
		// click actions
		
		// adult snowboard
		jQuery('.level-wizard-snowboard-adult').click(function(){
			jQuery('.LevelSelectionWizard').hide();
			jQuery('.AdultSnowboardLevelSelectDiv').show();
			jQuery('.wizard-title-wrapper').show();
			jQuery('.AdultSnowboardLevelSelect').show();
			jQuery('.ControlButtons.snowboard').show();
			jQuery('.SnowboardResultDisplay').hide();
			jQuery('.SnowboardConfirmButton').hide();
			jQuery('.SnowboardResetButton').hide();
			jQuery('.SnowboardLevel').val('');
			
			// show the starting questions
			jQuery('.AdultSnowboardSelect').hide();
			jQuery('.AdultSnowboardSelect.step-1').show();
			
			// add adult class to the reset button
			jQuery('.SnowboardResetButton').addClass('adult');
			jQuery('.SnowboardConfirmButton').addClass('adult');
			
			// prevent page from jumping up
			return false;
					
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step1_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step1_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.AdultSnowboardLevelSelect_Step1_Yes').addClass('selected');
			jQuery('.AdultSnowboardSelect.step-2').show();
			
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step1_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step1_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_A').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(1);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step2_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step2_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.AdultSnowboardLevelSelect_Step2_Yes').addClass('selected');
			jQuery('.AdultSnowboardSelect.step-3').show();
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step2_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step2_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_A').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(1);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step3_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step3_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.AdultSnowboardLevelSelect_Step3_Yes').addClass('selected');
			jQuery('.AdultSnowboardSelect.step-4').show();
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step3_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step3_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_B').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(2);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step4_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step4_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.AdultSnowboardLevelSelect_Step4_Yes').addClass('selected');
			jQuery('.AdultSnowboardSelect.step-5').show();
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step4_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step4_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_C').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(3);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step5_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step5_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.AdultSnowboardLevelSelect_Step5_Yes').addClass('selected');
			jQuery('.AdultSnowboardSelect.step-6').show();
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step5_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step5_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_D').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(4);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step6_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step6_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.AdultSnowboardLevelSelect_Step6_Yes').addClass('selected');
			jQuery('.AdultSnowboardSelect.step-7').show();
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step6_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step6_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_E').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(5);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step7_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step7_No').slideUp(500);
			
			// yes is selected, show the resulting level
			jQuery('.AdultSnowboardLevelSelect_Step7_Yes').addClass('selected');
			var resultText = jQuery('#LevelSelectResult_F').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(6);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.AdultSnowboardLevelSelect_Step7_No').click(function(){
			
			// hide the unselected choice
			jQuery('.AdultSnowboardLevelSelect_Step7_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_E').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(5);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// junior snowboard
		jQuery('.level-wizard-snowboard-junior').click(function(){
			jQuery('.LevelSelectionWizard').hide();
			jQuery('.JuniorSnowboardLevelSelectDiv').show();
			jQuery('.wizard-title-wrapper').show();
			jQuery('.JuniorSnowboardLevelSelect').show();
			jQuery('.ControlButtons.snowboard').show();
			jQuery('.SnowboardResultDisplay').hide();
			jQuery('.SnowboardConfirmButton').hide();
			jQuery('.SnowboardResetButton').hide();
			jQuery('.SnowboardLevel').val('');
			
			// show the starting questions
			jQuery('.JuniorSnowboardSelect').hide();
			jQuery('.JuniorSnowboardSelect.step-1').show();
			
			// add junior class to the reset button
			jQuery('.SnowboardResetButton').addClass('junior');
			jQuery('.SnowboardConfirmButton').addClass('junior');
			
			// prevent page from jumping up
			return false;
		});
		
		
		
		
		
		
		
		
		jQuery('.JuniorSnowboardLevelSelect_Step1_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step1_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.JuniorSnowboardLevelSelect_Step1_Yes').addClass('selected');
			jQuery('.JuniorSnowboardSelect.step-2').show();
			
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step1_No').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step1_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_A').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(1);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step2_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step2_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.JuniorSnowboardLevelSelect_Step2_Yes').addClass('selected');
			jQuery('.JuniorSnowboardSelect.step-3').show();
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step2_No').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step2_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_A').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(1);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step3_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step3_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.JuniorSnowboardLevelSelect_Step3_Yes').addClass('selected');
			jQuery('.JuniorSnowboardSelect.step-4').show();
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step3_No').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step3_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_B').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(2);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step4_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step4_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.JuniorSnowboardLevelSelect_Step4_Yes').addClass('selected');
			jQuery('.JuniorSnowboardSelect.step-5').show();
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step4_No').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step4_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_C').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(3);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		jQuery('.JuniorSnowboardLevelSelect_Step5_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step5_No').slideUp(500);
			
			// yes is selected, show final level
			jQuery('.JuniorSnowboardLevelSelect_Step5_Yes').addClass('selected');
			var resultText = jQuery('#LevelSelectResult_E').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(5);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		
		
		jQuery('.JuniorSnowboardLevelSelect_Step5_No').click(function(){
			
			// hide the unselected choice
			jQuery('.JuniorSnowboardLevelSelect_Step5_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			jQuery('.JuniorSnowboardLevelSelect_Step5_No').addClass('selected');
			var resultText = jQuery('#LevelSelectResult_D').val();
			
			jQuery('.SnowboardResultDisplay').text(resultText);
			jQuery('.SnowboardResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SnowboardLevel').val(4);
			
			// show the confirm and reset buttons
			jQuery('.SnowboardConfirmButton').slideDown(500);
			jQuery('.SnowboardResetButton').slideDown(500);
			
		});
		
		
		
		
		
		
		jQuery('.SnowboardConfirmButton').click(function(){
			
			jQuery('.wizard-title-wrapper ').hide();
			jQuery('.LevelSelectionWizard').slideUp(500);
			
			jQuery('.SnowboardResultDisplay').removeAttr('style');
			jQuery('.SnowboardResultDisplay').hide();
			
			jQuery(this).addClass('confirmed');
			
			// new skier reg does not present an index
			var idString = jQuery(this).attr('id');
			if (idString == 'NewSnowboarderIndex'){
				// set string to null, not needed for new skier entry
				var snowboarderIndex = '';
			} else {
				// get last two chars from the index name, i.e., _2 or _0
				var snowboarderIndex = idString.substr(idString.length - 2);
			}
			console.log('Snowboarder index: ' + snowboarderIndex);
			
			// update the ski ability level select, above
			var selectedLevel = jQuery('.SnowboardLevel').val();
			console.log('Snowboard level: ' + selectedLevel);
			
			// set select menu with ski level
			jQuery('#SkierAbilityLevelSnowboard' + snowboarderIndex).val(selectedLevel).prop('selected', true).trigger('change');
			
			// show success message
			var currentMsg = jQuery(this).text();
			var successMsg = jQuery('#SuccessMsg_Skier_LevelWizardUpdated').val();
			jQuery(this).text(successMsg);
			
			
		});
		
		
		// snowboard reset selection
		jQuery('.SnowboardResetButton').click(function(){
			jQuery('.SnowboardLevelSelect').show();
			
			jQuery('.SnowboardResultDisplay').removeAttr('style');
			jQuery('.SnowboardResultDisplay').hide();
			jQuery('.SnowboardResultDisplay').text('');
			
			jQuery('.SnowboardResetButton').hide();
			
			var confirmMsg = jQuery('#ConfirmMsg_LevelWizard').val();
			jQuery('.SnowboardConfirmButton').text(confirmMsg);
			jQuery('.SnowboardConfirmButton').removeClass('confirmed');
			jQuery('.SnowboardConfirmButton').hide();
			
			jQuery('.SnowboardLevel').val('');
			
			// show the starting questions
			jQuery('.wizard-title-wrapper ').show();
			if (jQuery(this).hasClass('junior')){
				
				// show the starting questions
				jQuery('.JuniorSnowboardSelect').hide();
				jQuery('.JuniorSnowboardLevelSelectDiv').show();
				jQuery('.JuniorSnowboardSelect').removeClass('selected');
				jQuery('.JuniorSnowboardSelect.step-1').show();
				
			} else if (jQuery(this).hasClass('adult')){
				
				// show the starting questions
				jQuery('.AdultSnowboardSelect').hide();
				jQuery('.AdultSnowboardSelect').removeClass('selected');
				jQuery('.AdultSnowboardLevelSelectDiv').show();
				jQuery('.AdultSnowboardSelect.step-1').show();
			};

			
			// reset select menu
			// reset the select menu directly above the wizard
			// new skier reg does not present an index
			var idString = jQuery(this).attr('id');
			if (idString == 'NewSnowboarderIndex'){
				// set string to null, not needed for new skier entry
				var snowboarderIndex = '';
			} else {
				// get last two chars from the index name, i.e., _2 or _0
				var snowboarderIndex = idString.substr(idString.length - 2);
			}
			console.log('Snowboarder reset index: ' + snowboarderIndex);
			
			// set select menu with default ski level = 1
			jQuery('#SkierAbilityLevelSnowboard' + snowboarderIndex).val(1).prop('selected', true).trigger('change');
			
		});
		
		
		
		
		
		
		
		
		
		
		
		// ski
		jQuery('.level-wizard-ski').click(function(){
			jQuery('.LevelSelectionWizard').hide();
			jQuery('.wizard-title-wrapper').show();
			jQuery('.LevelSelectionWizard.SkiLevelSelect').show();
			jQuery('.ControlButtons.ski').show();
			jQuery('.SkiResultDisplay').hide();
			jQuery('.SkiConfirmButton').hide();
			jQuery('.SkiResetButton').hide();
			jQuery('.SkiLevel').val('');
			
			// show the starting questions
			jQuery('.SkiSelect').hide();
			jQuery('.SkiSelect.step-1').show();
			
			// prevent page from jumping up
			return false;
			
		});
		
		jQuery('.SkiLevelSelect_Step1_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step1_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.SkiLevelSelect_Step1_Yes').addClass('selected');
			jQuery('.SkiSelect.step-2').show();
			
		});
		
		jQuery('.SkiLevelSelect_Step1_No').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step1_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_A').val();
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(1);
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		jQuery('.SkiLevelSelect_Step2_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step2_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.SkiLevelSelect_Step2_Yes').addClass('selected');
			jQuery('.SkiSelect.step-3').show();
			
		});
		
		jQuery('.SkiLevelSelect_Step2_No').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step2_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_B').val();
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(2);
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		jQuery('.SkiLevelSelect_Step3_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step3_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.SkiLevelSelect_Step3_Yes').addClass('selected');
			jQuery('.SkiSelect.step-4').show();
			
		});
		
		jQuery('.SkiLevelSelect_Step3_No').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step3_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_C').val();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(3);
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		jQuery('.SkiLevelSelect_Step4_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step4_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.SkiLevelSelect_Step4_Yes').addClass('selected');
			jQuery('.SkiSelect.step-5').show();
			
		});
		
		jQuery('.SkiLevelSelect_Step4_No').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step4_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_D').val();
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(4);
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		jQuery('.SkiLevelSelect_Step5_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step5_No').slideUp(500);
			
			// yes is selected, show the next choices
			jQuery('.SkiLevelSelect_Step5_Yes').addClass('selected');
			jQuery('.SkiSelect.step-6').show();
			
		});
		
		jQuery('.SkiLevelSelect_Step5_No').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step5_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_E').val();
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(5);
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		jQuery('.SkiLevelSelect_Step6_Yes').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step6_No').slideUp(500);
			
			// yes is selected, show the resulting level
			jQuery('.SkiLevelSelect_Step6_Yes').addClass('selected');
			var resultText = jQuery('#LevelSelectResult_F').val();
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(6);
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		jQuery('.SkiLevelSelect_Step6_No').click(function(){
			
			// hide the unselected choice
			jQuery('.SkiLevelSelect_Step6_Yes').slideUp(500);
			
			// no is selected, show the resulting level
			var resultText = jQuery('#LevelSelectResult_E').val();
			
			jQuery('.SkiResultDisplay').text(resultText);
			jQuery('.SkiResultDisplay').show();
			
			// set numerical level for use later when confirm is clicked
			jQuery('.SkiLevel').val(5);
			
			// show the confirm and reset buttons
			jQuery('.SkiConfirmButton').slideDown(500);
			jQuery('.SkiResetButton').slideDown(500);
			
		});
		
		
		
		
		
		// ski confirm selection
		jQuery('.SkiConfirmButton').click(function(){
			
			jQuery('.wizard-title-wrapper ').hide();
			jQuery('.LevelSelectionWizard').slideUp(500);
			
			jQuery('.SkiResultDisplay').removeAttr('style');
			jQuery('.SkiResultDisplay').hide();
			
			jQuery(this).addClass('confirmed');
			
			// new skier reg does not present an index
			var idString = jQuery(this).attr('id');
			if (idString == 'NewSkierIndex'){
				// set string to null, not needed for new skier entry
				var skierIndex = '';
			} else {
				// get last two chars from the index name, i.e., _2 or _0
				var skierIndex = idString.substr(idString.length - 2);
			}
			console.log('Skier index: ' + skierIndex);
			
			// update the ski ability level select, above
			var selectedLevel = jQuery('.SkiLevel').val();
			console.log('Ski level: ' + selectedLevel);
			
			// set select menu with ski level
			jQuery('#SkierAbilityLevelSki' + skierIndex).val(selectedLevel).prop('selected', true).trigger('change');
			
			// show success message
			var currentMsg = jQuery(this).text();
			var successMsg = jQuery('#SuccessMsg_Skier_LevelWizardUpdated').val();
			jQuery(this).text(successMsg);
			
			
		});
		
		
		// ski reset selection
		jQuery('.SkiResetButton').click(function(){
			jQuery('.SkiLevelSelect').show();
			
			jQuery('.SkiResultDisplay').removeAttr('style');
			jQuery('.SkiResultDisplay').hide();
			jQuery('.SkiResultDisplay').text('');
			
			jQuery('.SkiResetButton').hide();
			
			var confirmMsg = jQuery('#ConfirmMsg_LevelWizard').val();
			jQuery('.SkiConfirmButton').text(confirmMsg);
			jQuery('.SkiConfirmButton').removeClass('confirmed');
			jQuery('.SkiConfirmButton').hide();
			
			jQuery('.SkiLevel').val('');
			
			// show the starting questions
			jQuery('.SkiSelect').hide();
			jQuery('.SkiSelect').removeClass('selected');
			jQuery('.wizard-title-wrapper ').show();
			jQuery('.SkiSelect.step-1').show();

			
			// reset select menu
			// reset the select menu directly above the wizard
			// new skier reg does not present an index
			var idString = jQuery(this).attr('id');
			if (idString == 'NewSkierIndex'){
				// set string to null, not needed for new skier entry
				var skierIndex = '';
			} else {
				// get last two chars from the index name, i.e., _2 or _0
				var skierIndex = idString.substr(idString.length - 2);
			}
			console.log('Skier reset index: ' + skierIndex);
			
			// set select menu with default ski level = 1
			jQuery('#SkierAbilityLevelSki' + skierIndex).val(1).prop('selected', true).trigger('change');
			
		});
		 
}); //end ready function







