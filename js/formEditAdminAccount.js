// JavaScript Document

 $(document).ready(function() {
		 
	 	// Clear all entry fields
	 	$('#NewAccountPassword1').val('');
	 
	 	// Validation for account information udpate
		$('#AccountUpdateForm').validate({
			rules: {
				AccountEmail:{
					email: true
				}
			},
			submitHandler: function(form) {
	  	    	form.submit();
	       	}
		});
		
		// Validation for account password udpate
		$('#PasswordUpdateForm').validate({
			rules: {
				NewAccountPassword1: {
					minlength: 8
				},
				NewAccountPassword2: {
					minlength: 8,
					equalTo: "#NewAccountPassword1"
				}
			},
			messages: {
				NewAccountPassword1: {
					minlength: jQuery.format("At least 8 characters required.")
				},
				NewAccountPassword2: {
					minlength: jQuery.format("At least 8 characters required."),
					equalTo: jQuery.format("New passwords must be identical.")
				}
			},
			submitHandler: function(form) {
	  	    	form.submit();
	       	}
		});
		 
		 //Account update form validation
		 $('#AccountUpdateSubmit').click(function(){
			$('#AccountUpdateForm').validate({
				rules: {
					AccountEmail:{
						email: true
					},
					NewAccountPassword1: {
						minlength: 8
					},
					NewAccountPassword2: {
						minlength: 8,
						equalTo: "#NewAccountPassword1"
					}
				},
				messages: {
					NewAccountPassword1: {
						minlength: jQuery.format("At least 8 characters required.")
					},
					NewAccountPassword2: {
						minlength: jQuery.format("At least 8 characters required."),
						equalTo: jQuery.format("New passwords must be identical.")
					}
				}
   			});	 // end validate arguments
		}); // end AccountUpdateSubmit click function()
		 
		//TODO: Login, lost password form validation
		
		 
}); // end ready()