// JavaScript Document
jQuery(document).ready(function() {
	 
	// Hide current user entry on login page
	//jQuery('#RegisteredBuyerLoginForm').hide();
	
	
	
	
	
	// Set up click to show new user entry
	jQuery('#NewAccountEntryToggle').click(function(){
		jQuery('#NewBuyerForm').show();
		jQuery('#RegisteredBuyerLoginForm').hide();
		jQuery('#NewBuyerEmail').focus();
	});
	
	// Set up click to hide new user entry
	jQuery('#CurrentUserEntryToggle').click(function(){
		jQuery('#NewBuyerForm').hide();
		jQuery('#RegisteredBuyerLoginForm').show();
		jQuery('#RegisteredBuyerEmail').focus();
	});
		
			
	 // clear all values and set up form entry	 
	 jQuery('#RegisteredBuyerEmail').val('');
	 jQuery('#RegisteredBuyerPassword').val('');
	 jQuery('#NewBuyerEmail').val('');
	 jQuery('#NewBuyerPassword1').val('');
	 
	 
	 jQuery('#NewAccountPassword1').val('');
	 jQuery('#NewAccountPassword1').focus();
	 
	 jQuery('#RegisteredStaffEmail').val('');
	 jQuery('#RegisteredStaffPassword').val('');
	 
	
	
	// Registered parent login form
	 jQuery('#RegisteredBuyerSubmit').click(function(){
			jQuery('#RegisteredBuyerLoginForm').validate({
				  rules: {
				RegisteredBuyerEmail: {
			      required: true,
			      email: true
			    }
			
			  }
		 });							   
	 });
	 
	 
	// New parent registration form
	jQuery('#NewBuyerSubmit').click(function(){
			jQuery('#NewBuyerForm').validate({
				  rules: {
				NewBuyerEmail: {
			      required: true,
			      email: true
			    },
			    NewBuyerPassword1: {
					  minlength: 8
				},
				NewBuyerPassword2: {
					minlength: 8,
					equalTo: "#NewBuyerPassword1"
				}
			    
			  }
			});
						   
	  });
	 
	 
	 
	 // Reset password form
	 jQuery('#PasswordResetSubmit').click(function(){
			jQuery('#PasswordResetForm').validate({
				  rules: {
					  UserEmail: {
			      required: true,
			      email: true
			    }
			
			  }
			});							   
	 });
	 
	
	 
	 // Update password form
	 jQuery('#PasswordUpdateForm').submit(function(){
			jQuery('#PasswordUpdateForm').validate({
				  rules: {
				NewAccountPassword1: {
					  minlength: 8
				},
				NewAccountPassword2: {
					minlength: 8,
					equalTo: "#NewAccountPassword1"
				}
			    
			  }
			});
						   
		 });
	 
	 // fade out error message, if any
	 jQuery('p.error-msg').delay(2500).slideUp(1000);
		 
		 
}); // end ready()