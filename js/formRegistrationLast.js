// ready function, runs on page load
jQuery(document).ready(function() {
	
	// standard page load, not dependent on pageControl state	
	
	// buyer
	jQuery('#buyer-reg-summary').hide();
	jQuery('#BuyerSectionMsg').hide();
	jQuery('#BuyerRegCancel').hide();
	
	
	// skier
	jQuery('.page-section-body.skier').hide();
	jQuery('#SkierSectionMsg').hide();
	jQuery('.ControlButtons').hide();
    jQuery('.LevelSelectionWizard').hide();
    jQuery('#add-skier-button-div').hide();
    
    
    // lesson selection
    jQuery('#LessonParameterReset').hide();
	jQuery('#LessonSectionMsg').hide();
	jQuery('#lesson-parameter-selection').hide();
    jQuery('#LessonLocationMap').hide();
    jQuery('#lesson-container').hide();
    jQuery('#lesson-assistance-message').hide();
    
    
    
	// liability release
    jQuery('#LiabilityReleaseConfirm').hide();
    jQuery('#liability-release-section-body').hide();
    jQuery('.summary-view.liability-release').hide();
    
    
    // payment
    jQuery('#payment-section-body').hide();
    jQuery('.confirmation-discount-display').hide();
    jQuery('#PaymentMethodCC').prop('checked', true);
    jQuery('p.payment-method-explanation.bank-transfer').hide();
    jQuery('#CompleteRegistrationBankTransfer').hide();
    jQuery('#cc-error-display').hide();
    
    
    // general
    jQuery('i').hide();
	jQuery('#ErrorMsg_PaymentMethod_BankTransferUnavailable').hide();
	

	
	
	
	
	// updates to page display depending on the state of the order
	var pageControl = jQuery('#RegPageControl').val();
	
	// Buyer is brand new (not entered anything into buyer record)
	if (pageControl == 0){
		
		// Prep newsletter form entry for first-time buyers, neutral is offered
		jQuery('#BuyerReceivesNewsletterYes').prop('checked', false);
		
		// Select "I am skier" in form entry for first-time buyers
		jQuery('#BuyerIsSkierYes').prop('checked', true);
		
		// Scroll to top of page
		jQuery('body').scrollTop(0);
	}
	
	
	// Buyer is new skier (has registered buyer, not entered anything into skier record)
	if (pageControl == 1){
		
		// hide buyer
		jQuery('.summary-view.buyer').show();
		jQuery('.page-section-body.buyer').hide();
		
		// show the first skier reg form, hide its summary
        jQuery('#skier-reg-summary-0').hide();
        jQuery('#skier-reg-section-body-0').fadeIn();
        
        // force set the birthdate and language fields 
        // TODO: (don't know why this isn't displaying)
        var currentUserLang = jQuery('#CurrentUserLang').val();
        jQuery('#SkierInstructionLang').val(currentUserLang);
        
        jQuery('#SkierBirthMonth').val(1);
        jQuery('#SkierBirthDay').val(1);
        jQuery('#SkierBirthYear').val(1985);
        
        // two possible views: buyer is skier, and buyer is not skier
        // input for buyer as skier is already shown in the skier-reg-section-body above
        var buyerIsSkier = jQuery('#BuyerIsNewSkier').val();
        if (buyerIsSkier == 0){
        	// buyer is not skier
        	// no previously registered form is available, so show the new skier form for entry
            jQuery('#new-skier-reg-section-body').show();
            // hide the cancel input button in this case
            jQuery('#NewSkierRegCancel').hide();
        }
        
        
        
        // show success for buyer
        jQuery('#buyer-reg-section-head i').show();
        
        // Scroll to top of page
		jQuery('body').scrollTop(0);
	}
	
	// Skier(s) have been successfully registered
	if (pageControl == 2){
		
		// show all buyer, skier summaries
		jQuery('.summary-view.buyer').show();
		jQuery('.summary-view.skier').show();
		
		// hide all buyer, skier bodies
		jQuery('.page-section-body.buyer').hide();
		jQuery('.page-section-body.skier').hide();
		
		// force set the birthdate and language fields 
        // TODO: (don't know why this isn't displaying)
        var currentUserLang = jQuery('#CurrentUserLang').val();
        jQuery('#SkierInstructionLang').val(currentUserLang);
        
        jQuery('#SkierBirthMonth').val(1);
        jQuery('#SkierBirthDay').val(1);
        jQuery('#SkierBirthYear').val(1985);
        
        // show skier section action buttons
        jQuery('#add-skier-button-div').show();
		
        // show success for buyer, skiers
        jQuery('#buyer-reg-section-head i').show();
        jQuery('#skier-reg-section-head i').show();
		
	}
	
	// Lessons have been registered, customer ready to check liability release and pay
	if (pageControl == 3){
		
		// show all buyer, skier summaries
		jQuery('.summary-view.buyer').show();
		jQuery('.summary-view.skier').show();
		
		// hide all buyer, skier bodies
		jQuery('.page-section-body.buyer').hide();
		jQuery('.page-section-body.skier').hide();
		
		// hide edit link for buyer
		jQuery('.edit-link.buyer').hide();
		
		// hide edit link for skiers
		jQuery('.edit-link.skier').hide();
		
		// hide skier section action buttons
        jQuery('#add-skier-button-div').hide();
		
		// show the lesson selection summary
        jQuery('#lesson-reg-section-head').show();
        jQuery('#lesson-reg-section-head').addClass('completed');
        
        
        // show body of liability release
        jQuery('#liability-release-section-body').show();
        jQuery('#liability-release-section-head i').hide();
        jQuery('#liability-release-section-head').addClass('completed');
        
        
        // show success for buyer, skiers, and lessons
        jQuery('#buyer-reg-section-head i').show();
        jQuery('#skier-reg-section-head i').show();
        jQuery('#lesson-reg-section-head i').show();
        
        // set up credit card error display
        jQuery('.fa-square, .fa-exclamation, .fa-credit-card').hide();
        
  
        
        
        
        
        
	}
	
	// Page reload after credit card submission, to handle declined card
	if (pageControl == 4){
		
		// show all buyer, skier summaries
		jQuery('.summary-view.buyer').show();
		jQuery('.summary-view.skier').show();
		
		// hide all buyer, skier bodies
		jQuery('.page-section-body.buyer').hide();
		jQuery('.page-section-body.skier').hide();
		
		// hide edit link for skiers
		jQuery('.edit-link.skier').hide();
		
		// hide skier section action buttons
        jQuery('#add-skier-button-div').hide();
        
        // show the lesson selection summary
        jQuery('#lesson-reg-section-head').show();
        jQuery('#lesson-reg-section-head').addClass('completed');
        
        // hide body of liability release
        jQuery('#liability-release-section-body').hide();
        jQuery('#liability-release-section-head').addClass('completed');
        
        
        // show success for buyer, skiers, lessons, and liability release
        jQuery('#buyer-reg-section-head i').show();
        jQuery('#skier-reg-section-head i').show();
        jQuery('#lesson-reg-section-head i').show();
        jQuery('#liability-release-section-head i').show();
        
        // show payment section
    	jQuery('#payment-section-head').addClass('completed');
    	jQuery('#payment-section-body').show();
    	
    	// if coupon code is in use, enter contents and update payment table
    	// is there currently a coupon discount applied?
		var currentAppliedCouponCode = jQuery('#ReturnCouponCode').val();
		var currentAppliedCouponAmount = jQuery('#ReturnCouponAmount').val();
		if (currentAppliedCouponCode.length > 0){
			
			// reset the coupon along with its applied discount
			// update the form submit with the raw number at this point
			jQuery('#chargeCouponAmount').val(currentAppliedCouponAmount);
			var updatedCouponAmount = jQuery('#chargeCouponAmount').val();
			console.log('Coupon amount for CC returned to: ' + updatedCouponAmount);
			
			// while currentAppliedCouponAmount is still a number
			var currentTotal = jQuery('#OrderTotalJPY').val();
			var newTotal = currentTotal - currentAppliedCouponAmount;
			
			// update placeholder hidden input, for use later in processing order
			jQuery('#OrderTotalJPY').val(newTotal);
			
			// add a comma for display
			newTotal = newTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			// currentAppliedCouponAmount is now a string
			currentAppliedCouponAmount = currentAppliedCouponAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			// re-populate coupon field
			jQuery('#MarketingCouponCode').val(currentAppliedCouponCode);
			
    	
        	// update table
			// update submit form fields as well, this will be sent with the $_POST on submit
			jQuery('#coupon-amount').text(currentAppliedCouponAmount);
			jQuery('#coupon-code').text(currentAppliedCouponCode);
			jQuery('#total-display').text(newTotal);
			jQuery('#confirmation-discount-display').slideDown(700);
			
			jQuery('#chargeCouponCode').val(currentAppliedCouponCode);
			var updatedCouponCode = jQuery('#chargeCouponCode').val();
			console.log('Coupon code for CC returned to: ' + updatedCouponCode);
		}
		
		// show the cc error
		jQuery('#cc-error-display').show();
		jQuery('.fa-square, .fa-exclamation').show();
		
		
		// fill the customer's credit card entry
		jQuery('#CCcardName').val(jQuery('#ReturnCC_Name').val());
		jQuery('#CCcardNumber').val(jQuery('#ReturnCC_Number').val());
		jQuery('#CCexpMonth').val(jQuery('#ReturnCC_ExpMonth').val());
		jQuery('#CCexpYear').val(jQuery('#ReturnCC_ExpYear').val());
		jQuery('#CCcvCode').val(jQuery('#ReturnCC_CVC').val());
		
        
	}
	
	// set up page timeout functions
	// show warning
    function display_timeout_warning(){
    	
    	// display warning message z-index overlay at top of browser
    	var warningText = jQuery('#PageTimeoutWarning').val();
    	jQuery('#timeout-warning').text(warningText).fadeIn(500);	
    	
    };
    
    // go to initial login page
    function execute_page_timeout(){
    	
    	window.location.replace('template_login.php');	
    	
    };
    
    if (pageControl == 3){
    	
    	// display warning message, delay of 15 minutes
    	window.setTimeout(display_timeout_warning, 15*60*1000); 
    	
    	// force load login page, delay of 20 minutes
    	window.setTimeout(execute_page_timeout, 20*60*1000); 
    	
    }
    
    // user CC error, speed up timeout delay
    if (pageControl == 4){
    	
    	// display warning message, delay of 5 minutes
    	window.setTimeout(display_timeout_warning, 5*60*1000); 
    	
    	// force load login page, delay of 10 minutes
    	window.setTimeout(execute_page_timeout, 10*60*1000); 
    	
    }
    
    
		
		
		
}); // end ready()