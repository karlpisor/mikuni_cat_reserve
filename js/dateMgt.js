// JavaScript Document
jQuery(document).ready(function() {
	 
	
	 // get an initial date to work with, must load at least the beginning of the season
	 var todayDate = new Date();
	 var month = todayDate.getMonth()+1;
	 var day = todayDate.getDate();
	 var todayDateString = todayDate.getFullYear() + 
	 					   ((''+month).length<2 ? '0' : '') + month +
	 					  ((''+day).length<2 ? '0' : '') + day;
	 //console.log('Today the date is: ' + todayDateString);
	 
	 
	 // initialize date entry fields
	 jQuery('.date-entry-arrival, .date-entry-departure, .date-start-entry, .date-end-entry').datepicker({ 
		 dateFormat: "yy-mm-dd" 
	 });
	 
	 // global values
	 // constrain date selections to the start and end dates of the current ski season
	 // Javascript dates use 0-index for months. Who knew? 
	 var seasonStartDate = new Date(2016,11,10); // Dec 10, 2016
	 var defaultFinishDate = new Date(2016,11,12); // Dec 12, 2016
	 
	 // set up comparison string
	 var month = seasonStartDate.getMonth()+1;
	 var day = seasonStartDate.getDate();
	 var seasonStartDateString = seasonStartDate.getFullYear() + 
	 					   ((''+month).length<2 ? '0' : '') + month +
	 					  ((''+day).length<2 ? '0' : '') + day;
	 //console.log('Season start date is: ' + seasonStartDateString);
	 
	 var seasonEndDate = new Date(2017,5,1); // Jun 1, 2017, changed at client request 6-Mar-2017
	 // var seasonEndDate = new Date(2017,3,6); // Apr 6, 2017
	 
	 
	 // set start dates
	 if (todayDateString <= seasonStartDateString){
		 
		// if before season, load season start date and default end date
		console.log('Today\'s date: ' + todayDateString + ' is before the season start: ' + seasonStartDateString);
		jQuery('.new.date-entry-arrival, .registered.date-entry-arrival').datepicker('setDate', seasonStartDate);
		jQuery('.new.date-entry-departure, .registered.date-entry-departure').datepicker('setDate', defaultFinishDate);
		 
	 } else {
		 
		// if in season, load dates 1 and 3 days out respectively
		jQuery('.new.date-entry-arrival, .registered.date-entry-arrival').datepicker('setDate', +1);
		jQuery('.new.date-entry-departure, .registered.date-entry-departure').datepicker('setDate', +1);
		 
	 }
	 
	 
	 
	 // if entry made in first box, add 2 days and enter in second box (most common pattern)
	 // new user
	 jQuery('.new.date-entry-arrival').change(function(){
		 
		 // set variables
		 var formId = 'new';
		 var startDate = jQuery(this).datepicker('getDate');
		 var finishDate = checkStartDate(formId, startDate);
		 
		 // set corresponding finish date
		 jQuery('.new.date-entry-departure').datepicker('setDate', finishDate);
		 
	 });
	 // registered user
	 jQuery('.registered.date-entry-arrival').change(function(){
		 
		 // set variables
		 var formId = 'registered';
		 var startDate = jQuery(this).datepicker('getDate');
		 var finishDate = checkStartDate(formId, startDate);

		 // set corresponding finish date
		 jQuery('.registered.date-entry-departure').datepicker('setDate', finishDate);
		 
		 
	 });
	 // registration main form
	 jQuery('.date-start-entry.date-entry-arrival').change(function(){
		 
		 
		 
		 // set variables
		 var formId = 'date-end-entry';
		 var startDate = jQuery(this).datepicker('getDate');
		 var finishDate = checkStartDate(formId, startDate);

		 // set corresponding finish date
		 jQuery('.date-end-entry.date-entry-departure').datepicker('setDate', finishDate);
		 		
	 });
	 
	 
	 

	 function checkStartDate(formId, startDate){
		 
		 var today = new Date;
		 var finishDate = new Date;
		 
		 // start date too early, before season begins
		 if (startDate < seasonStartDate){
			
			 // reset entry to season start date
			 jQuery('.' + formId +'.date-entry-arrival').datepicker('setDate', seasonStartDate);
			 jQuery(this).blur();
			 startDate = seasonStartDate;
			   
			 // show alert
		     var message = jQuery('#ErrorMsg_Datepicker_TooEarlyStart').val();
		     alert(message);
		     
		 }
		 // start date too late
		 if (startDate > seasonEndDate){
			
			 // reset entry season end date
			 jQuery('.' + formId +'.date-entry-arrival').datepicker('setDate', seasonEndDate);
			 jQuery(this).blur();
			   
			 // show alert
			 
		     var message = jQuery('#ErrorMsg_Datepicker_TooLateFinish').val();
		     alert(message);
		     
		 }
		 
		 // check entered date, any date before the current date is invalid
		 var todayDateDifference = (startDate - today) / (86400000);
		 if (todayDateDifference < 0){
			 
			 // entered start date is set early than current day, throw error
			 var message = jQuery('#ErrorMsg_Datepicker_StartDate').val();
		     alert(message);
		     jQuery(this).datepicker('setDate', today);
		     return false; 
	         
		 }
		 
		 // set finish date
		 finishDate.setDate(startDate.getDate());
		 
		 // adjust output of date (datepicker not producing correct dates!!)
		 if (finishDate.getDate() <= 2){
			 
			 if (startDate.getMonth() == 11){
				 
				 finishDate.setMonth(startDate.getMonth() + 1); 
				 finishDate.setFullYear(startDate.getFullYear() + 1);
				 
			 } else {
				 
				 finishDate.setMonth(startDate.getMonth() + 1); 
			 }
			 
		 } else {
			 
			 finishDate.setMonth(startDate.getMonth()); 
			 finishDate.setFullYear(startDate.getFullYear());
		 }
		 if (startDate.getMonth() != 11){
			 
			 // month of April in start date, any date
			 if (startDate.getMonth() >= 3 && startDate.getDate() > 0){
				 
				 // these correspond to a season final date of April 30
				 finishDate.setMonth(3);
				 finishDate.setDate(30);
				 
			 } 
		 }
			
		 return finishDate;
		 
	 } // end function
	 
	 
	 
	 
	 // manage user-entered dates for finish date
	 jQuery('.new.date-entry-departure').change(function(){
		 
		 // set variables
		 var formId = 'new';
		 var startDate = jQuery('.new.date-entry-arrival').datepicker('getDate');
		 var finishDate = jQuery('.new.date-entry-departure').datepicker('getDate');
		 
		 // run function to validate entered dates
		 checkFinishDate(formId, startDate, finishDate);  		 
		 
	 });
	 jQuery('.registered.date-entry-departure').change(function(){
		 
		 // set variables
		 var formId = 'registered';
		 var startDate = jQuery('.registered.date-entry-arrival').datepicker('getDate');
		 var finishDate = jQuery('.registered.date-entry-departure').datepicker('getDate');
		 
		 // run function to validate entered dates
		 checkFinishDate(formId, startDate, finishDate); 
		 
	 });
	 jQuery('.date-end-entry.date-entry-departure').change(function(){
		 
		 // set variables
		 var formId = 'date-end-entry';
		 var startDate = jQuery('.date-start-entry.date-entry-arrival').datepicker('getDate');
		 var finishDate = jQuery('.date-end-entry.date-entry-departure').datepicker('getDate');
		 
		 // run function to validate entered dates
		 checkFinishDate(formId, startDate, finishDate); 
		 
	 });
	 
	
	 
	 function checkFinishDate(formId, startDate, finishDate){
		 
		// do not allow date set in second box earlier than the date in the first box
		// 86400000 is the number of milliseconds in one day
		var dateDifference = (finishDate - startDate ) / (86400000);
		if (dateDifference < 0){
			 
			 // reset entry
			 jQuery('.' + formId +'.date-entry-departure').datepicker('setDate', startDate);
			   
			 // show alert
		     var message = jQuery('#ErrorMsg_Datepicker_EndDate_BeforeStartDate').val();
		     alert(message);
	         
		}
		 
		// do not allow a length of more than 10 days in a single registration
		var dateDifference10days = (finishDate - startDate ) / (86400000 * 10);
		if (dateDifference10days > 1){
			 
			 // reset entry to 10 days in length (adding 9 days)
			 finishDate.setDate(startDate.getDate() +9);
			
			 // adjust output of date (datepicker not producing correct dates!!)
			 // resulting date is in next month
			 if (finishDate.getDate() < 10){
				 
				 // december start date
				 if (startDate.getMonth() == 11){
					 
					 finishDate.setMonth(startDate.getMonth() + 1); 
					 finishDate.setFullYear(startDate.getFullYear() + 1);
					 
				 } else {
					 
					 finishDate.setMonth(startDate.getMonth() + 1); 
				 }
				 
			 }
			 
			
			 // set finish date
			 jQuery('.' + formId +'.date-entry-departure').datepicker('setDate', finishDate);
			   
			 // show alert
		     var message = jQuery('#ErrorMsg_Datepicker_10DaysMaxLength').val();
		     alert(message);
	         
		}
		
		// finish date too late
		if (finishDate > seasonEndDate){
			
			 // reset entry season end date
			 jQuery('.' + formId +'.date-entry-departure').datepicker('setDate', seasonEndDate);
			 jQuery(this).blur();
			   
			 // show alert
		     var message = jQuery('#ErrorMsg_Datepicker_TooLateFinish').val();
		     alert(message);
	        
	         
		}
		 
	 } // end function
	 
	 
	 
	 
}); // end ready()