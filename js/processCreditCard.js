// JavaScript Document
jQuery(document).ready(function() {
	
	// global variable set
	var stripeStatus = 'test';
	
	var live_publicKey = 'pk_live_nb64TWYMpvVfFgUe69sjwDy5';
	var test_publicKey = 'pk_test_EgOxVV6N381tuozOvygp3Ln8';
	
		
	// bind validation behavior to the credit card form with the payment lib
    jQuery('.cc-number').payment('formatCardNumber');
    jQuery('.cc-cvc').payment('formatCardCVC');

    jQuery.fn.toggleInputError = function(erred) {
      this.parent('.form-group').toggleClass('has-error', erred);
      return this;
    };
    
    // set CC error code strings from hidden fields on the registration page
    var errorMsgIncorrectNumber = jQuery('#ErrorMsg_CC_IncorrectNumber').val();
    var errorMsgInvalidNumber = jQuery('#ErrorMsg_CC_InvalidNumber').val();
    var errorMsgInvalidCVC = jQuery('#ErrorMsg_CC_InvalidCVC').val();
    var errorMsgExpiredCard = jQuery('#ErrorMsg_CC_ExpiredCard').val();
    var errorMsgIncorrectCVC = jQuery('#ErrorMsg_CC_IncorrectCVC').val();
    var errorMsgCardDeclined = jQuery('#ErrorMsg_CC_CardDeclined').val();
    var errorMsgProcessingError = jQuery('#ErrorMsg_CC_ProcessingError').val();
    var errorMsgInvalidExpiryMonth = jQuery('#ErrorMsg_CC_InvalidExpiryMonth').val();
    var errorMsgInvalidExpiryYear = jQuery('#ErrorMsg_CC_InvalidExpiryYear').val();
    
    
    
    var errorMessages = {
		  incorrect_number: errorMsgIncorrectNumber,
		  invalid_number: errorMsgInvalidNumber,
		  invalid_cvc: errorMsgInvalidCVC,
		  expired_card: errorMsgExpiredCard,
		  incorrect_cvc: errorMsgIncorrectCVC,
		  card_declined: errorMsgCardDeclined,
		  processing_error: errorMsgProcessingError,
		  invalid_expiry_month: errorMsgInvalidExpiryMonth,
		  invalid_expiry_year: errorMsgInvalidExpiryYear,
		  missing: "001 There is no card on a customer that is being charged.",
		  rate_limit:  "002 An error occurred due to requests hitting the API too quickly. Please let us know if you're consistently running into this error."
	};

	
    // This public key identifies this website in the createToken call below
    if (stripeStatus == 'live'){
		
    	Stripe.setPublishableKey(live_publicKey);
		
	} else if (stripeStatus == 'test'){
		
		Stripe.setPublishableKey(test_publicKey);
		
	} 
	
	  
	var stripeResponseHandler = function(status, response){
      
		var ccForm = jQuery('#payment-form');
      
		if (response.error && response.error.type == 'card_error'){
		    
			// show the errors on the form
			jQuery('#cc-error-display .cc-error-msg').text(errorMessages[response.error.code]);
			jQuery('#cc-error-display').show();
			jQuery('.fa-square, .fa-exclamation').show();
			console.log(response.error.code);
		  
			// get the button working again so the user can update entry and try again
	        ccForm.find('button').prop('disabled', false);
        
	    } else {
	    	
	    	// no error triggered, go ahead and process card
	    	
	    	// token contains id, last4, and card type
	        var token = response.id;
	        
	        // insert the token into the form so it gets submitted to the server
	        ccForm.append(jQuery('<input type="hidden" name="stripeToken" />').val(token));
	         
	        // and re-submit
	        ccForm.get(0).submit();
	    }
      
	}; // end stripeResponseHandler
	
	
	jQuery(function($){
	    	
	      $('#payment-form').submit(function(e){
	    	  
	    	// don't want the submit to actually submit (this will reload the page)
	    	e.preventDefault();
	    	
	    	var cardType = jQuery.payment.cardType(jQuery('.cc-number').val());
	      	jQuery('.cc-number').toggleInputError(!jQuery.payment.validateCardNumber(jQuery('.cc-number').val()));
	      	jQuery('.cc-cvc').toggleInputError(!jQuery.payment.validateCardCVC(jQuery('.cc-cvc').val(), cardType));
	      	//jQuery('.cc-brand').text(cardType);  
	    	
	    	var ccForm = jQuery('#payment-form');
	    	 
	    	ccForm.validate();
	    	
	    	if (ccForm.valid()){
	    		
	    		// set the charge amount, order id for submission by the form
		    	var chargeAmount = jQuery('#OrderTotalJPY').val();
		    	jQuery('#chargeAmount').val(chargeAmount);
		    	var orderId = jQuery('#OrderId').val();
		    	jQuery('#chargeOrderId').val(orderId);
		    	
		    	// Disable the submit button to prevent repeated clicks
		    	ccForm.find('button').prop('disabled', true);
		        
		        // this submits the form
		        Stripe.card.createToken(ccForm, stripeResponseHandler);
		       
		        
	    	} else {
	    		
	    		// Prevent the form from submitting with the default action
		        return false;
	    	}
	    	  
	     });
	      
	  });
		 
}); //end ready function


