// JavaScript Document

jQuery(document).ready(function() {
		 
		//Dynamically style navigation links 
		//Target and add CSS class to "You are here" link
		jQuery("body#AdminContacts ul#Admin_Menu_Nav li#Contacts").addClass('Selected');
		
		
		//Remove the a element from the page, and insert its text in the parent li tag.
		var Link = jQuery("li.Selected a").text();
		jQuery("li.Selected a").remove();
		jQuery("li.Selected").text(Link);
			 
}); //end ready function