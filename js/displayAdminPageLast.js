// JavaScript Document
jQuery(document).ready(function() {
		
	// Admin pages
	
	// hide tabs to match user lang on page load
	jQuery('#page-admin').hide();
	
	var userLang = jQuery('#CurrentUserLang').val();
	if (userLang == 'EN'){
		
		jQuery('#page-jp-entry, #page-cn-entry').hide();
		jQuery('#japanese, #chinese, #admin').addClass('deselected');
		jQuery('#english').addClass('selected');
		
	} else if (userLang == 'CN'){
		
		jQuery('#page-jp-entry, #page-en-entry').hide();
		jQuery('#japanese, #english, #admin').addClass('deselected');
		jQuery('#chinese').addClass('selected');
		
	} else {
		
		// default is JP
		jQuery('#page-en-entry, #page-cn-entry').hide();
		jQuery('#english, #chinese, #admin').addClass('deselected');
		jQuery('#japanese').addClass('selected');
	}
		
		
			
			
		 
}); //end ready function