// JavaScript Document

// ready function, runs on page load
jQuery(document).ready(function() {
	
	// Page load show/hides by page control stage are managed in formRegistrationLast.js
	// Page load show/hides are executed after all page content has loaded

	// updates to page display depending on the state of the order
	var pageControl = jQuery('#RegPageControl').val();
	
	// string utilities, available to all scripts
	function ucwords(string){
	    return (string + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($f) {
	        return $f.toUpperCase();
	    });
	}

	function strtolower(string){
	    return (string + '').toLowerCase();
	}
	
	function ucHypenatedNames(string) {
	    var words = string.split(/(\s|-)+/), output = [];

	    for (var i = 0, len = words.length; i < len; i += 1) {
	        output.push(words[i][0].toUpperCase() +
	                    words[i].toLowerCase().substr(1));
	    }

	    return output.join('');
	}
	
	
	
	
	//--------------- Buyer --------------------------------------------/
		
	
	// Buyer registration form
	jQuery(function(){
		jQuery.fn.autoKana('#BuyerLastNameJP','#BuyerLastNameJP_Kana', {
            katakana : true
		});
	});
	jQuery(function(){
		jQuery.fn.autoKana('#BuyerFirstNameJP','#BuyerFirstNameJP_Kana', {
	        katakana : true
		});
	});
	   
	// buyer edit link
	jQuery('.buyer.edit-link').click(function(){
		
		// roll up the buyer summary and show the form body
		jQuery('#buyer-reg-summary').slideUp();
		jQuery('#buyer-reg-section-body').slideDown(500);
		
		// show all skier summaries, roll up all skier form bodies
		jQuery('.summary-view.skier').slideDown(500);
		jQuery('.page-section-body.skier').slideUp(500);
		
		// for foreign residents, show the prefecture advice
		var countryOfResidence = jQuery('#BuyerCountryOfResidenceFK').val();
		if (countryOfResidence != 109){
			jQuery('.example.prefecture').show();	
		}
		
		// hide skier section action buttons
		jQuery('#add-skier-button-div').hide();
		
		// conditional actions based on order status
    	var pageControl = jQuery('#RegPageControl').val();
    	if (pageControl > 1){
    		
    		// user is registered, has skier(s) registered
    		// show cancel on edit
    		jQuery('#BuyerRegCancel').show();
    		
    	}
		
		
		
	});   
	
	// set up JP prefecture selection list
    var defaultInput = jQuery('div.prefecture select').html();
	jQuery('#PrefectureSelectionList select').append(defaultInput);
	jQuery('#PrefectureSelectionList').hide();
	
	// manage users from different countries, prefectures are not loaded for overseas guests
    jQuery('.example.prefecture').hide();
    jQuery('#BuyerCountryOfResidenceFK').change(function(){
		
    	// depending on new value, replace the select content, show new advice
    	var newSelectValue = jQuery('#BuyerCountryOfResidenceFK').val();
    	
    	//console.log(newSelectValue);
    	if (newSelectValue == 109){
			
    		console.log('selected = Japan');
			// restore the select menu for Japanese prefectures
			var storedHTML = jQuery('#PrefectureSelectionList').html();
			jQuery('#BuyerPrefectureText').replaceWith(storedHTML);
			// add the id back to the select
			jQuery('div.prefecture select').attr('id','BuyerPrefectureFK');
        	jQuery('.example.prefecture').hide();	
			
		} else {
			console.log('selected = Other');
			// replace select with input text box
			var newInput = '<input type="text" name="BuyerPrefectureText" id="BuyerPrefectureText" class="required" title=""/>';
        	jQuery('#BuyerPrefectureFK').replaceWith(newInput);
        	jQuery('.example.prefecture').show();
		}
    });
    
    // cancel buyer update
    jQuery('#BuyerRegCancel').click(function(e){
		
    	// don't want the submit to actually submit (this will reload the page)
		e.preventDefault();
    	
    	// reset buyer area
    	jQuery('#buyer-reg-section-body').slideUp();
        jQuery('#buyer-reg-summary').fadeIn();
        
        if (pageControl < 3){
        	// show add new skier button, lesson proceed button
            jQuery('#add-skier-button-div').show();
		}
		
	}); // end BuyerRegCancel click function()
	    
    
    
    
    
    // new buyer update form validation
    // add custom method to the validator object = "accept", which will allow us to run a regex against the Chinese entry
    jQuery.validator.addMethod("accept", function(value, element, param) {
    	  return value.match(new RegExp("." + param + "$"));
    });
    	
	jQuery('#FirstTimeBuyerRegSubmit').click(function(){
		var currentUserLang = jQuery('#CurrentUserLang').val();
		
		if (currentUserLang == 'CN'){
			
			// force user to enter only letters, no Chinese characters permitted.
			jQuery('#BuyerRegForm').validate({
				rules: {
					  BuyerEmail: {
						  required: true,
						  email: true
					  },
					  BuyerLastNameJP: { 
						  accept: "[a-zA-Z\s]+"
					  },
					  BuyerFirstNameJP: { 
						  accept: "[a-zA-Z\s]+"
					  }	
				}
			});	 // end validate arguments	
			
		} else {
			
			jQuery('#BuyerRegForm').validate({
				rules: {
					  BuyerEmail: {
						  required: true,
						  email: true
					  }
				}
			});	 // end validate arguments	
		}
		
		// database update for first time buyer submit is handled in reg_database.php
		
		
	}); // end FirstTimeBuyerRegSubmit click function()
	 
	// EN buyer only: update to first or last name will update label on Question: Is this person taking lessons?
	jQuery('#BuyerLastNameJP, #BuyerFirstNameJP').change(function(){
		var currentUserLang = jQuery('#CurrentUserLang').val();
		if (currentUserLang == 'EN'){
			var updatedFirstName = jQuery('#BuyerFirstNameJP').val();
			var updatedLastName = jQuery('#BuyerLastNameJP').val();
			var newNameUpdate = updatedFirstName + ' ' + updatedLastName + ' ';
			jQuery("label[for=BuyerIsSkier]").text("Will " + newNameUpdate + "be taking a lesson?");
		}
	});
	 
	// previously registered buyer, use AJAX to submit buyer form and update in place
	// TODO: how to show/not show skier as required?
    jQuery('#BuyerRegSubmit').bind('click', function(e) {
        	
    		// don't want the submit to actually submit (this will reload the page)
    		e.preventDefault();
    	
            // validate form
    		var currentUserLang = jQuery('#CurrentUserLang').val();
    		if (currentUserLang == 'CN'){
    			
    			// force user to enter only letters, no Chinese characters permitted.
    			jQuery('#BuyerRegForm').validate({
    				rules: {
    					  BuyerEmail: {
    						  required: true,
    						  email: true
    					  },
    					  BuyerLastNameJP: { 
    						  accept: "[a-zA-Z\s]+"
    					  },
    					  BuyerFirstNameJP: { 
    						  accept: "[a-zA-Z\s]+"
    					  }	
    				}
    			});	 // end validate arguments	
    			
    		} else {
    			
    			jQuery('#BuyerRegForm').validate({
    				rules: {
    					  BuyerEmail: {
    						  required: true,
    						  email: true
    					  }
    				}
    			});	 // end validate arguments	
    		}
    		
    		
            
            if (jQuery('#BuyerRegForm').valid()) {
            
            	// set up variables for buyer record update
            	var BuyerCountryFK = jQuery('#BuyerCountryOfResidenceFK').val();
            	console.log('BuyerCountryFK: ' + BuyerCountryFK);
            	
            	// Japan country key value is 109
            	var BuyerIsOverseas = '';
            	if (BuyerCountryFK == 109){
            		BuyerIsOverseas = 0;
            	} else {
            		BuyerIsOverseas = 1;
            	}
            	console.log('BuyerIsOverseas: ' + BuyerIsOverseas);
            	
            	
            	// set prefecture key (Japanese domestic address) or string (overseas)
            	var BuyerPrefectureFK = '';
            	var BuyerPrefectureText = '';
            	if (BuyerIsOverseas == 1){
            		BuyerPrefectureText = jQuery('#BuyerPrefectureText').val();
            		BuyerPrefectureFK = 0;
            	} else {
            		BuyerPrefectureFK = jQuery('#BuyerPrefectureFK').val();
            	}
            	console.log('BuyerPrefectureFK: ' + BuyerPrefectureFK);
            	console.log('BuyerPrefectureText: ' + BuyerPrefectureText);
            	
            	
            	
            	
            	var BuyerLastNameJP = jQuery('#BuyerLastNameJP').val(); 
            	var BuyerFirstNameJP = jQuery('#BuyerFirstNameJP').val();
            		
            	// force correct capitalization of all words in name fields for CN and EN users
            	if (currentUserLang == 'CN' || currentUserLang == 'EN'){
            		
            		// process name strings
            		BuyerLastNameJP = ucwords(strtolower(BuyerLastNameJP));
            		BuyerFirstNameJP = ucwords(strtolower(BuyerFirstNameJP));
            		
            		// do the names have any hyphens? 
            		if (BuyerLastNameJP.indexOf('-') > -1)
            		{
            			// process hyphenated names
            			BuyerLastNameJP = ucHypenatedNames(BuyerLastNameJP);
            		}
            		if (BuyerFirstNameJP.indexOf('-') > -1)
            		{
            			// process hyphenated names
            			BuyerFirstNameJP = ucHypenatedNames(BuyerFirstNameJP);
            		}	
            	}
            	
            	var BuyerLastNameJP_Kana = jQuery('#BuyerLastNameJP_Kana').val(); 
            	var BuyerFirstNameJP_Kana = jQuery('#BuyerFirstNameJP_Kana').val();
            	var BuyerEmail = jQuery('#BuyerEmail').val();
            	
            	var BuyerPhone = jQuery('#BuyerPhone').val();
            	var BuyerPostalCode = jQuery('#BuyerPostalCode').val();
            	var BuyerCountryOfResidenceFK = jQuery('#BuyerCountryOfResidenceFK').val();
            	
            	var BuyerCityTown = jQuery('#BuyerCityTown').val();
            	
            	var BuyerAddress1 = jQuery('#BuyerAddress1').val();
            	var BuyerAddress2 = jQuery('#BuyerAddress2').val();
            	var BuyerEmergencyPhone = jQuery('#BuyerEmergencyPhone').val();
            	var BuyerEmergencyContact = jQuery('#BuyerEmergencyContact').val();
            	
            	// language set in hidden field
            	var BuyerCurrentUserLang = jQuery('#BuyerCurrentUserLang').val();
            	
            	
            	var BuyerReceivesNewsletter = '';
            	var selected = jQuery("input[type='radio'][name='BuyerReceivesNewsletter']:checked");
            	if (selected.length > 0) {
            		BuyerReceivesNewsletter = selected.val();
            	}
            	
            	var BuyerIsSkier = '';
            	var selected = jQuery("input[type='radio'][name='BuyerIsSkier']:checked");
            	if (selected.length > 0) {
            		BuyerIsSkier = selected.val();
            	}
            	
            	
            	console.log('LastJP '+BuyerLastNameJP+' FirstJP '+BuyerFirstNameJP);
            	console.log('LastJP_Kana: ' + BuyerLastNameJP_Kana + ' FirstJP_Kana: ' + BuyerFirstNameJP_Kana);
            	
            	
            	// ajax call to update record
            	jQuery.post(
            			
                	// set target	
                    'template_ajax_buyer_reg.php'
                	
                	// set variables to be passed to target	
                    , {BuyerLastNameJP: BuyerLastNameJP, 
                    	BuyerFirstNameJP: BuyerFirstNameJP, 
                    	BuyerLastNameJP_Kana: BuyerLastNameJP_Kana,
                    	BuyerFirstNameJP_Kana: BuyerFirstNameJP_Kana,
                    	BuyerEmail: BuyerEmail,
                    	
                    	BuyerPhone: BuyerPhone,
                    	BuyerPostalCode: BuyerPostalCode,
                    	BuyerCountryOfResidenceFK: BuyerCountryOfResidenceFK,
                    	BuyerPrefectureFK: BuyerPrefectureFK,
                    	BuyerPrefectureText: BuyerPrefectureText,
                    	BuyerCityTown: BuyerCityTown,
                    	
                    	BuyerAddress1: BuyerAddress1,
                    	BuyerAddress2: BuyerAddress2,
                    	BuyerEmergencyPhone: BuyerEmergencyPhone,
                    	BuyerEmergencyContact: BuyerEmergencyContact,
                    	BuyerCurrentUserLang: BuyerCurrentUserLang, 
                    	
                    	BuyerReceivesNewsletter: BuyerReceivesNewsletter,
                    	BuyerIsSkier: BuyerIsSkier,
                    	BuyerIsOverseas: BuyerIsOverseas}
                    	
                    // execute, creates html object from target
                    , function(data, status){
                        
                    	// want to limit the amount of return messaging going on at early page states
                    	var pageControl = jQuery('#RegPageControl').val();
                    	
                    	//console.log('Data return successful.');
                    	if (pageControl > 0){
                        	
                    		// insert returned data in summary area, roll up form
                        	jQuery('#buyer-reg-summary .summary-content').empty();
                            jQuery('#buyer-reg-summary .summary-content').prepend(data);
                            jQuery('#buyer-reg-section-body').slideUp();
                            jQuery('#buyer-reg-summary').fadeIn();
                            
                            // add success message to the section head
                            var successMsg = jQuery('#SuccessMsg_Buyer_Updated').val();
                            //console.log(successMsg);
                    		
                            jQuery('#BuyerSectionMsg').empty();
                            jQuery('#BuyerSectionMsg').append(successMsg);
                            jQuery('#BuyerSectionMsg').fadeIn(500).delay(1500).fadeOut(500);
                            
                            // find skier if buyer
                            var buyerId = jQuery('#BuyerId').val();
                            // only skier-0 can be the buyer, no others
                            var skier0Id = jQuery('#SkierPK_0').val();
                            
                            if (buyerId === skier0Id){
                            	
                            	// update skier entry hidden fields for buyer as skier
                            	var updateSkierLastName = jQuery('#BuyerLastNameJP').val();
                            	jQuery('#SkierLastNameJP_0').val(updateSkierLastName);
                            	
                            	var updateSkierLastNameKana = jQuery('#BuyerLastNameJP_Kana').val();
                            	jQuery('#SkierLastNameJP_Kana_0').val(updateSkierLastNameKana);
                            	
                            	var updateSkierFirstName = jQuery('#BuyerFirstNameJP').val();
                            	jQuery('#SkierFirstNameJP_0').val(updateSkierFirstName);
                            	
                            	var updateSkierFirstNameKana = jQuery('#BuyerFirstNameJP_Kana').val();
                            	jQuery('#SkierFirstNameJP_Kana_0').val(updateSkierFirstNameKana);
                            	
                            	console.log('After update, LastJP_Kana: ' + BuyerLastNameJP_Kana + ' FirstJP_Kana: ' + BuyerFirstNameJP_Kana);
                            	
                            	// update name display for buyer as skier
                            	// display is language-dependent
                            	var currentUserLang = jQuery('#CurrentUserLang').val();
                            	var updateSkierNameDisplay = '';
                            	if (currentUserLang == 'JP'){
                            		updateSkierNameDisplay = updateSkierLastName + ' ' + updateSkierFirstName;
                            	} else if (currentUserLang == 'EN'){
                            		updateSkierNameDisplay = updateSkierFirstName + ' ' + updateSkierLastName;
                            	} else if (currentUserLang == 'CN'){
                            		updateSkierNameDisplay = updateSkierLastName + ' ' + updateSkierFirstName;
                            	}
                            	
                            	// execute update: both entry and summary display
                            	jQuery('#SkierNameDisplay_0').text(updateSkierNameDisplay);
                            	jQuery('#skier-reg-summary-0 .summary-content .name-display').text(updateSkierNameDisplay);
                            }
                            
                            if (pageControl < 3){
                            	// show add new skier button, lesson proceed button
                                jQuery('#add-skier-button-div').show();
                    		}
                            
                        } // check for PageControl > 0
                        
                    }
                    // return format is html
                    , 'html'
                    
            	); // end ajax call         	
            	
            } // end valid check on form
            
    }); // end submit click for buyer update    
	    
	
    
    
    
    
    
    
    
    
    
    
    
    //--------------- Skiers --------------------------------------------/
		
	// Skier registration form, new skier (for skier update, this kana autofill not required)
	jQuery(function(){
		jQuery.fn.autoKana('#SkierLastNameJP','#SkierLastNameJP_Kana', {
	        katakana : true
		});
	});
	jQuery(function(){
		jQuery.fn.autoKana('#SkierFirstNameJP','#SkierFirstNameJP_Kana', {
		    katakana : true
		});
	});
		
	// scrub the new skier entry form
	function resetForm($form){
	    $form.find('input:text, input:password, input:file, select, textarea').val('');
	    $form.find('input:radio, input:checkbox')
	         .removeAttr('checked').removeAttr('selected');
	}
	resetForm(jQuery('#NewSkierRegForm'));	
		
	// Set up edit links to show hidden form(s)
	jQuery('.skier.edit-link').click(function(){
		
		// roll up buyer form, show summary
		jQuery('#buyer-reg-summary').fadeIn(300);
		jQuery('#buyer-reg-section-body').slideUp(500);
		
		// show all skier form summaries (some are already shown, we just want to make sure we get them all)
		jQuery('.summary-view.skier').fadeIn();
		
		// roll up all skier form bodies
		jQuery('.page-section-body.skier').slideUp(500);
		
		// roll up current skier summary
		jQuery(this).parent().parent().slideUp();
		
		// show current skier form body for editing
		jQuery(this).parent().parent().next().slideDown(500);
		
		// hide skier section action buttons
		jQuery('#add-skier-button-div').hide();
		
		// blank out any lesson selection, reset lesson selection area
		// TODO: show warning dialog with cancel
		// hide selection message and parameter form
		jQuery('#LessonSelectionMsg').slideUp(500);
		jQuery('#lesson-parameter-selection').slideUp(500);
    	
    	// roll up selected lessons and clear out the lesson target container
    	jQuery('#lesson-container').slideUp(500);
    	jQuery('#lesson-container').text('');
	});
		
	jQuery('#SkierRegConfirmAdd').click(function(){
		
		// roll up the buyer form and show the buyer summary (this should be closed already)
		jQuery('#buyer-reg-summary').slideDown(500);
		jQuery('#buyer-reg-section-body').slideUp(500);
		
		// show all skier form summaries (some are already shown, we just want to make sure we get them all)
		jQuery('.summary-view.skier').fadeIn();
		
		// roll up all skier form bodies
		jQuery('.page-section-body.skier').slideUp(500);
		
		// show new skier input form
		jQuery('#new-skier-reg-section-body').slideDown(500);
		jQuery('#NewSkierRegForm').children().children().children().first().next().focus();
		
		// hide add new skier button
		jQuery('#SkierRegConfirmAdd').hide();
		
		// hide proceed to lesson form
		jQuery('#LessonSelectionProceed').hide();
		
		
		// clear out all wizards
		jQuery('.ControlButtons').hide();
	    jQuery('.LevelSelectionWizard').hide();
	    
	    // scroll to top of form
	    jQuery('#new-skier-reg-section-body').scrollTop(150);
	    
	});
	
	
		
		
	
	
	// page control for skier data entry
        
    // NEW skier entry
    jQuery('#SkierAbilityLevelBlockSnowboard, #SkierAbilityLevelBlockSki').hide();
    
    // set up clicks to display preferred type of lesson
    jQuery('#SkierLessonTypeSki').click(function(){
    	
    	// hide and zero out the other ability level
    	jQuery('#SkierAbilityLevelBlockSnowboard').hide();
    	jQuery('#SkierAbilityLevelSnowboard').val(0);
    	var currentSnowboardLevel = jQuery('#SkierAbilityLevelSnowboard').val();
    	console.log('After update, snowboard ability level is = ' + currentSnowboardLevel);
    	
    	// show desired lesson type, preset starting value
    	// note this will reset anything in here
    	jQuery('#SkierAbilityLevelBlockSki').show();
    	jQuery('#SkierAbilityLevelSki').val(1).prop("selected");
    	
    	// clear any display from before
    	jQuery('.ControlButtons').hide();
    	jQuery('.LevelSelectionWizard').hide();
    	
    });
    jQuery('#SkierLessonTypeSnowboard').click(function(){
    	
    	// hide and zero out the other ability level
    	jQuery('#SkierAbilityLevelBlockSki').hide();
    	jQuery('#SkierAbilityLevelSki').val(0);
    	var currentSkiLevel = jQuery('#SkierAbilityLevelSki').val();
    	console.log('After update, ski ability level is = ' + currentSkiLevel);
    	
    	// show desired lesson type, preset starting value
    	// note this will reset anything in here
    	jQuery('#SkierAbilityLevelBlockSnowboard').show();
    	jQuery('#SkierAbilityLevelSnowboard').val(1).prop("selected");
    	
    	// clear any display from before
    	jQuery('.ControlButtons').hide();
    	jQuery('.LevelSelectionWizard').hide();
    	
    });
    jQuery('#SkierLessonTypeBoth').click(function(){
    	
    	// show both lesson types, preset values if none chosen
    	jQuery('#SkierAbilityLevelBlockSnowboard').show();
    	jQuery('#SkierAbilityLevelSnowboard').val(1).prop("selected");
    	
    	jQuery('#SkierAbilityLevelBlockSki').show();
    	jQuery('#SkierAbilityLevelSki').val(1).prop("selected");
    	
    	// clear any display from before
    	jQuery('.ControlButtons').hide();
    	jQuery('.LevelSelectionWizard').hide();
    	        	
    });
    
    
    // standard page load for NEW skier
    jQuery('#adult-snowboard-level-advice').show();
	jQuery('#adult-ski-level-advice').show();
	jQuery('#junior-snowboard-level-advice').hide();
	jQuery('#junior-ski-level-advice').hide();
	jQuery('#skier-food-allergies').hide();
	jQuery('#junior-age-advice').hide();
    
    // conditional display of adult vs. junior wizard links
    // hide wizards when page is loaded
    // show wizard on click
   jQuery('#SkierBirthYear').change(function(){
    	
    	// Typically, skiers 12 and older on the first day of their lesson are entered as adults
    	// can't use first date of ski lessons, as many users will not put this in correctly
    	// we calculate juniors with a broad rule that born after 2004-04-02, you are a junior
    	
    	var skierYear = jQuery('#SkierBirthYear').val();
    	var skierMonth = jQuery('#SkierBirthMonth').val();
    	if (skierMonth < 10){
    		skierMonth = skierMonth.toString();
    		skierMonth = '0' + skierMonth;
    	}
    	var skierDay = jQuery('#SkierBirthDay').val();
    	if (skierDay < 10){
    		skierDay = skierDay.toString();
    		skierDay = '0' + skierDay;
    	}
    	var skierBirthDate = parseInt(skierYear.toString() + skierMonth.toString() + skierDay.toString());
    	var toJuniorcutoffDate = parseInt(20040402);
    	var kidsToJuniorCutoffDate = parseInt(20100402);
    	
    	
    	
    	if (skierBirthDate > toJuniorcutoffDate){
    		
    		console.log(skierBirthDate + ' is greater than ' + toJuniorcutoffDate);
    		console.log('Skier is either a junior or a peewee ("kids")');
    		
    		if (skierBirthDate > kidsToJuniorCutoffDate){
    			
    			console.log(skierBirthDate + ' is greater than ' + kidsToJuniorCutoffDate);
    			console.log('Skier is a peewee ("kids")');
    		} else {
    			
    			console.log(skierBirthDate + ' is less than ' + kidsToJuniorCutoffDate);
    			console.log('Skier is a junior');
    		}
    		
    		// junior
    		jQuery('#adult-snowboard-level-advice').hide();
    		jQuery('#adult-ski-level-advice').hide();
    		jQuery('#junior-snowboard-level-advice').show();
    		jQuery('#junior-ski-level-advice').show();
    		
    		// language-dependent display of allergy input
    		// only JP-lang juniors may join junior lessons (and get the allergy input)
    		var currentSkierLang = jQuery('#SkierInstructionLang').val();
    		console.log('This skier\'s language of instruction is: ' +currentSkierLang);
    		if (currentSkierLang == 'JP'){
    			jQuery('#junior-age-advice').slideDown();
    			jQuery('#skier-food-allergies').show();
    		}
    		
    		// clear any display from before
        	jQuery('.ControlButtons').hide();
        	
        	
    	} else {
    		// adult
    		jQuery('#adult-snowboard-level-advice').show();
    		jQuery('#adult-ski-level-advice').show();
    		jQuery('#junior-snowboard-level-advice').hide();
    		jQuery('#junior-ski-level-advice').hide();
    		jQuery('#skier-food-allergies').hide();
    		jQuery('#junior-age-advice').slideUp();
    		
    		// clear any display from before
        	jQuery('.ControlButtons').hide();
        	jQuery('.LevelSelectionWizard').hide();
    	}
    	
    }); // end page control for new skier
    
   
   
    // REGD skier updates
    jQuery('.SkierRegForm').each(function(v) {
    	
    	// hide both selection blocks with standard page load
    	jQuery('#SkierAbilityLevelBlockSnowboard_' + v).hide();
    	jQuery('#SkierAbilityLevelBlockSki_' + v).hide();
    	
    	// show level setting based on existing skier data
    	var showSkiLevel = jQuery('#SkierLessonTypeSki_' + v).prop("checked");
    	if (showSkiLevel){
    		jQuery('#SkierAbilityLevelBlockSki_' + v).show();
    	}
    	var showSnowboardLevel = jQuery('#SkierLessonTypeSnowboard_' + v).prop("checked");
    	if (showSnowboardLevel){
    		jQuery('#SkierAbilityLevelBlockSnowboard_' + v).show();
    	}
    	var showBothLevels = jQuery('#SkierLessonTypeBoth_' + v).prop("checked");
    	if (showBothLevels){
    		jQuery('#SkierAbilityLevelBlockSki_' + v).show();
    		jQuery('#SkierAbilityLevelBlockSnowboard_' + v).show();
    	}
    	
    	// click to toggle display
    	jQuery('#SkierLessonTypeSki_' + v).click(function(){
        	jQuery('#SkierAbilityLevelBlockSki_' + v).show();
        	
        	jQuery('#SkierAbilityLevelSnowboard_' + v).val(0);
        	var currentSnowboardLevel = jQuery('#SkierAbilityLevelSnowboard_' + v).val();
        	console.log('After update, snowboard ability level is = ' + currentSnowboardLevel);
        	
        	jQuery('#SkierAbilityLevelBlockSnowboard_' + v).hide();
        	jQuery('.LevelSelectionWizard').hide();
			jQuery('.ControlButtons').hide();
        });
        jQuery('#SkierLessonTypeSnowboard_' + v).click(function(){
        	jQuery('#SkierAbilityLevelBlockSnowboard_' + v).show();
        	
        	
        	jQuery('#SkierAbilityLevelSki_' + v).val(0);
        	var currentSkiLevel = jQuery('#SkierAbilityLevelSki_' + v).val();
        	console.log('After update, ski ability level is = ' + currentSkiLevel);
        	
        	
        	jQuery('#SkierAbilityLevelBlockSki_' + v).hide();
        	jQuery('.LevelSelectionWizard').hide();
			jQuery('.ControlButtons').hide();
        });
        jQuery('#SkierLessonTypeBoth_' + v).click(function(){
        	jQuery('#SkierAbilityLevelBlockSnowboard_' + v).show();
        	var currentSnowboardLevel = jQuery('#SkierAbilityLevelSnowboard_' + v).val();
        	if (currentSnowboardLevel == 0){
        		jQuery('#SkierAbilityLevelSnowboard_' + v).val(1).prop("selected");
        	}
        	jQuery('#SkierAbilityLevelBlockSki_' + v).show();
        	var currentSkiLevel = jQuery('#SkierAbilityLevelSki_' + v).val();
        	if (currentSkiLevel == 0){
        		jQuery('#SkierAbilityLevelSki_' + v).val(1).prop("selected");
        	}
        	jQuery('.LevelSelectionWizard').hide();
			jQuery('.ControlButtons').hide();
        });
        
        
        
        
        // set display of adult or junior links based on skier age data
        // Typically, skiers 12 and older on the first day of their lesson are entered as adults
    	// but can't use first date of ski lessons, as many users will not put this in correctly
    	// we calculate juniors with a broad rule that born after 2004-04-02, you are an junior
        
        // on page load
    	var skierYear = jQuery('#SkierBirthYear_' + v).val();
    	var skierMonth = jQuery('#SkierBirthMonth_' + v).val();
    	if (skierMonth < 10){
    		skierMonth = skierMonth.toString();
    		skierMonth = '0' + skierMonth;
    	}
    	var skierDay = jQuery('#SkierBirthDay_' + v).val();
    	if (skierDay < 10){
    		skierDay = skierDay.toString();
    		skierDay = '0' + skierDay;
    	}
    	var currentRegdSkierBirthDate = parseInt(skierYear.toString() + skierMonth.toString() + skierDay.toString());
    	var toJuniorcutoffDate = parseInt(20040402);
    	var kidsToJuniorCutoffDate = parseInt(20100402);
    	console.log(currentRegdSkierBirthDate + ' different from ' + toJuniorcutoffDate);
        
    	
    	
    	
    	if (currentRegdSkierBirthDate > toJuniorcutoffDate){
    		
    		console.log('Skier is either a junior or a peewee ("kids")');
    		
    		if (currentRegdSkierBirthDate > kidsToJuniorCutoffDate){
    			
    			console.log(currentRegdSkierBirthDate + ' greater than ' + kidsToJuniorCutoffDate);
    			console.log('Skier is a peewee ("kids")');
    		} else {
    			
    			console.log(currentRegdSkierBirthDate + ' less than ' + kidsToJuniorCutoffDate);
    			console.log('Skier is a junior');
    		}
    		
    		
    		
    		// junior
    		jQuery('#adult-snowboard-level-advice_' + v).hide();
    		jQuery('#adult-ski-level-advice_' + v).hide();
    		jQuery('#junior-snowboard-level-advice_' + v).show();
    		jQuery('#junior-ski-level-advice_' + v).show();
    		jQuery('#skier-food-allergies_' + v).show();
    		
    		// language-dependent display of allergy input
    		// only JP-lang juniors may join junior lessons (and get the allergy input)
    		var currentSkierLang = jQuery('#SkierInstructionLang_' + v).val();
    		console.log('This skier\'s language of instruction is: ' +currentSkierLang);
    		if (currentSkierLang == 'JP'){
    			jQuery('#junior-age-advice').slideDown();
    			jQuery('#skier-food-allergies').show();
    		}
    		
    		// clear any display from before
        	jQuery('.ControlButtons').hide();
        	jQuery('.LevelSelectionWizard').hide();
        	
    	} else {
    		// adult
    		jQuery('#adult-snowboard-level-advice_' + v).show();
    		jQuery('#adult-ski-level-advice_' + v).show();
    		jQuery('#junior-snowboard-level-advice_' + v).hide();
    		jQuery('#junior-ski-level-advice_' + v).hide();
    		jQuery('#skier-food-allergies_' + v).hide();
    		
    		// clear any display from before
        	jQuery('.ControlButtons').hide();
        	jQuery('.LevelSelectionWizard').hide();
    	}
    	
        
    	// REGD skier update
        jQuery('#SkierBirthYear_'+ v).change(function(){
        	
        	// Typically, skiers 12 and older on the first day of their lesson are entered as adults
        	// but can't use first date of ski lessons, as many users will not put this in correctly
        	// we calculate juniors with a broad rule that born before 2004-04-02, you are an adult
        	
        	var skierYear = jQuery('#SkierBirthYear_' + v).val();
        	var skierMonth = jQuery('#SkierBirthMonth_' + v).val();
        	if (skierMonth < 10){
        		skierMonth = skierMonth.toString();
        		skierMonth = '0' + skierMonth;
        	}
        	var skierDay = jQuery('#SkierBirthDay_' + v).val();
        	if (skierDay < 10){
        		skierDay = skierDay.toString();
        		skierDay = '0' + skierDay;
        	}
        	var updatedRegdSkierBirthDate = parseInt(skierYear.toString() + skierMonth.toString() + skierDay.toString());
        	var cutoffDate = parseInt(20040402);
        	//alert(updatedRegdSkierBirthDate + ' different from ' + cutoffDate);
        	
        	if (updatedRegdSkierBirthDate > cutoffDate){
        		// junior
        		jQuery('#adult-snowboard-level-advice_' + v).hide();
        		jQuery('#adult-ski-level-advice_' + v).hide();
        		jQuery('#junior-snowboard-level-advice_' + v).show();
        		jQuery('#junior-ski-level-advice_' + v).show();
        		jQuery('#skier-food-allergies_' + v).show();
        	} else {
        		// adult
        		jQuery('#adult-snowboard-level-advice_' + v).show();
        		jQuery('#adult-ski-level-advice_' + v).show();
        		jQuery('#junior-snowboard-level-advice_' + v).hide();
        		jQuery('#junior-ski-level-advice_' + v).hide();
        		jQuery('#skier-food-allergies_' + v).hide();
        	}
        	
        }); // end page update function
        
    }); // end page control for regd skiers
    
	
     
   
    
    
    // NEW skier form validation
	jQuery('#NewSkierRegSubmit').click(function(){
		var currentUserLang = jQuery('#CurrentUserLang').val();
		
		if (currentUserLang == 'CN'){
			
			// force user to enter only letters, no Chinese characters permitted.
			jQuery('#NewSkierRegForm').validate({
				rules: {
					  SkierLastNameJP: { 
						  accept: "[a-zA-Z\s]+"
					  },
					  SkierFirstNameJP: { 
						  accept: "[a-zA-Z\s]+"
					  }	
				}
			});	 // end validate arguments	
			
		} else {
			
			jQuery('#NewSkierRegForm').validate();	
			
		}
		
		
	}); 
	// cancel entry, roll up form
	jQuery('#NewSkierRegCancel').click(function(){
		
		// Hide all skier body sections (current section included)
		jQuery('.page-section-body.skier').slideUp();
        
		// show add new skier button
        jQuery('#add-skier-button-div').show();
	}); 
	 
	 
	 
	// submit REGD skier form and update 
    // each one of these forms will trigger the ajax update
    jQuery('.SkierRegForm').each(function(v) {
    	
    	jQuery('#SkierRegSubmit_' + v).click(function(e){
        	
    		// don't want the submit to actually submit (this will reload the page)
    		e.preventDefault();
    	
    		// validate form
    		var currentUserLang = jQuery('#CurrentUserLang').val();
    		
    		if (currentUserLang == 'CN'){
    			
    			// force user to enter only letters, no Chinese characters permitted.
    			jQuery('#SkierRegForm_' + v).validate({
    				rules: {
    					  SkierLastNameJP: { 
    						  accept: "[a-zA-Z\s]+"
    					  },
    					  SkierFirstNameJP: { 
    						  accept: "[a-zA-Z\s]+"
    					  }	
    				}
    			});	 // end validate arguments	
    			
    		} else {
    			
    			jQuery('#SkierRegForm_' + v).validate();	
    			
    		}
          
            
            if (jQuery('#SkierRegForm_' + v).valid()){
            	
            	// set up variables for skier update
            	var SkierPK = jQuery('#SkierPK_' + v).val(); 
            	
            	// process name strings
            	var SkierLastNameJP = jQuery('#SkierLastNameJP_' + v).val(); 
            	var SkierFirstNameJP = jQuery('#SkierFirstNameJP_' + v).val();
            		
        		// force correct capitalization of all words in name fields for CN and EN users
            	if (currentUserLang == 'CN' || currentUserLang == 'EN'){
            		
            		// process name strings
            		SkierLastNameJP = ucwords(strtolower(SkierLastNameJP));
            		SkierFirstNameJP = ucwords(strtolower(SkierFirstNameJP));
            		
            		// do the names have any hyphens? 
            		if (SkierLastNameJP.indexOf('-') > -1)
            		{
            			// process hyphenated names
            			SkierLastNameJP = ucHypenatedNames(SkierLastNameJP);
            		}
            		if (SkierFirstNameJP.indexOf('-') > -1)
            		{
            			// process hyphenated names
            			SkierFirstNameJP = ucHypenatedNames(SkierFirstNameJP);
            		}	
            	}
                	
            	var SkierLastNameJP_Kana = jQuery('#SkierLastNameJP_Kana_' + v).val(); 
            	var SkierFirstNameJP_Kana = jQuery('#SkierFirstNameJP_Kana_' + v).val();
            	
            	var SkierGender = jQuery('input:radio[name=SkierGender_' + v + ']:checked').val();
            	var SkierBirthMonth = jQuery('#SkierBirthMonth_' + v).val();
            	var SkierBirthDay = jQuery('#SkierBirthDay_' + v).val();
            	var SkierBirthYear = jQuery('#SkierBirthYear_' + v).val();
            	var SkierInstructionLang = jQuery('#SkierInstructionLang_' + v).val();
            	
            	var SkierLessonType = jQuery('input:radio[name=SkierLessonType_'  + v + ']:checked').val();
            	var SkierAbilityLevelSnowboard = jQuery('#SkierAbilityLevelSnowboard_' + v).val();
            	var SkierAbilityLevelSki = jQuery('#SkierAbilityLevelSki_' + v).val();
        		var SkierFoodAllergies = jQuery('#SkierFoodAllergies_' + v).val();
            	var InstructorPrefs = jQuery('#InstructorPrefs_' + v + ' input:checkbox').filter(':checked').map(function() {
            	    return this.value;
            	  }).get().join();
            	
            	var InstructorPrefs_FreeText = jQuery('#InstructorPrefs_FreeText_' + v).val();
            	var SkierRegSubmit = jQuery('#SkierRegSubmit_' + v).text();

            	
            	console.log('LastJP '+SkierLastNameJP+
            			    ' FirstJP '+SkierFirstNameJP+
            			    ' SkierRegSubmit '+SkierRegSubmit);
                
            	
            	// ajax call to update skier record
            	jQuery.post(
            			
                	// set target	
                    'template_ajax_skier_update.php'
                	
                	// set variables to be passed to target	
                    , {SkierPK: SkierPK,
                    	SkierLastNameJP: SkierLastNameJP, 
                    	SkierFirstNameJP: SkierFirstNameJP, 
                    	SkierLastNameJP_Kana: SkierLastNameJP_Kana,
                    	SkierFirstNameJP_Kana: SkierFirstNameJP_Kana,
                    	
                    	SkierGender: SkierGender,
                    	SkierBirthMonth: SkierBirthMonth,
                    	SkierBirthDay: SkierBirthDay,
                    	SkierBirthYear: SkierBirthYear,
                    	SkierInstructionLang: SkierInstructionLang,
                    	
	            	    SkierLessonType: SkierLessonType,
	            	    SkierAbilityLevelSnowboard: SkierAbilityLevelSnowboard,
	            		SkierAbilityLevelSki: SkierAbilityLevelSki,
	            		SkierFoodAllergies: SkierFoodAllergies,
	            		InstructorPrefs: InstructorPrefs,
	            		
	            		InstructorPrefs_FreeText: InstructorPrefs_FreeText,
                    	SkierRegSubmit: SkierRegSubmit}
                    	
                    // execute, creates html object from target
                    , function(data, status){
                        
                    	// insert returned data in summary area, roll up form
                    	jQuery('#skier-reg-summary-' + v + ' .summary-content').empty();
                    	jQuery('#skier-reg-summary-' + v + ' .summary-content').append(data);
                    	
                        // Hide all skier body sections
                		jQuery('.page-section-body.skier').slideUp();
                        
                		// Show all skier reg summaries
                		jQuery('.summary-view.skier').fadeIn();
                        
                        // add success flag to the skier section head
                		// TODO: parse skier name into success message
                        var successMsg = jQuery('#SuccessMsg_Skier_Updated').val();
                        jQuery('#SkierSectionMsg').empty();
                        jQuery('#SkierSectionMsg').append(successMsg);
                        jQuery('#SkierSectionMsg').fadeIn(500).delay(1500).fadeOut(500);
                        jQuery('#skier-reg-section-head i').show();
                        
                        
                        // show add new skier button
                        jQuery('#add-skier-button-div').show(300);
            			jQuery('#SkierRegConfirmAdd').show(300);
            	        jQuery('#LessonSelectionProceed').show(300);
            	        
            	        // update display of page control, this won't update by itself because the page doesn't refresh
            	        jQuery('#page-control-display').text('2');
                        
                        
                    }); // end ajax call
            	
            } // end valid check on form
            
    	}); // end submit click
    
    }); // ends function(v) loop for skier update submit
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    //--------------- Lesson Selection --------------------------------------------/
        
    jQuery('#LessonSelectionProceed').click(function(){
		
		// roll up the buyer form and show the buyer summary (this should be closed already)
		jQuery('#buyer-reg-summary').slideDown(500);
		jQuery('#buyer-reg-section-body').slideUp(500);
		
		// show all skier form summaries (some are already shown, we just want to make sure we get them all)
		jQuery('.summary-view.skier').fadeIn();
		
		// roll up all skier form bodies
		jQuery('.page-section-body.skier').slideUp(500);
		
		// hide the new skier button, proceed button
    	jQuery('#add-skier-button-div').slideUp(500);
    	jQuery('#lesson-reg-section-head').addClass('completed');
    	
    	// roll up selected lessons and clear out the lesson target container
    	jQuery('#lesson-container').slideUp(500);
    	jQuery('#lesson-container').text('');
    	
    	// update dates for parameter selection from session vars
    	var lessonStartDate = jQuery('#SessionLessonStartDate').val();
    	var lessonFinishDate = jQuery('#SessionLessonFinishDate').val();
    	jQuery('#LessonStartDate').val(lessonStartDate); 
    	jQuery('#LessonEndDate').val(lessonFinishDate); 
    	
    	// show parameter selection
    	jQuery('#lesson-parameter-selection').slideDown(500);
    	jQuery('#LessonSelectionMsg').slideUp(500);
    	
    	
    	// conditional display of start location, not shown for CN or EN guests (fixed at 3 for them)
    	var currentUserLang = jQuery('#CurrentUserLang').val();
		if (currentUserLang == 'CN' || currentUserLang == 'EN'){
			jQuery('#LessonStartLocationContainer').hide();
		}
    	
		
	});
    
    jQuery('#LessonLocationMapLink .map-link').click(function(){
		
		// show the map
		jQuery('#LessonLocationMap').slideDown(500);		
		
	});
    
    jQuery('.LocationSelectionLink').click(function(){
		
		// selectively toggle the start location
    	if (jQuery(this).hasClass('West')){
    		jQuery('#LessonStartLocation1').prop("checked", true);
    	} else if (jQuery(this).hasClass('East')){
    		jQuery('#LessonStartLocation2').prop("checked", true);
    	}
    	// hide the map
		jQuery('#LessonLocationMap').slideUp(500);	
			
		
	});
    
    // click "reset parameters" to update order parameters
    jQuery('#LessonParameterReset').click(function(){
    	
    	// TODO: Implement warning dialog, with cancel, for user
    	
    	// hide selection message and show parameter form
		jQuery('#LessonSelectionMsg').slideUp(500);
		jQuery('#LessonParametersForm').slideDown(500);
    	
    	// roll up selected lessons and clear out the lesson target container
    	jQuery('#lesson-container').slideUp(500);
    	jQuery('#lesson-container').text('');
    	
    }); 
    
    
    // check start date, and update display of bank transfer method, below	
    function updatePaymentDisplay(){
    	
    	var today = new Date;
    	var startDate = jQuery('#LessonStartDate').datepicker('getDate');
    	console.log('Today is: ' +today);
    	console.log('Selected start date is: ' +startDate);
    	var dateDifference7days = (startDate - today) / (86400000 * 7);
    	if (dateDifference7days <= 1){
    		
    		console.log('Today is 7 days or less before lesson start date.');
    		// includes 7 days before
    		// prevent user from selecting bank transfer option below
    		jQuery('#PaymentMethodBankTransfer').prop('disabled', true);
    		
    		// show error message
    		jQuery('#BankTransferUnavailable_ErrorAdvice').show();
    		
    		
    	} else {
    		
    		// date may be reset, in that case reset the fields below
    		// enable bank transfer option
    		jQuery('#PaymentMethodBankTransfer').prop('disabled', false);
    		
    		// hide error message
    		jQuery('#BankTransferUnavailable_ErrorAdvice').hide();
    	}
    	
    }
 

    
    
    // click "confirm" to load parameters, and select available lessons for all active skiers
    jQuery('#LessonParametersConfirm').click(function(e){
    	
    	// don't want the submit to actually submit (that would reload the page)
		e.preventDefault();
    	
		// validate form
        jQuery('#LessonParametersForm').validate();	
        
        if (jQuery('#LessonParametersForm').valid()){
    	
        	// clear out the lesson target container
        	jQuery('#lesson-container').text('');
        	
        	// hide the new skier button, proceed button (should not be showing at this point)
        	jQuery('#add-skier-button-div').slideUp(500);
        	jQuery('#lesson-reg-section-head').addClass('completed');
        	
        	// display the summary for the lesson selection section, this hides parameter entry
        	jQuery('#LessonParametersForm').slideUp(500);
        	
        	// set up variables for lesson selection
        	var newStartDate = jQuery('#LessonStartDate').val(); 
        	var d = new Date(newStartDate);
        	var curr_date = d.getDate();
        	var curr_month = d.getMonth();
        	curr_month++;
        	var LessonStartDateDisplay = curr_month + '/' + curr_date;
        	
        	var newEndDate = jQuery('#LessonEndDate').val(); 
        	var d = new Date(newEndDate);
        	var curr_date = d.getDate();
        	var curr_month = d.getMonth();
        	curr_month++;
        	var LessonEndDateDisplay = curr_month + '/' + curr_date;
        	
        	var LessonStartLocation = jQuery('input:radio[name=LessonStartLocation]:checked').val();
        	
        	
        	// set location string
        	jQuery('#LessonStartLocationLabel').text('');
        	if (LessonStartLocation == 3){
        		var LessonStartLocationString = jQuery('#SelectedStartLocation3').val(); 
        		jQuery('#LessonStartLocationLabel').append(LessonStartLocationString);
        	} else if (LessonStartLocation == 6){
        		var LessonStartLocationString = jQuery('#SelectedStartLocation6').val();
        		jQuery('#LessonStartLocationLabel').append(LessonStartLocationString);
        	}
        	// set date string for display to user
        	jQuery('#LessonDateRangeLabel').text('');
        	var LessonStartDate = jQuery('#LessonStartDate').val(); 
        	var LessonEndDate = jQuery('#LessonEndDate').val(); 
        	if (LessonStartDate === LessonEndDate){
        		var LessonPeriod = LessonStartDateDisplay + jQuery('#OneDayLesson').val();
        		jQuery('#LessonDateRangeLabel').append(LessonPeriod);
        	} else {
        		var LessonPeriod = LessonStartDateDisplay + '&nbsp;-&nbsp;' + LessonEndDateDisplay;
        		jQuery('#LessonDateRangeLabel').append(LessonPeriod);
        	}
        	
        	// remove error class once string has been changed
        	jQuery('#LessonStartLocation-error.error').change(function(){
            	jQuery(this).text('');
            	jQuery(this).removeClass('error');
            });
        	
        	// update payment display as necessary
        	updatePaymentDisplay();
        	
        	
        	
        	// show message once ready
        	// set display of start location, show reset link
        	jQuery('#LessonSelectionMsg, #LessonParameterReset').slideDown(500);
        	
        	
        
        	
        	// select available lessons for all skiers
        	jQuery.post(
        			
        		// set target
        		'template_ajax_lessons_load.php' 
        			
        		// set variables to be passed to target	
                ,{LessonStartDate: LessonStartDate,
                  LessonEndDate: LessonEndDate,
                  LessonStartLocation: LessonStartLocation}
                    	
                // execute, creates html object from target
                , function(data, status){
                    
                	// on data load, just like on page load, must set up all actions, clicks, etc.
                	// insert returned data in summary area, roll up form
                	jQuery('#lesson-container').append(data);
                	jQuery('#lesson-container').slideDown(500);
                	jQuery('#lesson-assistance-message').fadeIn(600);
                	
                	
                	
                	
                	
                	
                	
                	// hide elements on initial load
                	
                	// the instructor selection mechanism for all lessons
                	jQuery('.designate').hide();
                	jQuery('.instructor-name-textbox-label, .instructor-name').hide();
                	
                	// hides names of participants
                	jQuery('.skier-name-holder').hide();
                	
                	// hide all summaries
                	jQuery('.summary-view.lesson-selection').hide();		// debug dec15	
                	
                	// hides lessons, calendars
                	jQuery('.lesson-wrapper, .calendar').hide(); 	// debug dec15	
                	jQuery('.pagination').each(function(index) {
                		jQuery('.skier-' + index).hide();		// debug dec15	
                	});	
                	
                	// only show the first skier on initial load, and select the first day of lessons
                	var firstSkier = jQuery('.skier-0');
                	//if ( (typeof(firstSkier) != "undefined") && (firstSkier !== null) ){
                	if (jQuery.trim(firstSkier.html()).length > 0)	{
                		// shows all elements with skier-0 class
                		firstSkier.show();
                		
                		// initialize
                		var firstSkiDay = '';
                		var firstSkierCalendar = '';
                		               		
                		firstSkiDay = jQuery('.calendar.skier-0').children('.day-numeral').first().val();
                		firstSkierCalendar = jQuery('.calendar.skier-0.' + firstSkiDay);
                		                    	
                		// add selected class to first calendar for first skier
                		firstSkierCalendar.addClass('selected');
                		
                		jQuery('.lesson.skier-0').not('.' + firstSkiDay).hide();		// debug dec15	
                		
                		// hide error
                		jQuery('.no-lessons-of-date-msg.skier-0').hide();
                	}
                	
                	
                	
                	
                	
                	
                	
                	// bind fancy tooltips
                	jQuery('.lesson a[href][title]').qtip({
                		style: {
                	        classes: 'qtip-youtube',
                	        tip: {corner: false},
                	        position: {
                	            viewport: jQuery(window)
                	        }
                	    }
                	});
                	
                	// bind click functions
                	// toggle instructor input textbox on click
                	jQuery('.instructor').change(function(){
                		
                		// clear all other checkboxes for this skier and day
                		var skierIndex = jQuery(this).parents('.designate').siblings('.lesson-skier-index').val();
                		var dayValue = jQuery(this).parents('.designate').siblings('.lesson-day-number').val();
                		var clickedLessonId = jQuery(this).parents('.designate').siblings('.lesson-id').val();
                		console.log('Clicked lesson ID is: ' +clickedLessonId);
                		
                		if (jQuery(this).is(':checked')) {
                	        
                			// when clicked, show the additional input
                    		// will show on all lessons with the same ID, across all skiers
    	                	jQuery('.designate.' + clickedLessonId)
    	                		.children('.instructor-name-textbox-label, .instructor-name').slideDown(500);
    	                	
                	    } else {
                	        
                	    	// when unclicked, delete input
                    		jQuery('.designate.' + clickedLessonId)
    	                		.children('.instructor-name-textbox-label, .instructor-name').slideUp(500);
                    		jQuery('.designate.' + clickedLessonId)
	                			.children('.instructor-name').val('');
    	                	
                	    }
                    	
                    	
                		
                		
	                   
                		
                		// prevent standard click behavior (scroll to top of page)
                        return false;
                    	
                    }); 
                	
                	// click on lesson to select
                	// click only affects the one skier currently active for the user
                    jQuery('.lesson').on('click', function(){
                    	
                    	console.log('-- new click --');
                    	
                    	// only execute code for non-selected lessons
                    	if (!jQuery(this).hasClass('lesson-selected')){
                    	
                    		// set variables
                        	var skierIndex = jQuery(this).children('.lesson-skier-index').val();
                    		var dayValue = jQuery(this).children('.lesson-day-number').val();
                    		// get lesson id
                    		var clickedLessonId = jQuery(this).children('.lesson-id').val();
                    		console.log('Clicked lesson id is: ' + clickedLessonId);
                    		
                    		// name of skier 
                    		var nameOfSkier = jQuery(this).children('.lesson-skier-name').val();
                    		console.log('This skier name is: ' + nameOfSkier);
                    		
                    		
                        	
                        	// decrement
                        	// from lesson(s) with same ID for *all* skiers, remove a single skier
                    		var lessonToBeDeselectedId = jQuery('.lesson.' + dayValue + '.' + skierIndex + '.lesson-selected')
                    			.children('.lesson-id').val();
                    		
                    		
                    		// do not execute if this skier-$i has not already selected a lesson
                    		if ( (typeof(lessonToBeDeselectedId) != "undefined") && (lessonToBeDeselectedId !== null) ){	
                    			
                    			console.log('Lesson id for lesson to be deselected: ' + lessonToBeDeselectedId);
                    			
                    			var lessonToBeDeselectedNumSkiers = jQuery('.lesson.' + dayValue + '.' + skierIndex + '.' + lessonToBeDeselectedId)
                    				.children('.lesson-regd-skiers').text();
	                    		
                    			console.log('Current skiers for lesson to be deselected: ' + lessonToBeDeselectedNumSkiers);
	                    		
	                    		lessonToBeDeselectedNumSkiers = parseInt(lessonToBeDeselectedNumSkiers);
	                    		var afterDeselectionNumSkiers = '';
	                    		afterDeselectionNumSkiers = lessonToBeDeselectedNumSkiers - 1;
	                    		
	                        	console.log('After deselection, ' + afterDeselectionNumSkiers + ' skiers are in lesson id: ' + lessonToBeDeselectedId);
	                        	
	                        	
	                        	
	                        	// update display for that lesson id across all skiers
	                        	jQuery('.lesson.' + lessonToBeDeselectedId).children('.lesson-regd-skiers').text(afterDeselectionNumSkiers);
	                        	
	                        	console.log('Skiers for lesson ID: ' + lessonToBeDeselectedId + ' set to: ' + afterDeselectionNumSkiers);
	                        	
	                        	console.log('Now removing "lesson full" errors + messages');
	                        	// remove "lesson full" error styling for this lesson ID, across all skiers
	                        	jQuery('.lesson.' + lessonToBeDeselectedId).removeClass('lesson-full');
	                        	// remove "lesson full" error message div
	                        	jQuery('.lesson.' + lessonToBeDeselectedId).children('.lesson-full-message').remove();
	                        	
	                        	
	                        	// remove the current skier's name from all lists
	                        	console.log('Skier name to be removed: ' + nameOfSkier);
	                        	var skierNameToBeRemoved = '<br><span class="lesson-skier-name-display">'  + nameOfSkier + '</span>';
	                        	console.log('HTML: ' + skierNameToBeRemoved + ' to be removed from lesson ID ' + lessonToBeDeselectedId);
	                        	jQuery('.skier-name-holder.'  + lessonToBeDeselectedId).each(function(){
	                        		//jQuery(this).html(jQuery(this).html().split(skierNameToBeRemoved).join(''));
	                        		
	                        	});
	                        	if (afterDeselectionNumSkiers == 0){
	                        		jQuery('.skier-name-holder.'  + lessonToBeDeselectedId).slideUp(500);
	                        	}
	                        	
	                        } // end check for deselected lesson
                    		
                    		
                    		
                    		
                    		
                    		// increment
                        	// to lesson(s) with same ID for *all* skiers, add a single skier
                        	// set variables
                        	var currentNumSkiers = jQuery(this).children('.lesson-regd-skiers').text();
                        	currentNumSkiers = parseInt(currentNumSkiers);
                        	var newNumSkiers = currentNumSkiers + 1;
                        	var maxSkiers = jQuery(this).children('.lesson-capacity').text();
                        	maxSkiers = parseInt(maxSkiers);
                        	
                        	
                        	if (newNumSkiers <= maxSkiers){
                        		
                        		// room for one more skier
                        		// update display for that lesson id across all skiers
                            	jQuery('.lesson.' + clickedLessonId).children('.lesson-regd-skiers').text(newNumSkiers);
                            	console.log('Skiers for lession ID: ' + clickedLessonId + ' set to: ' + newNumSkiers);
                            	
                            	// update selected display
                            	// set all other lessons for that day and skier to deselected
                            	console.log('Deselecting other lessons for day: ' + dayValue + ' skierIndex: ' + skierIndex);
                            	console.log('Not deselecting, because: Mikuni');
                        		//jQuery('.lesson.' + dayValue + '.' + skierIndex).removeClass('lesson-selected').addClass('deselected');
                        		
                        		// add selected class
                        		jQuery(this).removeClass('deselected').addClass('lesson-selected');
                        		
                        		// clean up other deselected lessons
                            	// set all other instructor input checkboxes for that day and skier to deselected
                            	//jQuery(this).parents().first().siblings().children('.deselected').children('.designate.private').slideUp(500);
                        		
                            	
                        		// add the current skier's name to it and add to all other lists of the same lesson ID
                            	var skierNameToBeAdded = '<br><span class="lesson-skier-name-display">'  + nameOfSkier + '</span>';
                            	jQuery('.skier-name-holder.'  + clickedLessonId).append(skierNameToBeAdded);
                            	
                            	// show (more than zero) skier name display on all lessons of that ID
                            	if (newNumSkiers > 0){
                            		jQuery('.skier-name-holder.'  + clickedLessonId).slideDown(500);
                            	} 
                        		
                            	// show the instructor selection mechanism, only available for privates
                            	/*if (jQuery(this).children('.designate').hasClass('private')){
                            		jQuery(this).children('.designate').slideDown(500);
                            	}*/ 
                            	
                        	} else {
                        		
                        		// lesson is full, cannot place this skier
                        		// update display to show that lesson is full
                        		jQuery(this).addClass('lesson-full');
                        		
                        		// show "lesson is full" message
                        		jQuery(this).children('.lesson-full-message').remove();
                        		var message = jQuery('#ErrorMsg_LessonSelection_LessonIsFull').val();
                        		var messageContainer = '<div class="lesson-full-message">' + message + '</div>';
                        		jQuery(this).append(messageContainer);
                        		
                        		
                        	}
                        	
                        	
                    	} // end check for non-selected lesson
                    	
                    }); // end click function
                    
                    
                    
                    // click to clear selections for that one skier's one day
                    jQuery('.selection-reset').on('click', function(){
                    	
                    	console.log('--reset click--');
                    	
                    	// set variables
                    	var skierIndex = jQuery(this).prev().children('.day-skier-index').val();
                    	var nameOfSkier = jQuery(this).prev().children('.skier-name').val();
                		var dayValue = jQuery('.calendar.skier-' + skierIndex + '.selected').children('.day-numeral').val();
                	
                		console.log('Skier index: ' + skierIndex + ' Name of skier: ' + nameOfSkier + ' Day: ' + dayValue);
                    	
                		
                		
                		// decrement any selected lessons
                    	// from lesson(s) with same ID for *all* skiers, remove a single skier
                		var lessonToBeDeselectedId = jQuery('.lesson.' + dayValue + '.' + skierIndex + '.lesson-selected')
            											.children('.lesson-id').val();
                		
                		// do not execute if this skier-$i has not already selected a lesson
                		if ( (typeof(lessonToBeDeselectedId) != "undefined") && (lessonToBeDeselectedId !== null) ){	
                			
                			console.log('Lesson id for lesson to be deselected: ' + lessonToBeDeselectedId);
                			
                			var lessonToBeDeselectedNumSkiers = jQuery('.lesson.' + dayValue + '.' + skierIndex + '.' + lessonToBeDeselectedId)
            													.children('.lesson-regd-skiers').text();
                    		
                			console.log('Current skiers for lesson to be deselected: ' + lessonToBeDeselectedNumSkiers);
                    		
                    		lessonToBeDeselectedNumSkiers = parseInt(lessonToBeDeselectedNumSkiers);
                    		var afterDeselectionNumSkiers = '';
                    		afterDeselectionNumSkiers = lessonToBeDeselectedNumSkiers - 1;
                    		
                        	console.log('After deselection, ' + afterDeselectionNumSkiers + ' skiers are in lesson id: ' + lessonToBeDeselectedId);
                        	
                        	
                        	
                        	// update display for that lesson id across all skiers
                        	jQuery('.lesson.' + lessonToBeDeselectedId).children('.lesson-regd-skiers').text(afterDeselectionNumSkiers);
                        	
                        	console.log('Skiers for lesson ID: ' + lessonToBeDeselectedId + ' set to: ' + afterDeselectionNumSkiers);
                        	
                        	console.log('Now removing "lesson full" errors + messages');
                        	// remove "lesson full" error styling for this lesson ID, across all skiers
                        	jQuery('.lesson.' + lessonToBeDeselectedId).removeClass('lesson-full');
                        	// remove "lesson full" error message div
                        	jQuery('.lesson.' + lessonToBeDeselectedId).children('.lesson-full-message').remove();
                        	
                        	// remove the current skier's name from all lists
                        	console.log('Skier name to be removed: ' + nameOfSkier);
                        	var skierNameToBeRemoved = '<br><span class="lesson-skier-name-display">'  + nameOfSkier + '</span>';
                        	console.log('HTML: ' + skierNameToBeRemoved + ' to be removed from lesson ID ' + lessonToBeDeselectedId);
                        	jQuery('.skier-name-holder.'  + lessonToBeDeselectedId).each(function(){
                        		jQuery(this).html(jQuery(this).html().split(skierNameToBeRemoved).join(''));
                        		
                        	});
                        	if (afterDeselectionNumSkiers == 0){
                        		jQuery('.skier-name-holder.'  + lessonToBeDeselectedId).slideUp(500);
                        	}
                        	
                        } // end check for deselected lesson
                		
                		// reset display class
                    	jQuery('.lesson.' + dayValue + '.' + skierIndex).removeClass('lesson-selected').removeClass('deselected');
                    	
                    	// set all other input checkboxes for that day and skier to deselected
                		jQuery('.lesson.' + dayValue + '.' + skierIndex + ' .instructor').prop('checked',false);
                		// other text inputs are hidden
                		jQuery('.lesson.' + dayValue + '.' + skierIndex + ' .instructor-name-textbox-label').hide();
                		jQuery('.lesson.' + dayValue + '.' + skierIndex + ' .instructor-name').val('').hide();
                		jQuery('.lesson.' + dayValue + '.' + skierIndex + ' .designate').hide();
                    	 
                    	
                    }); // end click function
                    
                    
                    
                  
                    // click calendar dates to filter display for a given skier
                    jQuery('.calendar').on('click', function(){
                    	
                    	// clear any error displays
                    	jQuery('.no-lessons-of-date-msg').hide();
                    	
                    	// get calendar date, skier index from the calendar div
                    	var calendarDate = jQuery(this).children('.day-numeral').val();
                    	var skierIndex = jQuery(this).children('.day-skier-index').val();
                    	//console.log('Calendar date: ' +calendarDate + ' Skier index: ' +skierIndex);
                    	
                		jQuery('.lesson').not('.' + calendarDate + '.' + skierIndex).hide();
                		
                		// check if lessons are available for that day
                		var lessonsOfDate = jQuery('.lesson.' + calendarDate + '.' + skierIndex);
                		if (lessonsOfDate.length){
                			
                			// lessons are available
                    		lessonsOfDate.show();	
                			
                		} else {
                			
                			// no lessons for this date for this skier, show error message
                			jQuery('.no-lessons-of-date-msg.' + skierIndex).show();
                			
                		}
                		
                		
                		
                		
                		
                		
                		// remove selected class from other calendars, all skiers
                		jQuery('.calendar').removeClass('selected');
                		
                		// add selected class to this one calendar
                		jQuery(this).addClass('selected');
                		
                    }); 
                    
                    // pagination to move between the skiers
                    jQuery('.pagination').on('click', function(){
                    	
                    	// set variables
                    	var currentSkierIndex = jQuery(this).children().val();
                    	var targetSkierIndex = '';
                    	
                    	if (jQuery(this).hasClass('next')){
                    		targetSkierIndex = Number(currentSkierIndex) + 1;
                    	} else if (jQuery(this).hasClass('previous')){
                    		targetSkierIndex = Number(currentSkierIndex) - 1;
                    	}
                    	
                    	//console.log('Current: ' + currentSkierIndex + ' Target: ' + targetSkierIndex);
                    	// hide current skier, show current skier summary
                    	jQuery('#skier-lesson-selection-' + currentSkierIndex).slideUp(500);
                    	//jQuery('#skier-lesson-reg-summary-' + currentSkierIndex).slideDown(500);
                    	
                    	// show next skier, hide summary
                    	jQuery('#skier-lesson-selection-' + targetSkierIndex).slideDown(500);
                    	jQuery('.skier-' + targetSkierIndex).slideDown(500);
                    	//jQuery('#skier-lesson-reg-summary-' + targetSkierIndex).hide();
                    	
                    	// prep first view of next skier (only first calendar day is highlighted)
                    	var firstSkiDay = jQuery('.calendar.skier-' + targetSkierIndex).children('.day-numeral').first().val();
	                	jQuery('.calendar.skier-'  + targetSkierIndex + '.' + firstSkiDay).addClass('selected');
	                	jQuery('.lesson.skier-'  + targetSkierIndex).not('.' + firstSkiDay).hide();
                    	
                    });
                    
                    // button to complete lesson registration and insert line items
                    jQuery('#LessonSelectionComplete').on('click', function(){
                    	
                    	var postArray = [];
                    	jQuery('.lesson-selected').each(function(i, obj) {
                    	    
                    		var skierId = jQuery(this).children('.skier-id').val();	
                    		var lessonId = jQuery(this).children('.lesson-id').val();	
                    		var price = jQuery(this).children('.price').val();	
                    		var hours = jQuery(this).children('.lesson-hours').val();
                    		var instructorName = '';
                    		
                    		var checkForInstructorName = jQuery(this).children('.designate').children('.instructor-name').val();
                    		if ( (typeof(checkForInstructorName) != "undefined") && (checkForInstructorName !== null) ){	
                        		instructorName = jQuery(this).children('.designate').children('.instructor-name').val();
                    		}
                    		postArray.push({
	                    	    SkierId : skierId, 
	                    	    LessonId : lessonId,
	                    	    Price: price,
	                    	    Hours: hours,
	                    	    InstructorName: instructorName
	                    	});
                    		
                    	});
                    	
                    	// sort array by LessonId
                    	function sortBy(column){
                    		return function(a,b){
                    		      if(a[column] > b[column]){
                    		          return 1;
                    		      } else if( a[column] < b[column] ){
                    		          return -1;
                    		      }
                    		      return 0;
                    		};
                		};
                    	postArray.sort(sortBy("LessonId"));
                    	
                    	console.log(JSON.stringify(postArray));
                    	
                    	
                    	// examine array, if empty, show error message and do not process
                    	if (postArray.length > 0){
                    	
                    		// ajax call to insert skier lessons in order
        	            	jQuery.post('template_ajax_lesson_reg.php'
	            				, JSON.stringify(postArray)
	            				, function(data){
	    	            			
	            					//console.log(data);
	            					var parsedData = jQuery.parseJSON(data);
	            					//console.log(parsedData);
	            					
	            					if ((typeof(parsedData.success_message) != "undefined") 
	            							&& (parsedData.success_message !== null) ){
	            						
	            						// no reservation conflicts
	            						// order and reservation line items inserted correctly
	            						console.log(parsedData.success_message);
	            						location.reload(true);
	            						
	            					} else if ((typeof(parsedData[0].skier_error_message) != "undefined") 
	            							&& (parsedData[0].skier_error_message !== null) ){
	            						
	            						// 1 or more reservation conflicts
	        	            			// user has tried to reserve a lesson that is already booked (private) 
	            				    	// or at capacity (group)
	            						
	            						// clear any current error messages from lesson container
	            						jQuery('#lesson-container .error-msg').remove();
	            						
	            						var arrayLength = parsedData.length;
	            						for (var i = 0; i < arrayLength; i++){
	            							
	            							//console.log(parsedData[i].skier_error_message);
	            							
	            							// prep message
	            							var errorMsgBase = parsedData[i].skier_error_message;
	            							var errorMsg = '<p class="error-msg"><i class="fa fa-times"></i>' 
	            								+ '<span>' + errorMsgBase + '</span></p>';
	            							
	            							//console.log(errorMsg);
	            							
	            							// append error messages to the bottom of the lesson container
	            							jQuery('#lesson-container').append(errorMsg);
	            							
	            							// remove any offending sessions from DOM (can't be re-found by user)
	            							var conflictingLessonId = parsedData[i].lesson_id;
	            							var conflictingDayNumber = parsedData[i].day_number;
	            							var conflictingLessonsCurrentlySelected = jQuery('.lesson.' + conflictingLessonId + '.lesson-selected');
	            							
	            							// for the skiers with conflicting lessons selected, we'll reset their display classes
	            							jQuery(conflictingLessonsCurrentlySelected).each(function(v) {
	            								
	            								// reset display of lessons for the day affected
	            								jQuery('.lesson.skier-' + v + '.' + conflictingDayNumber).removeClass('deselected');
	            			
	            							});
	            							
	            							// this removes the set of lesson wrappers (conflicting lessons inside) from the DOM
	            							jQuery('.lesson.' + conflictingLessonId).parent().remove();
	            							
	            							
	            							
	            							
	            						} // end for loop
	            						
	            					}
	            					
	            					
	            				})
	            				.fail(function(output, statusText, errorThrown ){
	    			            	console.log('Exception: '+ errorThrown );
	    			            	console.log(statusText);
	    			            	
	    			            }); // end post()

                    	} else {
                    		
                    		// no lessons selected, show alert
	               		    var message = jQuery('#ErrorMsg_LessonSelection_NoLessonsSelected').val();
	               		    alert(message);
                    	}
                    	
                    }); // end click binding on lesson selection complete button
                    
               }); // end return data from ajax call
    	
        } // end valid check
   
    }); // end click function to load parameters and select available lessons
    
    
    
    
	
	
	
	
	
	//--------------- Liability Release --------------------------------------------/
	
    // show session submit button after all terms&conditions checkboxes are checked
    jQuery('.liability-input').click(function(){
    	if (jQuery('.liability-input:checked').length == 12){
    		jQuery('#LiabilityReleaseConfirm').show();
    	} else if (jQuery('.liability-input:checked').length < 12){
    		jQuery('#LiabilityReleaseConfirm').hide();
    	}
    }); 
    
    // check all checkboxes if bottom one is checked
    jQuery('#LiabilityRelease_AgreeToAll').click(function(){
    	jQuery('input.liability-input').prop('checked', true);
    	jQuery('#LiabilityReleaseConfirm').show();
    }); 
    
    // on click, roll up liability release and show order summary, payment, coupon code entry
    jQuery('#LiabilityReleaseConfirm').click(function(){
    	jQuery('#liability-release-section-body').slideUp(500);
    	
		
		// show payment section
    	jQuery('#payment-section-head').addClass('completed');
    	jQuery('#payment-section-body').slideDown(500);
    	
    	// scroll to top of liability section
		jQuery('html, body').animate({
	        scrollTop: jQuery('#liability-release-section-head').offset().top - 20
	    }, 'slow');
		// show completed checkbox
    	jQuery('#liability-release-section-head i').fadeIn(500);
    	
    	// update payment display as necessary
    	updatePaymentDisplay();
    	
    	// set page control to 4, will be used by the submit order test below
    	jQuery('#RegPageControl').val(4);
    	
    	
    	
    }); 
	
	
	
	
	
	
	
  //--------------- Coupon Codes --------------------------------------------/

    // click to submit coupon code for check
    jQuery('#CheckCouponCodeLink').click(function(){
    	
    	// perform check on coupon code
    	var CouponCode = jQuery('#MarketingCouponCode').val();
    	console.log('function is called with code: ' + CouponCode);
        
    	var CouponCodeData = '';
        
    	jQuery.post(
            	// set target	
                'template_ajax_check_coupon.php'
            	
            	// set variables, json notation
                , {CouponCode: CouponCode}
                
                // success
                , function(data, status){
					CouponCodeData = data;
					processCouponCodeReturn(CouponCodeData);		         
                }
                
                // return format is json
                , 'json'
                
         ); // end ajax call to check coupon code string
                
        // prevent standard click behavior (scroll to top of page)
        return false;
     	
    }); // end click to submit coupon code
    
   
    
    // process coupon code
    function processCouponCodeReturn(CouponCodeData){
    	
    	console.log('Data.Level = ' +CouponCodeData.Level);
    	console.log('Data.Discount = ' +CouponCodeData.Discount);
    	
    	// the user has either not entered a coupon code, or they have and it is valid
        if(CouponCodeData.Level == 0 || CouponCodeData.Level == 1) {
        	
        	// display sucess message if valid code present
        	if (CouponCodeData.Level == 1){
			
				//console.log('Code is Valid');
        		var CouponCode = CouponCodeData.CouponCode;
        		
				// display success message if coupon code exists and is valid
				var couponSuccessMsgText = jQuery('#SuccessMsg_CouponCode_Valid').val();
				
				// is there currently a coupon discount applied?
				var currentDiscount = jQuery('.coupon-amount').first().text();
				
				if (currentDiscount.length > 0){
					//console.log(currentDiscount);
					currentDiscount = currentDiscount.replace(/\D/g,'');
					currentDiscountAsNumber = parseInt(currentDiscount);
					
					//console.log(currentDiscountAsNumber);
					if (currentDiscountAsNumber > 0){
						
						// add back the current coupon to the placeholder total
						var currentPlaceholderTotal = parseInt(jQuery('#OrderTotalJPY').val());
						console.log('Current placeholder value: ' +currentPlaceholderTotal);
						var updatedPlaceholderTotal = currentPlaceholderTotal + currentDiscountAsNumber;
						console.log('Updated placeholder value: ' +updatedPlaceholderTotal);
						jQuery('#OrderTotalJPY').val(updatedPlaceholderTotal);
						
					}
					
				}
				
				// add coupon code and discount content to the order summary, 
            	// display the coupon code line item, and update the total price
				var couponDiscount = CouponCodeData.Discount;
				
				// update the form submits with the raw number at this point
				// these are unique form inputs
				jQuery('#chargeCouponAmount').val(couponDiscount);
				var updatedCouponAmount = jQuery('#chargeCouponAmount').val();
				console.log('Coupon amount for CC updated to: ' + updatedCouponAmount);
				jQuery('#chargeCouponCode').val(CouponCode);
				var updatedCouponCode = jQuery('#chargeCouponCode').val();
				console.log('Coupon code for CC updated to: ' + updatedCouponCode);
				
				// while couponDiscount is still a number
				var currentTotal = jQuery('#OrderTotalJPY').val();
				console.log('Current total is: ' + currentTotal);
				var newTotal = currentTotal - couponDiscount;
				console.log('New total is: ' + newTotal);
				
				// update placeholder hidden input, for use later in processing order
				jQuery('#OrderTotalJPY').val(newTotal);
				
				// add a comma for display
				newTotal = newTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				// couponDiscount is now a string
				couponDiscount = couponDiscount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				
				// always show used coupon code in upper case
				CouponCode = CouponCode.toUpperCase();
        	
            	// update summary tables
				jQuery('.coupon-amount').text(couponDiscount);
				jQuery('.coupon-code').text(CouponCode);
				jQuery('.confirmation-discount-display').slideDown(700);
				jQuery('.total-display').text(newTotal);
				
				
				
			} else if (CouponCodeData.Level == 0){
				
				console.log('No code present');
				jQuery('#MarketingCouponCode').addClass('error');
	    		var couponErrorMsgText = jQuery('#ErrorMsg_CouponCode_NoCodeEntered').val();
	    		jQuery('#CheckCouponCodeReturnMsg')
	        		.html('<label id="CouponCodeErrorMsgLabel" class="error" for="MarketingCouponCode" generated="true" style="display:inline">' 
	        			+ couponErrorMsgText + "</label>");
	        	
	        	// remove error class once string has been changed
	        	jQuery('#MarketingCouponCode.error').change(function(){
	            	jQuery(this).next().next().text('');
	            	jQuery(this).removeClass('error');
	            });
				
				
			} 
        	
        } else {
        	
        	// show error message to the user, using the title attribute of MarketingCouponCode field
        	// add "error" class to MarketingCouponCode input to display error
    		
        	//console.log('Code is not valid');
        	jQuery('#MarketingCouponCode').addClass('error');
    		var couponErrorMsgText = jQuery('#ErrorMsg_CouponCode_Invalid').val();
    		jQuery('#CheckCouponCodeReturnMsg')
        		.html('<label id="CouponCodeErrorMsgLabel" class="error" for="MarketingCouponCode" generated="true" style="display:inline">' 
        			+ couponErrorMsgText + "</label>");
        	
        	// remove error class once string has been changed
        	jQuery('#MarketingCouponCode.error').change(function(){
            	jQuery(this).next().next().text('');
            	jQuery(this).removeClass('error');
            });
        	
        }
        
    } // end function processCouponCodeReturn
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //--------------- Confirmation and Payment --------------------------------------------/
	
    // process credit card: processCreditCard.js --> GO HERE to view relevant code
    
    // validate entry
    // show any errors on page and stop
    // on success (charge successful), must 1) load coupon code via AJAX, then 2) insert liability release
    // then load confirmation page
    
    
    
    // process bank transfer
    jQuery('#CompleteRegistrationBankTransfer').click(function(){
    	
    	// set up variables
        var LiabilityReleaseText = jQuery('#buyerLiabilityText').val();
        
        // pageControl will be 4 if the user has completed the liability release
        // reloading the page before finishing will keep the pageControl at 3
        var processPageControl = jQuery('#RegPageControl').val(); 
       
    	// insert coupon as order line item if valid code present
        ajaxInsertCouponCode();
        
        // check page control before passing through order
        // pageControl will be 4 if the user has completed the liability release
    	if (processPageControl == 4){
    	
    		console.log('Page control: ' + processPageControl);
    		
    		// insert liability release for this buyer and order
        	var resultLiabilityReleaseInsert = '';
            
            jQuery.post(
            		
                	// set target	
            		'template_ajax_liability_release_reg.php'
                	
                	// set variables, json notation
                    , {LiabilityReleaseText: LiabilityReleaseText}
                    
                    // success
                    , function(data){
                    	//console.log(data);
                    	var resultLiabilityReleaseInsert = data;
                    	console.log('Success = 1, result is: ' + resultLiabilityReleaseInsert.Result);	
                    	
                    	// pause for 0.5 seconds
                    	// then send user to confirmation page
                        function go_to_confirmation(){
                        	window.location.replace('order_confirmation.php');
                        };
                        window.setTimeout( go_to_confirmation, 500 ); 
                    }
                    
                    // return format is json
                    , 'json'
                    
             )
             .fail(function(resultLiabilityReleaseInsert, textStatus, errorThrown ){
            	 //console.log(data);
            	 console.log('Exception: '+ errorThrown );
             }
             ); // end ajax call to insert liability release

            
    		
    	} // end check for processPageControl = 4, liability release complete
        
    }); // end bank transfer complete reg click
    
    
    // generic function to insert coupon as order line item
    function ajaxInsertCouponCode() {
    	
    	// Check that coupon-amount has any text string in it
        if (jQuery('.coupon-amount').first().text().length > 0){
        	
        	// capture the code to send
        	var CouponCode = jQuery('.coupon-code').first().text();
        	
        	// insert coupon as line item
        	console.log('Insert function is called with code: ' + CouponCode);
            
            jQuery.post(
            		
                	// set target	
            		'template_ajax_insert_coupon.php'
                	
                	// set variables, json notation
                    , {CouponCode: CouponCode}
                    
                    // success
                    , function(data, status){
                    	var resultCouponCodeInsert = data;
        				console.log('Success = 2, this time is: ' + resultCouponCodeInsert.Level);		         
                    }
                    
                    // return format is json
                    , 'json'
                    
             )
             .fail(function(resultCouponCodeInsert, textStatus, errorThrown ){
              	console.log('Exception: '+ errorThrown );
             }
             ); // end ajax call to insert coupon code as order line item
            
            
        } // end check for coupon amount
        
    } // end function
    
    
    
    
    

    // select payment method
    jQuery('#PaymentMethodBankTransfer').change(function(){
    	
    	jQuery('.payment-method-explanation.bank-transfer').slideDown(500);
    	jQuery('.payment-method-explanation.credit-card').slideUp(500);
    	jQuery('#credit-card-box').slideUp(500);
    	jQuery('#PaymentMethodCC').prop('checked', false);
    	jQuery('#CompleteRegistrationBankTransfer').show();
    	
    	// prevent standard click behavior (scroll to top of page)
        return false;
    	
    });
    
    // select payment method
    jQuery('#PaymentMethodCC').change(function(){
    	
    	jQuery('.payment-method-explanation.bank-transfer').slideUp(500);
    	jQuery('.payment-method-explanation.credit-card').slideDown(500);
    	jQuery('#credit-card-box').slideDown(500);
    	jQuery('#PaymentMethodBankTransfer').prop('checked', false);
    	jQuery('#CompleteRegistrationBankTransfer').hide();
    	
    	// prevent standard click behavior (scroll to top of page)
        return false;
    	
    });  
    	
		 
}); // end ready()