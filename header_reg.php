<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width">
        
<?php
	// set page title
	echo '<title>' .PAGE_TITLE_STEM .' - ' .$RegistrationPageHeader .'</title>';
?>
<!-- Load base CSS -->
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link type="text/css" href="css/reset.css" rel="stylesheet" />
<link type="text/css" href="css/styles.<?php echo date("Ymd", filemtime('css/styles.css')); ?>.css" rel="stylesheet" />	
<link type="text/css" href="css/styles_front.<?php echo date("Ymd", filemtime('css/styles_front.css')); ?>.css" rel="stylesheet" />	
<?php 
	// set language-dependent font styles
	if ($_SESSION['UserLang'] == 'EN'): ?>
		<link type="text/css" href="css/fonts_en.<?php echo date("Ymd", filemtime('css/fonts_en.css')); ?>.css" rel="stylesheet" />
	<?php elseif ($_SESSION['UserLang'] == 'CN'): ?>
		<link type="text/css" href="css/fonts_jp.<?php echo date("Ymd", filemtime('css/fonts_jp.css')); ?>.css" rel="stylesheet" />
	<?php else: ?>
		<link type="text/css" href="css/fonts_jp.<?php echo date("Ymd", filemtime('css/fonts_jp.css')); ?>.css" rel="stylesheet" />
	<?php endif; ?>

<!-- Load Javascript library CSS -->
<link type="text/css" href="css/jquery-ui.min.css" rel="stylesheet" />
<link type="text/css" href="css/jquery.qtip.css" rel="stylesheet" />
<link type="text/css" href="css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="css/credit-card.css" rel="stylesheet" />


<!-- Load Javascript libraries -->
<script type="text/javascript" src="js/lib/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/lib/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/lib/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/lib/jquery.qtip.min.js"></script>
<script type="text/javascript" src="js/lib/jquery.autoKana.js"></script>



<!-- Load Javascript helper libraries -->
<script type="text/javascript" src="js/lib/h5.js"></script>
<script type="text/javascript" src="js/lib/modernizr-2.6.1.min.js"></script>

<!-- Datepicker js, conditional load -->
<?php 
	if ($_SESSION['UserLang'] == 'EN'){
		// nothing to load, use default settings
	} else if ($_SESSION['UserLang'] == 'CN'){
		echo '<script type="text/javascript" src="js/lib/datepicker-zh-TW.js"></script>';
	} else {
		// default is JP
		echo '<script type="text/javascript" src="js/lib/datepicker-ja.js"></script>';
	}
?>


<!-- Load coded Javascript -->
<script type="text/javascript" src="js/responsive.<?php echo date("Ymd", filemtime('js/responsive.js')); ?>.js"></script>
<script type="text/javascript" src="js/formLogin.<?php echo date("Ymd", filemtime('js/formLogin.js')); ?>.js"></script>
<script type="text/javascript" src="js/dateMgt.<?php echo date("Ymd", filemtime('js/dateMgt.js')); ?>.js"></script>

<script type="text/javascript" src="js/displayPage.<?php echo date("Ymd", filemtime('js/displayPage.js')); ?>.js"></script>
<script type="text/javascript" src="js/displayLevelWizard.<?php echo date("Ymd", filemtime('js/displayLevelWizard.js')); ?>.js"></script>
<script type="text/javascript" src="js/formRegistration.<?php echo date("Ymd", filemtime('js/formRegistration.js')); ?>.js"></script>

<!-- Load Stripe library -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="js/lib/jquery.payment.js"></script>
<script type="text/javascript" src="js/processCreditCard.<?php echo date("Ymd", filemtime('js/processCreditCard.js')); ?>.js"></script>

</head>

<?php 
// set body class as required by page type
echo '<body class="registration form">';
?>

<!-- Start Main Container -->
<div id="maincontainer">
		
<!-- Start Container -->
<div id="container">


<!-- Start Header -->
<header class="short">		
<a href="template_login.php?UserLang=<?php echo $_SESSION['UserLang']; ?>" id="logo">


<a href="#" class="menu-mobile">                     
</a>
                        



</header>
<!-- End Header -->
